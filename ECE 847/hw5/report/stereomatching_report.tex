\documentclass{report}
\usepackage{fullpage,doublespace,amsmath,amssymb,amsthm,graphicx}
\author{Shelby Solomon Darnell}
\title{ \Huge{Stereo Matching and Epipolar Line Drawing} \break \LARGE{ECE 847 Image Processing \break \LARGE{Homework \#5}} }
%\date{}
\begin{document}

\maketitle
\tableofcontents

\section{Introduction}

Epipolar geometry consists of the ideas of an epipolar plane, an epipolar line, 
an image plane and a baseline.  Epipolar geometry defines the perspective basis
for two stereo images.  This perspective basis for stereo images and the values
calculated using them leads to the technique of stereo matching.  A major 
purpose of stereo matching is to allow a computer to calculate the depth of 
objects in an image.  Without the perspective basis derived from the epipolar
geometry of images depth calculations from stereo matching is not possible.


\section{Theory}

\subsection{Epipolar Geometry Definitions}
Definitions are in order for the following words:
\begin{enumerate}
\item baseline - line segment that connect to two centers of projection
\item epipole - intersection of the baseline with the image plane;
                projection of projection center in other image;
                vanishing point of camera motion direction
\item epipolar plane - plane containing a baseline
\item epipolar line - the intersection of the epipolar plane with an image
\end{enumerate}


\subsection{Stereo Matching}
Stereo matching is a correlation based method using images whose epipoles are 
parallel with the rows of the image.  Stereo images whose epipoles are parallel
with the rows of the image are called rectified.  Stereo matching uses the 
dissimilarities between the two images to determine the relative depths of 
objects in the images using two dimensional data.  Attempting to match objects 
in rectified stereo images can not be done well using a single pixel to pixel
correlation.  Block matching uses a window of pixels to correlate objects
between stereo images.

\section{Algorithm and Methodology}

Drawing epipolar lines requires getting corresponding points in two stereo 
images and calculating a several matrices to solve a system of equations to get 
the fundamental matrix.  The fundamental matrix contains information that 
allows the calculation of epipolar lines in both stereo images.  Stereo 
matching of two stereo images determines the depth of objects in an image using 
rectified stereo images.

\subsection{Main Overview}

The following is a list of the main method definitions for epipolar line drawing and stereo matching:
\begin{enumerate}
\item{EpipolarLineDrawer::CreateColoredPoints()}
\item{EpipolarLineDrawer::CreateMatrixA()}
\item{EpipolarLineDrawer::ComputeFM()}
\item{EpipolarLineDrawer::CalculateX()}
\item{EpipolarLineDrawer::CalculateY()}
\item{EpipolarLineDrawer::getPointsAtLimit()}
\item{EpipolarLineDrawer::DrawEpipolarLine()}
\item{EpipolarLineDrawer::DrawEpipolarLines()}
\item{StereoMatcher::GetMinIn3D()}
\item{StereoMatcher::ComputeDbar()}
\item{StereoMatcher::BlockMatchOne()}
\item{StereoMatcher::BlockMatchTwo()}
\item{StereoMatcher::BlockMatchWithLeftToRightCheck()}
\end{enumerate}
For parameters and return types please view files EpipolarLineDrawer.h and StereoMatcher.h.

\subsection{Main files and duties}
The source code is organized to have four main responsibilities: interactive display dialog, epipolar line drawing methods, stereo matching methods, and general image processing methods.

The following files are responsible for creating the interactive display dialog:
\begin{itemize}
\item{StereoMatchingDlg.cpp}
\item{StereoMatchingDlg.h}
\item{StereoMatching.rc}
\end{itemize}
These files create and paint the interactive display as well as contain the event handlers that call methods in the other files.

The following files comprise the epipolar line drawing and stereo matcher specific methods:
\begin{itemize}
\item{StereoMatcher.cpp}
\item{StereoMatcher.h}
\item{EpipoplarLineDrawer.cpp}
\item{EpipoplarLineDrawer.h}
\end{itemize}

These files contain the methods listed in the main overview and rely on the ImageProcessing file for generic image operations.

The following files comprise the general image processing methods:
\begin{itemize}
\item{ImageProcessing.cpp}
\item{ImageProcessing.h}
\end{itemize}
ImageProcessing contains the methods necessary to complete past homework assignments.
WaterSegmenter and other classes are also a part of this package, but its methods are not used for this homework assignment.

\subsection{Step-by-Step}

\subsubsection{EpipolarLineDrawer::SetCorrespondingPoints()}
\begin{verbatim}
Description: Use the arrays provided by Dr. Birchfield to create vectors
             of points from which epipolar lines will be calculated.
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{EpipolarLineDrawer::CreateMatrixA()}
\begin{verbatim}
Description: This method takes the corresponding points and creates an 
n by 9 vector that will be used to computer the fundamental matrix.
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{EpipolarLineDrawer::ComputeFM()}
\begin{verbatim}
Description: This method uses the blepo::Svd method and a n-by-9 matrix to
compute the fundamental matrix to draw lines on corresponding stereo 
images.
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{EpipolarLineDrawer::CalculateX()}
\begin{verbatim}
Description:  Calculates the X coordinate of an epipolar line.
Parameters:
[+] y - a vertical location in an image
[+] a - calculated from the fundamental matrix to draw epipolar lines
[+] b - calculated from the fundamental matrix to draw epipolar lines
[+] c - calculated from the fundamental matrix to draw epipolar lines
Return: a horizontal location in an image
\end{verbatim}

\subsubsection{EpipolarLineDrawer::CalculateY()}
\begin{verbatim}
Description:  Calculates the Y coordinate of an epipolar line.
Parameters:
[+] y - a horizontal location in an image
[+] a - calculated from the fundamental matrix to draw epipolar lines
[+] b - calculated from the fundamental matrix to draw epipolar lines
[+] c - calculated from the fundamental matrix to draw epipolar lines
Return: a vertical location in an image
\end{verbatim}

\subsubsection{EpipolarLineDrawer::getPointsAtLimit()}
\begin{verbatim}
Description:  Method to get the points at the image limits using the
epipolar values for two stereo images.
Parameters:
[+] imgLmt - a point with the width and height limits for the images on
             which epipolar lines are to be drawn
[+]      a - value to calculate epipolar lines
[+]      b - value to calculate epipolar lines
[+]      c - value to calculate epipolar lines
Return: The two points where a stereo line intersect the image border
\end{verbatim}

\subsubsection{EpipolarLineDrawer::DrawEpipolarLine()}
\begin{verbatim}
Description: Method uses a point to calculate an epipolar line in a stereo
image.
Parameters:
[+]     img - The image on which an epipolar line will be drawn
[+] isPrime - A flag to let the method know how to calculate the epipolar 
              line
[+]   point - A point in a stereo image
Return: A set of points for a epipolar line
\end{verbatim}

\subsubsection{EpipolarLineDrawer::DrawEpipolarLines()}
\begin{verbatim}
Description: Method draws epipolar lines on two stereo images using the 
corresponding points vector given by Dr. Birchfield.
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{StereoMatcher::GetMinIn3D()}
\begin{verbatim}
Description:  This method takes a vector of dissimilarity matrices and a
coordinate that represents the same pixel in each matrix and returns the 
index of the smallest dissimilarity in the vector for that point.
Parameters:
[+] dbar - vector of images that are really dissimilarity matrices
[+]    x - horizontal coordinate location of image pixel
[+]    y - vertical coordinate location of image pixel
Return: An integer representing the index of a disparity level in the matrix
\end{verbatim}

\subsubsection{StereoMatcher::ComputeDbar()}
\begin{verbatim}
Description:  Creates a vector of dissimilarity matrices the size of the
left and right rectified images.
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{StereoMatcher::BlockMatchOne()}
\begin{verbatim}
Description:  Method to perform correlation based block matching for
rectified stereo images.
Parameters:
[+] leftImg  - Left rectified stereo image
[+] rightImg - Right rectified stereo image
[+] minDisp  - smallest image disparity value
[+] maxDisp  - largest  image disparity value
[+] halfWidth- half size of the window used to get the average area pixel 
               value
Return: Nothing
\end{verbatim}

\subsubsection{StereoMatcher::BlockMatchTwo()}
\begin{verbatim}
Description:  Method to perform correlation based block matching for
rectified stereo images.
Parameters:
[+] leftImg  - Left rectified stereo image
[+] rightImg - Right rectified stereo image
[+] minDisp  - smallest image disparity value
[+] maxDisp  - largest  image disparity value
[+] halfWidth- half size of the window used to convolve the disparity map
Return: Nothing
\end{verbatim}

\subsubsection{StereoMatcher::BlockMatchWithLeftToRightCheck()}
\begin{verbatim}
Description:  Takes the vector of dissimilarity values at each disparity
and makes sure the that resulting disparity map made from the smallest 
dissimilarity at each pixel between disparities contains image information
only from pixels visible in both left and right rectified images.
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\section{Results}

The two burgher images alongside a copy of each with epipolar lines overlaid.
\begin{center}
\includegraphics[width=1.5in,height=1.5in]{images/burgher1_small.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/burgher1_epipoles.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/burgher2_small.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/burgher2_epipoles.jpg} 
\end{center}

Left and right tsukuba images alongside the disparity map and left-to-right checked images.
\begin{center}
\includegraphics[width=1.5in,height=1.5in]{images/tsukuba_left.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/tsukuba_right.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/tsukuba_dispmap.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/tsukuba_checkedmap.jpg} 
\end{center}

Left and right lamp images alongside the disparity map and left-to-right checked images.
\begin{center}
\includegraphics[width=1.5in,height=1.5in]{images/lamp_left.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/lamp_right.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/lamp_dispmap.jpg}
\includegraphics[width=1.5in,height=1.5in]{images/lamp_checkedmap.jpg} 
\end{center}

\section{Discussion}

The more efficient algorithm used to calculate the disparity map produces slightly different, but better results than its more computationally intensive counterpart.  The less efficient stereo matching algorithm will be referred to as \emph{BlockMatcher1}.  The more efficient stereo matching algorithm will be referred to as \emph{BlockMatcher2}.

\subsection{Errors on tsukuba images}

\subsubsection{Question}
Run your code on tsukuba\_left.pgm and tsukuba\_right.pgm. Show the results both with and without the consistency check.  What kind of errors do you notice?

\subsubsection{Answer}
Errors include the lamp neck in tsukuba is lost, the items on the shelf and the shelf itself look like noise instead of objects in the resultant images.  The shadow from the head of the bust causes the algorithm to incorrectly map the table.  Most of the objects on the far wall blend into each other.

\subsection{Errors on lamp images}

\subsubsection{Question}
Now run the algorithm on lamp\_left.pgm and lamp\_right.pgm. What happens? Why is this image difficult?

\subsubsection{Answer}
In this image the small creases in doors become exaggerated.  Also the edges of 
the single leg of the table show up in the disparity map image as two separate
objects instead of one.  The bundle of cords in the left most part of the image
are shown as a single object, just like the doorknob or lock and the door 
crease.

This image is difficult because one of the main objects, the table on which the
lamp sits, is reflective and it is at several depths and not a single depth.
The block matching algorithm does not allow for a depth gradient for an object
at several depths simultaneously.  All of the edges and wires are difficult for
the block matching algorithm to correctly detect, because of the shadows they 
create and the combination of viewing the object from different angles.

\subsection{Latest Stereo Research}

\subsubsection{Question} 
Take a look at the results of the latest stereo research at http://vision.middlebury.edu/stereo (click on the "Evaluation" tab). Look only at the column (all) under the column Tsukuba. What errors do you see in the best algorithm (the one with minimum error in this column)?  What does this tell you about the difficulty of the problem?

\subsubsection{Answer}
In the best algorithm one of the errors is that the algorithm begins to outline the power cord to the lamp, but it is cut off before reaching the lamp head.  Another error is the malformed box on which the bust sits.  The right portion of the box in the real image is shadowed and the best algorithm missing some of the box because of it.

Even with small images, like the tsukuba images, being able to create an algorithm that correctly detects objects in the foreground and their depths is extremely involved and has yet to be done.  There are a lot of small features, like shadows, that throw off even the best algorithms.  Creating an algorithm with the ability to detect shadows in images and understand that they do not make the object, or part of an object,  different than the one being processed perplexes the best researchers.


\section{Conclusion}

Stereo matching has yet to be solved.  Stereo matching is an ingenious component of vision that allows machine to measure depths in the natural world.

\end{document}