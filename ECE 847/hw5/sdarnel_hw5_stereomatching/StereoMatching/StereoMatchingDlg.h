// StereoMatchingDlg.h : header file
//

#if !defined(AFX_STEREOMATCHINGDLG_H__E122AAA7_F34A_495F_AFDC_DB6BAC36C183__INCLUDED_)
#define AFX_STEREOMATCHINGDLG_H__E122AAA7_F34A_495F_AFDC_DB6BAC36C183__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ImageProcessing.h"
#include "WatershedSegmenter.h"

/////////////////////////////////////////////////////////////////////////////
// CStereoMatchingDlg dialog

class CStereoMatchingDlg : public CDialog
{
// Construction
public:
	CStereoMatchingDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CStereoMatchingDlg)
	enum { IDD = IDD_STEREOMATCHING_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStereoMatchingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	char * m_cFileDialogFilter;

	// Generated message map functions
	//{{AFX_MSG(CStereoMatchingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnFloodfill4();
	afx_msg void OnFloodfill4();
	afx_msg void OnFloodfill8();
	afx_msg void OnOutlineForeground();
	afx_msg void OnCannyDetectEdges();
	afx_msg void OnTempateMatching();
	afx_msg void OnWatershedSegmentation();
	afx_msg void OnPencilEpipolar();
	afx_msg void OnBtnStereoMatching();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STEREOMATCHINGDLG_H__E122AAA7_F34A_495F_AFDC_DB6BAC36C183__INCLUDED_)
