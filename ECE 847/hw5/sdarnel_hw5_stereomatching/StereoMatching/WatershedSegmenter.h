/*****************************************************************************
 * WatershedSegmenter.h
 *
 *****************************************************************************/

#pragma once

#ifndef _WATERSHEDSEGMENTER_H_
#endif // _WATERSHEDSEGMENTER_H_

#include "ImageProcessing.h"
#include <queue>

class WatershedSegmenter
{
public:
	WatershedSegmenter(blepo::ImgBgr img, CString imageTitle, double sigma);
	
	void ComputeGrayLevelPixelMap(blepo::ImgGray &img);
	
	int                                      GetGlobalLevel();
	int										 GetSmallestBorder();
	double                                   GetSigma();
	double									 GetThresholdRatio();
	CString                                  GetImageTitle();
	blepo::ImgBgr                            GetOriginalImage();
	blepo::ImgBgr                            getResultImage();
	blepo::ImgGray                           getGradientMagnitude();
	blepo::ImgInt                            getLabelsImage();
	blepo::ImgInt                            getChamferImage();
	blepo::ImgBinary                         getThresholdedImage();
	blepo::ImgBinary                         getEdgeImage();
	blepo::ImgBinary                         getMarkerImage();
	std::vector< std::vector<blepo::Point> > getMap();
	std::queue<blepo::Point>                 getFrontier();
	blepo::Point                             popFrontier();
	ImageProcessing							 getImageProcessor();
	void                                     pushFrontier(blepo::Point pixel);
	void                                     SegmentImage( int segmentationType );
	void                                     getNeighbors4( blepo::Point location, blepo::Point maxXY, std::queue<blepo::Point> * neighbors);
	void									 SetSmallestBorder(int border);
	void									 SetThresholdRatio(double ratio);

private:
	int              m_globalLevel;
	int              m_smallestBorder;

	double           m_sigma;
	double           m_thresholdRatio;

	CString          m_imageTitle;

	GaussianInfo     m_gi;

	ImageProcessing  m_imageProcessor;
	
	blepo::ImgBgr    m_originalImage;
	blepo::ImgGray   m_gradientMagnitude;
	blepo::ImgGray   m_constrainedGradientMagnitude;
	blepo::ImgInt    m_labels;
	blepo::ImgBinary m_thresholdedImage;
	blepo::ImgInt    m_chamferImage;
	blepo::ImgInt    m_labelImage;
	blepo::ImgBinary m_edgeImage;
	blepo::ImgBinary m_markerImage;
	blepo::ImgBgr    m_resultImage;

	
	std::vector<int>                         m_pixelLabels ;
	std::queue< blepo::Point >               m_frontier;
	std::vector< std::vector<blepo::Point> > m_grayLevelPixelMap;

	void Initialize(blepo::ImgBgr img, CString imageTitle, double sigma);
	void SetGlobalLevel( int level );
	void SetImageTitle( CString imageTitle );
	void SetGradientMagnitude(blepo::ImgGray img);
	void MarkerBasedSegmentation();
	void WatershedSegmentation( blepo::ImgGray & img, bool useMarkers );
	void MakeEdgeBinaryFromImgInt( const blepo::ImgInt &label, blepo::ImgBinary * edges ) ;
	void CreateMarkerImage( const blepo::ImgBinary & tImg, const blepo::ImgBinary & eImg, blepo::ImgBinary * mImg );
	void CreateResultImage( blepo::Bgr outlineColor, int basin );
	void GetValidLabels( blepo::ImgInt * labels, std::vector<int> * validLabels );
	void GetBorderPoints( blepo::ImgInt & labels, std::vector<int> & validLabels, std::vector< std::vector<blepo::Point> > * borderPoints );
};