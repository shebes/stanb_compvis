function [ DifferenceImages ] = CreateDifferenceImages( BasisImages, MeanFace )
%CREATEBASISVECTORS Summary of this function goes here
%   Detailed explanation goes here

    [rows,cols]      = size(  BasisImages(:,:,1));
    numfaces         = length(BasisImages(1,1,:));
    DifferenceImages = zeros(rows,cols,numfaces);

    for i = 1:numfaces
        DifferenceImages(:,:,i) = BasisImages(:,:,i) - MeanFace ;
    end

end
