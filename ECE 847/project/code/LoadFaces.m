function [ TrainingGroup, TestingGroup, TrainingClasses ] = LoadFaces()
%LOADFACES Summary of this function goes here
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loadfaces.m
% This file loads the images for training and testing the PNN.
% Training images are denoted using the prefix 'TR_', testing set images 
% are denoted with the prefix 'TE_'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    fid         = fopen('directorys.txt','r');
    imgFileName = 'images_normalized_size.lst';
    

    pathName = '';
    index = 1;    
    while 1   
        newlineValue = fgetl(fid);
        if ~ischar(newlineValue), break, end
        pathName = strcat( pathName, ',', newlineValue ) ;
        %disp(pathName);
        index = index + 1;
    end
    fclose(fid);
    
    %remain = 'j';
    %index = 0;
    %while remain
    %    [token,remain] = strtok(pathName,',');
        %display(token);
    %    if strcmp(remain,'')==1, break, end;
    %    fileName = strcat(token,'/',imgFileName);
    %    disp(fileName);
    %    imageDirectory = strcat(token,'/','images_normalized');
    %    LoadedImages = LoadImagesFromList(imageDirectory, fileName);
    %    size(LoadedImages)
    %    index = index + 1;
    %    pathName = remain;
    %end

    % Images from Set0 b/w males with facial hair
    TR_001 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/027288_4M60.pgm');
    TR_002 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/030088_00M59.pgm');
    TR_003 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/031940_0M52.pgm');
    TR_004 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/032572_4M54.pgm');
    TR_005 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/034913_3M55.pgm');
    
    % Images from Set1 b males without facial hair    
    TR_006 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/036051_1M54.pgm');
    TR_007 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/037969_00M57.pgm');
    TR_008 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/038059_1M51.pgm');
    TR_009 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/040007_03M55.pgm');
    TR_010 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/046091_1M57.pgm'); 
    
    % Images from Set2 w males without facial hair
    TR_011 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/020490_0M67.pgm');
    TR_012 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/032259_2M57.pgm');
    TR_013 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/033511_00M57.pgm');
    TR_014 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/043203_0M56.pgm');
    TR_015 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/046004_04M60.pgm');
    
    % Images from Set3 b males with facial hair
    TR_016 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/027017_03M62.pgm');
    TR_017 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/031293_0M57.pgm');
    TR_018 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/035838_1M52.pgm');
    TR_019 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/038390_1M57.pgm');
    TR_020 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/040025_1M52.pgm');
    
    % Images from Set4 w males with facial hair
    TR_021 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/029983_1M59.pgm');
    TR_022 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/037372_0M51.pgm');
    TR_023 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/037948_4M57.pgm');
    TR_024 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/041751_0M50.pgm');
    TR_025 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/042384_11M50.pgm');
    
    
    numberofsets           = 25;
    %trainingsize           = 10;
    %TrainingClasses        = [1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1];
    TrainingClasses        = [1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2];
    [rows,cols]            = size(TR_006);
    mbyn                   = rows*cols;
    %TrainingGroup          = zeros(rows*cols,trainingsize*numberofsets);
    TrainingGroup          = zeros(rows*cols,numberofsets);
    
    TrainingGroup(:,21) = reshape( TR_001, mbyn, 1 );
    TrainingGroup(:,22) = reshape( TR_002, mbyn, 1 );
    TrainingGroup(:,23) = reshape( TR_003, mbyn, 1 );
    TrainingGroup(:,24) = reshape( TR_004, mbyn, 1 );
    TrainingGroup(:,25) = reshape( TR_005, mbyn, 1 );
    TrainingGroup(:,1)  = reshape( TR_006, mbyn, 1 );
    TrainingGroup(:,2)  = reshape( TR_007, mbyn, 1 );
    TrainingGroup(:,3)  = reshape( TR_008, mbyn, 1 );
    TrainingGroup(:,4)  = reshape( TR_009, mbyn, 1 );
    TrainingGroup(:,5)  = reshape( TR_010, mbyn, 1 );
    TrainingGroup(:,6)  = reshape( TR_011, mbyn, 1 );
    TrainingGroup(:,7)  = reshape( TR_012, mbyn, 1 );
    TrainingGroup(:,8)  = reshape( TR_013, mbyn, 1 );
    TrainingGroup(:,9)  = reshape( TR_014, mbyn, 1 );
    TrainingGroup(:,10) = reshape( TR_015, mbyn, 1 );
    TrainingGroup(:,11) = reshape( TR_016, mbyn, 1 );
    TrainingGroup(:,12) = reshape( TR_017, mbyn, 1 );
    TrainingGroup(:,13) = reshape( TR_018, mbyn, 1 );
    TrainingGroup(:,14) = reshape( TR_019, mbyn, 1 );
    TrainingGroup(:,15) = reshape( TR_020, mbyn, 1 );
    TrainingGroup(:,16) = reshape( TR_021, mbyn, 1 );
    TrainingGroup(:,17) = reshape( TR_022, mbyn, 1 );
    TrainingGroup(:,18) = reshape( TR_023, mbyn, 1 );
    TrainingGroup(:,19) = reshape( TR_024, mbyn, 1 );
    TrainingGroup(:,20) = reshape( TR_025, mbyn, 1 );

    % Images from Set0 b/w males with facial hair
    TE_001 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/035110_1M52.pgm');
    TE_002 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized/037816_01M55.pgm');
    
    % Images from Set1 b males without facial hair  
    TE_003 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/046447_08M52.pgm');
    TE_004 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized/047136_0M48.pgm');
    
    % Images from Set2 w males without facial hair
    TE_005 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/046148_1M48.pgm');
    TE_006 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized/047534_02M63.pgm');
    
    % Images from Set3 b males with facial hair
    TE_007 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/040087_0M62.pgm');
    TE_008 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized/041436_1M55.pgm');
    
    % Images from Set4 w males with facial hair
    TE_009 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/044056_06M51.pgm');
    TE_010 = imread('/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized/047373_0M46.pgm');
    
    testingsize       = 10;
    %TestingGroup      = zeros(rows,cols,testingsize,numberofsets);
    TestingGroup      = zeros(rows*cols,testingsize);
    TestingGroup(:,9) = reshape( TE_001, mbyn, 1 );
    TestingGroup(:,10)= reshape( TE_002, mbyn, 1 );
    TestingGroup(:,1) = reshape( TE_003, mbyn, 1 );
    TestingGroup(:,2) = reshape( TE_004, mbyn, 1 );
    TestingGroup(:,3) = reshape( TE_005, mbyn, 1 );
    TestingGroup(:,4) = reshape( TE_006, mbyn, 1 );
    TestingGroup(:,5) = reshape( TE_007, mbyn, 1 );
    TestingGroup(:,6) = reshape( TE_008, mbyn, 1 );
    TestingGroup(:,7) = reshape( TE_009, mbyn, 1 );
    TestingGroup(:,8) = reshape( TE_010, mbyn, 1 );
    
end
