function [ BasisEigenFaces ] = ShowEigenFaces( U, rows, cols )
%SHOWEIGENFACES Summary of this function goes here
%   Detailed explanation goes here

    %BasisEigenFaces = zeros( sqrt(length(U(:,1))), sqrt(length(U(:,1))), length(U(1,:)) );
    BasisEigenFaces = zeros( rows, cols, length(U(1,:)) );

    for i = 1:length(U(1,:))
        BasisEigenFaces(:,:,i) = reshape( U(:,i), rows, cols );
        %figure, imagesc( BasisEigenFaces(:,:,i) ), colormap(gray);
    end

end
