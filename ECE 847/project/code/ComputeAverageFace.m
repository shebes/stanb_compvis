function [ AverageFace ] = ComputeAverageFace( BasisImages )
%COMPUTEAVERAGEFACE Sum the face images and divide by the number of images.
%   

    dims = ndims(BasisImages);
    
    % average matrices with rows and cols > 1
    if ( eq( dims, 3 ) )
        [rows,cols] = size(BasisImages(:,:,1));
        numfaces    = length(BasisImages(1,1,:));
        sumfaces    = zeros(rows,cols);

        for i = 1:numfaces
            sumfaces = sumfaces + BasisImages(:,:,i);
        end
        
    % average matrices with rows > 1 and cols == 1
    else
        rows         = length(BasisImages(:,1));
        numfaces     = length(BasisImages(1,:));
        sumfaces     = zeros(rows,1);
        
        for i = 1:numfaces
            sumfaces = sumfaces + BasisImages(:,i);
        end
        
    end
    
    AverageFace = sumfaces / numfaces;

end