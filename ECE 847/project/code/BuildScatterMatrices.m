function [ ScatterMatrices, Sw, Sb ] = BuildScatterMatrices( ...
               AverageFace, AverageFaces, SubtractedFaces, FacesPerPerson )
%BUILDSCATTERMATRICES Calculate scatter matrices
%   Returns the Scatter matrices and the within-class scatter matrix

    [numFeatures,numImages] = size(SubtractedFaces);
    
    ScatterMatrices         = zeros(numFeatures, numFeatures, numImages/FacesPerPerson);
    Sw                      = zeros(numFeatures, numFeatures, 1);
    Sb                      = zeros(numFeatures, numFeatures, 1);
    
    
    % create the within-class scatter matrix Sw
    idx = 1;
    for i = 1:numImages        
        ScatterMatrices(:,:,idx) = ScatterMatrices(:,:,idx) + ...
            SubtractedFaces(:,i) * SubtractedFaces(:,i)';
        if ( mod(i,FacesPerPerson) == 0 )
            Sw = Sw + ScatterMatrices(:,:,idx);
            idx = idx + 1;
        end
    end
    
    % create the between-class scatter matrix Sb
    for i = 1:( length(ScatterMatrices(1,1,:)) )
        tempmat = AverageFaces(:,i) - AverageFace;
        Sb(:,:,1) = Sb(:,:,1) + ...
                    ( 2 * ( tempmat * tempmat' ) );
    end

end
