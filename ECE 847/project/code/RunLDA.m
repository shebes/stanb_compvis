function [ W, LDAGallery, LDAProbe ] = ...
                      RunLDA( BasisProjection, GalleryP, ProbeP )
%RUNLDA Summary of this function goes here
%   Fisherfaces
%   Step 1 - Compute the average of all faces
%   Step 2 - Compute the average face of each person
%   Step 3 - Subtract the average face of each person from the training
%            faces
%   Step 4 - Build a scatter matrix for each person in the training set.
%   Step 5 - Build the within-class scatter matrix Sw.
%   Step 6 - Build the between-class scatter matrix.

    % Step 1
    AverageFace             = ComputeAverageFace(BasisProjection);
    
    % Step 2    
    FacesPerPerson          = length( BasisProjection(1,:) ) / ...
                              length( ProbeP(1,1,:) );                      
    AverageFaces            = ComputeAllAverageFaces(BasisProjection, ...
                              FacesPerPerson);
    
    % Step 3
    SubtractedFaces         = SubtractAverageFaces(BasisProjection, ...
                              AverageFaces);
    
    % Steps 4 - 6
    [ScatterMatrices,Sw,Sb] = BuildScatterMatrices(AverageFace, ...
                              AverageFaces, SubtractedFaces, FacesPerPerson);
    
    % Step 7
    W                       = inv(Sw) * Sb;
    LDAGallery              = LDAProjection(W, GalleryP);
    LDAProbe                = LDAProjection(W, ProbeP);

end
