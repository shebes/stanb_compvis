function [ TV ] = CreatePNNVector( TRP )
%CREATEPNNVECTOR Summary of this function goes here
%   Detailed explanation goes here

    [rowsandcols,sizeofsets,numberofsets] = size(TRP);
    TV = zeros(rowsandcols,sizeofsets*numberofsets);
    
    index = 1;
    for j = 1:numberofsets
        for i = 1:sizeofsets
            TV(:,index) = TRP(:,i,j);
            index = index + 1;
        end
    end

end
