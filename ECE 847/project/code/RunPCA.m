function [ EigenFaces, TrainingProjection, TestingProjection ]...
         = RunPCA( q, TrainingImages, TestingImages )
%RUNPCA Uses principle component analysis for face recognition
%   Detailed explanation goes here
    
    % load training and testing images along with the training classes
    [rows,cols]         = size( TrainingImages(:,:,1) );

    % get the mean face
    AverageFace         = ComputeAverageFace(TrainingImages);

    % subtract the mean from all images, at this point in the algorithm, 
    % all of the values are set as NxN matrices, we have to reshape them 
    % into 1xN^2 vectors
    [DifferenceImages]  = CreateDifferenceImages(TrainingImages, AverageFace);

    % method reshapes the NxN matrices into 1xN^2 vectors "A"
    [DifferenceVectors] = CreateDifferenceVectors(DifferenceImages);
    A                   = DifferenceVectors;    

    % the covariance matrix is created
    [CovarianceMatrix]  = DifferenceVectors' * DifferenceVectors;
    [V,D]               = eig(CovarianceMatrix);
    d                   = diag(D);
    [d,idx]             = sort(d);
    idx                 = flipud(idx);
    V                   = V(:, idx);
    VBest               = V(:, 1:q);
    U                   = A * VBest;

    % The following command shows the different eigenfaces calculated from 
    % the training images
    EigenFaces          = ShowEigenFaces(U, rows, cols);
    TrainingProjection  = GetProjections(U, AverageFace, TrainingImages);
    TestingProjection   = GetProjections(U, AverageFace, TestingImages);

end
