function [ AverageFaces ] = ComputeAllAverageFaces( BasisImages, FacesPerPerson )
%COMPUTEALLAVERAGEFACES Calculates the average face for each person in the
%                       training set
%   Detailed explanation goes here


    numFeatures  = length( BasisImages(:,1) );
    numFaces     = length( BasisImages(1,:) ) / FacesPerPerson;
    AverageFaces = zeros( numFeatures, numFaces );
    
    index = 1;
    i     = 1;
    while ( index < length(BasisImages(1,:)) )
        
        ImagesToAverage            = zeros( numFeatures, FacesPerPerson );
        ImagesToAverage(:,index)   = BasisImages(:,index);
        ImagesToAverage(:,index+1) = BasisImages(:,index+1);
        ImagesToAverage(:,index+2) = BasisImages(:,index+2);
        AverageFaces(:,i)          = ComputeAverageFace(ImagesToAverage);
        
        index = index + 3;        
        i     = i + 1;
        
    end

end
