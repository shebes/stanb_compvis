function [ BasisOmegas ] = CalculateOmegas( U, BasisImages )
%CALCULATEOMEGAS Summary of this function goes here
%   Detailed explanation goes here

    BasisOmegas = zeros( length(U(1,:)), length(BasisImages(1,1,:)) );

    for i = 1:length(BasisImages(1,1,:))
        BasisOmegas(:,i) = U' * reshape( BasisImages(:,:,i), length(U(:,1)), 1 );
    end
end
