function [] = RunFullDimensionTraining( TRData, TEData, TRC, TEC, incType, incSpread, minSpread, maxSpread )
%RUNFULLDIMENSIONTRAINING Method to train a PNN to detect facial hair pixel
%by pixel
%   RunFullDimensionTraining uses entire images as vector inputs to the
%   Probabilistic Neural Network (PNN).  The PNN trains on every pixel and
%   is very inefficient/slow.

    fidout = fopen('fulldimension_results.out','w');
    spread = minSpread;
    while ( spread < maxSpread )
        T      = ind2vec(TRC);
        net    = newpnn(TRData,T,spread);

        %{
        % We do not need to simulate how well the PNN classifies the 
        % training data.
        A  = sim(net,TRData);
        Ac = vec2ind(A);
        %}

        a  = sim(net,TEData);
        ac = vec2ind(a);

        resultTED             = abs(ac - TEC);
        correctClassification = ( length(ac(1,:)) - sum(resultTED) ) / length(ac(1,:));

        fprintf(fidout, 'spread value: %f\n', spread);
        fprintf(fidout, 'classification: %f\n', correctClassification);    

        if (incType == 1)
            spread = spread * incSpread;
        elseif (incType == 2)
            spread = spread + incSpread;
        elseif (incType == 3)
            spread = spread / incSpread;
        elseif (incType == 4)
            spread = spread - incSpread;
        else 
            spread = spread + 1;
        end
                  
        disp('run complete');
    end

    fclose(fidout);

end

