function [ DistMag ] = GetDistKnownFaces( GP, PP, distancemeasure )
%GETDISTKNOWNFACES Summary of this function goes here
%   Detailed explanation goes here

    numberGallery = length( GP(1,:) );
    numberProbe   = length( PP(1,:) );
    PPair         = zeros( length( GP(:,1) ), 2 );
    DistMag       = zeros( numberProbe, numberGallery );    
    
    for j = 1:numberProbe
        for i = 1:numberGallery
            PPair(:,1) = PP(:,j);
            PPair(:,2) = GP(:,i);
            if strcmp(distancemeasure,'norm') == 1
                DistMag(j,i) = norm( PP(:,j) - GP(:,i) );
            end
            if strcmp(distancemeasure,'norm1') == 1
                DistMag(j,i) = norm( PP(:,j) - GP(:,i), 1 );
            end
            if strcmp(distancemeasure,'norm2') == 1
                DistMag(j,i) = normest( PP(:,j) - GP(:,i) );
            end
            if strcmp(distancemeasure,'norminf') == 1
                DistMag(j,i) = norm( PP(:,j) - GP(:,i), 'inf' );
            end
            if strcmp(distancemeasure,'fro') == 1
                DistMag(j,i) = norm( PP(:,j) - GP(:,i), 'fro' );
            end
            if strcmp(distancemeasure,'mahal') == 1
                DistMag(j,i) = max( svd( mahal( GP(:,i), PP(:,j) ) ) );
            end
            if strcmp(distancemeasure,'minkowski') == 1
                DistMag(j,i) = max( svd( pdist( PPair, 'minkowski', 2) ) );
            end
            if strcmp(distancemeasure,'cityblock') == 1
                DistMag(j,i) = max( svd( pdist( PPair, 'cityblock' ) ) );
            end
            if strcmp(distancemeasure,'euclidean') == 1
                DistMag(j,i) = max( svd( pdist( PPair, 'euclidean' ) ) );
            end
        end
    end

end
