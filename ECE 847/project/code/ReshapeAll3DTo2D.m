function [ NewImageList ] = ReshapeAll3DTo2D( ImageList )
%RESHAPEALL3D Summary of this function goes here
%   Detailed explanation goes here

    [rows,cols,listSize] = size(ImageList);
    NewImageList         = zeros( rows*cols, listSize );
    
    for i = 1:listSize
        NewImageList(:,i) = reshape( ImageList(:,:,i), rows*cols, 1 );
    end

end
