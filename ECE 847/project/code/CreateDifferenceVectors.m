function [ DifferenceVectors ] = CreateDifferenceVectors( DifferenceImages )
%CREATEDIFFERENCEVECTORS Summary of this function goes here
%   Detailed explanation goes here

    [rows,cols]       = size(  DifferenceImages(:,:,1));
    numfaces          = length(DifferenceImages(1,1,:));
    DifferenceVectors = zeros(rows*cols,numfaces);
    
    for i = 1:numfaces
        count = 1;
        for j = 1:cols
            for k = 1:rows
                DifferenceVectors(count,i) = DifferenceImages(k,j,i);
                count = count + 1;
            end
        end
    end

end
