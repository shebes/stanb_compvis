function [ TRDataPCA, TRData, TEDataPCA, TEData, TRC, TEC ] = GetTrainAndTestData()
%GETTRAINANDTESTDATA Summary of this function goes here
%   Detailed explanation goes here


    % load training and testing data
    ImageDirectory    = '/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized';
    ImageList         = '/group/ival/users/sdarnel/morph/facialHair/data/set0/images_normalized_size.lst';
    [TR_SET0,TE_SET0] = LoadImagesFromList( ImageDirectory, ImageList );
    %{%}
    ImageDirectory    = '/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized';
    ImageList         = '/group/ival/users/sdarnel/morph/facialHair/data/set1/images_normalized_size.lst';
    [TR_SET1,TE_SET1] = LoadImagesFromList( ImageDirectory, ImageList );
    ImageDirectory    = '/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized';
    ImageList         = '/group/ival/users/sdarnel/morph/facialHair/data/set2/images_normalized_size.lst';
    [TR_SET2,TE_SET2] = LoadImagesFromList( ImageDirectory, ImageList );
    ImageDirectory    = '/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized';
    ImageList         = '/group/ival/users/sdarnel/morph/facialHair/data/set3/images_normalized_size.lst';
    [TR_SET3,TE_SET3] = LoadImagesFromList( ImageDirectory, ImageList );
    ImageDirectory    = '/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized';
    ImageList         = '/group/ival/users/sdarnel/morph/facialHair/data/set4/images_normalized_size.lst';
    [TR_SET4,TE_SET4] = LoadImagesFromList( ImageDirectory, ImageList );

    % create training data
    [rows,cols,set0size] = size(TR_SET0);
    set1size             = length( TR_SET1(1,1,:) );
    set2size             = length( TR_SET2(1,1,:) );
    set3size             = length( TR_SET3(1,1,:) );
    set4size             = length( TR_SET4(1,1,:) );
    twhLength            = set0size + set3size + set4size;
    twohLength           = set1size + set2size;

    Training_WH = zeros( rows, cols, twhLength );
    mLenSum     = zeros(3,1);
    mLenSum(1)  = set0size;
    mLenSum(2)  = mLenSum(1) + set3size;
    mLenSum(3)  = mLenSum(2) + set4size;

    Training_WH( :, :, 1:mLenSum(1) )            = TR_SET0; 
    Training_WH( :, :, mLenSum(1)+1:mLenSum(2) ) = TR_SET3;
    Training_WH( :, :, mLenSum(2)+1:mLenSum(3) ) = TR_SET4;

    Training_WOH = zeros( rows, cols, twohLength );
    mLenSum      = zeros(2,1);
    mLenSum(1)   = set1size;
    mLenSum(2)   = mLenSum(1) + set2size;

    Training_WOH( :, :, 1:mLenSum(1) )            = TR_SET1; 
    Training_WOH( :, :, mLenSum(1)+1:mLenSum(2) ) = TR_SET2;

    TRDataPCA = zeros( rows, cols, twhLength + twohLength );
    mLenSum          = zeros(2,1);
    mLenSum(1)       = twhLength;
    mLenSum(2)       = mLenSum(1) + twohLength;

    TRDataPCA( :, :, 1:mLenSum(1) )            = Training_WH; 
    TRDataPCA( :, :, mLenSum(1)+1:mLenSum(2) ) = Training_WOH;

    TRData = ReshapeAll3DTo2D(TRDataPCA);
    
    tca    = ones(1,twhLength);
    tcb    = ones(1,twohLength) + ones(1,twohLength);
    TRC(1,1:twhLength)                           = tca;
    TRC(1,twhLength+1:length(TRData(1,:))) = tcb;
    
    % create testing data
    set0size = length( TE_SET0(1,1,:) );
    set1size = length( TE_SET1(1,1,:) );
    set2size = length( TE_SET2(1,1,:) );
    set3size = length( TE_SET3(1,1,:) );
    set4size = length( TE_SET4(1,1,:) );
    tewhLen  = set0size + set3size + set4size
    tewohLen = set1size + set2size

    Testing_WH  = zeros( rows, cols, tewhLen );
    mLenSum     = zeros(3,1);
    mLenSum(1)  = set0size;
    mLenSum(2)  = mLenSum(1) + set3size;
    mLenSum(3)  = mLenSum(2) + set4size;

    Testing_WH( :, :, 1:mLenSum(1) )            = TE_SET0; 
    Testing_WH( :, :, mLenSum(1)+1:mLenSum(2) ) = TE_SET3;
    Testing_WH( :, :, mLenSum(2)+1:mLenSum(3) ) = TE_SET4;

    Testing_WOH = zeros( rows, cols, tewohLen );
    mLenSum     = zeros(2,1);
    mLenSum(1)  = set1size;
    mLenSum(2)  = mLenSum(1) + set2size;

    Testing_WOH( :, :, 1:mLenSum(1) )            = TE_SET1; 
    Testing_WOH( :, :, mLenSum(1)+1:mLenSum(2) ) = TE_SET2;

    TEDataPCA = zeros( rows, cols, tewhLen + tewohLen );
    mLenSum          = zeros(2,1);
    mLenSum(1)       = tewhLen;
    mLenSum(2)       = mLenSum(1) + tewohLen;

    TEDataPCA( :, :, 1:mLenSum(1) )            = Testing_WH; 
    TEDataPCA( :, :, mLenSum(1)+1:mLenSum(2) ) = Testing_WOH;

    TEData = ReshapeAll3DTo2D(TEDataPCA);
    
    
    tca    = ones(1,tewhLen);
    tcb    = ones(1,tewohLen) + ones(1,tewohLen);
    TEC(1,1:tewhLen)                     = tca;
    TEC(1,tewhLen+1:length(TEData(1,:))) = tcb;

end
