
% load training and testing images along with the training classes
% TRI - training images
% TEI - testing images
% TRC - training classes
%[ TRG, TEG, TRC ] = LoadFaces();
[TRDataPCA,TRData,TEDataPCA,TEData,TRC,TEC] = GetTrainAndTestData();


%Training_WOH  = zeros( rows, cols, set1size+set2size ); 
%{%}

%BEF - Basis Eigen Faces
%TRP - Projection of the training set onto the face space (an input to the
%      LDA) algorithm
%TEP - Projection of the testing images onto the face space
%q   - The size of the feature space, there are 25 images with which we
%      train PCA, but to do recognition we only want the N best, which is q
q                 = 10;
% group0 - black and white males with facial hair
%[ BEF.g0, TRP(:,:,1), TEP(:,:,1) ] = RunPCA( q, TRG(:,:,:,1), TEG(:,:,:,1) );
% group1 - black males without facial hair
%[ BEF.g1, TRP(:,:,1), TEP(:,:,1) ] = RunPCA( q, TRG(:,:,:,1), TEG(:,:,:,1) );
% group2 - white males without facial hair
%[ BEF.g2, TRP(:,:,2), TEP(:,:,2) ] = RunPCA( q, TRG(:,:,:,2), TEG(:,:,:,2) );
% group3 - black males with facial hair
%[ BEF.g3, TRP(:,:,4), TEP(:,:,4) ] = RunPCA( q, TRG(:,:,:,4), TEG(:,:,:,4) );
% group4 - white males with facial hair
%[ BEF.g4, TRP(:,:,5), TEP(:,:,5) ] = RunPCA( q, TRG(:,:,:,5), TEG(:,:,:,5) );

%PNNTrain = CreatePNNVector(TRP);
%PNNTest  = CreatePNNVector(TEP);

% incType of 1 is multiplication
% incType of 2 is addition
% incType of 3 is division
% incType of 4 is subtraction
incType   = 1;
incSpread = 10;
RunFullDimensionTraining( TRData, TEData, TRC, TEC, incType, incSpread, minSpread, maxSpread );
