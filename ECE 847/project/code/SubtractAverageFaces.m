function [ SubtractedFaces ] = SubtractAverageFaces( BasisProjection, AverageFaces )
%SUBTRACTAVERAGEFACES Summary of this function goes here
%   Detailed explanation goes here

    [rows,cols]      = size( BasisProjection );
    FacesPerPerson   = length( BasisProjection(1,:) ) / ...
                       length( AverageFaces(1,:) );
    SubtractedFaces  = zeros( rows, cols );
    
    averageFaceIndex = 1;
    
    for i = 1:cols
        SubtractedFaces(:,i) = BasisProjection(:,i) - ...
                               AverageFaces(:,averageFaceIndex);
        
        if  eq( mod(i,FacesPerPerson), 0 )
            averageFaceIndex = averageFaceIndex + 1;
        end
    end

end
