function [FAR,FRR,cmc] = CreateRocCmcWithMeasures( GP, PP, DM, AlgType )
%CREATEROCCMCWITHMEASURES Create ROC and CMC curves from Gallery and Probe
%   Detailed explanation goes here

    for i = 1:length(DM)
        dist          = GetDistKnownFaces( GP, PP, DM(i) );
        [FAR,FRR,MAT] = GetFarFrr(dist);
        cmc           = GetCMC(dist);
       
        FAR_FRR_DIST  = abs(FAR - FRR);
        EER           = min(FAR_FRR_DIST);
        for j = 1:length( FAR(1,:) )
            if ( FAR_FRR_DIST(1,j) == EER )
                fareq = FAR(1,j);
                frreq = FRR(1,j);
                break;
            end
        end
        
        % Draw ROC curve
        xtitle    = 'X False Accept Rate (FAR)';
        ytitle    = 'Y False Reject Rate (FRR)';
        plottitle = [AlgType ...
                     'Receiver Operating Characteristic (ROC) curve for '...
                     , DM(i), ' Distance'];
        figure,plot(FAR,FRR),xlabel(xtitle),ylabel(ytitle),title(plottitle);        
        
        % Annotate the EER
        text(fareq,frreq,'\leftarrow Equal Error Rate (EER)',...
                         'HorizontalAlignment','left');

        % Draw CMC curve
        xtitle    = 'Rank';
        ytitle    = 'Values at Rank';
        plottitle = [AlgType ...
                     'Cumulative Match Characteristic (CMC) curve for ',...
                     DM(i)];
        figure,plot(cmc),xlabel(xtitle),ylabel(ytitle),title(plottitle);
    end

end
