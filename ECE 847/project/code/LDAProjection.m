function [ LDASpace ] = LDAProjection(W, ImagesToProject)
%LDAPROJECTION Summary of this function goes here
%   Detailed explanation goes here

    LDASpace = zeros( length(W(1,:)), length(ImagesToProject(1,:)) );

    for i = 1:length(ImagesToProject(1,:))
        % compute projection onto LDA face space
        LDASpace(:,i) = W' * ImagesToProject(:,i);
    end

end
