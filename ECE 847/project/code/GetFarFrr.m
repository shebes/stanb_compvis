function [ FAR, FRR, MAT ] = GetFarFrr( SM )
%CREATEFARFRR Summary of this function goes here
%   Detailed explanation goes here

   [rows,cols] = size(SM);
    MAT         = zeros(1,rows);
    
    % create the vector containing the real matches
    index = 1;
    while ( index <= rows )
        MAT(1,index) = SM( index, index );
        index = index + 1;
    end
    
    minThreshold    = min(min(SM));
    maxThreshold    = max(max(SM));
    thresholdNumber = 30;
    thresholdStep   = floor( (maxThreshold-minThreshold) / thresholdNumber);   
    thold           = minThreshold;   
    FAR             = zeros(1, thresholdNumber);
    FRR             = zeros(1, thresholdNumber);
    numImposters    = (rows * cols) - cols;
    
    % This outer loop goes over the similarity matrices for 
    for k = 1:thresholdNumber
        sumFARS    = 0;
        sumFRRS    = 0;
        
        for j = 1:rows
            sumFAR = 0;
            sumFRR = 0;
            for i = 1:cols
                % Count the number of false accepts.
                % The lower value denotes the better match.
                % False accepts are found when i and j are not the same 
                % (i and j being the same denotes the true match) and the
                % threshold value of the tested value is lower or equal to
                % the threshold.
                if ( i ~= j && SM(j,i) <= thold )
                    sumFAR = sumFAR + 1;
                end
                % Count the number of false rejects.
                % We count a false reject when the threshold is lower than
                % the actual match value.
                if ( eq(i,j) && SM(j,i) > thold )
                    sumFRR = sumFRR + 1;
                end
            end
            sumFARS = sumFARS + sumFAR;
            sumFRRS = sumFRRS + sumFRR;
        end

        % FAR = # false accepts / # imposters
        FAR(1,k) = sumFARS / numImposters ;
        % FRR = # false rejects / # genuine accepts
        FRR(1,k) = sumFRRS / cols ;
        thold = thold + thresholdStep;
        if ( thold > maxThreshold )
            break;
        end
    end

end
