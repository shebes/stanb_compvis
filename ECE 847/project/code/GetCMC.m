function [ curve ] = GetCMC( SimMat )
%GETCMC Gives rankings for the finding of matches for a biometric algorithm
%   A cumulative matching characteristic (CMC) curve is one that is 
%   montonically non-decreasing.  A CMC curve gives an idea of how well a
%   biometric and distance function work to match probe and gallery images.
%   A probe image is one that is taken when an individual is being 
%   evaluated by a biometric system.  A gallery image is one that was taken
%   during registration.

    [rows,cols] = size(   SimMat );
    matches     = diag(   SimMat );
    curve       = zeros( cols, 1 );
    
    for i = 1:cols
        tempVec = SimMat(:,i);
        tempVec = sort(tempVec);
        rows    = length( tempVec( :, 1 ) );
        matchfound = 0 ;
        for j = 1:rows
            % set a flag when a match is found
            if ( matches(i,1) == tempVec(j,1) )
                matchfound = 1;
            end
            % once a match is found increment each following value in the
            % cumulative matching curve
            if ( matchfound == 1 )
                curve(j,1) = curve(j,1) + 1;
            end
        end
    end
end
