function [ OmegaSpace ] = GetProjections( U, AverageFace, ImagesToProject )
%CALCULATEOMEGAS Summary of this function goes here
%   Detailed explanation goes here

    OmegaSpace = zeros( length(U(1,:)), length(ImagesToProject(1,1,:)) );
    % the size times the height of the original images
    % we assume, by the naming of the variable that images are square
    %[rows,cols] = size(AverageFace);
    mbyn       = length( U(:,1) ) ;
    MeanFace   = reshape( AverageFace(:,:), mbyn, 1 );

    for i = 1:length(ImagesToProject(1,1,:))
        % subtract the average image from the input image
        r = reshape( ImagesToProject(:,:,i), mbyn, 1 ) - MeanFace ;
        % compute projection onto face space
        OmegaSpace(:,i) = U' * r;
    end
end
