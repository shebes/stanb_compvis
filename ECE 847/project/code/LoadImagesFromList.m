function [ TRImages, TEImages ] = LoadImagesFromList( ImageDir, ListName )
%LOADFACESNOPCA Summary of this function goes here
%   Detailed explanation goes here


    fileListId   = fopen(ListName,'r');
    numImages    = str2num(fgetl(fileListId));
    rows         = str2num(fgetl(fileListId));
    cols         = str2num(fgetl(fileListId));
    trImgSize    = int16((numImages/3) * 2);
    teImgSize    = numImages - trImgSize;
    TRImages   = zeros( rows, cols, trImgSize ) ;
    TEImages   = zeros( rows, cols, teImgSize ) ;
    i = 1;
    while 1   
        newlineValue = fgetl(fileListId);
        if ~ischar(newlineValue), break, end
        imageName = strcat( ImageDir, '/', newlineValue ) ;
        %disp(imageName);
        if ( i < trImgSize + 1 )
            TRImages(:,:,i)     = imread(imageName);
        else
            index = i - trImgSize;
            TEImages(:,:,index) = imread(imageName);
        end
        i = i + 1;
    end    
    
    fclose(fileListId);


end
