/* 
 * Copyright (c) 2004,2005,2006 Stan Birchfield.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
      Blepo Version 0.5   (8 July 2006)
*/

#ifndef __BLEPO_H__
#define __BLEPO_H__

// This file includes all the top-level header files for the library.
// Someone using the library should only have to include this header file.
// For developers, whenever a new top-level header file is written please 
// be sure to add it here.  By top-level I mean a header file containing 
// classes, structures, functions, etc., intended for a user of the library.

//// ----------------------------- begin Intel IPL include stuff
//#if defined(WIN32)
//#include <afx.h>  // must be included before including "ipl.h", or else will get some strange error in "afxv_w32.h"; error also goes away if <stdafx.h> is included
//#endif //defined(WIN32)
//#include "../../blepo_internal/external/IPLib/ipl.h"
//#include "../../blepo_internal/external/IPLib/iplError.h"  // not needed for most applications, but contains some error codes (like IPL_StsOK) that are occasionally checked
//#if defined(WIN32)
//#include <afxwin.h>
//#endif //defined(WIN32)
//#include "../external/Intel/OpenCV/cv.h"
//#include "../external/Intel/OpenCV/cvaux.h"
//#include "../external/Intel/OpenCV/highgui.h"
//// ----------------------------- end Intel IPL include stuff

// blepo includes
#include "Figure/Figure.h"
#include "Image/Image.h"
#include "Image/ImageAlgorithms.h"
#include "Image/ImageOperations.h"
//#include "Image/ImgIplImage.h"
#include "Utilities/Array.h"
#include "Utilities/Exception.h"
#include "Utilities/Math.h"
#include "Utilities/PointSizeRect.h"
#include "Utilities/Utilities.h"
#include "Matrix/Matrix.h"
#include "Matrix/MatrixOperations.h"
#include "Matrix/LinearAlgebra.h"
#include "Capture/CaptureDt3120.h"
#include "Capture/CaptureDirectShow.h"
#include "Capture/CaptureIEEE1394.h"
#include "Capture/CaptureAvi.h"
//#include "Capture/CaptureMAudio.h"
#include "Capture/CanonVcc4.h"
#include "Capture/AVIFrameReader.h"
#endif //__BLEPO_H__
