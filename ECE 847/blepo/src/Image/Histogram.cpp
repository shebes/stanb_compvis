/* 
 * Copyright (c) 2005.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "Image.h"
#include <vector>

// -------------------- all includes must go before these lines ------------------
#if defined(DEBUG) && defined(WIN32) && !defined(NO_MFC)
#include <afxwin.h>
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
// -------------------- all code must go after these lines -----------------------

using namespace std;

namespace blepo
{

void HistogramGray(const ImgGray& img,const int bin, vector<int>* out)
{
	
	(*out).resize(bin);  
	const int img_height=img.Height();
	const int img_width=img.Width();
	int temp;
	int bin_width;
	bin_width=(int)256/bin;
	
	for(int i=0;i<img_width;i++)
	{
		for(int j=0;j<img_height;j++)
		{
			temp=(int)img(i,j)/bin_width;
			if((img(i,j)%bin_width)!=0 && img(i,j)>bin_width) 
			{
				temp++;
			}
			if(temp>bin-1) 
			{
				temp=bin-1;
			}
			(*out).at(temp)=(*out).at(temp)+1;
			
		}
	}
		
}


			
void HistogramBinary(const ImgGray& img,int* white, int* black)
{
	const int img_height=img.Height();
	const int img_width=img.Width();
	(*white)=0;(*black)=0;
	for(int i=0;i<img_width;i++)
	{
		for(int j=0;j<img_height;j++)
		{
	
			if(img(i,j)==255)
			{
				(*white)++;
			}
			else if (img(i,j)==0)
			{
				(*black)++;
			}
		}
	}
		
}
			


void ConservativeSmoothing(const ImgGray& img,  const int win_width,const int win_height, ImgGray* out)
{
	out->Reset(img.Width(), img.Height());  
	const int img_height=img.Height();
	const int img_width=img.Width();
		
	int x_len,y_len;
	int min,max;
	int i,j;
	for (i=0;i<img_width;i++)
	{
		for (j=0;j<img_height;j++)
		{
			*(out->Begin(i,j)) = *(img.Begin(i,j));
			
		}
	}
	
	
	for(i=0;i<img_width-1;i++)
	{
		for(j=0;j<img_height-1;j++)
		{
		min=999;
		max=0;
		for(int x=-win_width/2;x<=win_width/2;x++)	
		{
			for(int y=-win_height/2;y<=win_height/2;y++)
				{
					
					x_len=i+x;y_len=j+y;
					if((x_len)<0)
					{
						x_len=0;
					}
					if((y_len)<0)
					{
						y_len=0;
					}
					if(x_len>img_width-1)
					{
						x_len=img_width-2;
					}
					if(y_len>img_height-1)
					{
						y_len=img_height-2;
					}
					//Find the minimum and the maximum value in the window
					if(img(x_len,y_len)<min && x_len!=i && y_len!=j)
					{
						min=img(x_len,y_len);
					}
					if(img(x_len,y_len)>max && x_len!=i && y_len!=j)
					{
						max=img(x_len,y_len);
					}

					
					}
				     if(img(i,j)<min)//If the centre pixel is less than minimum in the window,set it to min.
					 {
						 *(out->Begin(i,j))=min;
					 }
					if(img(i,j)>max)//If the centre pixel is greater than maximum in the window, set it to max.
					{
						*(out->Begin(i,j))=max;
					}
		}
		}
	}
}

void ConservativeSmoothing(const ImgFloat& img,  const int win_width,const int win_height, ImgFloat* out)
{
	out->Reset(img.Width(), img.Height());  
	const int img_height=img.Height();
	const int img_width=img.Width();
		
	int x_len,y_len;
	int min,max;
	int i,j;
	for (i=0;i<img_width;i++)
	{
		for (j=0;j<img_height;j++)
		{
			*(out->Begin(i,j)) = *(img.Begin(i,j));
			
		}
	}
	
	
	for(i=0;i<img_width-1;i++)
	{
		for(j=0;j<img_height-1;j++)
		{
		min=999;
		max=0;
		for(int x=-win_width/2;x<=win_width/2;x++)	
		{
			for(int y=-win_height/2;y<=win_height/2;y++)
				{
					
					x_len=i+x;y_len=j+y;
					if((x_len)<0)
					{
						x_len=0;
					}
					if((y_len)<0)
					{
						y_len=0;
					}
					if(x_len>img_width-1)
					{
						x_len=img_width-2;
					}
					if(y_len>img_height-1)
					{
						y_len=img_height-2;
					}
					//Find the minimum and the maximum value in the window
					if(img(x_len,y_len)<min && x_len!=i && y_len!=j)
					{
						min=(int)img(x_len,y_len);
					}
					if(img(x_len,y_len)>max && x_len!=i && y_len!=j)
					{
						max=(int)img(x_len,y_len);
					}

					
					}
				     if(img(i,j)<min)//If the centre pixel is less than minimum in the window,set it to min.
					 {
						 *(out->Begin(i,j))=(float)min;
					 }
					if(img(i,j)>max)//If the centre pixel is greater than maximum in the window, set it to max.
					{
						*(out->Begin(i,j))=(float)max;
					}
		}
		}
	}
}

void ConservativeSmoothing(const ImgInt& img,  const int win_width,const int win_height, ImgInt* out)
{
	out->Reset(img.Width(), img.Height());  
	const int img_height=img.Height();
	const int img_width=img.Width();
		
	int x_len,y_len;
	int min,max;
	int i,j;
	for (i=0;i<img_width;i++)
	{
		for (j=0;j<img_height;j++)
		{
			*(out->Begin(i,j)) = *(img.Begin(i,j));
			
		}
	}
	
	
	for(i=0;i<img_width-1;i++)
	{
		for(j=0;j<img_height-1;j++)
		{
		min=999;
		max=0;
		for(int x=-win_width/2;x<=win_width/2;x++)	
		{
			for(int y=-win_height/2;y<=win_height/2;y++)
				{
					
					x_len=i+x;y_len=j+y;
					if((x_len)<0)
					{
						x_len=0;
					}
					if((y_len)<0)
					{
						y_len=0;
					}
					if(x_len>img_width-1)
					{
						x_len=img_width-2;
					}
					if(y_len>img_height-1)
					{
						y_len=img_height-2;
					}
					//Find the minimum and the maximum valu in the window
					if(img(x_len,y_len)<min && x_len!=i && y_len!=j)
					{
						min=img(x_len,y_len);
					}
					if(img(x_len,y_len)>max && x_len!=i && y_len!=j)
					{
						max=img(x_len,y_len);
					}

					
					}
				     if(img(i,j)<min)//If the centre pixel is less than minimum in the window,set it to min.
					 {
						 *(out->Begin(i,j))=min;
					 }
					if(img(i,j)>max)//If the centre pixel is greater than maximum in the window, set it to max.
					{
						*(out->Begin(i,j))=max;
					}
		}
		}
	}
}

};  // end namespace blepo

