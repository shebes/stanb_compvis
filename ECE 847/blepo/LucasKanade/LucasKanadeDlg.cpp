// LucasKanadeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LucasKanade.h"
#include "LucasKanadeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// uncomment exactly one of these
#define G_DIR "..\\..\\"   // normal

// ================> begin local functions (available only to this translation unit)
namespace
{

	CString iGetExecutableDirectory()
	{
		const char* help_path = AfxGetApp()->m_pszHelpFilePath;
		char* s = const_cast<char*>( strrchr(help_path, '\\') );
		*(s+1) = '\0';
		return CString(help_path);
	}

	CString iGetImagesDir()
	{
		return iGetExecutableDirectory() + G_DIR;
	}

};
// ================< end local functions

using namespace std;
using namespace blepo;

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeDlg dialog

CLucasKanadeDlg::CLucasKanadeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLucasKanadeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLucasKanadeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLucasKanadeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLucasKanadeDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CLucasKanadeDlg, CDialog)
	//{{AFX_MSG_MAP(CLucasKanadeDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_FLOODFILL4, OnFloodfill4)
	ON_BN_CLICKED(IDC_FLOODFILL8, OnFloodfill8)
	ON_BN_CLICKED(IDC_OUTLINE_FOREGROUND, OnOutlineForeground)
	ON_BN_CLICKED(IDC_CANNY, OnCanny)
	ON_BN_CLICKED(IDC_TEMPATE_MATCHING, OnTempateMatching)
	ON_BN_CLICKED(IDC_WATERSHED_SEGMENTATION, OnWatershedSegmentation)
	ON_BN_CLICKED(IDC_LK_OPEN_DLG, OnLkOpenDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeDlg message handlers

BOOL CLucasKanadeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// define the m_cFileDialogFilter	
	m_cFileDialogFilter = "All image files|*.pgm;*ppm;*.bmp;*.jpg;*.jpeg|PGM/PPM files (*.pgm)|*.pgm;*.ppm|BMP files (*.bmp)|*.bmp|JPEG files (*.jpg,.jpeg)|*.jpg;*.jpeg|All files (*.*)|*.*||";  // filter
	
	CString defaultSigmaValue(    "1.0" );
	CString motionSigmaValue(     "0.1" );
	CString defaultThresholdRatio("0.25");
	CString defaultSmallestBorder("7"   );
	CString defaultDisparityMin(  "0"   );
	CString defaultDisparityMax(  "16"  );
	CString defaultHalfWidth(     "2"   );
	CString defaultWindowSize(    "5"   );
	
	SetDlgItemText( IDC_EDIT_THRESHOLD,         defaultThresholdRatio );
	SetDlgItemText( IDC_EDIT_BORDER_PIXEL_SIZE, defaultSmallestBorder );
	SetDlgItemText( IDC_EDIT_WATERSHED_SIGMA,   defaultSigmaValue     );
	SetDlgItemText( IDC_EDIT_CANNY_SIGMA,       defaultSigmaValue     );
	SetDlgItemText( IDC_EDIT_LK_SIGMA,          motionSigmaValue      );

	SetDlgItemText( IDC_BOX_DISPMIN,            defaultDisparityMin   );
	SetDlgItemText( IDC_BOX_DISPMAX,            defaultDisparityMax   );
	SetDlgItemText( IDC_BOX_HALFWIDTH,          defaultHalfWidth      );
	SetDlgItemText( IDC_EDIT_WINDOW_SIZE,       defaultWindowSize     );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLucasKanadeDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLucasKanadeDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CLucasKanadeDlg::OnFloodfill4() 
{
	try
	{
		ImageProcessing imageProcessor;
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"quantized.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,  // filter
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			Load(fname, &img);
			Figure fig("Selction loaded");
			fig.Draw(img);
			Point seedPoint = fig.GrabMouseClick();
			ImgBgr filledImg ;
			filledImg = imageProcessor.Floodfill4Bgr(seedPoint, img);
			Figure figAgain("Flood Filled Image");
			figAgain.Draw(filledImg);
		}
	} catch (const Exception & exp)
	{
		exp.Display();
	}
}

void CLucasKanadeDlg::OnFloodfill8() 
{
	try
	{
		ImageProcessing imageProcessor;
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"quantized.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,  // filter
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			Load(fname, &img);
			Figure fig("Selction loaded");
			fig.Draw(img);
			Point seedPoint = fig.GrabMouseClick();
			ImgBgr filledImg ;
			filledImg = imageProcessor.Floodfill8Bgr(seedPoint, img);
			Figure figAgain("Flood Filled Image");
			figAgain.Draw(filledImg);
		}
	} catch (const Exception & exp)
	{
		exp.Display();
	}
}

void CLucasKanadeDlg::OnOutlineForeground() 
{
	try 
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"fruit1.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			ImageProcessing imageProcessor;
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			ImgBinary mergedImg;
			map< int, eigeninfo > ei;
			map< int, vector<Point> > allPoints;
			map< int, vector<double> > momentMap;
			
			Load(fname, &img);
			Figure fig(dlg.GetFileTitle() + " loaded");
			fig.Draw(img);

			// use high threshold on image
			ImgBgr highThresholdedImg = imageProcessor.CreateBinaryImgUsingThreshold(img, 0.85);
			// use low threshold on image
			ImgBgr lowThresholdImg = imageProcessor.CreateBinaryImgUsingThreshold(img, 0.68);

			// merge and display the two thresholded images
			ImageProcessing::MergeThresholdedImages(&highThresholdedImg, &lowThresholdImg, &mergedImg, &allPoints, &momentMap);
			Figure figMerged(dlg.GetFileTitle() + " binary merged image");
			figMerged.Draw(mergedImg);

			ImageProcessing::CalculateEigenValues(&ei, momentMap, allPoints);
			ImageProcessing::DrawBorders( &img, allPoints, ei );
			
			CString str;
			map<int, eigeninfo>::iterator it;
			for ( it = ei.begin() ; it != ei.end() ; it++ )
			{
				str.Format("Properties of region:\r\n"
					"  area (number of pixels):  %10.0f\r\n"
					"  centroid:  (%5.1f, %5.1f)\r\n"
					"  compactness:  [%1.5f]\r\n"
					"  direction (clockwise from horizontal):  %5.5f radians\r\n"
					"  eccentricity:  %5.5f\r\n"
					"  moments:\r\n"
					"    m00:  %10.3f\r\n"
					"    m10:  %10.3f\r\n"
					"    m01:  %10.3f\r\n"
					"    m11:  %10.3f\r\n"
					"    m20:  %10.3f\r\n"
					"    m02:  %10.3f\r\n"
					"  centralized moments:\r\n"
					"    mu10:  %10.3f\r\n"
					"    mu01:  %10.3f\r\n"
					"    mu11:  %10.3f\r\n"
					"    mu20:  %10.3f\r\n"
					"    mu02:  %10.3f\r\n",
					it->second.m00, 
					it->second.xc, it->second.yc,
					it->second.compactness,
					it->second.direction,
					it->second.eccentricity,
					it->second.m00, it->second.m10, it->second.m01, it->second.m11, it->second.m20, it->second.m02,
					it->second.mu10, it->second.mu01, it->second.mu11, it->second.mu20, it->second.mu02);
			  AfxMessageBox(str, MB_ICONINFORMATION);
			}

			// do this for test, created for Local Enthropy Thresholding
			//ImageProcessing::CalculateCoOccurrenceMatrix(img);
		}

	} catch (const Exception& e)
	{
		// image failed to load, so notify user
		e.Display();
	}
}

void CLucasKanadeDlg::OnCanny() 
{
	try
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"cat.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		
		if (dlg.DoModal() == IDOK)
		{
			ImageProcessing imageProcessor;
			// Get the path name to add to the figure displays
			CString fname      = dlg.GetPathName();
			// Get the file title to add to the figure display windows
			CString fTitle     = dlg.GetFileTitle();

			// Get the sigma value from the text box and calculate the rest
			// of the gaussian information.			
			GaussianInfo gi;
			CString sigmaValue;
			GetDlgItemText( IDC_EDIT_CANNY_SIGMA, sigmaValue);
			ImageProcessing::CalculateGaussianInfo( &gi, atof(sigmaValue) );

			ImgBgr img;
			ImgInt chamImg;
			ImgBinary elImg;
			ImgFloat gx, gy, gmag, gdir, nmsImg;

			Load(fname, &img);
			// create title for original image
			Figure fig( fTitle + " loaded" );
			fig.Draw(img);

			imageProcessor.MakeGradientImageYXPrime(&gi, img, &gx);
			imageProcessor.MakeGradientImageXYPrime(&gi, img, &gy);
			imageProcessor.CalculateGradientMagnitude( gx, gy, &gmag);
			imageProcessor.CalculateGradientDirection( gx, gy, &gdir);
			imageProcessor.NonMaximalSuppression( gmag, gdir, &nmsImg );
			imageProcessor.HysteresisEdgeLinking( nmsImg, &elImg );

			// create title and display the image gradient Gx
			Figure figGradientX( fTitle + " Convolved with Gaussian of Y and Derivative of Gaussian of X" );
			figGradientX.Draw(gx);			
			// create title and display the image gradient Gy
			Figure figGradientY( fTitle + " Convolved with Gaussian of X and Derivative of Gaussian of Y");
			figGradientY.Draw(gy);
			// create title for the gradient magnitude image
			Figure figGradientMag( fTitle + " Gradient magnitude");
			figGradientMag.Draw(gmag);
			// create title for the gradient direction image
			Figure figGradientDir( fTitle + " Gradient direction");
			figGradientDir.Draw(gdir);
			// create title for the non maximal suppression
			Figure figNonMaxSuppression( fTitle + " Non-maximally suppressed");
			figNonMaxSuppression.Draw(nmsImg);
			// create title for the hysteresis edge linking
			Figure figHysteresis( fTitle + " hysteresis edge linked");
			figHysteresis.Draw(elImg);
			
			// Calculate the chamfer image using manhattan distance and display the result
			//imageProcessor.Chamfer( elImg, &chamImg);
			//Figure figChamfer( fTitle + " chamfered up" );
			//figChamfer.Draw(chamImg);
		}
	} catch (const Exception& exp)
	{
		// image failed to load, so notify user
		exp.Display();
	}	
}

void CLucasKanadeDlg::OnTempateMatching() 
{
	try
	{
		CString message;
		message.Format( "How this works:\r\n"
			"You will choose an image from the hard drive upon which you want to match a template.\r\n"
			"Whatever the image name, this application will assume the name of the template is\r\n"
			"imagename_template.extension\r\n"
			"This application will also use the Sigma from the GUI to do edge detection.");
		AfxMessageBox(message, MB_ICONINFORMATION);

		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"cherrypepsi.jpg",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			ImageProcessing imageProcessor;
			// Get the path name to add to the figure displays
			CString fname      = dlg.GetPathName();
			//CString folderPath = dlg.GetFolderPath();
			CString fExtension = dlg.GetFileExt();
			CString fTitle     = dlg.GetFileTitle();

			CString fTemplateName = dir + "\\" + fTitle + "_template." + fExtension;			
			AfxMessageBox(fTemplateName, MB_ICONINFORMATION);

			// Get the sigma value from the text box and calculate the rest
			// of the gaussian information.			
			GaussianInfo gi;
			CString sigmaValue;
			GetDlgItemText( IDC_EDIT_CANNY_SIGMA, sigmaValue);
			ImageProcessing::CalculateGaussianInfo( &gi, atof(sigmaValue) );

			// regular image variables
			ImgBgr img;
			ImgInt chamImg, probabilityMatrix;
			ImgBinary elImg;
			ImgFloat gx, gy, gmag, gdir, nmsImg;

			// template image variables
			ImgBgr imgT;
			ImgBinary elImgT, emImg;
			ImgFloat gxT, gyT, gmagT, gdirT, nmsImgT;

			// Loads normal Image
			Load(fname, &img);
			// create title for original image
			Figure fig( fTitle + " loaded" );
			fig.Draw(img);
			
			imageProcessor.MakeGradientImageYXPrime(   &gi,   img,     &gx );
			imageProcessor.MakeGradientImageXYPrime(   &gi,   img,     &gy );
			imageProcessor.CalculateGradientMagnitude( gx,     gy,   &gmag );
			imageProcessor.CalculateGradientDirection( gx,     gy,   &gdir );
			imageProcessor.NonMaximalSuppression(      gmag, gdir, &nmsImg );
			imageProcessor.HysteresisEdgeLinking( nmsImg, &elImg );
			// create title for the hysteresis edge linking
			Figure figHysteresis(dlg.GetFileTitle() + " hysteresis edge linked");
			figHysteresis.Draw(elImg);
			
			imageProcessor.Chamfer( elImg, &chamImg);
			Figure figChamfer( fTitle + " chamfered up" );
			figChamfer.Draw(chamImg);

			// Loads template image
			Load(fTemplateName, &imgT);
			// create title for original image
			Figure figTemplate( fTitle + " template loaded" );
			figTemplate.Draw(imgT);
			
			// Do Canny Edge Detection on the template image
			imageProcessor.MakeGradientImageYXPrime(   &gi,    imgT,     &gxT );
			imageProcessor.MakeGradientImageXYPrime(   &gi,    imgT,     &gyT );
			imageProcessor.CalculateGradientMagnitude( gxT,     gyT,   &gmagT );
			imageProcessor.CalculateGradientDirection( gxT,     gyT,   &gdirT );
			imageProcessor.NonMaximalSuppression(      gmagT, gdirT, &nmsImgT );
			imageProcessor.HysteresisEdgeLinking( nmsImgT, &elImgT );
			// create title for the hysteresis edge linking
			Figure figHysteresisT(dlg.GetFileTitle() + " template hysteresis edge linked");
			figHysteresisT.Draw(elImgT);		

			// do some template matching
			imageProcessor.MatchTemplate(chamImg, elImgT, &probabilityMatrix);
			Figure figPM( fTitle + " probability matrix with template" );
			figPM.Draw(probabilityMatrix);

			imageProcessor.DrawRectangle( &img, imgT, probabilityMatrix );
			Figure figBorder("Rectangle");
			figBorder.Draw(img);

		}
	} catch (const Exception& exp)
	{
		// image failed to load, so notify user
		exp.Display();
	}		
}

void CLucasKanadeDlg::OnWatershedSegmentation() 
{
	try
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"holes.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			ImageProcessing imageProcessor;
			// Get the path name to add to the figure displays
			CString fname      = dlg.GetPathName();
			//CString folderPath = dlg.GetFolderPath();
			CString fExtension = dlg.GetFileExt();
			CString fTitle     = dlg.GetFileTitle();

			// Get the sigma value from the text box
			CString sigmaValue;
			GetDlgItemText( IDC_EDIT_WATERSHED_SIGMA, sigmaValue);

			// Get the smallest border value
			CString smallestBorder;
			GetDlgItemText( IDC_EDIT_BORDER_PIXEL_SIZE, smallestBorder);

			// Get the threshold ratio
			CString thresholdRatio;
			GetDlgItemText( IDC_EDIT_THRESHOLD, thresholdRatio);

			// regular image variable
			ImgBgr img;
			// Loads normal Image
			Load(fname, &img);
			
			// create title for original image
			Figure fig( fTitle + " loaded" );
			fig.Draw(img);

			// create Marker-based watershed segmenter object and perform segmentation operations
			WatershedSegmenter segmenter( img, fTitle, atof(sigmaValue) );
			segmenter.SetThresholdRatio( atof(thresholdRatio) );
			segmenter.SetSmallestBorder( atoi(smallestBorder) );
			segmenter.SegmentImage(1);
			segmenter.SegmentImage(2);

			//imageProcessor.ToFileImgInt( &segmenter.getLabelsImage(), "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\labelsimage.out" ) ;

			// Display intermediate and final images
			Figure figThreshold( fTitle + " " + thresholdRatio + " thresholded image" );
			figThreshold.Draw( segmenter.getThresholdedImage() );

			Figure figChamfer( fTitle + " chamfer distance" );
			figChamfer.Draw( segmenter.getChamferImage() );
			
			Figure figGM( fTitle + " gradient magnitude");
			figGM.Draw( segmenter.getGradientMagnitude() ) ;

			Figure figEdges( fTitle + " canny edges" );
			figEdges.Draw( segmenter.getEdgeImage() );

			Figure figMarkers( fTitle + " markers" );
			figMarkers.Draw( segmenter.getMarkerImage() );

			Figure figResult( fTitle + " segments outlined with smallest border of " + smallestBorder + " pixels" );
			figResult.Draw( segmenter.getResultImage() ) ;
		}
	} catch (const Exception& exp)
	{
		// image failed to load, so notify user
		exp.Display();
	}		
}

void CLucasKanadeDlg::OnLkOpenDlg() 
{
	
	try
	{
		ImageProcessing imageProcessor;
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"img030.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,  // filter
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			CString fExtension = dlg.GetFileExt();
			CString fTitle     = dlg.GetFileTitle();
			CString fDir       = fname;
			fDir.Replace( fTitle + "." + fExtension, "\\*" );

			
			CFileDialog dlgAgain(TRUE, // open file dialog
				NULL, // default extension
				"img058.pgm",  // default filename
				OFN_HIDEREADONLY,  // flags
				m_cFileDialogFilter,  // filter
				NULL);
			CString dir = iGetImagesDir() + "images";
			dlgAgain.m_ofn.lpstrInitialDir = (const char*) dir;
			dlgAgain.m_ofn.lpstrTitle = "Load image";
			if (dlgAgain.DoModal() == IDOK)
			{
				CString fnameTwo  = dlg.GetPathName();
				CString fExtTwo   = dlgAgain.GetFileExt();
				CString fTitleTwo = dlgAgain.GetFileTitle();
				

				// Get the sigma value from the text box
				CString sigmaValue, windowSize;
				GetDlgItemText( IDC_EDIT_LK_SIGMA, sigmaValue    );
				GetDlgItemText( IDC_EDIT_WINDOW_SIZE, windowSize );

				vector<CString> fileList;
				
				// regular image variable
				ImgBgr img, imgLast;
				// Loads normal Image
				Load(fname, &img);
				Load(fnameTwo, &imgLast);

				CString fNumber     = fTitle;
				CString fNumberLast = fTitleTwo;
				fNumber.Replace(     "img", "" );
				fNumberLast.Replace( "img", "" );

				imageProcessor.CreateFileList( &fileList, fname, fExtension, fTitle, atoi(fNumber), atoi(fNumberLast) );				
				//imageProcessor.ToFileCStringVector( fileList, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\filelisting.out" );
				
				LucasAndKanade motionMagic( atof(sigmaValue), atoi(windowSize) );

				Figure figMovie( "motion magic" );
				int i = 0 ;
				vector<CString>::iterator it = fileList.begin();
				while ( it != fileList.end() )
				{

					if ( it == fileList.begin() )
					{
						motionMagic.InitImage( *it );
					} else {
						motionMagic.NextImage( *it );
					}

					figMovie.Draw( motionMagic.getBgrImage() );

					it++;
					i++ ;

					// we are only testing n-1 images after the original
					/* /
					int n = fileList.size()-2;
					if ( i > n ) 
					{
						break;
					}
					/ */
				}
			}
		}
	} catch (const Exception & exp)
	{
		exp.Display();
	}
}
