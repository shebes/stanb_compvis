//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by LucasKanade.rc
//
#define IDD_LUCASKANADE_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_IMGPROC_GROUPBOX            1000
#define IDC_FLOODFILL4                  1001
#define IDC_FLOODFILL8                  1002
#define IDC_OUTLINE_FOREGROUND          1003
#define IDC_CANNY                       1004
#define IDC_WATERSHED_GROUP_BOX         1005
#define IDC_WATERSHED_SEGMENTATION      1006
#define IDC_EDIT_THRESHOLD              1007
#define IDC_LBL_THRESHOLD               1008
#define IDC_EDIT_BORDER_PIXEL_SIZE      1009
#define IDC_BORDER_MAX                  1010
#define IDC_EDIT_WATERSHED_SIGMA        1011
#define IDC_EDIT_CANNY_SIGMA            1012
#define IDC_CANNY_SIGMA                 1013
#define IDC_TEMPATE_MATCHING            1014
#define IDC_CANNY_MATCHING_GROUP        1015
#define IDC_STEREO_GROUP                1016
#define IDC_PENCIL_EPIPOLAR             1017
#define IDC_BTN_STEREO_MATCHING         1018
#define IDC_LBL_DISPARY_MIN             1019
#define IDC_BOX_HALFWIDTH               1020
#define IDC_LBL_DISPMAX                 1021
#define IDC_BOX_DISPMAX                 1022
#define IDC_BOX_DISPMIN                 1023
#define IDC_LK_GRP                      1024
#define IDC_LK_OPEN_DLG                 1025
#define IDC_LK_SIGMA                    1026
#define IDC_EDIT_LK_SIGMA               1027
#define IDC_EDIT_WINDOW_SIZE            1028
#define IDC_LBL_WINDOW_SIZE             1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
