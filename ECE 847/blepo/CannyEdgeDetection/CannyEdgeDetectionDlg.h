// CannyEdgeDetectionDlg.h : header file
//

#if !defined(AFX_CANNYEDGEDETECTIONDLG_H__01496DC4_E74E_4BDC_81CC_8549AF71735B__INCLUDED_)
#define AFX_CANNYEDGEDETECTIONDLG_H__01496DC4_E74E_4BDC_81CC_8549AF71735B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionDlg dialog

class CCannyEdgeDetectionDlg : public CDialog
{
// Construction
public:
	CCannyEdgeDetectionDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CCannyEdgeDetectionDlg)
	enum { IDD = IDD_CANNYEDGEDETECTION_DIALOG };
	double	m_sigmaValue;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCannyEdgeDetectionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	char * m_cFileDialogFilter;

	// Generated message map functions
	//{{AFX_MSG(CCannyEdgeDetectionDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnClickFloodfill();
	afx_msg void OnClickClassifyForeground();
	afx_msg void OnBtnClickCannyEdgeDetect();
	afx_msg void OnClickMatchWithTemplate();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CANNYEDGEDETECTIONDLG_H__01496DC4_E74E_4BDC_81CC_8549AF71735B__INCLUDED_)
