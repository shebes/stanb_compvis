; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCannyEdgeDetectionDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CannyEdgeDetection.h"

ClassCount=3
Class1=CCannyEdgeDetectionApp
Class2=CCannyEdgeDetectionDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_CANNYEDGEDETECTION_DIALOG

[CLS:CCannyEdgeDetectionApp]
Type=0
HeaderFile=CannyEdgeDetection.h
ImplementationFile=CannyEdgeDetection.cpp
Filter=N

[CLS:CCannyEdgeDetectionDlg]
Type=0
HeaderFile=CannyEdgeDetectionDlg.h
ImplementationFile=CannyEdgeDetectionDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=CCannyEdgeDetectionDlg

[CLS:CAboutDlg]
Type=0
HeaderFile=CannyEdgeDetectionDlg.h
ImplementationFile=CannyEdgeDetectionDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_CANNYEDGEDETECTION_DIALOG]
Type=1
Class=CCannyEdgeDetectionDlg
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDC_FLOODFILL,button,1342242816
Control3=IDC_FOREGROUNDCLASSIFICATION,button,1342242816
Control4=IDC_CANNYEDGEDETECTION,button,1342242816
Control5=IDC_SIGMASAMPLES_INPUT,edit,1350631554
Control6=IDC_THETA,static,1342308352
Control7=IDC_TEMPLATEMATCHING,button,1342242816

