// CannyEdgeDetection.h : main header file for the CANNYEDGEDETECTION application
//

#if !defined(AFX_CANNYEDGEDETECTION_H__F24A7C9A_4284_40D6_8B4B_9E10B96DD209__INCLUDED_)
#define AFX_CANNYEDGEDETECTION_H__F24A7C9A_4284_40D6_8B4B_9E10B96DD209__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "ImageProcessing.h"

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionApp:
// See CannyEdgeDetection.cpp for the implementation of this class
//

class CCannyEdgeDetectionApp : public CWinApp
{
public:
	CCannyEdgeDetectionApp();
	static void CalculateGaussianInfo(GaussianInfo * gi, double sigma);
	static void MakeGradientImageYXPrime(GaussianInfo * gi, blepo::ImgBgr & img, blepo::ImgFloat * out);
	static void MakeGradientImageXYPrime(GaussianInfo * gi, blepo::ImgBgr & img, blepo::ImgFloat * out);
	static void CalculateGradientMagnitude(blepo::ImgFloat & imgGx, blepo::ImgFloat & imgGy, blepo::ImgFloat * out);
	static void CalculateGradientDirection(blepo::ImgFloat & imgGx, blepo::ImgFloat & imgGy, blepo::ImgFloat * out);
	static void NonMaximalSuppression(blepo::ImgFloat &imgMag, blepo::ImgFloat &imgDir, blepo::ImgFloat * out);	
	static int  ValueIsInRegion(double value);
	static void HysteresisEdgeLinking(blepo::ImgFloat & imgSup, blepo::ImgBinary * out);
	static void MatchTemplate(blepo::ImgInt & img, blepo::ImgBinary & imgT, blepo::ImgInt * pMatrix);
	static void DrawRectangle( blepo::ImgBgr * img, blepo::ImgBgr & imgT, blepo::ImgInt & probabilityMatrix );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCannyEdgeDetectionApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCannyEdgeDetectionApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CANNYEDGEDETECTION_H__F24A7C9A_4284_40D6_8B4B_9E10B96DD209__INCLUDED_)
