// CannyEdgeDetection.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "CannyEdgeDetection.h"
#include "CannyEdgeDetectionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;
using namespace blepo;

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionApp

BEGIN_MESSAGE_MAP(CCannyEdgeDetectionApp, CWinApp)
	//{{AFX_MSG_MAP(CCannyEdgeDetectionApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionApp construction

CCannyEdgeDetectionApp::CCannyEdgeDetectionApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CCannyEdgeDetectionApp object

CCannyEdgeDetectionApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionApp initialization

BOOL CCannyEdgeDetectionApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CCannyEdgeDetectionDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates the gaussian kernel from the input sigma value.
 * This method also creates the derivative gaussian kernel and other useful
 * values for gaussian calculations.
 * 
 * Parameters:
 * [+] gi    - the gaussian information including its kernels and derivatives
 * [+] sigma - the standard deviation for a gaussian kernel
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::CalculateGaussianInfo(GaussianInfo * gi, double sigma)
{	
	try
	{
		FILE *fout = fopen( "C:\\Users\\shebes\\hw\\ece847\\blepo\\CannyEdgeDetection\\gaussianvalues.txt", "w" );
		CString str;
		int i; // counter
		gi->sigma = sigma;
		gi->variance = pow(gi->sigma, 2);

		if ( ceil(gi->variance + 0.5) < ceil(gi->variance) )
		{
			gi->k = (int)floor(2 * gi->variance);
		} else
		{
			gi->k = (int)ceil(2 * gi->variance);
		}
		gi->mu = (int)ceil(2 * gi->sigma);
		gi->w  = 2 * gi->mu + 1;
		gi->widthOverSigma = (double)gi->w / sigma;

		gi->sum  = 0.0;
		gi->dsum = 0.0;
		gi->g.resize(gi->w, 0.0);
		gi->dg.resize(gi->w, 0.0);

		for ( i = 0 ; i < gi->w ; i++ )
		{
			double x  = i - gi->mu;
			double varianceTimesTwo = 2 * gi->variance;
			double xSquared = pow( x, 2 );

			gi->g.at(i)  = exp( - xSquared / varianceTimesTwo );
			gi->dg.at(i) = (-x * gi->g.at(i)) ;
			gi->sum  += gi->g.at(i);
			gi->dsum += gi->dg.at(i) * i ;

			fprintf(fout,"Gaussian at %d: %f\tDerivative: %f\n",i,gi->g.at(i), gi->dg.at(i));
		}

		fprintf(fout, "Normalized Gaussian\n");
		for ( i = 0 ; i < gi->w ; i++ )
		{
			gi->g.at(i)  /= gi->sum ;
			gi->dg.at(i) /= gi->dsum ;
			fprintf(fout,"Gaussian at %d: %f\tDerivative: %f\n",i,gi->g.at(i), gi->dg.at(i) );
		}

		str.Format("\n\n"
			"Sigma value: %f\r\n"
			"Variance: %f\r\n"
			"Ceiling of variance: %f\r\n"
			"k: %d\r\n"
			"Mu: %d\r\n"
			"Width: %d\r\n"
			"Width over Sigma: %f\r\n"
			"Sum of values in gaussian: %f\r\n"
			"Sum of values in derivative gaussian: %f\r\n", 
			gi->sigma, gi->variance, ceil(gi->variance), gi->k, gi->mu, gi->w, gi->widthOverSigma, gi->sum, gi->dsum);
		//fprintf(fout, "%s", str);
		//AfxMessageBox(str, MB_ICONINFORMATION);

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient image by convolving an image by a 
 * gaussian and its derivative.
 * 
 * Parameters:
 * [+] gi  - the gaussian information including its kernels and derivatives
 * [+] img - the original image
 * [+] out - a float image that represents the original image convolved with 
 *           the vertical gaussian kernel, and then in turn with the 
 *           horizontal kernel derivative
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::MakeGradientImageYXPrime(GaussianInfo * gi, ImgBgr &img, ImgFloat *out)
{
	try
	{
		ImgFloat floatImg, floatImgAgain, finalImg ;
		floatImgAgain.Reset( img.Width(), img.Height() );
		finalImg.Reset( img.Width(), img.Height() );

		Convert(img, &floatImg);

		ImageProcessing::InitializeImgFloat(&floatImgAgain, 0.0);
		ImageProcessing::InitializeImgFloat(&finalImg, 0.0);

		ImageProcessing::ConvolveVertical( *gi, floatImg, &floatImgAgain, false);
		ImageProcessing::ConvolveHorizontal( *gi, floatImgAgain, &finalImg, true);

		*out = finalImg;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient image by convolving an image by a 
 * gaussian and its derivative.
 * 
 * Parameters:
 * [+] gi  - the gaussian information including its kernels and derivatives
 * [+] img - the original image
 * [+] out - a float image that represents the original image convolved with 
 *           the horizontal gaussian kernel, and then in turn with the 
 *           vertical kernel derivative
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::MakeGradientImageXYPrime(GaussianInfo * gi, ImgBgr &img, ImgFloat *out)
{
	try
	{
		ImgFloat floatImg, floatImgAgain, finalImg ;
		floatImgAgain.Reset( img.Width(), img.Height() );
		finalImg.Reset( img.Width(), img.Height() );

		Convert(img, &floatImg);

		ImageProcessing::InitializeImgFloat(&floatImgAgain, 0.0);
		ImageProcessing::InitializeImgFloat(&finalImg, 0.0);

		ImageProcessing::ConvolveHorizontal( *gi, floatImg, &floatImgAgain, false);
		ImageProcessing::ConvolveVertical( *gi, floatImgAgain, &finalImg, true);

		*out = finalImg;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient magnitude image from two gradient 
 * convolved images in the horizontal and vertical directions.
 * 
 * Parameters:
 * [+] imgGx - the gradient image in the x direction (horizontal)
 * [+] imgGy - the gradient image in the y direction (vertical)
 * [+] out   - a float image that represents the merging gradient magnitude
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::CalculateGradientMagnitude(ImgFloat &imgGx, ImgFloat &imgGy, ImgFloat *out)
{
	try
	{
		ImgFloat result;

		result.Reset( imgGx.Width(), imgGx.Height() );
		ImageProcessing::InitializeImgFloat(&result, 0.0);

		int x, y;
		for ( y = 0 ; y < imgGx.Height() ; y++ )
		{
			for ( x = 0 ; x < imgGx.Width() ; x++ )
			{
				double gxSquared = pow( imgGx(x,y), 2 ) ;
				double gySquared = pow( imgGy(x,y), 2 ) ;
				result(x,y) = sqrt( gxSquared + gySquared );
			}
		}

		*out = result;
	} catch(Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient direction image from two gradient 
 * convolved images in the horizontal and vertical directions.
 * 
 * Parameters:
 * [+] imgGx - the gradient image in the x direction (horizontal)
 * [+] imgGy - the gradient image in the y direction (vertical)
 * [+] out   - a float image that represents the merged gradient images used
 *             for creating the gradient magnitude image
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::CalculateGradientDirection(ImgFloat &imgGx, ImgFloat &imgGy, ImgFloat * out)
{
	try
	{
		//FILE *fout = fopen( "C:\\Users\\shebes\\hw\\ece847\\blepo\\CannyEdgeDetection\\directionvalues.out", "w" );
		ImgFloat result;

		result.Reset( imgGx.Width(), imgGx.Height() );
		ImageProcessing::InitializeImgFloat(&result, 0.0);

		int x, y;
		for ( y = 0 ; y < imgGx.Height() ; y++ )
		{
			for ( x = 0 ; x < imgGx.Width() ; x++ )
			{
				double directionValue = atan2( imgGy(x,y), imgGx(x,y));
				result(x,y) = directionValue;
				//if ( x < 10 && y < imgGx.Height())
				//{
				//	fprintf( fout, "[(%3d,%3d) %1d]", x, y, ImageProcessing::valueIsInRegion(directionValue) );
				//}
				//if ( x == 10 ) fprintf(fout, "\r\n");
			}
		}

		*out = result;
		//fclose(fout);
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient direction image from two gradient 
 * convolved images in the horizontal and vertical directions.
 * 
 * Parameters:
 * [+] imgMag - the magnitude image created from the image convolved by the 
 *              X and Y gaussian gradients
 * [+] imgDir - the angle or direction images created from the original image
 *              convolved by the X and Y gaussian gradients
 * [+] out    - a float image that represents the magnitude image with only 
 *              local maxima present
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::NonMaximalSuppression(ImgFloat &imgMag, ImgFloat &imgDir, ImgFloat *out)
{
	try 
	{
		out->Reset( imgMag.Width(), imgMag.Height() );
		ImageProcessing::InitializeImgFloat( out, 0.0 );
		int x, y;
		for ( y = 0 ; y < imgMag.Height() ; y++ )
		{
			for ( x = 0 ; x < imgMag.Width() ; x++ )
			{
				ImgFloat::Pixel neighborA;
				ImgFloat::Pixel neighborB;
				//int region = ValueIsInRegion( atan( abs( imgDir(x,y) ) ) );
				int region = ValueIsInRegion( abs( imgDir(x,y) ) );
				if ( region == 1 )
				{
					if ( x-1 > -1 && x+1 < imgMag.Width() )
					{
						neighborA = imgMag(x-1,y) ;
						neighborB = imgMag(x+1,y) ;
						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else if ( region == 2 ) {
					if ( x-1 > -1 && x+1 < imgMag.Width() && y-1 > -1 && y+1 < imgMag.Height() )
					{
						neighborA = imgMag(x-1,y-1) ;
						neighborB = imgMag(x+1,y+1) ;

						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else if ( region == 3 ) {
					if ( x-1 > -1 && x+1 < imgMag.Width() && y-1 > -1 && y+1 < imgMag.Height() )
					{
						neighborA = imgMag(x,y-1) ;
						neighborB = imgMag(x,y+1) ;

						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else if ( region == 4 ) {
					if ( x-1 > -1 && x+1 < imgMag.Width() && y-1 > -1 && y+1 < imgMag.Height() )
					{
						neighborA = imgMag(x+1,y-1) ;
						neighborB = imgMag(x-1,y+1) ;

						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else
				{
					(*out)(x,y) = imgMag(x,y) ;
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
}

int CCannyEdgeDetectionApp::ValueIsInRegion(double value)
{
	int result = 0 ;
	try
	{
		double pi = 22.0/7.0;
		if ( value < (pi/8) && value >= (-pi/8) )
		{
			result = 1;
		} else if ( value < (3*pi/8) && value >= (pi/8)  )
		{
			result = 2;
		} else if ( value < (5*pi/8) && value >= (3*pi/8)  )
		{
			result = 3;
		} else if ( value < (7*pi/8) && value >= (5*pi/8)  )
		{
			result = 4;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result ;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method uses the data in two disparately thresholded images and 
 * combines them to get an image with the best defined edges.
 * 
 * Parameters:
 * [+] imgSup - the image with local maxima suppressed to reduce noise and 
 *              allow for the identification of better edges
 * [+] out    - a float image that represents the merging of disparately
 *              thresholded images
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::HysteresisEdgeLinking(ImgFloat &imgSup, ImgBinary * out)
{
	try
	{
		FILE *fout = fopen( "C:\\Users\\shebes\\hw\\ece847\\blepo\\CannyEdgeDetection\\sortedmaximalimage.out", "w" );

		int thresholdHi, thresholdLo;
		Point hiThreshold, loThreshold;

		ImgBinary binaryImg, binaryImgLo;
		binaryImgLo.Reset( imgSup.Width(), imgSup.Height() );
		binaryImg.Reset( imgSup.Width(), imgSup.Height() );
		ImgFloat suppressedImg = imgSup ;
		sort( imgSup.Begin(), imgSup.End() );

		thresholdHi = ( imgSup.Width() * imgSup.Height() ) / 6 ;
		thresholdLo = thresholdHi / 5 ;

		fprintf( fout, "Image Width: %d\nImage Height: %d\n", imgSup.Width(), imgSup.Height() );
		fprintf( fout, "High Threshold: %d\nLow Threshold: %d\n\n", thresholdHi, thresholdLo );

		ImgFloat::Pixel lowThreshold = imgSup( imgSup.Width() * imgSup.Height() - thresholdLo );
		ImgFloat::Pixel highThreshold = imgSup( imgSup.Width() * imgSup.Height() - thresholdHi );

		fprintf( fout, "Low threshold pixel value: %f\nHigh threshold pixel value: %f\n\n", lowThreshold, highThreshold );

		// Loop creates the high and low threshold images from the non-maximally suppressed image
		int x, y ;
		for ( y = 0 ; y < imgSup.Height() ; y++ )
		{			
			for ( x = 0 ; x < imgSup.Width() ; x++ )
			{
				if ( suppressedImg(x,y) >= highThreshold )
				{
					binaryImg(x,y) = 1;
				} else
				{
					binaryImg(x,y) = 0;
				}

				if ( suppressedImg(x,y) >= lowThreshold )
				{
					binaryImgLo(x,y) = 1;
				} else 
				{
					binaryImgLo(x,y) = 0;
				}

				if ( x < 5 && y < imgSup.Height())
				{
					fprintf( fout, "[(%3d,%3d) %3.3f, %3.3f]", x, y, imgSup(x,y), suppressedImg(x,y) );
				}
				if ( x == 5 ) fprintf(fout, "\r\n");
			}
		}

		Figure fig("Low Threshold Binary Image");
		fig.Draw(binaryImg);

		Figure figLo("High Threshold Binary Image");
		figLo.Draw(binaryImgLo); 
		
		ImgInt labels, labelsB;
		out->Reset(binaryImg.Width(), binaryImg.Height());
		vector< ConnectedComponentProperties < ImgBinary::Pixel > > ccProps, ccPropsB;

		int nLabels  = ConnectedComponents8(binaryImgLo, &labels, &ccProps);
		int nLabelsB = ConnectedComponents8(binaryImg, &labelsB, &ccPropsB);

		fprintf( fout, "Number of high threshold labels %d\nNumber of low threshold labels %d\n\n", nLabels, nLabelsB );

		vector<int> validLabels = ImageProcessing::MergeImages( labels, labelsB, out ) ;
		
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a probability map which denotes, by its minimum
 * value, the best most likely spot in the image where the template should be
 * centered.
 * 
 * Parameters:
 * [+] img     - an image in which we want to match an object to a template
 * [+] imgT    - the template for an object in another image
 * [+] pMatrix - an integer image containing probability values to say which
 *               point in the image is would be the best spot to place the 
 *               template
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::MatchTemplate(ImgInt & img, ImgBinary & imgT, ImgInt * pMatrix)
{
	try
	{
		int x, y, tHalfWidth, tHalfHeight;
		pMatrix->Reset( img.Width(), img.Height() );
		ImageProcessing::InitializeImgInt(pMatrix, INT_MAX - 1);

		tHalfWidth  = imgT.Width()  / 2;
		tHalfHeight = imgT.Height() / 2;

		for ( y = tHalfHeight ; y < img.Height() - tHalfHeight ; y++ )
		{
			for ( x = tHalfWidth ; x < img.Width() - tHalfWidth ; x++ )
			{
				int xp, yp, i, j ;
				int pValue = 0;
				for( xp = x - tHalfWidth, i = 0 ; xp < x + tHalfWidth ; xp++, i++ )
				{
					for( yp = y - tHalfHeight, j = 0 ; yp < y + tHalfHeight ; yp++, j++ )
					{
						pValue += imgT(i,j) * img(xp,yp);
					}
				}
				(*pMatrix)(x,y) = pValue;
			}
		}
		ImgInt::Pixel minValue = Min(*pMatrix);
		ImgInt::Iterator it = pMatrix->Begin();
		while ( it != pMatrix->End() )
		{
			if ( *it == INT_MAX-1 )
			{
				*it = minValue * 10;
			}
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method draws a rectangle around an object in the original image
 * based on the lowest value in the probability map.
 * 
 * Parameters:
 * [+] img     - an image in which we want to match an object to a template
 *               on which we will draw a rectangle around the matched object
 * [+] imgT    - the template for an object in another image
 * [+] pMatrix - an integer image containing probability values to say which
 *               point in the image is would be the best spot to place the 
 *               template
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void CCannyEdgeDetectionApp::DrawRectangle( ImgBgr * img, ImgBgr &imgT, ImgInt & probabilityMatrix )
{
	try
	{
		FILE * fout = fopen("C:\\Users\\shebes\\hw\\ece847\\blepo\\CannyEdgeDetection\\drawrectangle.out", "w");
		ImgInt::Pixel minValue = Min(probabilityMatrix);
		fprintf( fout, "Min value %d\n", minValue );

		int x, y, xc, yc;
		for ( y = 0 ; y < img->Height() ; y++ )
		{
			for ( x = 0 ; x < img->Width() ; x++ )
			{
				if ( probabilityMatrix(x,y) == minValue ) 
				{
					xc = x;
					yc = y;
					break;
				}
			}
		}
		fprintf( fout, "Min found at (x,y)=(%3d,%3d)\n", xc, yc );

		int tHalfWidth, tHalfHeight;
		
		tHalfWidth  = imgT.Width() / 2;
		tHalfHeight = imgT.Height() / 2;

		Point loLeft( xc - tHalfWidth, yc + tHalfHeight );
		Point loRight( xc + tHalfWidth, yc + tHalfHeight );
		Point hiLeft( xc - tHalfWidth, yc - tHalfHeight );
		Point hiRight( xc + tHalfWidth, yc - tHalfHeight );

		DrawLine( loLeft,   hiLeft, img, Bgr::YELLOW );
		DrawLine( hiLeft,  hiRight, img, Bgr::YELLOW );
		DrawLine( hiRight, loRight, img, Bgr::YELLOW );
		DrawLine( loRight,  loLeft, img, Bgr::YELLOW );

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}
