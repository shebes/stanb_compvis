/*****************************************************************************
 * ImageProcessing.cpp
 * Description: Holds the implementation of image processing methods developed 
 *              for ECE847 homework assignments.
 *
 * Date: Wednesday September 9, 2009 Matthew's 22nd birthday
 *****************************************************************************/
#include "stdafx.h"
#include "ImageProcessing.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of four to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) originImg - the image wherein an area of pixels will be colored
 * Returns: A blepo color image
 *---------------------------------------------------------------------------*/
ImgBgr ImageProcessing::Floodfill(blepo::Point seedPoint, ImgBgr originImg)
{
	ImgBgr result ;
	
	try 
	{
		Bgr oldColor = Bgr( *originImg.Begin(seedPoint.x, seedPoint.y) );
		Bgr fllColor = Bgr(100, 0, 0);	
		Point imgLmt(originImg.Width(), originImg.Height());
		
		result = originImg;
		result(seedPoint.x, seedPoint.y) = fllColor ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			Point neighbor;

			// get north neighbor
			neighbor = Point( location.x, location.y-1 );
			if ( neighbor.y > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get east neighbor
			neighbor = Point( location.x + 1, location.y );
			if (  neighbor.x < imgLmt.x ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get south neighbor
			neighbor = Point( location.x, location.y + 1 );
			if ( neighbor.y < imgLmt.y ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get west neighbor
			neighbor = Point( location.x - 1, location.y );
			if ( neighbor.x > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * This method belongs in a different file, a utilities file.
 *---------------------------------------------------------------------------*/
char *        ImageProcessing::getLocalTime()
{
	time_t theTime;
	time( &theTime );
	tm *t = localtime( &theTime );
	return asctime(t);
}


/*---------------------------------------------------------------------------*
 * Description: It tests whether or not an element is present in a vector.
 *              A function that works in linear time.  
 * 
 * Parameters:
 * [+] element - an integer that you want to test to see whether or not it is
 *               in the passed in vector
 * [+] data - the vector against which you will test for the presence of an 
 *            element
 * Return: A boolean that says whether or not the element is in the passed in
 *         vector.
 *---------------------------------------------------------------------------*/
bool ImageProcessing::IsInVector( int element, vector<int> data )
{
	bool result = false;
	try 
	{
		vector<int>::iterator it;
		for ( it = data.begin() ; it != data.end() ; it++ )
		{
			if ( element == *it )
			{
				result = true;
				break;
			}
		}
	} 
	catch (Exception exp)
	{
		exp.Display();
	}
	return result ;
}


/*---------------------------------------------------------------------------*
 * Method creates a normalized histogram from a gray scale image.
 * 
 * Parameters:
 * (+) img - the image for which we will calculate a normalized histogram.
 * Returns: A vector of type float containing the normalized histogram.
 *---------------------------------------------------------------------------*/
vector<float> ImageProcessing::CreateNormHist(ImgBgr img)
{
	// integer variables
	int i;
	int nPixels = 0 ;
	int hist[HIST_MAX];

	// containers
	vector<float> resultHist(HIST_MAX, 0.0);

	// input/output
	FILE *fout = fopen("ImgHistogram.txt", "w");

	try 
	{
		for ( i = 0 ; i < HIST_MAX ; i++ )
		{
			hist[i] = 0;
		}

		// create initial histogram and count the number of pixels
		ImgBgr::Iterator it ;
		for ( it = img.Begin() ; it != img.End() ; it++ )
		{
			hist[it->ToGray()]++;
			nPixels++;
		}
		
		fprintf( fout, "%s\n", getLocalTime() );
		fprintf( fout, "There are %d pixels in the loaded image.\n", nPixels );
		
		float sum = 0 ;
		// create normalized histogram
		for ( i = 0 ; i < HIST_MAX ; i++ )
		{
			resultHist.at(i) = (float)hist[i] / (float)nPixels;
			sum += resultHist.at(i);
			fprintf( fout, "[%3d] %f\n", i, resultHist.at(i) );
		}
		fprintf( fout, "Average normalized value %f", (sum/HIST_MAX) );
		fclose(fout);

	} catch (Exception exp)
	{
		exp.Display();
	}

	return resultHist;
}



/*---------------------------------------------------------------------------*
 * Method creates a cumulative histogram from a gray scale image.
 * 
 * Parameters:
 * (+) img - the image for which a cumulative histogram will be made
 * Returns: A vector of type float containing the cumulative histogram.
 *---------------------------------------------------------------------------*/
vector<float> ImageProcessing::CreateCumuHist(ImgBgr img)
{
	return CreateCumuHist( CreateNormHist(img) ) ;
}


/*---------------------------------------------------------------------------*
 * Method creates a cumulative histogram from a gray scale image.
 * 
 * Parameters:
 * (+) imgHist - the normalized histogram for an image
 * Returns: A vector of type float containing the cumulative histogram.
 *---------------------------------------------------------------------------*/
vector<float> ImageProcessing::CreateCumuHist(vector<float> imgHist)
{
	// container
	vector<float> resultHist(HIST_MAX, 0.0);

	// input/output
	FILE *fout = fopen("ImgCumulativeHistogram.txt", "w");

	try 
	{
		fprintf( fout, "%s\n", getLocalTime() );
		for ( int idx = 0 ; idx < HIST_MAX ; idx++ )
		{
			float cumVal = 0.0;
			if ( idx == 0 )
			{
				cumVal = 0.0;
			}
			else 
			{
				cumVal = resultHist.at(idx-1);
			}
			resultHist.at(idx) = imgHist.at(idx) + cumVal;
			fprintf( fout, "[%3d] %f\n", idx, resultHist.at(idx) );
		}
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}

	return resultHist;
}



/*----------------------------------------------------------------------------*
 * Take a cumulative histogram of an image and transform the image.
 *
 * Parameters:
 * (*) img - A color image
 * Return: A transformed color image.
 *----------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::TransformImageToBgr(ImgBgr img)
{
	ImgBgr result;

	vector<float> cumuHist = CreateCumuHist(img);
	
	try 
	{
		ImgBgr::Iterator it ;
		for ( it = img.Begin() ; it != img.End() ; it++ )
		{
			it->FromInt( (int)((HIST_MAX-1) * cumuHist.at( it->ToGray() )), Bgr::BLEPO_BGR_GRAY);
		}

		result = img;
	} catch(Exception exp)
	{
		exp.Display();
	}

	return  result;
}


/*----------------------------------------------------------------------------*
 * Take a cumulative histogram of an image and transform the image.
 *
 * Parameters:
 * (*) img - A color image
 * Return: A transformed gray image.
 *----------------------------------------------------------------------------*/
ImgGray       ImageProcessing::TransformImageToGray(ImgBgr img)
{
	ImgGray result( img.Width(), img.Height() );
	ImgBgr transformed = TransformImageToBgr(img);

	try 
	{
		ImgBgr::Iterator it = transformed.Begin();
		ImgGray::Iterator grayIt = result.Begin();
		while ( it != transformed.End() )
		{
			*grayIt = it->ToGray() ;
			it++;
			grayIt++;
		}
	} catch( Exception exp )
	{
		exp.Display();
	}

	return  result ;
}




/*---------------------------------------------------------------------------*
 * Description: Takes an image and uses its cumulative gray scale histogram
 *              to turn it into a binary image using the passed in threshold.
 *
 * Parameters:
 * [+] img - A Bgr image we want to see as a binary image
 * [+] threshold - A value that differentiates the foreground from the 
 *                 background in a gray scale image
 * Return: A binary image based on the passed in threshold value
 *---------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::CreateBinaryImgUsingThreshold(ImgBgr img, float threshold)
{
	ImgBgr result = img;

	try 
	{
		vector<float> cumuHist = CreateCumuHist(img);

		ImgBgr::Iterator it = result.Begin();
		while( it != result.End() )
		{
			if ( cumuHist[it->ToGray()] > threshold ) 
			{
				it->FromInt( HIST_MAX-1, Bgr::BLEPO_BGR_GRAY); // HIST_MAX-1 is white
			}
			else
			{
				it->FromInt( 0, Bgr::BLEPO_BGR_GRAY); // 0 is black
			}

			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}

	return result;
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image and dilates it based on a
 *              3x3 matrix with a color value that is passed in as a 
 *              parameter.
 *
 * Parameters: 
 * (+) img - A binary image
 * (+) matchingValue - An integer of 0 or 255, the color value of the pixels 
 *                     in the 3x3 matrix.
 * Return value: A dilated binary image
 *---------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::DilateImage(ImgBgr img, int matchingValue, int numTimes)
{
	ImgBgr result = img;
	try
	{
		int nonMatchingValue = ( matchingValue == 0 )? HIST_MAX-1 : 0 ;
		while ( numTimes-- > 0 ) {
			for ( int y = 0 ; y < img.Height() ; y++ )
			{
				for ( int x = 0 ; x < img.Width() ; x++ ) 
				{
					if ( x-1 > 0 && x+1 < img.Width() &&
						 y-1 > 0 && y+1 < img.Height() )
					{
						ImgBgr::Iterator it5 = result.Begin(  x,  y);
						ImgBgr::Iterator it6 = result.Begin(x+1,  y);
						ImgBgr::Iterator it7 = result.Begin(x-1,y+1);
						ImgBgr::Iterator it8 = result.Begin(  x,y+1);
						ImgBgr::Iterator it9 = result.Begin(x+1,y+1);
						if ( it5->ToGray() == matchingValue || 
							 it6->ToGray() == matchingValue || 
							 it7->ToGray() == matchingValue || 
							 it8->ToGray() == matchingValue || 
							 it9->ToGray() == matchingValue )
						{
							it5->FromInt( matchingValue, Bgr::BLEPO_BGR_GRAY );
						} else
						{
							it5->FromInt( nonMatchingValue, Bgr::BLEPO_BGR_GRAY );
						}
					}
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image and erodes it based on a
 *              3x3 matrix with a color value that is passed in as a 
 *              parameter.
 *
 * Parameters: 
 * (+) img - A binary image
 * (+) matchingValue - An integer of 0 or 255, the color value of the pixels 
 *                     in the 3x3 matrix.
 * Return value: An eroded binary image
 *---------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::ErodeImage(ImgBgr img, int matchingValue, int numTimes)
{
	ImgBgr result = img;
	try
	{
		int nonMatchingValue = ( matchingValue == 0 )? HIST_MAX-1 : 0 ;
		while ( numTimes-- > 0 )
		{
			for ( int y = 0 ; y < img.Height() ; y++ )
			{
				for ( int x = 0 ; x < img.Width() ; x++ ) 
				{
					if ( x-1 > 0 && x+1 < img.Width() &&
						 y-1 > 0 && y+1 < img.Height() )
					{
						ImgBgr::Iterator it5 = result.Begin(  x,  y);
						ImgBgr::Iterator it6 = result.Begin(x+1,  y);
						ImgBgr::Iterator it7 = result.Begin(x-1,y+1);
						ImgBgr::Iterator it8 = result.Begin(  x,y+1);
						ImgBgr::Iterator it9 = result.Begin(x+1,y+1);
						if ( it5->ToGray() == matchingValue && 
							 it6->ToGray() == matchingValue && 
							 it7->ToGray() == matchingValue && 
							 it8->ToGray() == matchingValue && 
							 it9->ToGray() == matchingValue )
						{
							it5->FromInt( matchingValue, Bgr::BLEPO_BGR_GRAY );
						} else
						{
							it5->FromInt( nonMatchingValue, Bgr::BLEPO_BGR_GRAY );
						}
					}
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}


/*---------------------------------------------------------------------------*
 * Description: This method creates a composite image from two thresholded 
 *              images in order to create a binary image that clearly 
 *              delineates between the image background and foreground.
 * 
 * Parameters:
 * [+] htImg - an integer image that was given a high threshold value to 
 *             discriminate between the images background and foreground
 * [+] ltImg - an integer image that was given a low threshold value to 
 *             discriminate between the images background and foreground
 * [+] out - a binary image that represents the merging of the 2 thresholded 
 *           images
 * Returns: A vector containing the labels that are found in both the high
 *          and low thresholded images.
 *---------------------------------------------------------------------------*/
vector<int>   ImageProcessing::MergeImages(const ImgInt& htImg, const ImgInt& ltImg, ImgBinary *out)
{
	vector<int> validLabels;

	try
	{
		FILE *fout = fopen("C:\\Users\\shebes\\hw\\ece847\\blepo\\CannyEdgeDetection\\SimilarLabels.out", "w");
		out->Reset( htImg.Width(), htImg.Height() );
		ImgInt::ConstIterator p, q ;
		int i = 0;
		for ( p = ltImg.Begin(), q = htImg.Begin() ; p != ltImg.End() ; p++, q++ )
		{
			if ( *p > 0 && *q > 0 )
			{
				if ( validLabels.empty() ) 
				{
					fprintf( fout, "[%5d] %3d\n", ++i, *p);
					validLabels.push_back(*p);
				} 
				else if ( !IsInVector(*p, validLabels) )
				{
					fprintf( fout, "[%5d] %3d\n", ++i, *p);
					validLabels.push_back(*p);
				}
			}
		}
		ImgBinary::Iterator r;
		for ( p = ltImg.Begin(), r = out->Begin() ; p != ltImg.End() ; p++, r++ )
		{
			if ( IsInVector(*p, validLabels) )
			{
				*r = ImgBinary::Pixel(true);
			}
			else
			{
				*r = ImgBinary::Pixel(false);
			}
		}
		fclose(fout);
	} 
	catch (Exception exp) 
	{
		exp.Display();
	}

	return validLabels;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a map...
 * 
 * Parameters:
 * [+] validLabels  - ...
 * [+] labelsB      - ...
 * Returns: A map of ...
 *---------------------------------------------------------------------------*/
std::map< int,vector<double> >    ImageProcessing::CalculateMoments( vector<int> validLabels, const ImgInt& labelsB )
{
	std::map< int,vector<double> > momentMap;
	try 
	{
		// create the empty moment map
		vector<double> moments(6, 0.0);
		vector<int>::iterator it;
		for ( it = validLabels.begin() ; it != validLabels.end() ; it++ )
		{
			momentMap[*it] = moments;
		}

		//FILE *fout = fopen("momentslist.out", "w");
		for ( int y = 0 ; y < labelsB.Height() ; y++ )
		{
			for ( int x = 0 ; x < labelsB.Width() ; x++ )
			{
				int label = *labelsB.Begin(x, y);
				if ( IsInVector( label, validLabels) )
				{
					momentMap[label].at(0) = momentMap[label].at(0) + 1;
					momentMap[label].at(1) = momentMap[label].at(1) + (double)x;
					momentMap[label].at(2) = momentMap[label].at(2) + (double)y;
					momentMap[label].at(3) = momentMap[label].at(3) + ((double)x * (double)y);
					momentMap[label].at(4) = momentMap[label].at(4) + ((double)x * (double)x);
					momentMap[label].at(5) = momentMap[label].at(5) + ((double)y * (double)y);
				}
			}
		}

		map< int,vector<double> >::iterator mapit;
		for( mapit = momentMap.begin() ; mapit != momentMap.end() ; mapit++ )
		{
			vector<double> moments = mapit->second ;
			//vector<double>::iterator miter;
			//fprintf( fout, "[%2d] ", mapit->first ) ;
			//int i = 0 ;
			//for( miter = moments.begin() ; miter != moments.end() ; miter++ )
			//{
			//	fprintf( fout, "(Moment%d %10.0f) ", i++, *miter );
			//}
			//fprintf( fout, "[(Xc,Yc) (%5.5f,%5.5f)]", moments.at(1)/moments.at(0), moments.at(2)/moments.at(0) );
			//fprintf( fout, "\n" );
		}
		//fclose(fout);
	} 
	catch (Exception exp)
	{
		exp.Display();
	}

	return momentMap;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a map...
 * 
 * Parameters:
 * [+] htImg - A reference to the high threshold image
 * [+] ltImg - A reference to the low threshold image
 * [+] out   - A reference to the binary image to be created from the
 *             thresholded images
 * [+] allPointsltImg - ...
 * [+] momentsMap - ..
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void  ImageProcessing::MergeThresholdedImages(ImgBgr * htImg, ImgBgr * ltImg, ImgBinary * out, map< int, vector< Point > > * allPoints, map< int, vector< double > > * momentsMap)
{
	ImgBgr result;
	ImgInt labels, labelsB;
	vector< ConnectedComponentProperties < ImgBgr::Pixel > > ccProps, ccPropsB;
	try 
	{
		ImgBgr openClosedImg  = DilateImage(ErodeImage(*htImg, HIST_MAX-1, 3), HIST_MAX-1, 3);
		ImgBgr openClosedImgB = DilateImage(ErodeImage(*ltImg, HIST_MAX-1, 3), HIST_MAX-1, 3);
		int nLabels  = ConnectedComponents4(openClosedImg, &labels, &ccProps);
		int nLabelsB = ConnectedComponents4(openClosedImgB, &labelsB, &ccPropsB);

		vector<int> validLabels = MergeImages( labels, labelsB, out ) ;
		map< int, vector<double> > momentMap = CalculateMoments( validLabels, labelsB );		
		*momentsMap = momentMap;

		vector<int>::iterator labelit;
		map< int, vector<Point> > chainedPoints;
		for ( labelit = validLabels.begin() ; labelit != validLabels.end() ; labelit++ )
		{
			vector<Point> chain;
			WallFollow(labelsB, *labelit, &chain) ;
			chainedPoints[*labelit] = chain;
		}
		*allPoints = chainedPoints;
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method draws borders around the foreground objects of a processed 
 *      image based on different eigenvalues.
 * 
 * Parameters:
 * [+] img       - A reference to the unmodified original image
 * [+] allPoints - ...
 * [+] ei        - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void     ImageProcessing::DrawBorders( ImgBgr * img, const map< int, vector<Point> >& allPoints, map<int, eigeninfo> ei )
{
	map< int, vector<Point> >::const_iterator allit;
	for( allit = allPoints.begin() ; allit != allPoints.end() ; allit++ )
	{
		Bgr borderColor = Bgr::RED;
		eigeninfo ein = ei[allit->first];
		// if eccentrictiy is greater than 2.5 you have a banana
		if ( ein.eccentricity > 2.5 )
		{
			borderColor = Bgr::YELLOW;
		} else if ( ein.chain.size() < 300 )
		{
			borderColor = Bgr::RED;
		} else if ( ein.chain.size() > 300 && ein.chain.size() < 500 ) 
		{
			borderColor = Bgr::GREEN;
		}
		Point centroid(ein.xc, ein.yc);

		Point crossAOne( ein.xc + 5, ein.yc) ;
		Point crossATwo( ein.xc - 5, ein.yc) ;
		Point crossBOne( ein.xc, ein.yc + 5) ;
		Point crossBTwo( ein.xc, ein.yc - 5) ;
		DrawLine( crossAOne, crossATwo, img, Bgr::BLUE );
		DrawLine( crossBOne, crossBTwo, img, Bgr::BLUE );

		Point offCentroidA(ein.xc+ein.majorx, ein.yc+ein.majory);		
		Point offCentroidB(ein.xc-ein.majorx, ein.yc-ein.majory);
		Point offCentroidTwoA(ein.xc+ein.minorx, ein.yc+ein.minory);
		Point offCentroidTwoB(ein.xc-ein.minorx, ein.yc-ein.minory);

		DrawLine( offCentroidA, offCentroidB, img, Bgr::CYAN );
		DrawLine( offCentroidTwoA, offCentroidTwoB, img, Bgr::CYAN );
		vector<Point> tempVector = allit->second;
		vector<Point>::iterator pointit ;
		for ( pointit = tempVector.begin() ; pointit != tempVector.end() ; pointit++ )
		{
			Point tempPoint = *pointit;
			*(img->Begin(tempPoint.x, tempPoint.y)) =  borderColor;
		}
	}
	Figure fig("Outlined image") ;
	fig.Draw(*img);
}


/*---------------------------------------------------------------------------*
 * Description: 
 *		This method calculates eigen values for the foreground objects in an
 * image using the information passed into the function.
 * 
 * Parameters:
 * [+] ei          - ...
 * [+] momentMap   - ...
 * [+] chainPoints - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::CalculateEigenValues(map<int, eigeninfo> * ei, const map< int, vector<double> >& momentMap, map< int, vector<Point> > chainPoints)
{
	try 
	{
		FILE *fout = fopen("eigen.out", "w");
		map<int, eigeninfo> eiginfo;

		map< int, vector<double> >::const_iterator mit;
		map<int, eigeninfo>::iterator eit;

		for ( mit = momentMap.begin() ; mit != momentMap.end() ; mit++ )
		{
			eigeninfo ein;
			
			ein.chain = chainPoints[mit->first] ;

			ein.m00 = mit->second.at(0);
			ein.m10 = mit->second.at(1);
			ein.m01 = mit->second.at(2);
			ein.m11 = mit->second.at(3);
			ein.m20 = mit->second.at(4);
			ein.m02 = mit->second.at(5);

			double xc = mit->second.at(1) / mit->second.at(0);
			double yc = mit->second.at(2) / mit->second.at(0);
			double mu00 = ein.mu00 = mit->second.at(0);
			double mu10 = ein.mu10 = 0.0;
			double mu01 = ein.mu01 = 0.0;
			double mu11 = ein.mu11 = mit->second.at(3) - ( yc * mit->second.at(1) );
			double mu20 = ein.mu20 = mit->second.at(4) - ( xc * mit->second.at(1) );
			double mu02 = ein.mu02 = mit->second.at(5) - ( yc * mit->second.at(2) );

			ein.xc = xc;
			ein.yc = yc;

			// calculate direction using moment info
			// y = 2mu11
			// x = mu20 - mu02
			double y = 2 * mu11;
			double x = mu20 - mu02;
			ein.direction = 0.5 * atan2(y, x) ;

			// a = mu20+mu02
			// b = 4m11^2
			// x = mu20-mu02
			// c = x^2
			// d = b + c
			// e = sqrt(d)
			// f = a + e
			// g = a - e
			double a = mu20 + mu02 ;
			double b = 4 * pow(mu11, 2);
			double c = pow( x, 2 );
			double d = b + c ;
			double e = sqrt(d);
			double f = a + e ;
			double g = a - e ;

			ein.lambdamax = sqrt(f);
			ein.lambdamin = sqrt(g);
			ein.eccentricity = ein.lambdamax / ein.lambdamin ;			
			ein.compactness = (4 * 3.14 * ein.m00) / ein.chain.size();

			ein.eigenmax = (1 / (2*mit->second.at(0))) * f;
			ein.eigenmin = (1 / (2*mit->second.at(0))) * g;
			
			// calculate major axis directions for line drawing
			ein.majorx = sqrt(ein.eigenmax) * cos(ein.direction);
			ein.majory = sqrt(ein.eigenmax) * sin(ein.direction);

			// calculate minor axis directions for line drawing
			ein.minorx = - ( sqrt(ein.eigenmin) * sin(ein.direction) );
			ein.minory = sqrt(ein.eigenmin) * cos(ein.direction);

			eiginfo[mit->first] = ein;
			fprintf( fout, "[Label %2d] [eccentricity %5.3f] [direction %5.3f]\n", mit->first, ein.eccentricity, ein.direction);
		}
		*ei = eiginfo;
		fclose(fout);
	} catch(Exception exp) { exp.Display() ; }
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvolveVertical  (GaussianInfo &gi, ImgFloat &img, ImgFloat *out, bool useDerivative)
{
	try 
	{
		int x, y, z;
		int mu, width;
		double convolutionValue;
		
		vector<double> kernel;
		if ( useDerivative )
		{
			kernel = gi.dg;
		} else 
		{
			kernel = gi.g;
		}
		width = gi.w;
		mu = gi.mu;

		*out = img;

		for( y = mu ; y < img.Height()-mu ; y++ )
		{
			for( x = 0 ; x < img.Width() ; x++ )
			{
				convolutionValue = 0.0;
				for ( z = 0 ; z < width ; z++ ) 
				{
					double cv = kernel.at(z) * img(x,y+mu-z);
					convolutionValue += cv;
				}
				(*out)(x,y) = convolutionValue;
			}
		}
	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvolveHorizontal(GaussianInfo &gi, ImgFloat &img, ImgFloat *out, bool useDerivative)
{
	try 
	{
		int x, y, z;
		int mu, width;
		double convolutionValue;
		
		vector<double> kernel;
		if ( useDerivative )
		{
			kernel = gi.dg;
		} else 
		{
			kernel = gi.g;
		}
		width = gi.w;
		mu = gi.mu;

		out->Reset( img.Width(), img.Height() );

		for( y = 0 ; y < img.Height() ; y++ )
		{
			for( x = mu ; x < img.Width()-mu ; x++ )
			{
				convolutionValue = 0.0;
				for ( z = 0 ; z < width ; z++ ) 
				{
					convolutionValue += kernel.at(z) * img(x+mu-z,y);
				}
				(*out)(x,y) = convolutionValue;
			}
		}
	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgFloat(ImgFloat * img, float initializationValue)
{
	try
	{
		ImgFloat::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = initializationValue;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgGray(ImgGray * img, int initializationValue)
{
	try
	{
		ImgGray::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = initializationValue;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}


/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgInt(blepo::ImgInt * img, int initializationValue)
{
	try
	{
		ImgInt::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = initializationValue;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This is a Shrinivas inspired method to convert a blepo float image 
 * into a blepo gray image.
 * 
 * Parameters:
 * [+] iFloat - a float image you want transformed into a gray image
 * [+] iGray  - a reference to the blepo gray image you want to create from a 
 *             float image
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvertFG( ImgFloat & iFloat, ImgGray * iGray )
{
	try 
	{
		iGray->Reset( iFloat.Width(), iFloat.Height() );
		InitializeImgGray(iGray, 0);
		
		ImgFloat::Pixel min = Min(iFloat);
		ImgFloat::Pixel max = Max(iFloat);

		int i = 0;
		ImgFloat::Iterator itFlt;
		ImgGray::Iterator itGry;
		for ( itGry = iGray->Begin(), itFlt = iFloat.Begin() ; itFlt != iFloat.End() ; itFlt++, itGry++ )
		{
			double value = ( (*itFlt - min) / (max - min) ) * 255;
			*itGry = (int)value;
		}
	}
	catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method uses a Shrinivas inspired image convert function to
 * turn a blepo float image into a blepo gray image and displays it.
 * 
 * Parameters:
 * [+] img   - a float image you want displayed as a gray image
 * [+] title - the name you want for a title for the image to be displayed
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ViewFloatImageAsGray( ImgFloat * img, char * title)
{
	try
	{
		ImgGray result ;
		ConvertFG( *img, &result ) ;
		Figure fig(title);
		fig.Draw(result);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * 
 * Parameters:
 * [+] bImg - a binary image from which chamfer distances will be calculated
 * [+] iImg - integer image with chamfer manhattan distances
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::Chamfer(blepo::ImgBinary & bImg, blepo::ImgInt * iImg)
{
	int x, y, maxInt, upperLimit ;

	try 
	{
		FILE *fout = fopen("C:\\Users\\shebes\\hw\\ece847\\blepo\\CannyEdgeDetection\\chamfer.out", "w") ;
		maxInt     = INT_MAX;
		upperLimit = maxInt - 1;
		fprintf( fout, "Maximum integer value: %d\n", maxInt );

		iImg->Reset( bImg.Width(), bImg.Height() );

		InitializeImgInt( iImg, 0 );

		// 1st pass
		for( y = 0 ; y < bImg.Height() ; y++ )
		{
			for ( x = 0 ; x < bImg.Width() ; x++ )
			{
				if ( x-1 > -1 && y-1 > -1 )
				{
					int leftValue = (*iImg)(x-1, y);
					int topValue  = (*iImg)(x, y-1);
					if ( upperLimit > leftValue )
					{
						leftValue++;
					}
					if ( upperLimit > topValue )
					{
						topValue++;
					}

					(*iImg)(x,y) = bImg(x,y) ? 0 : min( maxInt, min( topValue, leftValue ) );
				}
			}
		}

		//Figure figChamferPassOne("Chamfer pass 1");
		//figChamferPassOne.Draw( *iImg );

		// 2nd pass
		for( y = bImg.Height() - 1 ; y > -1 ; y-- )
		{
			for ( x = bImg.Width() - 1 ; x > -1 ; x-- )
			{
				if ( x+1 < bImg.Width() && y+1 < bImg.Height() )
				{
					int rightValue   = (*iImg)(x+1, y);
					int bottomValue  = (*iImg)(x, y+1);
					if ( upperLimit > rightValue )
					{
						rightValue++;
					}
					if ( upperLimit > bottomValue )
					{
						bottomValue++;
					}

					(*iImg)(x,y) = bImg(x,y) ? 0 : min( (*iImg)(x,y), min( bottomValue, rightValue ) );
				}
			}
		}

		//Figure figChamferPassTwo("Chamfer pass 2");
		//figChamferPassTwo.Draw( *iImg );
		

		// 3rd pass
		//for( y = 0 ; y < bImg.Height() ; y++ )
		//{
		//	for ( x = 0 ; x < 10 ; x++ )//for ( x = 0 ; x < bImg.Width() ; x++ )
		//	{
		//		fprintf( fout, "[(%3d,%3d)=%10d] ", x, y, (*iImg)(x,y) );
		//	}
		//	fprintf( fout, "\n" );
		//}

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}