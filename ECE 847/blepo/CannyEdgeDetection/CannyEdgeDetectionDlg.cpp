// CannyEdgeDetectionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CannyEdgeDetection.h"
#include "CannyEdgeDetectionDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// uncomment exactly one of these
#define G_DIR "..\\..\\"   // normal

// ================> begin local functions (available only to this translation unit)
namespace
{

	CString iGetExecutableDirectory()
	{
		const char* help_path = AfxGetApp()->m_pszHelpFilePath;
		char* s = const_cast<char*>( strrchr(help_path, '\\') );
		*(s+1) = '\0';
		return CString(help_path);
	}

	CString iGetImagesDir()
	{
		return iGetExecutableDirectory() + G_DIR;
	}

};
// ================< end local functions

using namespace std;
using namespace blepo;

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionDlg dialog

CCannyEdgeDetectionDlg::CCannyEdgeDetectionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCannyEdgeDetectionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCannyEdgeDetectionDlg)
	m_sigmaValue = 0.0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCannyEdgeDetectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCannyEdgeDetectionDlg)
	DDX_Text(pDX, IDC_SIGMASAMPLES_INPUT, m_sigmaValue);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCannyEdgeDetectionDlg, CDialog)
	//{{AFX_MSG_MAP(CCannyEdgeDetectionDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_FLOODFILL, OnBtnClickFloodfill)
	ON_BN_CLICKED(IDC_FOREGROUNDCLASSIFICATION, OnClickClassifyForeground)
	ON_BN_CLICKED(IDC_CANNYEDGEDETECTION, OnBtnClickCannyEdgeDetect)
	ON_BN_CLICKED(IDC_TEMPLATEMATCHING, OnClickMatchWithTemplate)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCannyEdgeDetectionDlg message handlers

BOOL CCannyEdgeDetectionDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// define the m_cFileDialogFilter	
	m_cFileDialogFilter = "All image files|*.pgm;*ppm;*.bmp;*.jpg;*.jpeg|PGM/PPM files (*.pgm)|*.pgm;*.ppm|BMP files (*.bmp)|*.bmp|JPEG files (*.jpg,.jpeg)|*.jpg;*.jpeg|All files (*.*)|*.*||";  // filter
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCannyEdgeDetectionDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCannyEdgeDetectionDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCannyEdgeDetectionDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CCannyEdgeDetectionDlg::OnBtnClickFloodfill() 
{
	try 
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"quantized.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,  // filter
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			Load(fname, &img);
			Figure fig("Selction loaded");
			fig.Draw(img);
			Point seedPoint = fig.GrabMouseClick();
			ImgBgr filledImg ;
			filledImg = ImageProcessing::Floodfill(seedPoint, img);
			Figure figAgain("Flood Filled Image");
			figAgain.Draw(filledImg);
		}

	} catch (const Exception& e)
	{
		// image failed to load, so notify user
		e.Display();
	}
	
}

void CCannyEdgeDetectionDlg::OnClickClassifyForeground() 
{
	try 
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"fruit1.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			ImgBinary mergedImg;
			map< int, eigeninfo > ei;
			map< int, vector<Point> > allPoints;
			map< int, vector<double> > momentMap;
			
			Load(fname, &img);
			Figure fig(dlg.GetFileTitle() + " loaded");
			fig.Draw(img);

			// use high threshold on image
			ImgBgr highThresholdedImg = ImageProcessing::CreateBinaryImgUsingThreshold(img, 0.85);
			// use low threshold on image
			ImgBgr lowThresholdImg = ImageProcessing::CreateBinaryImgUsingThreshold(img, 0.68);

			// merge and display the two thresholded images
			ImageProcessing::MergeThresholdedImages(&highThresholdedImg, &lowThresholdImg, &mergedImg, &allPoints, &momentMap);
			Figure figMerged(dlg.GetFileTitle() + " binary merged image");
			figMerged.Draw(mergedImg);

			ImageProcessing::CalculateEigenValues(&ei, momentMap, allPoints);
			ImageProcessing::DrawBorders( &img, allPoints, ei );
			
			CString str;
			map<int, eigeninfo>::iterator it;
			for ( it = ei.begin() ; it != ei.end() ; it++ )
			{
				str.Format("Properties of region:\r\n"
					"  area (number of pixels):  %10.0f\r\n"
					"  centroid:  (%5.1f, %5.1f)\r\n"
					"  compactness:  [%1.5f]\r\n"
					"  direction (clockwise from horizontal):  %5.5f radians\r\n"
					"  eccentricity:  %5.5f\r\n"
					"  moments:\r\n"
					"    m00:  %10.3f\r\n"
					"    m10:  %10.3f\r\n"
					"    m01:  %10.3f\r\n"
					"    m11:  %10.3f\r\n"
					"    m20:  %10.3f\r\n"
					"    m02:  %10.3f\r\n"
					"  centralized moments:\r\n"
					"    mu10:  %10.3f\r\n"
					"    mu01:  %10.3f\r\n"
					"    mu11:  %10.3f\r\n"
					"    mu20:  %10.3f\r\n"
					"    mu02:  %10.3f\r\n",
					it->second.m00, 
					it->second.xc, it->second.yc,
					it->second.compactness,
					it->second.direction,
					it->second.eccentricity,
					it->second.m00, it->second.m10, it->second.m01, it->second.m11, it->second.m20, it->second.m02,
					it->second.mu10, it->second.mu01, it->second.mu11, it->second.mu20, it->second.mu02);
			  AfxMessageBox(str, MB_ICONINFORMATION);
			}

			// do this for test, created for Local Enthropy Thresholding
			//ImageProcessing::CalculateCoOccurrenceMatrix(img);
		}

	} catch (const Exception& e)
	{
		// image failed to load, so notify user
		e.Display();
	}	
}

void CCannyEdgeDetectionDlg::OnBtnClickCannyEdgeDetect() 
{	
	try 
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"cat.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		
		if (dlg.DoModal() == IDOK)
		{
			// Get the path name to add to the figure displays
			CString fname      = dlg.GetPathName();
			// Get the file title to add to the figure display windows
			CString fTitle     = dlg.GetFileTitle();

			// Get the sigma value from the text box and calculate the rest
			// of the gaussian information.			
			GaussianInfo gi;
			CString sigmaValue;
			GetDlgItemText( IDC_SIGMASAMPLES_INPUT, sigmaValue);
			CCannyEdgeDetectionApp::CalculateGaussianInfo( &gi, atof(sigmaValue) );

			ImgBgr img;
			ImgInt chamImg;
			ImgBinary elImg;
			ImgFloat gx, gy, gmag, gdir, nmsImg;

			Load(fname, &img);
			// create title for original image
			Figure fig( fTitle + " loaded" );
			fig.Draw(img);

			CCannyEdgeDetectionApp::MakeGradientImageYXPrime(&gi, img, &gx);
			// create title for the image that gradient
			Figure figGradientX( fTitle + " Convolved with Gaussian of Y and Derivative of Gaussian of X" );
			figGradientX.Draw(gx);

			CCannyEdgeDetectionApp::MakeGradientImageXYPrime(&gi, img, &gy);
			// create title for the image that gradient
			Figure figGradientY( fTitle + " Convolved with Gaussian of X and Derivative of Gaussian of Y");
			figGradientY.Draw(gy);

			CCannyEdgeDetectionApp::CalculateGradientMagnitude( gx, gy, &gmag);
			// create title for the gradient magnitude image
			Figure figGradientMag( fTitle + " Gradient magnitude");
			figGradientMag.Draw(gmag);

			CCannyEdgeDetectionApp::CalculateGradientDirection( gx, gy, &gdir);
			// create title for the gradient direction image
			Figure figGradientDir( fTitle + " Gradient direction");
			figGradientDir.Draw(gdir);

			CCannyEdgeDetectionApp::NonMaximalSuppression( gmag, gdir, &nmsImg );
			// create title for the non maximal suppression
			Figure figNonMaxSuppression( fTitle + " Non-maximally suppressed");
			figNonMaxSuppression.Draw(nmsImg);

			CCannyEdgeDetectionApp::HysteresisEdgeLinking( nmsImg, &elImg );
			// create title for the hysteresis edge linking
			Figure figHysteresis( fTitle + " hysteresis edge linked");
			figHysteresis.Draw(elImg);
			
			// Calculate the chamfer image using manhattan distance and display the result
			ImageProcessing::Chamfer( elImg, &chamImg);
			Figure figChamfer( fTitle + " chamfered up" );
			figChamfer.Draw(chamImg);
		}
	} catch (const Exception& exp)
	{
		// image failed to load, so notify user
		exp.Display();
	}
}

void CCannyEdgeDetectionDlg::OnClickMatchWithTemplate() 
{
	try
	{
		CString message;
		message.Format( "How this works:\r\n"
			"You will choose an image from the hard drive upon which you want to match a template.\r\n"
			"Whatever the image name, this application will assume the name of the template is\r\n"
			"imagename_template.extension\r\n"
			"This application will also use the Sigma from the GUI to do edge detection.");
		AfxMessageBox(message, MB_ICONINFORMATION);

		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"cherrypepsi.jpg",  // default filename
			OFN_HIDEREADONLY,  // flags
			m_cFileDialogFilter,
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			// Get the path name to add to the figure displays
			CString fname      = dlg.GetPathName();
			//CString folderPath = dlg.GetFolderPath();
			CString fExtension = dlg.GetFileExt();
			CString fTitle     = dlg.GetFileTitle();

			CString fTemplateName = dir + "\\" + fTitle + "_template." + fExtension;			
			AfxMessageBox(fTemplateName, MB_ICONINFORMATION);

			// Get the sigma value from the text box and calculate the rest
			// of the gaussian information.			
			GaussianInfo gi;
			CString sigmaValue;
			GetDlgItemText( IDC_SIGMASAMPLES_INPUT, sigmaValue);
			CCannyEdgeDetectionApp::CalculateGaussianInfo( &gi, atof(sigmaValue) );

			// regular image variables
			ImgBgr img;
			ImgInt chamImg, probabilityMatrix;
			ImgBinary elImg;
			ImgFloat gx, gy, gmag, gdir, nmsImg;

			// template image variables
			ImgBgr imgT;
			ImgBinary elImgT, emImg;
			ImgFloat gxT, gyT, gmagT, gdirT, nmsImgT;

			// Loads normal Image
			Load(fname, &img);
			// create title for original image
			Figure fig( fTitle + " loaded" );
			fig.Draw(img);
			
			CCannyEdgeDetectionApp::MakeGradientImageYXPrime(   &gi,   img,     &gx );
			CCannyEdgeDetectionApp::MakeGradientImageXYPrime(   &gi,   img,     &gy );
			CCannyEdgeDetectionApp::CalculateGradientMagnitude( gx,     gy,   &gmag );
			CCannyEdgeDetectionApp::CalculateGradientDirection( gx,     gy,   &gdir );
			CCannyEdgeDetectionApp::NonMaximalSuppression(      gmag, gdir, &nmsImg );
			CCannyEdgeDetectionApp::HysteresisEdgeLinking( nmsImg, &elImg );
			// create title for the hysteresis edge linking
			Figure figHysteresis(dlg.GetFileTitle() + " hysteresis edge linked");
			figHysteresis.Draw(elImg);
			
			ImageProcessing::Chamfer( elImg, &chamImg);
			Figure figChamfer( fTitle + " chamfered up" );
			figChamfer.Draw(chamImg);

			// Loads template image
			Load(fTemplateName, &imgT);
			// create title for original image
			Figure figTemplate( fTitle + " template loaded" );
			figTemplate.Draw(imgT);
			
			// Do Canny Edge Detection on the template image
			CCannyEdgeDetectionApp::MakeGradientImageYXPrime(   &gi,    imgT,     &gxT );
			CCannyEdgeDetectionApp::MakeGradientImageXYPrime(   &gi,    imgT,     &gyT );
			CCannyEdgeDetectionApp::CalculateGradientMagnitude( gxT,     gyT,   &gmagT );
			CCannyEdgeDetectionApp::CalculateGradientDirection( gxT,     gyT,   &gdirT );
			CCannyEdgeDetectionApp::NonMaximalSuppression(      gmagT, gdirT, &nmsImgT );
			CCannyEdgeDetectionApp::HysteresisEdgeLinking( nmsImgT, &elImgT );
			// create title for the hysteresis edge linking
			Figure figHysteresisT(dlg.GetFileTitle() + " template hysteresis edge linked");
			figHysteresisT.Draw(elImgT);		

			// do some template matching
			CCannyEdgeDetectionApp::MatchTemplate(chamImg, elImgT, &probabilityMatrix);
			Figure figPM( fTitle + " probability matrix with template" );
			figPM.Draw(probabilityMatrix);

			CCannyEdgeDetectionApp::DrawRectangle( &img, imgT, probabilityMatrix );
			Figure figBorder("Rectangle");
			figBorder.Draw(img);

		}
	} catch (const Exception& exp)
	{
		// image failed to load, so notify user
		exp.Display();
	}
	
}
