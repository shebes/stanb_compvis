/*****************************************************************************
 * ImageProcessing.h
 * File to contain all the image processing functions developed from the
 * ECE847 projects.
 *****************************************************************************/

#pragma once

#ifndef _IMAGEPROCESSING_H_
#endif // _IMAGEPROCESSING_H_

#include <cmath>
#include <ctime>
#include <stack>
#include <vector>
#include <map>
#include <limits.h>
#include "../src/blepo.h"

#define HIST_MAX 256

struct eigeninfo
{
	std::vector<blepo::Point> chain;
	double compactness;
	double eigenmax;
	double eigenmin;
	double lambdamax;
	double lambdamin;
	double eccentricity;
	double direction;
	double xc;
	double yc;
	double m00;
	double m10;
	double m01;
	double m11;
	double m20;
	double m02;
	double mu00;
	double mu10;
	double mu01;
	double mu11;
	double mu20;
	double mu02;
	double minorx;
	double minory;
	double majorx;
	double majory;
};

struct GaussianInfo
{
	int f;                   // f=2 is reasonable, to capture +-2.5sigma
	int k;                   //
	int mu;                  // is mu of the half-width
	int w;                   // width of kernel
	double  sum ;            // normalization factor
	double dsum ;            // normalization factor for derivative
	double sigma;            // standard deviation for gaussian
	double variance;         //
	double widthOverSigma;   //
	std::vector<double>   g; // Gaussian kernel
	std::vector<double>  dg; // Gaussian kernel derivative
	//std::vector<double>  Gx; // Gaussian kernel in the X direction 
	//std::vector<double>  Gy; // Gaussian kernel in the Y direction
	//std::vector<double> dGy; // Gaussian kernel derivative in the Y direction
};

class ImageProcessing
{
public:	
	static blepo::ImgBgr Floodfill(blepo::Point seedPoint, blepo::ImgBgr originImg);
	static char * getLocalTime();
	static bool IsInVector(int element, std::vector<int> data);
	static std::vector<float> CreateNormHist(blepo::ImgBgr img);
	static std::vector<float> CreateCumuHist(blepo::ImgBgr img);
	static std::vector<float> CreateCumuHist(std::vector<float> imgHist);
	static blepo::ImgGray     TransformImageToGray(blepo::ImgBgr img);
	static blepo::ImgBgr      TransformImageToBgr(blepo::ImgBgr img);
	static blepo::ImgBgr      CreateBinaryImgUsingThreshold(blepo::ImgBgr img, float threshold);
	static blepo::ImgBgr      DilateImage(blepo::ImgBgr img, int matchingValue, int numTimes);
	static blepo::ImgBgr      ErodeImage(blepo::ImgBgr img, int matchingValue, int numTimes);
	static std::vector<int>   MergeImages(const blepo::ImgInt& htImg, const blepo::ImgInt& ltImg, blepo::ImgBinary *out);
	static void               MergeThresholdedImages(blepo::ImgBgr * htImg, blepo::ImgBgr * ltImg, blepo::ImgBinary *out, std::map< int, std::vector< blepo::Point > > * allPoints, std::map< int, std::vector< double > > * momentsMap);
	static std::map< int, std::vector<double> > CalculateMoments( std::vector<int> validLabels, const blepo::ImgInt& labelsB );
	static void               DrawBorders(blepo::ImgBgr * img, const std::map< int, std::vector<blepo::Point> > & allPoints, std::map<int, eigeninfo> ei);
	static void               CalculateEigenValues( std::map<int, eigeninfo> * ei, const std::map< int, std::vector<double> >& momentMap, std::map< int, std::vector<blepo::Point> > chainPoints );	
	static void ConvolveVertical  (GaussianInfo &gi, blepo::ImgFloat &img, blepo::ImgFloat *out, bool useDerivative);
	static void ConvolveHorizontal(GaussianInfo &gi, blepo::ImgFloat &img, blepo::ImgFloat *out, bool useDerivative);
	static void ViewFloatImageAsGray( blepo::ImgFloat * img, char * title);
	static void InitializeImgFloat(blepo::ImgFloat * img, float initializationValue);
	static void InitializeImgGray(blepo::ImgGray * img, int initializationValue);
	static void InitializeImgInt(blepo::ImgInt * img, int initializationValue);
	static void ConvertFG(blepo::ImgFloat & iFloat, blepo::ImgGray * iGray);
	static void Chamfer(blepo::ImgBinary & bImg, blepo::ImgInt * iImg);
};
