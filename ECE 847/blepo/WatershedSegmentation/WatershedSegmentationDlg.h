// WatershedSegmentationDlg.h : header file
//

#if !defined(AFX_WATERSHEDSEGMENTATIONDLG_H__307980E1_506B_403B_A322_DE0A92EA2D48__INCLUDED_)
#define AFX_WATERSHEDSEGMENTATIONDLG_H__307980E1_506B_403B_A322_DE0A92EA2D48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WatershedSegmentation.h"

/////////////////////////////////////////////////////////////////////////////
// CWatershedSegmentationDlg dialog

class CWatershedSegmentationDlg : public CDialog
{
// Construction
public:
	CWatershedSegmentationDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CWatershedSegmentationDlg)
	enum { IDD = IDD_WATERSHEDSEGMENTATION_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWatershedSegmentationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	char * m_cFileDialogFilter;

	// Generated message map functions
	//{{AFX_MSG(CWatershedSegmentationDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnClickFloodfill();
	afx_msg void OnBtnClickClassifyForeground();
	afx_msg void OnBtnClickCannyEdgeDetect();
	afx_msg void OnBtnClickMatchTemplate();
	afx_msg void OnBtnClickWatershedSegment();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WATERSHEDSEGMENTATIONDLG_H__307980E1_506B_403B_A322_DE0A92EA2D48__INCLUDED_)
