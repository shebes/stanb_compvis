; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CWatershedSegmentationDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "WatershedSegmentation.h"

ClassCount=4
Class1=CWatershedSegmentationApp
Class2=CWatershedSegmentationDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_WATERSHEDSEGMENTATION_DIALOG

[CLS:CWatershedSegmentationApp]
Type=0
HeaderFile=WatershedSegmentation.h
ImplementationFile=WatershedSegmentation.cpp
Filter=N

[CLS:CWatershedSegmentationDlg]
Type=0
HeaderFile=WatershedSegmentationDlg.h
ImplementationFile=WatershedSegmentationDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=WatershedSegmentationDlg.h
ImplementationFile=WatershedSegmentationDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_WATERSHEDSEGMENTATION_DIALOG]
Type=1
Class=CWatershedSegmentationDlg
ControlCount=15
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_IMGPROCTECHS,button,1342177287
Control4=IDC_SEGMENTATION,button,1342177287
Control5=IDC_FLOODFILL,button,1342242816
Control6=IDC_FRGRDCLASSIFICATION,button,1342242816
Control7=IDC_CANNYEDGEDETECTION,button,1342242816
Control8=IDC_SIGMASAMPLES_INPUT,edit,1350631554
Control9=IDC_SIGMALABEL,static,1342308352
Control10=IDC_TEMPLATEMATCHING,button,1342242816
Control11=IDC_WATERSHED,button,1342242816
Control12=IDC_STATIC_THRESHOLD,static,1342308352
Control13=IDC_EDIT_THRESHOLD,edit,1350631554
Control14=IDC_STATIC_SMALLEST_BORDER,static,1342308352
Control15=IDC_EDIT_SMALLEST_BORDER,edit,1350631554

