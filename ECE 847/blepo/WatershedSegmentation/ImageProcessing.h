/*****************************************************************************
 * ImageProcessing.h
 * File to contain all the image processing functions developed from the
 * ECE847 projects.
 *****************************************************************************/

#pragma once

#ifndef _IMAGEPROCESSING_H_
#endif // _IMAGEPROCESSING_H_

#include <cmath>
#include <ctime>
#include <stack>
#include <vector>
#include <map>
#include <queue>
#include <limits.h>
#include "../src/blepo.h"

#define HIST_MAX 256

struct eigeninfo
{
	std::vector<blepo::Point> chain;
	double compactness;
	double eigenmax;
	double eigenmin;
	double lambdamax;
	double lambdamin;
	double eccentricity;
	double direction;
	double xc; // centroid in the horizontal or X direction
	double yc; // centroid in the vertical   or Y direction
	double m00;
	double m10;
	double m01;
	double m11;
	double m20;
	double m02;
	double mu00;
	double mu10;
	double mu01;
	double mu11;
	double mu20;
	double mu02;
	double minorx;
	double minory;
	double majorx;
	double majory;
};

struct GaussianInfo
{
	int f;                   // f=2 is reasonable, to capture +-2.5sigma
	int k;                   //
	int mu;                  // is mu of the half-width
	int w;                   // width of kernel
	double  sum ;            // normalization factor
	double dsum ;            // normalization factor for derivative
	double sigma;            // standard deviation for gaussian
	double variance;         //
	double widthOverSigma;   //
	std::vector<double>   g; // Gaussian kernel
	std::vector<double>  dg; // Gaussian kernel derivative
};

class ImageProcessing
{
public:	
	// Default constructor
	ImageProcessing();
	/*
	 * Method implemented for homework 1
	 */
	blepo::ImgBgr Floodfill(blepo::Point seedPoint, blepo::ImgBgr originImg);
	void Floodfill4(blepo::Point seedPoint, blepo::ImgGray &seedImage, blepo::ImgInt * labels, int fllClr);
	void Floodfill8(blepo::Point seedPoint, blepo::ImgGray &seedImage, blepo::ImgInt * labels, int fllClr);

	/*****
	 * Methods implemented for Fruit Classification homework 2
	 *****/
	static int  ValueIsInRegion(double value); // figures out which pi/8 region a number is in
	static char * getLocalTime();
	static bool IsInVector(int element, std::vector<int> data);  // searches vector for element
	static void InitializeImgFloat(blepo::ImgFloat * img, float initializationValue);
	static void InitializeImgGray( blepo::ImgGray  * img, int   initializationValue);
	static void InitializeImgInt(  blepo::ImgInt   * img, int   initializationValue);
	static void ViewFloatImageAsGray(blepo::ImgFloat * img, char * title);

	static std::vector<float> CreateNormHist(blepo::ImgBgr img);
	static std::vector<float> CreateCumuHist(blepo::ImgBgr img);
	static std::vector<float> CreateCumuHist(std::vector<float> imgHist);

	static blepo::ImgGray     TransformImageToGray(blepo::ImgBgr img);
	static blepo::ImgBgr      TransformImageToBgr(blepo::ImgBgr img);
	blepo::ImgBgr             CreateBinaryImgUsingThreshold(blepo::ImgBgr img, double threshold);
	blepo::ImgBgr             DilateImage(blepo::ImgBgr img, int matchingValue, int numTimes);
	blepo::ImgBgr             ErodeImage(blepo::ImgBgr img, int matchingValue, int numTimes);

	static std::vector<int>   MergeImages(const blepo::ImgInt& htImg, const blepo::ImgInt& ltImg, blepo::ImgBinary *out);
	static std::map< int, std::vector<double> > CalculateMoments( std::vector<int> validLabels, const blepo::ImgInt& labelsB );
	
	static void MergeThresholdedImages(blepo::ImgBgr * htImg, blepo::ImgBgr * ltImg, blepo::ImgBinary *out, std::map< int, std::vector< blepo::Point > > * allPoints, std::map< int, std::vector< double > > * momentsMap);
	static void DrawBorders(blepo::ImgBgr * img, const std::map< int, std::vector<blepo::Point> > & allPoints, std::map<int, eigeninfo> ei);
	static void CalculateEigenValues( std::map<int, eigeninfo> * ei, const std::map< int, std::vector<double> >& momentMap, std::map< int, std::vector<blepo::Point> > chainPoints );
	static void ConvolveVertical  (GaussianInfo &gi, blepo::ImgFloat &img, blepo::ImgFloat *out, bool useDerivative);
	static void ConvolveHorizontal(GaussianInfo &gi, blepo::ImgFloat &img, blepo::ImgFloat *out, bool useDerivative);

	/*****
	 * Methods implemented for Canny Edge Detection homework 3
	 *****/
	static void CalculateGaussianInfo(GaussianInfo * gi, double sigma);
	static void ConvertFG(blepo::ImgFloat & iFloat, blepo::ImgGray * iGray);
	void Chamfer(blepo::ImgBinary & bImg, blepo::ImgInt * iImg);
	void MakeGradientImageYXPrime(GaussianInfo * gi, blepo::ImgBgr & img, blepo::ImgFloat * out);
	void MakeGradientImageXYPrime(GaussianInfo * gi, blepo::ImgBgr & img, blepo::ImgFloat * out);
	void CalculateGradientMagnitude(blepo::ImgFloat & imgGx, blepo::ImgFloat & imgGy, blepo::ImgFloat * out);
	void CalculateGradientDirection(blepo::ImgFloat & imgGx, blepo::ImgFloat & imgGy, blepo::ImgFloat * out);
	void NonMaximalSuppression(blepo::ImgFloat &imgMag, blepo::ImgFloat &imgDir, blepo::ImgFloat * out);
	void HysteresisEdgeLinking(blepo::ImgFloat & imgSup, blepo::ImgBinary * out);
	void MatchTemplate(blepo::ImgInt & img, blepo::ImgBinary & imgT, blepo::ImgInt * pMatrix);
	void DrawRectangle( blepo::ImgBgr * img, blepo::ImgBgr & imgT, blepo::ImgInt & probabilityMatrix );

	/*****
	 * Methods implemented for Marker-based Watershed Segmentation homework 4
	 *****/
	void          ToFilePointVectorVector( std::vector< std::vector<blepo::Point> > * pointVV, char * fileName );
	void          ToFileImgInt(blepo::ImgInt * img, char * filename);
	void          ToFileIntVector( std::vector<int> *value, char * filename );
	void          GetNeighbors4( blepo::Point location, blepo::Point maxXY, std::queue<blepo::Point> * neighbors);
	void          GetNeighbors8( blepo::Point location, blepo::Point maxXY, std::queue<blepo::Point> * neighbors);
	blepo::Point  PopQueue( std::queue<blepo::Point> * value );
	blepo::ImgBgr Floodfill4Bgr(blepo::Point seedPoint, blepo::ImgBgr &seedImage);
	void          CreateImgBinaryWithThreshold(blepo::ImgBgr &img, blepo::ImgBinary * out, double threshold);
};
