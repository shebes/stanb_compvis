// WatershedSegmentation.h : main header file for the WATERSHEDSEGMENTATION application
//

#if !defined(AFX_WATERSHEDSEGMENTATION_H__1803A46E_9779_4BE9_B314_2B27E26CCA1E__INCLUDED_)
#define AFX_WATERSHEDSEGMENTATION_H__1803A46E_9779_4BE9_B314_2B27E26CCA1E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "ImageProcessing.h"

/////////////////////////////////////////////////////////////////////////////
// CWatershedSegmentationApp:
// See WatershedSegmentation.cpp for the implementation of this class
//

class CWatershedSegmentationApp : public CWinApp
{
public:
	CWatershedSegmentationApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWatershedSegmentationApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CWatershedSegmentationApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WATERSHEDSEGMENTATION_H__1803A46E_9779_4BE9_B314_2B27E26CCA1E__INCLUDED_)
