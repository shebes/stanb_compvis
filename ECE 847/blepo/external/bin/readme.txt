To simplify the system path, all the necessary DLLs for running the library are included in this directory.  As a result, we cannot rely on a directory structure to indicate the origin of each of these files.  Instead it is incumbent upon us to do so manually.

Therefore, if you add a DLL file from this directory, please update this list.

DLL files:
ijl*		Intel JPEG Library
ipl*		Intel Image Processing Library
                (iplpx Pentium; iplm5 Pentium MMX; iplp6 PPro; iplm6 Pentium2; 
                 ipla6 Pentium3; iplw7 Pentium4)
nsp*		Intel Signal Processing Library
cv097		Intel Computer Vision Library (OpenCV)
cxcore097		"
highgui097		"
olimg32 	Data Translation DT3120 frame grabber
olfg32          "
DtColorSDK	"
Vcc4*		Canon VC-C4 pan-tilt-zoom camera
WinGsl		Windows version of GNU's scientific library (gsl)
1394camera.dll	Carnegie Mellon 1394 camera interface DLL
1394CameraDemo.exe	Carnegie Mellon 1394 camera demo
