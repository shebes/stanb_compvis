// LucasKanade.h : main header file for the LUCASKANADE application
//

#if !defined(AFX_LUCASKANADE_H__313BA672_A632_4071_AE23_02412198D342__INCLUDED_)
#define AFX_LUCASKANADE_H__313BA672_A632_4071_AE23_02412198D342__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "ImageProcessing.h"
#include "WatershedSegmenter.h"

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeApp:
// See LucasKanade.cpp for the implementation of this class
//

class CLucasKanadeApp : public CWinApp
{
public:
	CLucasKanadeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLucasKanadeApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLucasKanadeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LUCASKANADE_H__313BA672_A632_4071_AE23_02412198D342__INCLUDED_)
