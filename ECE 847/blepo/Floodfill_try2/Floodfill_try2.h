
// Floodfill_try2.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "../src/blepo.h"
#include <stack>


// CFloodfill_try2App:
// See Floodfill_try2.cpp for the implementation of this class
//

class CFloodfill_try2App : public CWinAppEx
{
public:
	CFloodfill_try2App();

// Overrides
	public:
	virtual BOOL InitInstance();
	static blepo::ImgBgr Floodfill(CPoint seedPoint, blepo::ImgBgr originImg);
	static blepo::Bgr GetColor(blepo::ImgBgr value, CPoint point);
	static std::vector <CPoint> GetPointsNeighbors(CPoint seed, CPoint imgLimit);
	static blepo::Bgr GetFillColor(blepo::Bgr value);

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CFloodfill_try2App theApp;