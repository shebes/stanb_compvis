
// Floodfill_try2.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Floodfill_try2.h"
#include "Floodfill_try2Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFloodfill_try2App

BEGIN_MESSAGE_MAP(CFloodfill_try2App, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

using namespace blepo;

// CFloodfill_try2App construction

CFloodfill_try2App::CFloodfill_try2App()
{
	EnableHtmlHelp();
}


// The one and only CFloodfill_try2App object

CFloodfill_try2App theApp;


// CFloodfill_try2App initialization

BOOL CFloodfill_try2App::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CFloodfill_try2Dlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}



/*****************************************************************************
 * This method returns the inverse color of the color passed in.
 *
 * @parameter
 *   value  - a color
 *
 * @return  
 *   result - A color object.
 *****************************************************************************/
Bgr CFloodfill_try2App::GetFillColor(Bgr value)
{
	Bgr result;
	try
	{
		if ( value == value.BLACK )
		{
			result = value.RED;
		}
		else if ( value.b > value.g && value.b > value.r )
		{
			result.b = value.b;
			result.g = 0;
			result.r = 0;
		} 
		else if ( value.g > value.b && value.g > value.r )
		{
			result.g = value.g;
			result.b = 0;
			result.r = 0;
		}
		else
		{
			result.r = value.r;
			result.b = 0;
			result.g = 0;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*****************************************************************************
 * This method returns the color information from a point on an image and 
 * returns it as a Bgr color object.
 *
 * @parameter
 *   value  - an image from which you want the color of a point
 *   point  - an x,y coordinate on an image
 *
 * @return  
 *   result - A color image.
 *****************************************************************************/
Bgr CFloodfill_try2App::GetColor(ImgBgr value, CPoint point)
{
	Bgr result;
	try
	{
		unsigned char b = value(point.x, point.y).b ;
		unsigned char g = value(point.x, point.y).g ;
		unsigned char r = value(point.x, point.y).r ;
		result = Bgr(b,g,r);
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*****************************************************************************
 * A method that places the north, east, south, west pixel points in a vector.
 *
 * @parameter
 *   seed     - a point passed in from an image
 *   imgLimit - the height and width limits of the limit
 *
 * @return 
 *   neighbors - A vector of points that are neighbors to the seed 
 *               point passed as an input.
 *****************************************************************************/
std::vector <CPoint> CFloodfill_try2App::GetPointsNeighbors(CPoint seed, CPoint imgLimit)
{
	std::vector <CPoint> neighbors;

	try
	{
		int index = 0;
		int x = seed.x;
		int y = seed.y;
		int xLmt = imgLimit.x;
		int yLmt = imgLimit.y;

		if ( y-1 > -1 )
		{
			neighbors.push_back(CPoint(x, y-1));
		}

		if ( x+1 < xLmt )
		{
			neighbors.push_back(CPoint(x+1, y));
		}

		if ( y+1 < yLmt )
		{
			neighbors.push_back(CPoint(x, y+1));
		}

		if ( x-1 > -1 )
		{
			neighbors.push_back(CPoint(x-1, y));
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}

	return neighbors;
}


/*****************************************************************************
 * A method that repaints neighboring pixels with the same color to a 
 * different color.
 *
 * @parameter
 *   seedPoint - a point passed in from an image
 *   originImg - the image in which you wish you want an area to be filled
 *
 * @return 
 *   resultImg - the image in which a section of pixels with the same color
 *               were filled to another color
 *****************************************************************************/
ImgBgr CFloodfill_try2App::Floodfill(CPoint seedPoint, ImgBgr originImg)
{
	ImgBgr resultImg;
	Bgr fillColor;

	try 
	{
		Bgr oldColor = GetColor(originImg, seedPoint);
		fillColor = GetFillColor(oldColor);
		CPoint imgLmt(originImg.Width(), originImg.Height());
		
		resultImg = originImg;		
		std::stack <CPoint> frontier;
		frontier.push(seedPoint);
		resultImg(seedPoint.x, seedPoint.y) = fillColor ;
		while (!frontier.empty())
		{
			CPoint tmpPnt = frontier.top();
			frontier.pop();
			
			std::vector<CPoint> neighborhood = GetPointsNeighbors(tmpPnt, imgLmt);
			std::vector<CPoint>::iterator it;

			for ( it = neighborhood.begin() ; it < neighborhood.end(); it++ )
			{
				if ( oldColor == GetColor(resultImg, *it) )
				{
					frontier.push(*it);
					resultImg(it->x, it->y) = fillColor ;
				}
				else {}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}

	return resultImg;
}
