
// Floodfill_try2Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "Floodfill_try2.h"
#include "Floodfill_try2Dlg.h"
#include "../src/blepo.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// uncomment exactly one of these
#define G_DIR "..\\..\\"   // normal

// ================> begin local functions (available only to this translation unit)
namespace
{

CString iGetExecutableDirectory()
{
  const char* help_path = AfxGetApp()->m_pszHelpFilePath;
  char* s = const_cast<char*>( strrchr(help_path, '\\') );
  *(s+1) = '\0';
  return CString(help_path);
}

CString iGetImagesDir()
{
  return iGetExecutableDirectory() + G_DIR;
}

};
// ================< end local functions

using namespace blepo;

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CFloodfill_try2Dlg dialog




CFloodfill_try2Dlg::CFloodfill_try2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFloodfill_try2Dlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFloodfill_try2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFloodfill_try2Dlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CFloodfill_try2Dlg::BtnClickLoadFile)
END_MESSAGE_MAP()


// CFloodfill_try2Dlg message handlers

BOOL CFloodfill_try2Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFloodfill_try2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFloodfill_try2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFloodfill_try2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFloodfill_try2Dlg::BtnClickLoadFile()
{
  try 
  {
    CFileDialog dlg(TRUE, // open file dialog
                    NULL, // default extension
                    "quantized.pgm",  // default filename
                    OFN_HIDEREADONLY,  // flags
                    "All image files|*.pgm;*ppm;*.bmp;*.jpg;*.jpeg|PGM/PPM files (*.pgm)|*.pgm;*.ppm|BMP files (*.bmp)|*.bmp|JPEG files (*.jpg,.jpeg)|*.jpg;*.jpeg|All files (*.*)|*.*||",  // filter
                    NULL);
    CString dir = iGetImagesDir() + "images";
	  dlg.m_ofn.lpstrInitialDir = (const char*) dir;
	  dlg.m_ofn.lpstrTitle = "Load image";
    if (dlg.DoModal() == IDOK)
    {
      CString fname      = dlg.GetPathName();
      ImgBgr img;
      Load(fname, &img);
	  bool isGrayScale = blepo::IsGrayscale(img);
      Figure fig("Image loaded");
      fig.Draw(img);
	  CPoint seedPoint = fig.GrabMouseClick();
	  ImgBgr filledImg;
	  Bgr fillColor(0,0,255);

	  filledImg = CFloodfill_try2App::Floodfill(seedPoint, img);
	  Figure figAgain("Modded Image");
	  figAgain.Draw(filledImg);
    }

  } catch (const Exception& e)
  {
    // image failed to load, so notify user
    e.Display();
  }
}
