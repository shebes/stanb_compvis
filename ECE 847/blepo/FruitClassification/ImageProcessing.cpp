/*****************************************************************************
 * ImageProcessing.cpp
 * Description: Holds the implementation of image processing methods developed 
 *              for ECE847 homework assignments.
 *
 * Date: Wednesday September 9, 2009 Matthew's 22nd birthday
 *****************************************************************************/
#include "stdafx.h"
#include "ImageProcessing.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of four to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) originImg - the image wherein an area of pixels will be colored
 * Returns: A blepo color image
 *---------------------------------------------------------------------------*/
ImgBgr ImageProcessing::Floodfill(blepo::Point seedPoint, ImgBgr originImg)
{
	ImgBgr result ;
	
	try 
	{
		Bgr oldColor = Bgr( *originImg.Begin(seedPoint.x, seedPoint.y) );
		Bgr fllColor = Bgr(100, 0, 0);	
		Point imgLmt(originImg.Width(), originImg.Height());
		
		result = originImg;
		result(seedPoint.x, seedPoint.y) = fllColor ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			Point neighbor;

			// get north neighbor
			neighbor = Point( location.x, location.y-1 );
			if ( neighbor.y > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get east neighbor
			neighbor = Point( location.x + 1, location.y );
			if (  neighbor.x < imgLmt.x ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get south neighbor
			neighbor = Point( location.x, location.y + 1 );
			if ( neighbor.y < imgLmt.y ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get west neighbor
			neighbor = Point( location.x - 1, location.y );
			if ( neighbor.x > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}