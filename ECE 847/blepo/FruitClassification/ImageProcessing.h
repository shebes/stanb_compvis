/*****************************************************************************
 * ImageProcessing.h
 * File to contain all the image processing functions developed from the
 * ECE847 projects.
 *****************************************************************************/

#pragma once

#ifndef _IMAGEPROCESSING_H_
#endif // _IMAGEPROCESSING_H_

#include <stack>
#include "../src/blepo.h"

class ImageProcessing
{
public:	
	static blepo::ImgBgr Floodfill(blepo::Point seedPoint, blepo::ImgBgr originImg);
};