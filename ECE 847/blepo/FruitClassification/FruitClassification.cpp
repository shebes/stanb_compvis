// FruitClassification.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FruitClassification.h"
#include "FruitClassificationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

// CFruitClassificationApp

BEGIN_MESSAGE_MAP(CFruitClassificationApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFruitClassificationApp construction

CFruitClassificationApp::CFruitClassificationApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CFruitClassificationApp object

CFruitClassificationApp theApp;

// CFruitClassificationApp initialization

BOOL CFruitClassificationApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CFruitClassificationDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

/*---------------------------------------------------------------------------*
 * This method belongs in a different file, a utilities file.
 *---------------------------------------------------------------------------*/
char *        CFruitClassificationApp::getLocalTime()
{
	time_t theTime;
	time( &theTime );
	tm *t = localtime( &theTime );
	return asctime(t);
}

/***/
void CFruitClassificationApp::BinaryColor(const ImgInt& img, ImgBgr* out)
{
  FILE *fout = fopen( "binarycolor.txt", "w" );
  // convert to black and white
  out->Reset(img.Width(), img.Height());
  ImgInt::ConstIterator p;
  ImgBgr::Iterator q;
  int i = 0 ;
  for (p = img.Begin(), q = out->Begin() ; p != img.End() ; p++, q++)
  {
	  fprintf(fout, "[%6d] %3d\n", ++i, *p);
	  // There is no significance to the integer here
	  //*q = ImgBgr::Pixel((*p * 12342223) % 0xFFFFFF, Bgr::BLEPO_BGR_XBGR);
  }
  fclose(fout);
}
/*---------------------------------------------------------------------------*
 * Method creates a normalized histogram from a gray scale image.
 * 
 * Parameters:
 * (+) img - the image for which we will calculate a normalized histogram.
 * Returns: A vector of type float containing the normalized histogram.
 *---------------------------------------------------------------------------*/
vector<float> CFruitClassificationApp::CreateNormHist(ImgBgr img)
{
	// integer variables
	int nPixels = 0 ;
	int hist[HIST_MAX];

	// containers
	vector<float> resultHist(HIST_MAX, 0.0);

	// input/output
	FILE *fout = fopen("ImgHistogram.txt", "w");

	try 
	{
		for ( int i = 0 ; i < HIST_MAX ; i++ )
		{
			hist[i] = 0;
		}

		// create initial histogram and count the number of pixels
		ImgBgr::Iterator it ;
		for ( it = img.Begin() ; it != img.End() ; it++ )
		{
			hist[it->ToGray()]++;
			nPixels++;
		}
		
		fprintf( fout, "%s\n", getLocalTime() );
		fprintf( fout, "There are %d pixels in the loaded image.\n", nPixels );
		
		float sum = 0 ;
		// create normalized histogram
		for ( int i = 0 ; i < HIST_MAX ; i++ )
		{
			resultHist.at(i) = (float)hist[i] / (float)nPixels;
			sum += resultHist.at(i);
			fprintf( fout, "[%3d] %f\n", i, resultHist.at(i) );
		}
		fprintf( fout, "Average normalized value %f", (sum/HIST_MAX) );
		fclose(fout);

	} catch (Exception exp)
	{
		exp.Display();
	}

	return resultHist;
}


/*---------------------------------------------------------------------------*
 * Description: Takes an image and uses its cumulative gray scale histogram
 *              to turn it into a binary image using the passed in threshold.
 *
 * Parameters:
 * [+] img - A Bgr image we want to see as a binary image
 * [+] threshold - A value that differentiates the foreground from the 
 *                 background in a gray scale image
 * Return: A binary image based on the passed in threshold value
 *---------------------------------------------------------------------------*/
ImgBgr        CFruitClassificationApp::CreateBinaryImgUsingThreshold(ImgBgr img, float threshold)
{
	ImgBgr result = img;

	try 
	{
		vector<float> cumuHist = CreateCumuHist(img);

		ImgBgr::Iterator it = result.Begin();
		while( it != result.End() )
		{
			if ( cumuHist[it->ToGray()] > threshold ) 
			{
				it->FromInt( HIST_MAX-1, Bgr::BLEPO_BGR_GRAY); // HIST_MAX-1 is white
			}
			else
			{
				it->FromInt( 0, Bgr::BLEPO_BGR_GRAY); // 0 is black
			}

			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}

	return result;
}

/*---------------------------------------------------------------------------*
 * Method creates a cumulative histogram from a gray scale image.
 * 
 * Parameters:
 * (+) img - the image for which a cumulative histogram will be made
 * Returns: A vector of type float containing the cumulative histogram.
 *---------------------------------------------------------------------------*/
vector<float> CFruitClassificationApp::CreateCumuHist(ImgBgr img)
{
	return CreateCumuHist( CreateNormHist(img) ) ;
}


/*---------------------------------------------------------------------------*
 * Method creates a cumulative histogram from a gray scale image.
 * 
 * Parameters:
 * (+) imgHist - the normalized histogram for an image
 * Returns: A vector of type float containing the cumulative histogram.
 *---------------------------------------------------------------------------*/
vector<float> CFruitClassificationApp::CreateCumuHist(vector<float> imgHist)
{
	// container
	vector<float> resultHist(HIST_MAX, 0.0);

	// input/output
	FILE *fout = fopen("ImgCumulativeHistogram.txt", "w");

	try 
	{
		fprintf( fout, "%s\n", getLocalTime() );
		for ( int idx = 0 ; idx < HIST_MAX ; idx++ )
		{
			float cumVal = 0.0;
			if ( idx == 0 )
			{
				cumVal = 0.0;
			}
			else 
			{
				cumVal = resultHist.at(idx-1);
			}
			resultHist.at(idx) = imgHist.at(idx) + cumVal;
			fprintf( fout, "[%3d] %f\n", idx, resultHist.at(idx) );
		}
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}

	return resultHist;
}


/*----------------------------------------------------------------------------*
 * Take a cumulative histogram of an image and transform the image.
 *
 * Parameters:
 * (*) img - A color image
 * Return: A transformed color image.
 *----------------------------------------------------------------------------*/
ImgBgr        CFruitClassificationApp::TransformImageToBgr(ImgBgr img)
{
	ImgBgr result;

	vector<float> cumuHist = CreateCumuHist(img);
	
	try 
	{
		ImgBgr::Iterator it ;
		for ( it = img.Begin() ; it != img.End() ; it++ )
		{
			it->FromInt( (int)((HIST_MAX-1) * cumuHist.at( it->ToGray() )), Bgr::BLEPO_BGR_GRAY);
		}

		result = img;
	} catch(Exception exp)
	{
		exp.Display();
	}

	return  result;
}


/*----------------------------------------------------------------------------*
 * Take a cumulative histogram of an image and transform the image.
 *
 * Parameters:
 * (*) img - A color image
 * Return: A transformed gray image.
 *----------------------------------------------------------------------------*/
ImgGray       CFruitClassificationApp::TransformImageToGray(ImgBgr img)
{
	ImgGray result( img.Width(), img.Height() );
	ImgBgr transformed = TransformImageToBgr(img);

	try 
	{
		ImgBgr::Iterator it = transformed.Begin();
		ImgGray::Iterator grayIt = result.Begin();
		while ( it != transformed.End() )
		{
			*grayIt = it->ToGray() ;
			it++;
			grayIt++;
		}
	} catch( Exception exp )
	{
		exp.Display();
	}

	return  result ;
}


/*---------------------------------------------------------------------------*
 * Method creates a vector-vector of integers containing an images graylevel
 * sums calculated using the rules for a co-occurrence matrix.  It is best
 * to calculate the co-occurrence matrix using the original image.
 * 
 * Parameters:
 * (+) img - the image for which you want the co-occurrence matrix calculated
 * Returns: A vector-vector of integers
 *---------------------------------------------------------------------------*/
vector<vector<int>>  CFruitClassificationApp::CalculateCoOccurrenceMatrix(ImgBgr img)
{
	vector<int> internalVector(HIST_MAX,0);
	vector<vector<int>> glcm(HIST_MAX, internalVector);

	try 
	{
		ImgBgr::Iterator it;
		for( int i = 0 ; i < img.Width() ; i++ )
		{
			for( int j = 0 ; j < img.Height() ; j++ )
			{
				it = img.Begin(i,j);
				int glx = it->ToGray();
				int glyRight, glyDown;

				if ( i + 1 < img.Width() )
				{
					it = img.Begin(i+1, j);
					glyRight = it->ToGray();
					if ( glyRight > 0 )
					{
						glcm.at(glx).at(glyRight)++;
					}
				}

				if ( j + 1 < img.Height() )
				{
					it = img.Begin(i, j+1);
					glyDown = it->ToGray();
					if ( glyDown > 0 )
					{
						glcm.at(glx).at(glyDown)++;
					}
				}
			}
		}
		//PrintIntVVToFile("IntVVOut.txt", glcm);

	} catch(Exception exp)
	{
		exp.Display();
	}

	return glcm;
}


/*---------------------------------------------------------------------------*
 * Method prints the data in an int vector-vector to a file.
 * 
 * Parameters:
 * (+) glcm - the co-occurrence matrix calculated for a graylevel image
 * Returns: 
 *---------------------------------------------------------------------------*/
void          CFruitClassificationApp::PrintIntVVToFile(CString name, std::vector<std::vector<int>> glcm)
{
	FILE *fout = fopen(name, "w");

	try 
	{
		vector<vector<int>>::iterator iteratorOne = glcm.begin();
		while( iteratorOne != glcm.end() )
		{
			int index = 0 ;
			vector<int>::iterator iteratorTwo = iteratorOne->begin();
			while( iteratorTwo != iteratorOne->end() )
			{
				fprintf(fout, "[%3d:%3d] ", index, *iteratorTwo);
				//fprintf(fout, "[%3d] ", *iteratorTwo);
				iteratorTwo++;
				index++;
			}
			fprintf(fout, "\n");
			iteratorOne++;
		}
		fclose(fout);
	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image and dilates it based on a
 *              3x3 matrix with a color value that is passed in as a 
 *              parameter.
 *
 * Parameters: 
 * (+) img - A binary image
 * (+) matchingValue - An integer of 0 or 255, the color value of the pixels 
 *                     in the 3x3 matrix.
 * Return value: A dilated binary image
 *---------------------------------------------------------------------------*/
ImgBgr        CFruitClassificationApp::DilateImage(ImgBgr img, int matchingValue, int numTimes)
{
	ImgBgr result = img;
	try
	{
		int nonMatchingValue = ( matchingValue == 0 )? HIST_MAX-1 : 0 ;
		while ( numTimes-- > 0 ) {
			for ( int y = 0 ; y < img.Height() ; y++ )
			{
				for ( int x = 0 ; x < img.Width() ; x++ ) 
				{
					if ( x-1 > 0 && x+1 < img.Width() &&
						 y-1 > 0 && y+1 < img.Height() )
					{
						ImgBgr::Iterator it5 = result.Begin(  x,  y);
						ImgBgr::Iterator it6 = result.Begin(x+1,  y);
						ImgBgr::Iterator it7 = result.Begin(x-1,y+1);
						ImgBgr::Iterator it8 = result.Begin(  x,y+1);
						ImgBgr::Iterator it9 = result.Begin(x+1,y+1);
						if ( it5->ToGray() == matchingValue || 
							 it6->ToGray() == matchingValue || 
							 it7->ToGray() == matchingValue || 
							 it8->ToGray() == matchingValue || 
							 it9->ToGray() == matchingValue )
						{
							it5->FromInt( matchingValue, Bgr::BLEPO_BGR_GRAY );
						} else
						{
							it5->FromInt( nonMatchingValue, Bgr::BLEPO_BGR_GRAY );
						}
					}
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image and erodes it based on a
 *              3x3 matrix with a color value that is passed in as a 
 *              parameter.
 *
 * Parameters: 
 * (+) img - A binary image
 * (+) matchingValue - An integer of 0 or 255, the color value of the pixels 
 *                     in the 3x3 matrix.
 * Return value: An eroded binary image
 *---------------------------------------------------------------------------*/
ImgBgr        CFruitClassificationApp::ErodeImage(ImgBgr img, int matchingValue, int numTimes)
{
	ImgBgr result = img;
	try
	{
		int nonMatchingValue = ( matchingValue == 0 )? HIST_MAX-1 : 0 ;
		while ( numTimes-- > 0 )
		{
			for ( int y = 0 ; y < img.Height() ; y++ )
			{
				for ( int x = 0 ; x < img.Width() ; x++ ) 
				{
					if ( x-1 > 0 && x+1 < img.Width() &&
						 y-1 > 0 && y+1 < img.Height() )
					{
						ImgBgr::Iterator it5 = result.Begin(  x,  y);
						ImgBgr::Iterator it6 = result.Begin(x+1,  y);
						ImgBgr::Iterator it7 = result.Begin(x-1,y+1);
						ImgBgr::Iterator it8 = result.Begin(  x,y+1);
						ImgBgr::Iterator it9 = result.Begin(x+1,y+1);
						if ( it5->ToGray() == matchingValue && 
							 it6->ToGray() == matchingValue && 
							 it7->ToGray() == matchingValue && 
							 it8->ToGray() == matchingValue && 
							 it9->ToGray() == matchingValue )
						{
							it5->FromInt( matchingValue, Bgr::BLEPO_BGR_GRAY );
						} else
						{
							it5->FromInt( nonMatchingValue, Bgr::BLEPO_BGR_GRAY );
						}
					}
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}


/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void  CFruitClassificationApp::MergeThresholdedImages(ImgBgr * htImg, ImgBgr * ltImg, ImgBinary * out, map<int, vector<Point>> * allPoints, map<int, vector<double>> * momentsMap)
{
	ImgBgr result;
	ImgInt labels, labelsB;
	vector<ConnectedComponentProperties <ImgBgr::Pixel>> ccProps, ccPropsB;
	try 
	{
		ImgBgr openClosedImg  = DilateImage(ErodeImage(*htImg, HIST_MAX-1, 3), HIST_MAX-1, 3);
		ImgBgr openClosedImgB = DilateImage(ErodeImage(*ltImg, HIST_MAX-1, 3), HIST_MAX-1, 3);
		int nLabels  = ConnectedComponents4(openClosedImg, &labels, &ccProps);
		int nLabelsB = ConnectedComponents4(openClosedImgB, &labelsB, &ccPropsB);

		vector<int> validLabels = MergeImages( labels, labelsB, out ) ;
		map<int, vector<double>> momentMap = CalculateMoments( validLabels, labelsB );		
		*momentsMap = momentMap;

		vector<int>::iterator labelit;
		map<int, vector<Point>> chainedPoints;
		for ( labelit = validLabels.begin() ; labelit != validLabels.end() ; labelit++ )
		{
			vector<Point> chain;
			WallFollow(labelsB, *labelit, &chain) ;
			chainedPoints[*labelit] = chain;
		}
		*allPoints = chainedPoints;
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method creates a composite image from two thresholded 
 *              images in order to create a binary image that clearly 
 *              delineates between the image background and foreground.
 * 
 * Parameters:
 * [+] htImg - an integer image that was given a high threshold value to 
 *             discriminate between the images background and foreground
 * [+] ltImg - an integer image that was given a low threshold value to 
 *             discriminate between the images background and foreground
 * [+] out - a binary image that represents the merging of the 2 thresholded 
 *           images
 * Returns: A vector containing the labels that are found in both the high
 *          and low thresholded images.
 *---------------------------------------------------------------------------*/
vector<int>   CFruitClassificationApp::MergeImages(const ImgInt& htImg, const ImgInt& ltImg, ImgBinary *out)
{
	vector<int> validLabels;

	try
	{
		FILE *fout = fopen("SimilarLabels.txt", "w");
		out->Reset( ltImg.Width(), htImg.Height() );
		ImgInt::ConstIterator p, q ;
		int i = 0;
		for ( p = ltImg.Begin(), q = htImg.Begin() ; p != ltImg.End() ; p++, q++ )
		{
			if ( *p > 0 && *q > 0 )
			{
				if ( validLabels.empty() ) 
				{
					fprintf( fout, "[%5d] %3d\n", ++i, *p);
					validLabels.push_back(*p);
				} 
				else if ( !IsInVector(*p, validLabels) )
				{
					fprintf( fout, "[%5d] %3d\n", ++i, *p);
					validLabels.push_back(*p);
				}
			}
		}
		ImgBinary::Iterator r;
		for ( p = ltImg.Begin(), r = out->Begin() ; p != ltImg.End() ; p++, r++ )
		{
			if ( IsInVector(*p, validLabels) )
			{
				*r = ImgBinary::Pixel(true);
			}
			else
			{
				*r = ImgBinary::Pixel(false);
			}
		}
		fclose(fout);
	} 
	catch (Exception exp) 
	{
		exp.Display();
	}

	return validLabels;
}


/*---------------------------------------------------------------------------*
 * Description: It tests whether or not an element is present in a vector.
 *              A function that works in linear time.  
 * 
 * Parameters:
 * [+] element - an integer that you want to test to see whether or not it is
 *               in the passed in vector
 * [+] data - the vector against which you will test for the presence of an 
 *            element
 * Return: A boolean that says whether or not the element is in the passed in
 *         vector.
 *---------------------------------------------------------------------------*/
bool CFruitClassificationApp::IsInVector( int element, vector<int> data )
{
	bool result = false;
	try 
	{
		vector<int>::iterator it;
		for ( it = data.begin() ; it != data.end() ; it++ )
		{
			if ( element == *it )
			{
				result = true;
				break;
			}
		}
	} 
	catch (Exception exp)
	{
		exp.Display();
	}
	return result ;
}


/**
 **/
map<int,vector<double>>    CFruitClassificationApp::CalculateMoments( vector<int> validLabels, const ImgInt& labelsB )
{
	map<int,vector<double>> momentMap;
	try 
	{
		// create the empty moment map
		vector<double> moments(6, 0.0);
		vector<int>::iterator it;
		for ( it = validLabels.begin() ; it != validLabels.end() ; it++ )
		{
			momentMap[*it] = moments;
		}

		FILE *fout = fopen("momentslist.out", "w");
		for ( int y = 0 ; y < labelsB.Height() ; y++ )
		{
			for ( int x = 0 ; x < labelsB.Width() ; x++ )
			{
				int label = *labelsB.Begin(x, y);
				if ( IsInVector( label, validLabels) )
				{
					momentMap[label].at(0) = momentMap[label].at(0) + 1;
					momentMap[label].at(1) = momentMap[label].at(1) + (double)x;
					momentMap[label].at(2) = momentMap[label].at(2) + (double)y;
					momentMap[label].at(3) = momentMap[label].at(3) + ((double)x * (double)y);
					momentMap[label].at(4) = momentMap[label].at(4) + ((double)x * (double)x);
					momentMap[label].at(5) = momentMap[label].at(5) + ((double)y * (double)y);
				}
			}
		}

		map<int,vector<double>>::iterator mapit;
		for( mapit = momentMap.begin() ; mapit != momentMap.end() ; mapit++ )
		{
			vector<double> moments = mapit->second ;
			vector<double>::iterator miter;
			fprintf( fout, "[%2d] ", mapit->first ) ;
			int i = 0 ;
			for( miter = moments.begin() ; miter != moments.end() ; miter++ )
			{
				fprintf( fout, "(Moment%d %10.0f) ", i++, *miter );
			}
			fprintf( fout, "[(Xc,Yc) (%5.5f,%5.5f)]", moments.at(1)/moments.at(0), moments.at(2)/moments.at(0) );
			fprintf( fout, "\n" );
		}
		fclose(fout);
	} 
	catch (Exception exp)
	{
		exp.Display();
	}

	return momentMap;
}

/**
 **/
void     CFruitClassificationApp::DrawBorders( ImgBgr * img, const map<int, vector<Point>>& allPoints, map<int, eigeninfo> ei )
{
	map<int, vector<Point>>::const_iterator allit;
	for( allit = allPoints.begin() ; allit != allPoints.end() ; allit++ )
	{
		Bgr borderColor = Bgr::RED;
		eigeninfo ein = ei[allit->first];
		// if eccentrictiy is greater than 2.5 you have a banana
		if ( ein.eccentricity > 2.5 )
		{
			borderColor = Bgr::YELLOW;
		} else if ( ein.chain.size() < 300 )
		{
			borderColor = Bgr::RED;
		} else if ( ein.chain.size() > 300 && ein.chain.size() < 500 ) 
		{
			borderColor = Bgr::GREEN;
		}
		Point centroid(ein.xc, ein.yc);

		Point crossAOne( ein.xc + 5, ein.yc) ;
		Point crossATwo( ein.xc - 5, ein.yc) ;
		Point crossBOne( ein.xc, ein.yc + 5) ;
		Point crossBTwo( ein.xc, ein.yc - 5) ;
		DrawLine( crossAOne, crossATwo, img, Bgr::BLUE );
		DrawLine( crossBOne, crossBTwo, img, Bgr::BLUE );

		Point offCentroidA(ein.xc+ein.majorx, ein.yc+ein.majory);		
		Point offCentroidB(ein.xc-ein.majorx, ein.yc-ein.majory);
		Point offCentroidTwoA(ein.xc+ein.minorx, ein.yc+ein.minory);
		Point offCentroidTwoB(ein.xc-ein.minorx, ein.yc-ein.minory);

		DrawLine( offCentroidA, offCentroidB, img, Bgr::CYAN );
		DrawLine( offCentroidTwoA, offCentroidTwoB, img, Bgr::CYAN );
		vector<Point> tempVector = allit->second;
		vector<Point>::iterator pointit ;
		for ( pointit = tempVector.begin() ; pointit != tempVector.end() ; pointit++ )
		{
			Point tempPoint = *pointit;
			*(img->Begin(tempPoint.x, tempPoint.y)) =  borderColor;
		}
	}
	Figure fig("Outlined image") ;
	fig.Draw(*img);
}

/**
 **/
void CFruitClassificationApp::CalculateEigenValues(map<int, eigeninfo> * ei, const map<int, vector<double>>& momentMap, map<int, vector<Point>> chainPoints)
{
	try 
	{
		FILE *fout = fopen("eigen.out", "w");
		map<int, eigeninfo> eiginfo;

		map<int, vector<double>>::const_iterator mit;
		map<int, eigeninfo>::iterator eit;

		for ( mit = momentMap.begin() ; mit != momentMap.end() ; mit++ )
		{
			eigeninfo ein;
			
			ein.chain = chainPoints[mit->first] ;

			ein.m00 = mit->second.at(0);
			ein.m10 = mit->second.at(1);
			ein.m01 = mit->second.at(2);
			ein.m11 = mit->second.at(3);
			ein.m20 = mit->second.at(4);
			ein.m02 = mit->second.at(5);

			double xc = mit->second.at(1) / mit->second.at(0);
			double yc = mit->second.at(2) / mit->second.at(0);
			double mu00 = ein.mu00 = mit->second.at(0);
			double mu10 = ein.mu10 = 0.0;
			double mu01 = ein.mu01 = 0.0;
			double mu11 = ein.mu11 = mit->second.at(3) - ( yc * mit->second.at(1) );
			double mu20 = ein.mu20 = mit->second.at(4) - ( xc * mit->second.at(1) );
			double mu02 = ein.mu02 = mit->second.at(5) - ( yc * mit->second.at(2) );

			ein.xc = xc;
			ein.yc = yc;

			// calculate direction using moment info
			// y = 2mu11
			// x = mu20 - mu02
			double y = 2 * mu11;
			double x = mu20 - mu02;
			ein.direction = 0.5 * atan2(y, x) ;

			// a = mu20+mu02
			// b = 4m11^2
			// x = mu20-mu02
			// c = x^2
			// d = b + c
			// e = sqrt(d)
			// f = a + e
			// g = a - e
			double a = mu20 + mu02 ;
			double b = 4 * pow(mu11, 2);
			double c = pow( x, 2 );
			double d = b + c ;
			double e = sqrt(d);
			double f = a + e ;
			double g = a - e ;

			ein.lambdamax = sqrt(f);
			ein.lambdamin = sqrt(g);
			ein.eccentricity = ein.lambdamax / ein.lambdamin ;			
			ein.compactness = (4 * 3.14 * ein.m00) / ein.chain.size();

			ein.eigenmax = (1 / (2*mit->second.at(0))) * f;
			ein.eigenmin = (1 / (2*mit->second.at(0))) * g;
			
			// calculate major axis directions for line drawing
			ein.majorx = sqrt(ein.eigenmax) * cos(ein.direction);
			ein.majory = sqrt(ein.eigenmax) * sin(ein.direction);

			// calculate minor axis directions for line drawing
			ein.minorx = - ( sqrt(ein.eigenmin) * sin(ein.direction) );
			ein.minory = sqrt(ein.eigenmin) * cos(ein.direction);

			eiginfo[mit->first] = ein;
			fprintf( fout, "[Label %2d] [eccentricity %5.3f] [direction %5.3f]\n", mit->first, ein.eccentricity, ein.direction);
		}
		*ei = eiginfo;
		fclose(fout);
	} catch(Exception exp) { exp.Display() ; }
}
