// FruitClassificationDlg.cpp : implementation file
//

#include "stdafx.h"
//#include "FruitClassification.h"
#include "FruitClassificationDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// uncomment exactly one of these
#define G_DIR "..\\..\\"   // normal

// ================> begin local functions (available only to this translation unit)
namespace
{

	CString iGetExecutableDirectory()
	{
		const char* help_path = AfxGetApp()->m_pszHelpFilePath;
		char* s = const_cast<char*>( strrchr(help_path, '\\') );
		*(s+1) = '\0';
		return CString(help_path);
	}

	CString iGetImagesDir()
	{
		return iGetExecutableDirectory() + G_DIR;
	}

};
// ================< end local functions

using namespace blepo;
using namespace std;


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CFruitClassificationDlg dialog




CFruitClassificationDlg::CFruitClassificationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFruitClassificationDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFruitClassificationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFruitClassificationDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON2, &CFruitClassificationDlg::OnBtnClickFloodfill)
	ON_BN_CLICKED(IDC_BUTTON1, &CFruitClassificationDlg::OnBtnClickLoadImage)
END_MESSAGE_MAP()


// CFruitClassificationDlg message handlers

BOOL CFruitClassificationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CFruitClassificationDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFruitClassificationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFruitClassificationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CFruitClassificationDlg::OnBtnClickFloodfill()
{
	try 
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"quantized.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			"All image files|*.pgm;*ppm;*.bmp;*.jpg;*.jpeg|PGM/PPM files (*.pgm)|*.pgm;*.ppm|BMP files (*.bmp)|*.bmp|JPEG files (*.jpg,.jpeg)|*.jpg;*.jpeg|All files (*.*)|*.*||",  // filter
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			Load(fname, &img);
			Figure fig("Selction loaded");
			fig.Draw(img);
			Point seedPoint = fig.GrabMouseClick();
			ImgBgr filledImg ;
			filledImg = ImageProcessing::Floodfill(seedPoint, img);
			Figure figAgain("Flood Filled Image");
			figAgain.Draw(filledImg);
		}

	} catch (const Exception& e)
	{
		// image failed to load, so notify user
		e.Display();
	}
}

void CFruitClassificationDlg::OnBtnClickLoadImage()
{
	try 
	{
		CFileDialog dlg(TRUE, // open file dialog
			NULL, // default extension
			"quantized.pgm",  // default filename
			OFN_HIDEREADONLY,  // flags
			"All image files|*.pgm;*ppm;*.bmp;*.jpg;*.jpeg|PGM/PPM files (*.pgm)|*.pgm;*.ppm|BMP files (*.bmp)|*.bmp|JPEG files (*.jpg,.jpeg)|*.jpg;*.jpeg|All files (*.*)|*.*||",  // filter
			NULL);
		CString dir = iGetImagesDir() + "images";
		dlg.m_ofn.lpstrInitialDir = (const char*) dir;
		dlg.m_ofn.lpstrTitle = "Load image";
		if (dlg.DoModal() == IDOK)
		{
			CString fname      = dlg.GetPathName();
			ImgBgr img;
			ImgBinary mergedImg;
			map<int, eigeninfo> ei;
			map<int, vector<Point>> allPoints;
			map<int, vector<double>> momentMap;
			
			Load(fname, &img);
			Figure fig(dlg.GetFileTitle() + " loaded");
			fig.Draw(img);

			// use high threshold on image
			ImgBgr highThresholdedImg = CFruitClassificationApp::CreateBinaryImgUsingThreshold(img, 0.85);			
			// use low threshold on image
			ImgBgr lowThresholdImg = CFruitClassificationApp::CreateBinaryImgUsingThreshold(img, 0.68);

			// merge and display the two thresholded images
			CFruitClassificationApp::MergeThresholdedImages(&highThresholdedImg, &lowThresholdImg, &mergedImg, &allPoints, &momentMap);
			Figure figMerged(dlg.GetFileTitle() + " binary merged image");
			figMerged.Draw(mergedImg);

			CFruitClassificationApp::CalculateEigenValues(&ei, momentMap, allPoints);
			CFruitClassificationApp::DrawBorders( &img, allPoints, ei );
			
			CString str;
			map<int, eigeninfo>::iterator it;
			for ( it = ei.begin() ; it != ei.end() ; it++ )
			{
				str.Format("Properties of region:\r\n"
					"  area (number of pixels):  %10.0f\r\n"
					"  centroid:  (%5.1f, %5.1f)\r\n"
					"  compactness:  [%1.5f]\r\n"
					"  direction (clockwise from horizontal):  %5.5f radians\r\n"
					"  eccentricity:  %5.5f\r\n"
					"  moments:\r\n"
					"    m00:  %10.3f\r\n"
					"    m10:  %10.3f\r\n"
					"    m01:  %10.3f\r\n"
					"    m11:  %10.3f\r\n"
					"    m20:  %10.3f\r\n"
					"    m02:  %10.3f\r\n"
					"  centralized moments:\r\n"
					"    mu10:  %10.3f\r\n"
					"    mu01:  %10.3f\r\n"
					"    mu11:  %10.3f\r\n"
					"    mu20:  %10.3f\r\n"
					"    mu02:  %10.3f\r\n",
					it->second.m00, 
					it->second.xc, it->second.yc,
					it->second.compactness,
					it->second.direction,
					it->second.eccentricity,
					it->second.m00, it->second.m10, it->second.m01, it->second.m11, it->second.m20, it->second.m02,
					it->second.mu10, it->second.mu01, it->second.mu11, it->second.mu20, it->second.mu02);
			  AfxMessageBox(str, MB_ICONINFORMATION);
			}

			// do this for test, created for 
			CFruitClassificationApp::CalculateCoOccurrenceMatrix(img);
		}

	} catch (const Exception& e)
	{
		// image failed to load, so notify user
		e.Display();
	}
}
