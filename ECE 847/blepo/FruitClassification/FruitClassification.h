// FruitClassification.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <cstdio>
#include <cmath>
#include <ctime>
#include <stack>
#include <vector>
#include <map>
#include "resource.h"		// main symbols
#include "stdafx.h"
#include "ImageProcessing.h"

#define HIST_MAX 256

struct eigeninfo
{
	std::vector<blepo::Point> chain;
	double compactness;
	double eigenmax;
	double eigenmin;
	double lambdamax;
	double lambdamin;
	double eccentricity;
	double direction;
	double xc;
	double yc;
	double m00;
	double m10;
	double m01;
	double m11;
	double m20;
	double m02;
	double mu00;
	double mu10;
	double mu01;
	double mu11;
	double mu20;
	double mu02;
	double minorx;
	double minory;
	double majorx;
	double majory;
};


// CFruitClassificationApp:
// See FruitClassification.cpp for the implementation of this class
//

class CFruitClassificationApp : public CWinApp
{
public:
	CFruitClassificationApp();

// Overrides
	public:
	virtual BOOL InitInstance();
	static char * getLocalTime();
	static bool IsInVector(int element, std::vector<int> data);
	static std::vector<float> CreateNormHist(blepo::ImgBgr img);
	static std::vector<float> CreateCumuHist(blepo::ImgBgr img);
	static std::vector<float> CreateCumuHist(std::vector<float> imgHist);
	static blepo::ImgGray     TransformImageToGray(blepo::ImgBgr img);
	static blepo::ImgBgr      TransformImageToBgr(blepo::ImgBgr img);
	static blepo::ImgBgr      ThresholdTest(blepo::ImgBgr img, float threshold);
	static blepo::ImgBgr      CreateBinaryImgUsingThreshold(blepo::ImgBgr img, float threshold);
	static std::vector<std::vector<int>> CalculateCoOccurrenceMatrix(blepo::ImgBgr img);
	static blepo::ImgBgr      DilateImage(blepo::ImgBgr img, int matchingValue, int numTimes);
	static blepo::ImgBgr      ErodeImage(blepo::ImgBgr img, int matchingValue, int numTimes);
	static std::vector<int>   MergeImages(const blepo::ImgInt& htImg, const blepo::ImgInt& ltImg, blepo::ImgBinary *out);
	static void               PrintIntVVToFile(CString name, std::vector<std::vector<int>> glcm);
	static void               BinaryColor(const blepo::ImgInt& img, blepo::ImgBgr* out);
	static blepo::Point       Calculate2DMoments(blepo::ImgBgr *img, int *m00, int *m10, int *m01, int *m11, int *m20, int *m02);
	static std::map<int, std::vector<double>> CalculateMoments( std::vector<int> validLabels, const blepo::ImgInt& labelsB );
	static void MergeThresholdedImages(blepo::ImgBgr * htImg, blepo::ImgBgr * ltImg, blepo::ImgBinary *out, std::map<int, std::vector<blepo::Point>> * allPoints, std::map<int, std::vector<double>> * momentsMap);
	static void               DrawBorders(blepo::ImgBgr * img, const std::map<int, std::vector<blepo::Point>> & allPoints, std::map<int, eigeninfo> ei);
	static void               CalculateEigenValues( std::map<int, eigeninfo> * ei, const std::map<int, std::vector<double>>& momentMap, std::map<int, std::vector<blepo::Point>> chainPoints );
// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CFruitClassificationApp theApp;