/*****************************************************************************
 * WatershedSegmenter.cpp
 *
 * Date: Tuesday October 6, 2009
 *****************************************************************************/
#include "stdafx.h"
#include "WatershedSegmenter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
WatershedSegmenter::WatershedSegmenter( ImgBgr img, CString imageTitle, double sigma )
{
	Initialize( img, imageTitle, sigma );
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameters:
 * [+] img        - original color image on which segmentation will be 
 *                  performed
 * [+] imageTitle - name of the original image
 * [+] sigma      - the standard deviation, used for creating the gradient
 *                  magnitude images
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::Initialize( ImgBgr img, CString imageTitle, double sigma )
{
	try 
	{		
		if ( sigma > 0.0 )
		{			
			m_sigma         = sigma;
			m_originalImage = img;
			m_imageTitle    = imageTitle;
		
			ImageProcessing::CalculateGaussianInfo( &m_gi, m_sigma );

			// regular image variables
			ImgInt chamImg, probabilityMatrix;
			ImgBinary elImg;
			ImgFloat gx, gy, gmag, gdir, nmsImg;
			
			getImageProcessor().MakeGradientImageYXPrime(   &m_gi,   img,     &gx );
			getImageProcessor().MakeGradientImageXYPrime(   &m_gi,   img,     &gy );

			getImageProcessor().CalculateGradientMagnitude( gx,     gy,     &gmag );

			// convert input image to its graylevel representation
			m_constrainedGradientMagnitude.Reset( img.Width(), img.Height() );
			m_labels.Reset( img.Width(), img.Height() );
			ImageProcessing::ConvertFG( gmag, &m_constrainedGradientMagnitude );
			ImageProcessing::InitializeImgInt( &m_labels, -1 );

			ComputeGrayLevelPixelMap(m_constrainedGradientMagnitude);
			// set globallabel to 0
			SetGlobalLevel(0);
		}
	} catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: Makes a map of the different pixels for each gray level for
 *              the image passed in as a paramter.
 *
 * Parameters:
 * [+] img - A gray level image, most likely a gradient magnitude image for
 *           which the gray level values are calculated to help improve the
 *           calculation speed for watershed segmentation.
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::ComputeGrayLevelPixelMap(ImgGray &img)
{
	try
	{
		// create a vector containing empty Point vectors
		vector<Point> pixelList;
		vector< vector< Point > > gryLvlPxlMp( HIST_MAX, pixelList );

		// set this to the member variable for the gray level pixel map
		m_grayLevelPixelMap = gryLvlPxlMp;

		// Create pixel list for each graylevel
		int x, y ;
		for( y = 0 ; y < img.Height() ; y++ )
		{
			for( x = 0 ; x < img.Width() ; x++ )
			{
				Point crrntPxl(x,y);
				m_grayLevelPixelMap.at( img(x,y) ).push_back(crrntPxl);
			}
		}

		// Print contents of pixel map to file
		getImageProcessor().ToFilePointVectorVector( &m_grayLevelPixelMap, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\pixelmap.out" );
	} catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: Method to choose between regular and marker-based watershed 
 *              segmentation.
 * 
 * Parameter(s):
 * [+] segmentationType - an integer for the type of segmentation algorithm 
 *                        to run on an image: 1 for normal watershed and
 *                        2 for marker-based watershed
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::SegmentImage( int segmentationType )
{
	try
	{
		switch ( segmentationType )
		{
		case 1:
			WatershedSegmentation(m_constrainedGradientMagnitude, false);
			break;
		case 2:
			MarkerBasedSegmentation();
			break;
		default:
			break;
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Watershed segmentation algorithm that can do marker-based
 *              segmentation as well.
 * 
 * Parameter(s):
 * [+] img - image to be segmented
 * [+] useMarkers - if set to true marker-based watershed segmentation is used
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::WatershedSegmentation(ImgGray &img, bool useMarkers)
{	
	// precompute array of pixel lists for each graylevel
	//ComputeGrayLevelPixelMap( m_constrainedGradientMagnitude );
	// set label to -1 for all pixels
	m_labels.Reset( img.Width(), img.Height() );
	ImageProcessing::InitializeImgInt( &m_labels, -1 );
	SetGlobalLevel(0);

	for ( int i = 0 ; i < HIST_MAX ; i++ )
	{
		vector<Point> gVector = getMap().at(i);
		vector<Point>::iterator it;
		Point maxXY( img.Width(), img.Height() );
		queue<Point> neighbors;
		////// Begin PART A
		it = gVector.begin() ;
		while ( it != gVector.end() )
		{
			getImageProcessor().GetNeighbors4( *it, maxXY, &neighbors );
			while ( !neighbors.empty() )
			{
				Point neighbor = getImageProcessor().PopQueue(&neighbors);
				if ( m_labels(neighbor.x, neighbor.y) >= 0 )
				{
					m_labels(it->x, it->y) = m_labels(neighbor.x, neighbor.y);
					pushFrontier(*it);
					break;
				}
			}

			it++;
		}
		////// End PART A

		////// Begin PART B
		while ( !getFrontier().empty() )
		{
			Point p = popFrontier() ;
			getImageProcessor().GetNeighbors4(p, maxXY, &neighbors);
			while ( !neighbors.empty() )
			{
				Point q = getImageProcessor().PopQueue(&neighbors);
				if ( !useMarkers )
				{
					if ( img(q.x, q.y) == i && m_labels(q.x, q.y) == -1 ) 
					{
						m_labels(q.x, q.y) = m_labels(p.x, p.y) ;
						pushFrontier(q);
					}
				} else
				{
					if ( img(q.x, q.y) <= i && m_labels(q.x, q.y) == -1 ) 
					{
						m_labels(q.x, q.y) = m_labels(p.x, p.y) ;
						pushFrontier(q);
					}
				}
			}
		}
		////// End   PART B
			
		////// Begin PART C		
		it = gVector.begin() ;
		while ( it != gVector.end() )
		{
			if ( !useMarkers ) 
			{
				if ( m_labels(it->x, it->y) == -1 )
				{
					getImageProcessor().Floodfill4( *it, img, &m_labels, GetGlobalLevel() );
					SetGlobalLevel( GetGlobalLevel() + 1 );
				}
			} else
			{
				if ( m_markerImage(it->x, it->y) == 0 )
				{
					getImageProcessor().Floodfill4( *it, img, &m_labels, GetGlobalLevel() );
					SetGlobalLevel( GetGlobalLevel() + 1 );
				}
			}
			it++;
		}
		////// End   PART C
	}
}

/*---------------------------------------------------------------------------*
 * Description: Method to do the pre-requisite operations for marker-based
 *              watershed segmentation.
 * 
 * Parameter(s):
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::MarkerBasedSegmentation()
{
	try 
	{
		getImageProcessor().CreateImgBinaryWithThreshold( GetOriginalImage(), &m_thresholdedImage, GetThresholdRatio() );
		
		ImgGray thresholdedImage;
		thresholdedImage.Reset( m_thresholdedImage.Width(), m_thresholdedImage.Height() );
		blepo::Convert( m_thresholdedImage, &thresholdedImage);

		blepo::Chamfer(thresholdedImage, &m_chamferImage);

		ImgGray chamferImage;
		chamferImage.Reset( m_chamferImage.Width(), m_chamferImage.Height() );
		blepo::Convert( m_chamferImage, &chamferImage );

		ComputeGrayLevelPixelMap( chamferImage );
		WatershedSegmentation( chamferImage, false );

		ImgGray grayLabelImage;
		grayLabelImage.Reset( m_labelImage.Width(), m_labelImage.Height() );
		Convert(m_labels, &grayLabelImage);

		Figure figGrayLabels( GetImageTitle() + " gray labels in WatershedSegmenter.cpp");
		figGrayLabels.Draw(m_labels);

		m_edgeImage.Reset(chamferImage.Width(), chamferImage.Height() );
		MakeEdgeBinaryFromImgInt( m_labels, &m_edgeImage );
		CreateMarkerImage( m_thresholdedImage, m_edgeImage, &m_markerImage );

		ComputeGrayLevelPixelMap( m_constrainedGradientMagnitude );
		WatershedSegmentation( m_constrainedGradientMagnitude, true );

		CreateResultImage(Bgr::CYAN, -1) ;

	} catch (Exception exp)
	{
		exp.Display() ;
	}
}


/*---------------------------------------------------------------------------*
 * Description: Use the information of different images created during the
 *              run of the segmentation algorithms to overlay the original
 *              image with the outlines of the background areas.
 * 
 * Parameter(s):
 * [+] outlineColor - the color of the background area border
 * [+] basin - the value of the basin that denotes which areas have been 
 *             specified by the watershed segmenter as background areas
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::CreateResultImage( Bgr outlineColor, int basin )
{	
	try
	{
		vector<int> validLabels;
		ImgInt labels;
		GetValidLabels( &labels, &validLabels );

		vector< vector<Point> > borderPoints;
		GetBorderPoints( labels, validLabels, &borderPoints );
		
		m_resultImage = GetOriginalImage() ;

		vector< vector<Point> >::iterator it = borderPoints.begin() ;
		while ( it != borderPoints.end() )
		{
			vector<Point>::iterator itNext = it->begin();
			while ( itNext != it->end() )
			{
				m_resultImage( itNext->x, itNext->y ) = outlineColor;
				itNext++;
			}
			it++;
		}
	} catch (const Exception &exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: Uses the final labeled watershedded image to get the 
 *              connected components.  From the connected components, 
 *              watershed segments, and binary thresholded image we find the 
 *              labels of the background segments.
 * 
 * Parameter(s):
 * [+] labels      - segmented image labeled with connected components
 * [+] validLabels - list of integers that denote the background areas
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::GetValidLabels( ImgInt * labels, vector<int> * validLabels)
{
	try
	{
		vector< ConnectedComponentProperties < ImgInt::Pixel > > ccProps;
		int nLabels = ConnectedComponents4( m_labels, labels, &ccProps);

		ImgBinary::Iterator it = m_thresholdedImage.Begin();
		ImgInt::Iterator   iit = labels->Begin();
		ImgInt::Iterator   lit = m_labels.Begin();

		while( it != m_thresholdedImage.End() )
		{
			if ( *it == true && *lit == -1 )
			{
				if ( !ImageProcessing::IsInVector(*iit, *validLabels) )
				{
					validLabels->push_back(*iit);
				}
			}
			it++;
			iit++;
			lit++;
		}
		
		getImageProcessor().ToFileIntVector( validLabels, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\validlabels.out" );

	} catch ( const Exception &exp )
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: Use the labeled watershed segmented image and the list of 
 *              background areas to get a list of points that outline each
 *              background area.
 * 
 * Parameter(s):
 * [+] labels       - a connected components image based off of the 
 *                    marker-based watershed segmented image
 * [+] validLabels  - integer labels of background areas
 * [+] borderPoints - list of all boundary points of background areas
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::GetBorderPoints( ImgInt & labels, vector<int> & validLabels, vector< vector<blepo::Point> > * borderPoints )
{
	try
	{
		vector<int>::iterator it = validLabels.begin() ;
		vector< vector<blepo::Point> > allPoints;
		while ( it != validLabels.end() )
		{
			vector<blepo::Point> bPts;
			WallFollow( labels, *it, &bPts ) ;
			if ( bPts.size() > GetSmallestBorder() )
			{
				allPoints.push_back(bPts) ;
			}
			it++;
		}
		*borderPoints = allPoints;
	} catch (const Exception & exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method uses a watershed segmented image based on the 
 *              chamfer distance to make a binary image that delineates the 
 *              border between the different background and foreground areas 
 *              in an image.  The resulting binary image should mostly be 
 *              black with white lines to show borders.
 * 
 * Parameter(s):
 * [+] label - The segmented image labeled.
 * [+] edges - The resulting binary image that show the foreground/background
 *             sections.
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::MakeEdgeBinaryFromImgInt( const blepo::ImgInt &label, blepo::ImgBinary * edges )
{
	ImgBinary result ;
	int x, y, lastX, lastY;
	try
	{
		lastX = lastY = 0 ;
		result.Reset(label.Width(), label.Height() );
		for ( y = 0 ; y < label.Height() ; y++ )
		{
			for ( x = 0 ; x < label.Width() ; x++ )
			{
				if ( x == 0 )
				{
					lastX = label(x,y);
				}
				(*edges)(x,y) = ( label(x,y) == lastX ) ? 0 : 1;
				lastX = label(x,y);
			}
		}
		for ( x = 0 ; x < label.Width() ; x++ )
		{
			for ( y = 0 ; y < label.Height() ; y++ )
			{
				if ( y == 0 )
				{
					lastY = label(x,y);
				}
				if ( (*edges)(x,y) == 0 ) {
					(*edges)(x,y) = ( label(x,y) == lastY ) ? 0 : 1;
				}
				lastY = label(x,y);
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Combine the thresholded and edge images into a single binary
 *              image called the marker image.
 * 
 * Parameter(s):
 * [+] tImg - threshold image
 * [+] eImg - edge image
 * [+] mImg - marker image
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::CreateMarkerImage( const ImgBinary & tImg, const ImgBinary & eImg, ImgBinary * mImg )
{
	try
	{
		int x, y ;
		mImg->Reset( tImg.Width(), tImg.Height() ) ;
		for ( y = 0 ; y < tImg.Height() ; y++ )
		{
			for ( x = 0 ; x < tImg.Width() ; x++ )
			{
				if ( tImg(x,y) == 1 || eImg(x,y) == 1 )
				{
					(*mImg)(x,y) = 1 ;
				} else
				{
					(*mImg)(x,y) = 0 ;
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method take a point and a 2D images limits and places
 *              the points 4 neighbors in a queue
 * 
 * Parameter(s):
 * [+] location  - a point in an image
 * [+] maxXY     - a point that contains the width and height limits of an 
 *                 image
 * [+] neighbors - a queue that contains the neighbors of a point 
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::getNeighbors4( Point location, Point maxXY, queue<Point> * neighbors)
{
	queue<Point> result;
	try
	{
		if ( location.x-1 > -1 ) 
		{
			Point a( location.x - 1, location.y );
			result.push(a);
		}
		if ( location.x+1 < maxXY.x ) 
		{
			Point b( location.x + 1, location.y );
			result.push(b);
		}
		if ( location.y - 1 > -1 )
		{
			Point c( location.x, location.y - 1 );
			result.push(c);
		}
		if ( location.y + 1 < maxXY.y )
		{
			Point d( location.x, location.y + 1 );
			result.push(d);
		}
		*neighbors = result;
	} catch (Exception exp)
	{
		exp.Display();
	}
}



/*---------------------------------------------------------------------------*
 * Description: Get the queue of points used for the watershed algorithm.
 * 
 * Parameter(s):
 * [+] None
 * Return: A queue of points
 *---------------------------------------------------------------------------*/
queue<Point>   WatershedSegmenter::getFrontier()
{
	return m_frontier;
}

/*---------------------------------------------------------------------------*
 * Description: Get method for removing a blepo::Point from the frontier 
 *              used in the watershed algorithm.
 * 
 * Parameter(s):
 * [+] None
 * Return: The 1st point in the frontier's queue.
 *---------------------------------------------------------------------------*/
Point          WatershedSegmenter::popFrontier()
{
	Point result = NULL ;
	try
	{
		if ( !getFrontier().empty() )
		{
			result = getFrontier().front();
			getFrontier().pop();
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Push a pixel onto the frontier used for watershed 
 *              segmentation.
 * 
 * Parameter(s):
 * [+] pixel - a point in an image
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void           WatershedSegmenter::pushFrontier(Point pixel)
{
	try
	{
		getFrontier().push(pixel);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// SETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description: Set the globalLevel which tells the watershed algorithm which
 *              basin to fill
 * 
 * Parameter(s):
 * [+] level - the number to set the basin to
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::SetGlobalLevel( int level )
{
	if ( level > -1 ) 
	{
		m_globalLevel = level;
	}
}

/*---------------------------------------------------------------------------*
 * Description: Set the name of the image so that it can be used when 
 *              displaying the image as a figure with blepo.
 * 
 * Parameter(s):
 * [+] imageTitle - title of the image the is being segmented
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::SetImageTitle( CString imageTitle )
{
	if ( imageTitle != NULL )
	{
		m_imageTitle = imageTitle;
	}
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::SetGradientMagnitude( ImgGray img )
{
	if ( img.Width() > 0 )
	{
		m_constrainedGradientMagnitude = img;
	}
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::SetSmallestBorder( int border )
{
	if ( border > 0 )
	{
		m_smallestBorder = border;
	}
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
void WatershedSegmenter::SetThresholdRatio( double ratio )
{
	if ( ratio > 0.0 )
	{
		m_thresholdRatio = ratio;
	}
}


///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// GETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
int WatershedSegmenter::GetGlobalLevel()
{
	return m_globalLevel;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
double WatershedSegmenter::GetSigma()
{
	return m_sigma;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
CString WatershedSegmenter::GetImageTitle()
{
	return m_imageTitle;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImgBgr WatershedSegmenter::GetOriginalImage()
{
	return m_originalImage;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImgBgr WatershedSegmenter::getResultImage()
{
	return m_resultImage;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImgBinary WatershedSegmenter::getThresholdedImage()
{
	return m_thresholdedImage;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImgBinary WatershedSegmenter::getMarkerImage()
{
	return m_markerImage;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImgBinary WatershedSegmenter::getEdgeImage()
{
	return m_edgeImage;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImgGray WatershedSegmenter::getGradientMagnitude()
{
	return m_constrainedGradientMagnitude;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
std::vector< std::vector<blepo::Point> > WatershedSegmenter::getMap()
{
	return m_grayLevelPixelMap;
}

/*---------------------------------------------------------------------------*
 *---------------------------------------------------------------------------*/
ImageProcessing WatershedSegmenter::getImageProcessor()
{
	return m_imageProcessor;
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameter(s):
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgInt         WatershedSegmenter::getLabelsImage()
{
	return m_labels;
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameter(s):
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgInt         WatershedSegmenter::getChamferImage()
{
	return m_chamferImage;
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameter(s):
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
int         WatershedSegmenter::GetSmallestBorder()
{
	return m_smallestBorder;
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameter(s):
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
double         WatershedSegmenter::GetThresholdRatio()
{
	return m_thresholdRatio;
}
