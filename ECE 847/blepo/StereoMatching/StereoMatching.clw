; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CStereoMatchingDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "StereoMatching.h"

ClassCount=4
Class1=CStereoMatchingApp
Class2=CStereoMatchingDlg

ResourceCount=3
Resource2=IDR_MAINFRAME
Resource3=IDD_STEREOMATCHING_DIALOG

[CLS:CStereoMatchingApp]
Type=0
HeaderFile=StereoMatching.h
ImplementationFile=StereoMatching.cpp
Filter=N

[CLS:CStereoMatchingDlg]
Type=0
HeaderFile=StereoMatchingDlg.h
ImplementationFile=StereoMatchingDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_STEREOMATCHING_DIALOG]
Type=1
Class=CStereoMatchingDlg
ControlCount=28
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_IMGPROC_GROUPBOX,button,1342177287
Control4=IDC_FLOODFILL4,button,1342242816
Control5=IDC_FLOODFILL8,button,1342242816
Control6=IDC_OUTLINE_FOREGROUND,button,1342242816
Control7=IDC_WATERSHED_GROUP_BOX,button,1342177287
Control8=IDC_WATERSHED_SEGMENTATION,button,1342242816
Control9=IDC_EDIT_THRESHOLD,edit,1350631554
Control10=IDC_LBL_THRESHOLD,static,1342308352
Control11=IDC_EDIT_BORDER_PIXEL_SIZE,edit,1350631554
Control12=IDC_BORDER_MAX,static,1342308352
Control13=IDC_EDIT_WATERSHED_SIGMA,edit,1350631554
Control14=IDC_STATIC,static,1342308352
Control15=IDC_CANNY_MATCHING_GROUP,button,1342177287
Control16=IDC_CANNY,button,1342242816
Control17=IDC_CANNY_SIGMA,static,1342308352
Control18=IDC_EDIT_CANNY_SIGMA,edit,1350631554
Control19=IDC_TEMPATE_MATCHING,button,1342242816
Control20=IDC_STEREO_GROUP,button,1342177287
Control21=IDC_PENCIL_EPIPOLAR,button,1342242816
Control22=IDC_BTN_STEREO_MATCHING,button,1342242816
Control23=IDC_LBL_DISPARY_MIN,static,1342308352
Control24=IDC_STATIC,static,1342308352
Control25=IDC_BOX_HALFWIDTH,edit,1350631552
Control26=IDC_LBL_DISPMAX,static,1342308352
Control27=IDC_BOX_DISPMAX,edit,1350631552
Control28=IDC_BOX_DISPMIN,edit,1350631552

