// FloodFill.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FloodFill.h"
#include "FloodFillDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

// CFloodFillApp

BEGIN_MESSAGE_MAP(CFloodFillApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CFloodFillApp construction

CFloodFillApp::CFloodFillApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CFloodFillApp object

CFloodFillApp theApp;


// CFloodFillApp initialization

BOOL CFloodFillApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CFloodFillDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}



ImgBgr CFloodFillApp::Floodfill(CPoint seedPoint, ImgBgr originImg)
{
	ImgBgr result ;
	
	try 
	{
		Bgr oldColor = Bgr( *originImg.Begin(seedPoint.x, seedPoint.y) );
		Bgr fllColor = Bgr(100, 0, 0);	
		CPoint imgLmt(originImg.Width(), originImg.Height());
		
		result = originImg;
		result(seedPoint.x, seedPoint.y) = fllColor ;

		stack <CPoint> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			CPoint location = frontier.top();
			frontier.pop();

			CPoint neighbor;

			// get north neighbor
			neighbor = CPoint( location.x, location.y-1 );
			if ( neighbor.y > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get east neighbor
			neighbor = CPoint( location.x + 1, location.y );
			if (  neighbor.x < imgLmt.x ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get south neighbor
			neighbor = CPoint( location.x, location.y + 1 );
			if ( neighbor.y < imgLmt.y ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get west neighbor
			neighbor = CPoint( location.x - 1, location.y );
			if ( neighbor.x > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}

	return result;
}