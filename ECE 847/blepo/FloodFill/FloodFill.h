// FloodFill.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <stack>
#include "resource.h"		// main symbols
#include "../src/blepo.h"


// CFloodFillApp:
// See FloodFill.cpp for the implementation of this class
//

class CFloodFillApp : public CWinApp
{
public:
	CFloodFillApp();

// Overrides
	public:
	virtual BOOL InitInstance();
	static blepo::ImgBgr Floodfill(CPoint seedPoint, blepo::ImgBgr originImg);

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CFloodFillApp theApp;