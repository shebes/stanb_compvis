\contentsline {section}{\numberline {0.1}Introduction}{1}
\contentsline {section}{\numberline {0.2}Theory}{1}
\contentsline {subsection}{\numberline {0.2.1}Feature Points}{1}
\contentsline {subsection}{\numberline {0.2.2}Bilinear Interpolation}{2}
\contentsline {subsection}{\numberline {0.2.3}Lucas-Kanade approach(1981)}{2}
\contentsline {section}{\numberline {0.3}Algorithm and Methodology}{2}
\contentsline {subsection}{\numberline {0.3.1}Iterative Lucas-Kanade Algorithm}{2}
\contentsline {subsection}{\numberline {0.3.2}Main Overview}{2}
\contentsline {subsection}{\numberline {0.3.3}Main files and duties}{3}
\contentsline {subsection}{\numberline {0.3.4}Step-by-Step}{4}
\contentsline {subsubsection}{FeatureEigenPoints::Add()}{4}
\contentsline {subsubsection}{FeatureEigenPoints::AddWithMinDist()}{5}
\contentsline {subsubsection}{FeatureEigenPoints::DistanceToNeighbor()}{5}
\contentsline {subsubsection}{FeatureEigenPoints::Sort()}{6}
\contentsline {subsubsection}{FeatureEigenPoints::GetFeatures()}{6}
\contentsline {subsubsection}{FeatureEigenPoints::GetFeaturesIteratorBegin()}{6}
\contentsline {subsubsection}{FeatureEigenPoints::GetFeaturesIteratorEnd()}{6}
\contentsline {subsubsection}{LucasKanade::DetectMostSalientFeatures()}{7}
\contentsline {subsubsection}{LucasKanade::Interpolate()}{7}
\contentsline {subsubsection}{LucasKanade::InterpolateW()}{7}
\contentsline {subsubsection}{LucasKanade::Compute2x2GradientMatrix()}{8}
\contentsline {subsubsection}{LucasKanade::Compute2x1ErrorVector()}{8}
\contentsline {subsubsection}{LucasKanade::Solve()}{8}
\contentsline {subsubsection}{LucasKanade::SwapImages()}{9}
\contentsline {subsubsection}{LucasKanade::DrawFeaturePoints()}{9}
\contentsline {section}{\numberline {0.4}Results}{9}
\contentsline {section}{\numberline {0.5}Discussion}{10}
\contentsline {section}{\numberline {0.6}Conclusion}{11}
