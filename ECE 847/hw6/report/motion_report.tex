\documentclass{report}
\usepackage{fullpage,doublespace,amsmath,amssymb,amsthm,graphicx}
\author{Shelby Solomon Darnell}
\title{ \Huge{Lucas and Kanade \\ Motion Tracking} \break \LARGE{ECE 847 Image Processing \break \LARGE{Homework \#6}} }
%\date{}
\begin{document}

\maketitle
\tableofcontents

\section{Introduction}

Learning to follow the motion of objects in a sequence images in two dimensional 
space is to be accomplished with this assignment.  Before following the motion
of an object, one must figure out how to track an object.  The answer the
Lucas-Kanade algorithm provides is by finding interesting points in the first
image and following the interesting points in the following sequence of images.

\section{Theory}

\subsection{Feature Points}
Features or interesting points can be every single pixel in an image.  In most
images every pixel is not distinquishable from its neighbors.  An interesting
pixel is one that lies in a group of pixels that are not very similar.  An
interesting pixel is one in an area of high variance.  To find the variance 
of the pixels in an image we use the x and y gradients of an image and with 
those derivative images we calculate the eigen vector.  In this assignment we
were tasked to find one hundred feature points a distance of eight pixels 
apart.  The feature points were ranked by their minimum eigen value in the
eigen vectors.

\subsection{Bilinear Interpolation}
Bilinear interpolation is used to estimate the motion of a point between two
images.  Though a normal coordinate point is an integer, when using the 
iterative Lucas-Kanade algorithm a coordinate point can use floating point 
values.  Bilinear interpolation computes the double weighted average of the
four nearest pixels and keeps in consideration the proximity of the float
line to its surrounding neighbors.

\subsection{Lucas-Kanade approach(1981)}
\begin{itemize}
\item{Assume neighboring pixels are same}
\item{Use additional equations to solve for motion of pixel}
\item{Compute (u,v) for a small number of pixels (features); 
each feature is treated independently of other features}
\item{Sparse optical flow}
\end{itemize}

\section{Algorithm and Methodology}

\subsection{Iterative Lucas-Kanade Algorithm}
\begin{enumerate}
\item{Solve 2x2 equation to get motion for each pixel}
\item{Shift second image using estimated motion \\(Use interpolation to improve accuracy)}
\item{Repeat until convergence}
\end{enumerate}

\subsection{Main Overview}

The following is a list of the main method definitions for the Lucas and Kanade implementation:
\begin{itemize}
\item{FeatureEigenPoints::Add()}
\item{FeatureEigenPoints::AddWithMinDist()}
\item{FeatureEigenPoints::DistanceToNeighbor()}
\item{FeatureEigenPoints::Sort()}
\item{FeatureEigenPoints::GetFeatures()}
\item{FeatureEigenPoints::GetFeaturesIteratorBegin()}
\item{FeatureEigenPoints::GetFeaturesIteratorEnd()}
\item{LucasKanade::DetectMostSalientFeatures()}
\item{LucasKanade::Interpolate()}
\item{LucasKanade::InterpolateW()}
\item{LucasKanade::Compute2x2GradientMatrix()}
\item{LucasKanade::Compute2x1ErrorVector()}
\item{LucasKanade::Solve()}
\item{LucasKanade::SwapImages()}
\item{LucasKanade::DrawFeaturePoints()}
\end{itemize}
For parameters and return types please view files FeatureEigenPoints.h and LucasKanade.h.

\subsection{Main files and duties}
The source code is organized to have three main responsibility areas: interactive display dialog, Lucas-Kanade implementation methods, and general image processing methods.

The following files are responsible for creating the interactive display dialog:
\begin{itemize}
\item{LucasKanadeDlg.cpp}
\item{LucasKanadeDlg.h}
\item{LucasKanade.rc}
\end{itemize}
These files create and paint the interactive display as well as contain the event handlers that call methods in the other files.

The following files comprise the motion following/Lucas-Kanade implementation specific methods:
\begin{itemize}
\item{LucasKanade.cpp}
\item{LucasKanade.h}
\item{FeatureEigenPoints.cpp}
\item{FeatureEigenPoints.h}
\end{itemize}

These files contain the methods listed in the main overview and rely on the ImageProcessing file for generic image operations.

The following files comprise the general image processing methods:
\begin{itemize}
\item{ImageProcessing.cpp}
\item{ImageProcessing.h}
\end{itemize}
ImageProcessing contains most of the methods necessary to complete past homework assignments.
WaterSegmenter, stereo matching and other classes are also a part of this package, but its methods are not used for this homework assignment.

\subsection{Step-by-Step}

\subsubsection{FeatureEigenPoints::Add()}
\begin{verbatim}
Description: Method to add FeatureEigenPoint to this object vector of
FeatureEigenPoints

Parameters:
[+] fep - a FeatureEigenPoint to add to this object
Return: Nothing
\end{verbatim}

\subsubsection{FeatureEigenPoints::AddWithMinDist()}
\begin{verbatim}
Description:  Method that adds a feature point to the feature point list 
if it is a minimum distance from the other feature points present in the 
list.

Parameters:
[+] fep - a FeatureEigenPoint object that groups the point and eigen values
[+] d   - the minimum distance value for two feature points
Return: True if the feature point is added, false if it is too close to 
        another feature point.
\end{verbatim}

\subsubsection{FeatureEigenPoints::DistanceToNeighbor()}
\begin{verbatim}
Description: A method that returns the integer distance between two
FeatureEigenPoints.
Parameters:
[+] a - a FeatureEigenPoint whose Manhattan distance to neighbor we must 
        determine
[+] b - a FeatureEigenPoint whose Manhattan distance to neighbor we must 
        determine
Return: An integer that gives the floor of the distance between points
\end{verbatim}

\subsubsection{FeatureEigenPoints::Sort()}
\begin{verbatim}
Description: stl sort vector of FeatureEigenPoints, used to be a
bubble sort
Parameters:
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{FeatureEigenPoints::GetFeatures()}
\begin{verbatim}
Description: Return a reference to the vector of FeatureEigenPoint 
structures
Parameters:
[+] none
Return: A reference to the vector of FeatureEigenPoint structures
\end{verbatim}

\subsubsection{FeatureEigenPoints::GetFeaturesIteratorBegin()}
\begin{verbatim}
Description: Return the beginning iterator for the vector of 
FeatureEigenPoints
Parameters:
[+] none
Return: The beginning iterator for the vector of FeatureEigenPoints
\end{verbatim}

\subsubsection{FeatureEigenPoints::GetFeaturesIteratorEnd()}
\begin{verbatim}
Description: Return the end iterator for the vector of FeatureEigenPoints
Parameters:
[+] none
Return: The end iterator for the vector of FeatureEigenPoints
\end{verbatim}

\subsubsection{LucasKanade::DetectMostSalientFeatures()}
\begin{verbatim}
Description: Method that creates a list of N interesting points to track
in the sequence of images
Parameters:
[+] img        - image in which features are to be detected
[+] fNumber    - number of features to be found and used
[+] fProximity - distance features must be from one another
Return: nothing
\end{verbatim}

\subsubsection{LucasKanade::Interpolate()}
\begin{verbatim}
Description: Ascertain a value for a non integer coordinate in an image
Parameters:
[+] img - image of gray level values
[+] cp  - a coordinate in the image
Return: a value for a non integer coordinate in the image
\end{verbatim}

\subsubsection{LucasKanade::InterpolateW()}
\begin{verbatim}
Description: Interpolate for a group points in a piece or window of an
image

Parameters:
[+] img - image of gray level values
[+] cp  - a coordinate in the image
Return: a small set of points whose values have been interpolated using
        its surrounding pixels
\end{verbatim}

\subsubsection{LucasKanade::Compute2x2GradientMatrix()}
\begin{verbatim}
Description: We use a 5x5, by default, window to calculate the 2x2 
covariance matrix.  The window size can be changed.
Parameters:
[+] covmat - reference to a 2x2 covariance matrix
[+] gx     - x gradient of 1st image
[+] gy     - y gradient of 1st image
[+] cp     - point for which we are calculating the covariance matrix
Return: nothing
\end{verbatim}

\subsubsection{LucasKanade::Compute2x1ErrorVector()}
\begin{verbatim}
Description: Calculate the error vector using the temporal distance 
between two images and values of the image gradients

Parameters:
[+] errvec - error vector between two images
[+] img1w  - window containing interpolated values for the 1st image
[+] img2w  - window containing interpolated values for the 2nd image
[+] gradxw - window containing interpolated values for the x gradient
[+] gradyw - window containing interpolated values for the y gradient
Return: nothing
\end{verbatim}

\subsubsection{LucasKanade::Solve()}
\begin{verbatim}
Description: Solves a linear equation to find the motion vector for an
interesting point.
Parameters:
[+] deltaD - the motion vector for the interesting point
[+] covmat - the covariance matrix
[+] errvec - the error vector
Return: nothing
\end{verbatim}

\subsubsection{LucasKanade::SwapImages()}
\begin{verbatim}
Description: Swap image 2 and image 1, getting the object ready for the 
next image in the movie sequence.
Parameters:
[+] none
Return: nothing
\end{verbatim}

\subsubsection{LucasKanade::DrawFeaturePoints()}
\begin{verbatim}
Description: Use the FeatureEigenPoints object to draw the interesting
points on the original image.
Parameters:
[+] img  - a pointer to the image on which feature points will be drawn
[+] feps - a pointer to the FeatureEigenPoints object
Return: Nothing
\end{verbatim}

\section{Results}

The first and last three images in the flower garden sequence with the interesting points shown as red dots.
\begin{center}
\includegraphics[width=1in,height=1in]{images/flowergarden01.jpg}
\includegraphics[width=1in,height=1in]{images/flowergarden02.jpg}
\includegraphics[width=1in,height=1in]{images/flowergarden03.jpg} 
\includegraphics[width=1in,height=1in]{images/flowergarden04.jpg} 
\includegraphics[width=1in,height=1in]{images/flowergarden05.jpg} 
\includegraphics[width=1in,height=1in]{images/flowergarden06.jpg} 
\end{center}

The first three images in the statue sequence with the interesting points shown as red dots.
\begin{center}
\includegraphics[width=2in,height=2in]{images/statue01.jpg} 
\includegraphics[width=2in,height=2in]{images/statue02.jpg}
\includegraphics[width=2in,height=2in]{images/statue03.jpg} 
\end{center}

The last three images in the statue sequence with the interesting points shown as red dots.
\begin{center}
\includegraphics[width=2in,height=2in]{images/statue04.jpg} 
\includegraphics[width=2in,height=2in]{images/statue05.jpg}
\includegraphics[width=2in,height=2in]{images/statue06.jpg} 
\end{center}

\section{Discussion}

Apparently the sigma value is extremely important in image processing and 
computer vision.  A sigma value of 1 for tracking the feature points causes
the motion vector values to be too large.  A sigma value of 0.1 for tracking
feature points causes the feature points to move more succinctly with the
initial point in the image it was meant to track.  Using a window size of
five with the flowergarden sequence is sufficient to track the motion of
objects.  Using a window size of five with the statue sequence allows some
of the feature points, on the walking person, to be lost before the end of 
the sequence.  Using a larger window size of eleven for the statue sequence
allows the Lucas-Kanade implementation to better follow the motion of the
walking person.

Tracking motion using eigen values to locate interesting points and the 
Lucas-Kanade algorithm to interpolate where the point should be in a
sequential image works okay on the small sequences we use for this report, but
methods using machine learning to follow interesting points may be feasible.
A learning algorithm could replace the Lucas-Kanade algorithm by learning 
what the feature point represents and find it in a sequential image.

\section{Conclusion}

Using eigen vectors and values with two dimensional interpolation to first find 
interesting points in an image and second create a motion vector to allow the 
interesting points to be tracked in a sequence of images is exciting.  Using 
the debugger in Microsoft Visual studio was essential to getting the 
Lucas-Kanade implementation to work, unlike all previous assignments.

\end{document}
