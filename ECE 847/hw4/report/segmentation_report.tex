\documentclass{report}
\usepackage{fullpage,doublespace,amsmath,amssymb,amsthm,graphicx}
\author{Shelby Solomon Darnell}
\title{ \Huge{Marker-based Watershed Segmentation} \break \LARGE{ECE 847 Image Processing \break \LARGE{Homework \#4}} }
%\date{}
\begin{document}

\maketitle
\tableofcontents

\section{Introduction}
A purpose of segmentation is to allow for easier identification and classification of images.  Image segmentation divides an image into groups with classifications like gray level, color, texture and motion (Birchfield lecture slides \#4).  According to Shapiro and Stockman in Computer Vision pages 280-281 the following are good properties for a region:
\begin{itemize}
\item a segment should be smooth, not ragged
\item a segment should be spatially accurate
\item a segment interior should be simple and without many small holes
\item a segment of an image should be uniform
\end{itemize}


\section{Theory}

One way to think of segmentation is a partitioning.  An image partition is the set of homogeneous subsets that comprise the entire image and do not overlap (Birchfield lecture slides \#4).  There are two main ways to segment an image: splitting and merging (Birchfield lecture slides \#4).  Watershed segmentation uses merging to accomplish segmentation.  The steps of merging are:
\begin{enumerate}
\item start with each pixel as a separate region
\item repeat: merge adjacent regions if union is homogeneous
\item even better: merge two closest clusters
\end{enumerate} (Birchfield lecture slides \#4).
Watershed segmentation uses the gradient magnitude image as a topographical surface, a surface with valleys and hills (Birchfield lecture slides \#4).  Watershed segmentation treats the valleys as segments and the algorithm grows the segment from the bottom of the valley.  In order to grow the segment from the bottom of the valley one is to imagine a valley slowly being flooded.  A flooding valley can continue to flood into an adjacent valley if the two regions are similar enough given how the implementer wishes to define homogeneous subsets.  The floodfill operation is used to flood the valleys found in the gradient magnitude image.  This method alone has it problems that are somewhat improved upon adding markers to the segmentation algorithm.  Marker-based watershed segmentation divides an image into regions and those regions are used as markers when performing watershed segmentation.  The markers serve as boundaries that keep the watershed algorithm from over segmenting the input image.

\section{Algorithm and Methodology}

The homogeneity criterion for region growing is the adjacent pixel must be the same color, and the adjacent pixel is in the four connected neighborhood of the pixel in question.

\subsection{Main Overview}

The following is a list of the main method definitions for marker-based watershed segmentation:
\begin{enumerate}
\item{ComputerGrayLevelPixelMap()}
\item{SegmentImage()}
\item{WatershedSegmentation()}
\item{MarkerBasedSegmentation()}
\item{GetValidLabels()}
\item{GetBorderPoints()}
\item{CreateMarkerImage()}
\item{CreateResultImage()}
\end{enumerate}
For parameters and return types please view file WatershedSegmenter.h.

\subsection{Main files and duties}
The source code is organized to have three main sections: interactive display dialog, marker-based watershed segmentation methods, and general image processing methods.

The following files are responsible for creating the interactive display dialog:
\begin{itemize}
\item{WatershedSegmentationDlg.cpp}
\item{WatershedSegmentationDlg.h}
\item{WatershedSegmentation.rc}
\end{itemize}
These files create and paint the interactive display as well as contain the event handlers that call methods in the other files.

The following files comprise the marker-based watershed segmentation specific methods:
\begin{itemize}
\item{WatershedSegmenter.cpp}
\item{WatershedSegmenter.h}
\end{itemize}
These files contain the methods listed in the main overview and rely on the ImageProcessing file for generic image operations.

The following files comprise the general image processing methods:
\begin{itemize}
\item{ImageProcessing.cpp}
\item{ImageProcessing.h}
\end{itemize}
ImageProcessing contains the methods necessary to complete past homework assignments.

\subsection{Step-by-Step}

\subsubsection{ComputerGrayLevelPixelMap()}
\begin{verbatim}
Description: Makes a map of the different pixels for each gray level for the image passed in as a parameter.
Parameters: 
[+] img - A gray level image, most likely a gradient magnitude image for
          which the gray level values are calculated to help improve the
          calculation speed for watershed segmentation.          
Returns: Nothing
\end{verbatim}

\subsubsection{SegmentImage()}
\begin{verbatim}
Description: Method to choose between regular and marker-based watershed 
             segmentation. 
Parameter(s):
[+] segmentationType - an integer for the type of segmentation algorithm 
                       to run on an image: 1 for normal watershed and
                       2 for marker-based watershed
Return: Nothing
\end{verbatim}

\subsubsection{WatershedSegmentation()}
\begin{verbatim}
Description: Watershed segmentation algorithm that can do marker-based
             segmentation as well.
Parameter(s):
[+] img - image to be segmented
[+] useMarkers - if set to true marker-based watershed segmentation is used
Return: Nothing
\end{verbatim}

\subsubsection{MarkerBasedSegmentation()}
\begin{verbatim}
Description: Method to do the pre-requisite operations for marker-based
             watershed segmentation.
Parameter(s):
[+] None
Return: Nothing
\end{verbatim}

\subsubsection{GetValidLabels()}
\begin{verbatim}
Description: Uses the final labeled watershedded image to get the 
             connected components.  From the connected components, 
             watershed segments, and binary thresholded image we find the 
             labels of the background segments. 
Parameter(s):
[+] labels      - segmented image labeled with connected components
[+] validLabels - list of integers that denote the background areas
Return: Nothing
\end{verbatim}

\subsubsection{GetBorderPoints()}
\begin{verbatim}
Description: Use the labeled watershed segmented image and the list of 
             background areas to get a list of points that outline each
             background area. 
Parameter(s):
[+] labels       - a connected components image based off of the 
                   marker-based watershed segmented image
[+] validLabels  - integer labels of background areas
[+] borderPoints - list of all boundary points of background areas
Return: Nothing
\end{verbatim}

\subsubsection{CreateMarkerImage()}
\begin{verbatim}
Description: Combine the thresholded and edge images into a single binary
             image called the marker image. 
Parameter(s):
[+] tImg - threshold image
[+] eImg - edge image
[+] mImg - marker image
Return: Nothing
\end{verbatim}

\subsubsection{CreateResultImage()}
\begin{verbatim}
Description: Use the information of different images created during the
             run of the segmentation algorithms to overlay the original
             image with the outlines of the background areas. 
Parameter(s):
[+] outlineColor - the color of the background area border
[+] basin - the value of the basin that denotes which areas have been 
            specified by the watershed segmenter as background areas
Return: Nothing
\end{verbatim}

\section{Results}

Original and final outlined segmented images of cells\_small.jpg.  The threshold value is twenty-five percent and the smallest border size is seven.
\begin{center}
\includegraphics[width=3in,height=3in]{cells_small.jpg} 
\includegraphics[width=3in,height=3in]{cells_small_segments_outlined_t25.jpg}
\end{center}

\begin{center}
\includegraphics[width=2in,height=2in]{cells_small_gradient_magnitude.jpg}
\includegraphics[width=2in,height=2in]{cells_small_threshold_t25.jpg} 
\includegraphics[width=2in,height=2in]{cells_small_chamfer.jpg}
\end{center}

\begin{center}
\includegraphics[width=2in,height=2in]{cells_small_watershed_t25.jpg}
\includegraphics[width=2in,height=2in]{cells_small_canny_edges.jpg} 
\includegraphics[width=2in,height=2in]{cells_small_markers.jpg} 
\end{center}

Original and final outlined segmented images of holes.jpg.  The threshold value is fifteen percent and the smallest border size is seven.
\begin{center}
\includegraphics[width=1.8in,height=1.8in]{holes.jpg}
\includegraphics[width=1.8in,height=1.8in]{holes_segments_outlined_t15.jpg}
\end{center}

\begin{center}
\includegraphics[width=0.8in,height=1in]{holes_gradient_magnitude_t15.jpg}
\includegraphics[width=0.8in,height=1in]{holes_threshold_t15.jpg}
\includegraphics[width=0.8in,height=1in]{holes_chamfer_t15.jpg}
\includegraphics[width=0.8in,height=1in]{holes_watershed_t15.jpg}
\includegraphics[width=0.8in,height=1in]{holes_canny_edges_t15.jpg}
\includegraphics[width=0.8in,height=1in]{holes_marker_t15.jpg}
\end{center}

\begin{center}
\end{center}

\begin{center}
\end{center}

\section{Discussion}

\subsection{What Makes a Difference}
The results of marker-based watershed segmentation are dependent on the sigma value made to create the Gaussian value used for the gradient magnitude image and the threshold ratio used to differentiate the foreground from the background.  When using marker-based watershed segmentation a couple of errors can crop up along the edges that are based on the chamfer distance of the thresholded image.  Fortunately these errors form small pixelated areas that can be removed using a simple technique.

\subsection{What Defines Good}
Intermediate files refers to six images: binary threshold, chamfer distance, gradient magnitude, Canny edges, watershed and markers.  The binary threshold image is created using a threshold value to differentiate the foreground from the background in the original input image.  The chamfer distance image comes from running the chamfer distance algorithm on the binary threshold image.  The gradient magnitude image comes from convolutions using the Gaussian made from the variance input in the GUI.  Run the Canny algorithm on the chamfer distance image to find the edges.  The watershed segmentation algorithm without markers is run on the chamfer distance algorithm to generate the watershed image.  The markers image comes from the combination of the threshold and edge images.  Good intermediate files refers to the binary threshold and watershed images.  A good binary threshold file makes clear separable background and foreground areas.  A good watershed segmented image contains primarily obtuse segments over the entire image.  

\subsection{The Smallest Border} 

The technique to get rid of the small pixelated errors uses a definition of the smallest border.  With watershed segmentation we a looking to outline background areas in a image.  The smallest border definition is the smallest number of pixels to use for a background outline.  If the watershed algorithm identifies an area that is only a single pixel in size, it may be erroneous or not very useful on which to perform further analysis.  A smallest border value of five signifies an area of a single pixel.  If the smallest border value is too large we may remove positive identification of small segments.  It is best to test the watershed results by using different smallest border sizes to get the best results.  By defining the smallest border we tell the algorithm the smallest area we consider useful during background identification.  The smallest border value has the most effect on the \emph{cells\_small.pgm} image, because when segmented there are some small areas identified as background.  If the smallest border value is too large we keep the algorithm from giving its best results or small segments.  If the smallest border value is too small we allow too much noise in the results.

\subsection{Effects of the Threshold Ratio}

The threshold value determines how dark, by comparison, the background is when included as a watershed segment that will be outlined in the final result image.  In order to get the best segments on the \emph{cells\_small.pgm} file 0.25 is the best threshold ratio.  The threshold ratio of 0.25 allows the outlines to be most encompassing of the darker areas of \emph{cells\_small.pgm}.  This threshold allows for segment location of smaller background areas.  The threshold 0.25 is the best for the \emph{cells\_small.pgm} file.
A threshold value of either 0.15 or 0.20 are best for file \emph{holes.pgm}.  The threshold values of 0.15 and 0.20 give the best intermediate files for \emph{holes.pgm}.  The main difference in the two threshold values when operating on the \emph{holes.pgm} image is the size of the segment outlines.  A threshold value of 0.20 makes the segment outline area larger than a threshold value of 0.15.
The variance used to create the Gaussian influences the final segment outline image greatly.  A larger variance value can make the segment outlines smaller on average and tends toward over segmentation.

\section{Conclusion}

A smallest border limit of seven throws away unnecessarily small region boundaries, but a smallest border value of twenty-one does better.  On the \emph{cells\_small.pgm} image the smallest border value of seven allows for the outlining of small background areas, whereas a smallest border value of twenty one keeps the segments looking more smooth, therefore more desirable as defined by Shapiro and Stockman in Computer Vision pages 280-281.  Because of the size of all of the segments for \emph{holes.pgm} the smallest border values of seven and twenty one make no difference in the border outlines.  Using a sigma value of one is the best for segmentation on both test images \emph{cells\_small.pgm} and \emph{holes.pgm}.  A sigma value of one gives the best blend of background detection and smoothness of boundaries.  A sigma value less than one causes more jagged segment outlines to be drawn on the final image.  A sigma value greater than one causes more jagged segments as well as slightly off center segments.  Obtaining the best results from marker-based segmentation depends on finding the sigma, threshold, and smallest border values that work best for an input image.

\end{document}