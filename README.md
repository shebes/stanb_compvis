# StanB_CompVis

During my PhD studies into biometrics, I took interesting course from an even more interesting teacher. The teacher was Stanley Birchfield and the course was Computer Vision, split into two semesters.

This repository contains the homework assignments I coded for the class.

## Computer Vision part 1
#### Electrical and Computer Engineering (ECE) 847

### Assignment 1
### Assignment 2
### Assignment 3
##### Canny Edge Detection
An implementation of the Canny edge detector is the first focus of this assignment. The Canny edge detector uses a Gaussian function to detect edges, and we will allow a user to enter a sigma with which we automatically calculate the Gaussian. The implementation for the Canny edge detector has three main steps: gradient estimation, non-maximum suppression, and thresholding (with hysteresis).

An implementation of the chamfer distance algorithm using Manhattan distance is the second focus for this assignment. The chamfer distance algorithm will be used to identify an object in an image from a template.

### Assignment 4
### Assignment 5
##### Stereo Matching and Epipolar Line drawing
Epipolar geometry consists of the ideas of an epipolar plane, an epipolar line, an image plane and a baseline. Epipolar geometry defines the perspective basis for two stereo images. This perspective basis for stereo images and the values calculated using them leads to the technique of stereo matching. A major purpose of stereo matching is to allow a computer to calculate the depth of objects in an image. Without the perspective basis derived from the epipolar geometry of images depth calculations from stereo matching is not possible.

### Assignment 6
##### Lucas & Kanade Motion Tracking
Learning to follow the motion of objects in a sequence images in two dimensional space is to be accomplished with this assignment. Before following the motion of an object, one must figure out how to track an object. The answer the Lucas-Kanade algorithm provides is by finding interesting points in the first image and following the interesting points in the following sequence of images.


### Project
##### Classifying facial hair using neural networks.
Facial occlusions present many issues in facial recognition. One such common occlusion is facial hair and dramatically affects the usability of full-facial algorithms. In an effort to improve the usability of a face database where facial hair is often present in facial images, there is a need for automatic facial hair identification with a large and dynamically expanding data set. We present several methods for determining if a subject has facial hair such that it will interfere with facial recognition algorithms. The algorithms presented are robust to different ethnic groups and cover both pixel-based processing and computational intelligence.

## Computer Vision part 2
#### Electrical and Computer Engineering (ECE) 877

### Assignment 1
##### Pattern Detection
Pattern detection in an image is a basic computer vision task. In order for robots to self-navigate, being able to detect patterns from images it takes of its surroundings is paramount. Pattern detection can be accomplished by finding the edges of a particular object for high resolution images; however, when detecting patterns of small objects in low resolution images a different model must be used. In more simple images, like one of a book chapter, in order to detect the pattern represented by a character, say an ’s’, simplicity is our friend. The model for a single character or string of characters can be an image of the single character or the image of the string.

### Assignment 2
##### Chan Vese Level Sets
Segmentation is one of the main problems plaguing computer vision and image processing research. Seg- menting can be accomplished by finding edges in an image using the intensity of neighboring pixels. Another way to perform segmenting is to use snakes that starts as a simple contour of an object that forms itself to the outline of an object. Snakes work well for convex objects, but it has several problems: they are sensitive to initial position, parameters, have a small capture range and cannot detect concavities. Level sets are an improvement of snakes which are both active contours that use the energyof an object to draw a contour around said object. Snakes are level sets are active contours that evolve based on the average intensity found inside of a contour. Active contours are a way to segment an image. Active contours represent a way to track a foreground object by minimizing the energy functional of the implicit function. In 2001, Chan and Vese came up with an implementation of level sets that has become the most widely used active contour in practice today.

### Assignment 3
##### Speeded Up Robust Features (SURF)
Finding corresponding features in a group of images is a problem on which computer vision researchers are constantly working to improve. Lucas and Kanade have a method of detecting features in an image using eigen values derived from image gradients. The interesting points are found in areas of the image with high variance. Bay et. al. use a different approach to accomplish the same goal. Bay et. al. use speeded-up robust features (SURF) that constructs an integral image that is used to convolve a grayscale image about different kernels then constructs Hessian matrices on which non-maximal suppression is performed to find interesting features.

### Assignment 4
##### Image Mosaicking
We have learned to find corresponding features in a group of images using speeded up robust features (SURF). One of the well known and very useful applications of finding corresponding features in a group of images is to create a panorama by way of image mosaicking.

### Assignment 5
##### Flat World Reconstruction
Recently we have worked on a project that taught us to warp images to a new coordinate system using corresponding points. This project will make us take that idea further by transforming an image in the projective plane into one in a flat plane for which world coordinates can be used. The type of extreme image warping we will use is referred to as Flat World - Reconstruction. Flat world reconstruction allows one to take a seemingly skewed view of a scene and transform into a scene with considerably less skew.

### Assignment 6
##### Normalized Epipolar Lines and 3D Reconstruction
Epipolar geometry refers to epipolar planes, lines, image planes and a base line. Epipolar geometry defines the perspective basis for two stereo images. This perspective basis for stereo images and the values calculated using them leads to the technique of stereo matching. A major purpose of stereo matching is to allow a computer to calculate the depth of objects in an image. Without the perspective basis derived from the epipolar geometry of images depth calculations from stereo matching is not possible. However, when using corresponding points in similar images taken from different viewpoints, the epipolar geometry may have a focal point that can be found within the boundaries of the image. Normalizing the coordinates of the similar images moves the epipole from inside the image to a point at infinity.

Recently we have worked on a project that taught us to warp images to a new coordinate system using corresponding points. This project will make us take that idea further by transforming an image in the projective plane into one in a flat plane for which world coordinates can be used. The type of extreme image warping we will use is referred to as Flat World - Reconstruction. Flat world reconstruction allows one to take a seemingly skewed view of a scene and transform into a scene with considerably less skew. 3D reconstruction takes flat world reconstruction one step further and uses the foreground to calculate 3D values.