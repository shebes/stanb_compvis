/*****************************************************************************
 * Surf.cpp
 *
 * Date: Thursday February 17, 2011
 *****************************************************************************/
#include "stdafx.h"
#include "Surf.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/*---------------------------------------------------------------------------*
 * Description:  Constructor for the Surf feature detector algorithm
 *
 * Parameters:
 * [+] img     - 
 * Return: Nothing
 *---------------------------------------------------------------------------*/
Surf::Surf( CString fileName )
{
	debug = true;
	Initialize(   fileName );
	CreateScaledImages( mIntegralImg, &mScaledImages );
	PrintScaledImages( 9 );
	FindInterestPoints( &mInterestImages, &mScaledImages );
	CircleInterestPoints( &mInterestImages, mImg, &mResult );
}

/*---------------------------------------------------------------------------*
 * Description: Initialization loads and converts the original color image 
 *              and computes the integral image (using a blepo function)
 *
 * Parameters:
 * [+] fileName - the name and path of an image to load
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void Surf::Initialize( CString fileName )
{
	Load(    fileName, &mImg );
	Convert( mImg, &mGrayImg );
	Convert( mGrayImg, &mIntImg );

	ComputeIntegralImage( mIntImg, &mIntegralImg );
	Figure figII( "Integral Image" );
	figII.Draw( mIntegralImg );
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the transpose of an image.
 *
 * Parameters:
 * [+] img - any ImgInt
 * [+] out - transposed image
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void Surf::TransposeImgInt(const ImgInt& img, ImgInt* out) 
{ 
	int width = img.Width();
	int height = img.Height();
	ImgInt result;

	result.Reset( height, width );

	for ( int i = 0 ; i < width ; i++ )
	{
		for ( int j = 0 ; j < height ; j++ )
		{
			result(j,i) = img(i,j);
		}
	}

	*out = result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the images for an octave at 4 different scales.
 *               CreateScaledImages takes the integral image and does the fast 
 *               convolution using approximated 2nd derivative kernels.
 *
 * Parameters:
 * [+] img         - the integral image
 * [+] scaledImage - Hessian determinant images at different filter levels
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void Surf::CreateScaledImages( const ImgInt& img, vector <ImgInt> * scaledImages )
{
	int scaleStart     = 9;
	int numScales      = 4;
	int scaleIncrement = 6;
	int scale, halfScale;      
	int i, j, k;
	ImgInt dxx, dyy, dxy;
	dxx.Reset( img.Width(), img.Height() ) ;
	dxy.Reset( img.Width(), img.Height() ) ;
	dyy.Reset( img.Width(), img.Height() ) ;
	Set( &dxx, 0 );
	Set( &dxy, 0 );
	Set( &dyy, 0 );

	vector <ImgInt> result(numScales);

	for ( i = 0 ; i < numScales ; i++ )
	{
		scale        = scaleStart + (i * scaleIncrement);
		halfScale    = floor( scale / 2 );
		result.at(i).Reset( img.Width(), img.Height() ) ;
		Set( &result.at(i), 0 );

		// start iterating over the image so that the 
		// scale for the octave doesn't go out of bounds
		for ( j = halfScale+1 ; j < img.Width() - halfScale-1 ; j++ )
		{
			for ( k = halfScale+1 ; k < img.Height() - halfScale - 1 ; k++ )
			{
				Rect myRect = GetFilterBorders( scale, Point(j,k) );
				dxx(j,k) = GetDxx( scale, myRect, img );
				dxy(j,k) = GetDxy( scale, myRect, img );
				dyy(j,k) = GetDyy( scale, myRect, img );
				/*result.at(i)(j,k) = GetDxx( scale, myRect, img ) * 
					                GetDyy( scale, myRect, img ) - 
									pow( ( 0.9 * GetDxy( scale, myRect, img ) ), 2 );
									*/
				result.at(i)(j,k) = dxx(j,k) * dyy(j,k) - pow( ( 0.9 * dxy(j,k) ), 2 );
			}
		}
		if ( debug )
		{
			char buffer[20], bufxy[20], bufyy[20];
			itoa( scale, buffer, 10 );
			strcat( buffer, " - filter, Dxx");
			CString strDxx( buffer );
			Figure figDxx(strDxx);
			figDxx.Draw( dxx );
			
			itoa( scale, bufxy, 10 );
			strcat( bufxy, " - filter, Dxy");
			CString strDxy( bufxy );
			Figure figDxy(strDxy);
			figDxy.Draw( dxy );
			
			itoa( scale, bufyy, 10 );
			strcat( bufyy, " - filter, Dyy");
			CString strDyy( bufyy );
			Figure figDyy(strDyy);
			figDyy.Draw( dyy );
		}
	}

	*scaledImages = result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to print the scaled images created for an octave.
 *
 * Parameters:
 * [+] scaleStart - the size of the smallest scale in an octave
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void Surf::PrintScaledImages( int scaleStart )
{
	int numScales      = 4;
	int scaleIncrement = 6;
	int scale;

	// print out images at each scale
	for ( int i = 0 ; i < numScales ; i++ )
	{
		scale = scaleStart + (i * scaleIncrement);
		char buffer[20];
		itoa( scale, buffer, 10 );
		strcat( buffer, " - filter, octave 1");
		CString title( buffer );
		Figure fig( title );
		fig.Draw( mScaledImages.at(i) );
	}
}


/*---------------------------------------------------------------------------*
 * Description:  Method to get the values of convolving an area using a 2nd 
 *               derivative filter about the horizontal.
 *
 * Parameters:
 * [+] scale  - the size of the filter 
 * [+] myRect - the rectangle with the boundaries for a filter
 * [+] img    - the integral image
 * Return: Sum of intensities in side rectangle
 *---------------------------------------------------------------------------*/
int Surf::GetDxx( int scale, const Rect& myRect, const ImgInt& img )
{
	int result = 0;
	int l, r, t, b;
	int pad = floor(scale/6) + 1;
	int thirdScale = floor(scale/3);

	// Get sum of 1st rectangle
	l = myRect.left;
	r = myRect.left   + thirdScale;
	t = myRect.top    + pad;
	b = myRect.bottom - pad;
	result += img(l,t) + img(r,b) - img(l,b) - img(r,t);

	// Get sum of 2nd rectangle
	l = myRect.left   + thirdScale;
	r = myRect.right  - thirdScale;
	t = myRect.top    + pad;
	b = myRect.bottom - pad;
	result += ( -2 * ( img(l,t) + img(r,b) - img(l,b) - img(r,t) ) );

	// Get sum of 3rd rectangle
	l = myRect.right  - thirdScale;
	r = myRect.right;
	t = myRect.top    + pad;
	b = myRect.bottom - pad;
	result += img(l,t) + img(r,b) - img(l,b) - img(r,t);

	// return the result
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the values of convolving an area using a 2nd 
 *               derivative filter about the vertical.
 *
 * Parameters:
 * [+] scale  - the size of the filter 
 * [+] myRect - the rectangle with the boundaries for a filter
 * [+] img    - the integral image
 * Return: Sum of intensities in side rectangle
 *---------------------------------------------------------------------------*/
int Surf::GetDyy( int scale, const Rect& myRect, const ImgInt& img )
{
	int result = 0;
	int l, r, t, b;
	int pad        = floor(scale/6) + 1;
	int thirdScale = floor(scale/3);

	// Get sum of 1st rectangle
	l = myRect.left  + pad;
	r = myRect.right - pad;
	t = myRect.top;
	b = myRect.top   + thirdScale;
	result += img(l,t) + img(r,b) - img(l,b) - img(r,t);

	// Get sum of 2nd rectangle
	l = myRect.left  + pad;
	r = myRect.right - pad;
	t = myRect.top   + thirdScale;
	b = myRect.top   + ( thirdScale * 2 );
	result += ( -2 * ( img(l,t) + img(r,b) - img(l,b) - img(r,t) ) );

	// Get sum of 3rd rectangle
	l = myRect.left  + pad;
	r = myRect.right - pad;
	t = myRect.top   + ( thirdScale * 2 );
	b = myRect.bottom;
	result += img(l,t) + img(r,b) - img(l,b) - img(r,t);

	// return the result
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the values of convolving an area using a 2nd 
 *               derivative filter about the horizontal and vertical.
 *
 * Parameters:
 * [+] scale  - the size of the filter 
 * [+] myRect - the rectangle with the boundaries for a filter
 * [+] img    - the integral image
 * Return: Sum of intensities in side rectangle
 *---------------------------------------------------------------------------*/
int Surf::GetDxy( int scale, const Rect& myRect, const ImgInt& img )
{
	int result = 0;
	int l, r, t, b;
	int halfScale = floor(scale/2);
	int pad       = floor(scale/6);

	// Get sum of 1st rectangle
	l = myRect.left + pad;
	r = myRect.left + halfScale;
	t = myRect.top  + pad;
	b = myRect.top  + halfScale;
	result += img(l,t) + img(r,b) - img(l,b) - img(r,t);

	// Get sum of 2nd rectangle
	l = myRect.right  - halfScale;
	r = myRect.right  - pad;
	t = myRect.top    + pad;
	b = myRect.top    + halfScale;
	result += ( -1 * ( img(l,t) + img(r,b) - img(l,b) - img(r,t) ) );

	// Get sum of 3rd rectangle
	l = myRect.left   + pad;
	r = myRect.left   + halfScale;
	t = myRect.bottom - halfScale;
	b = myRect.top    + halfScale;
	result += ( -1 * ( img(l,t) + img(r,b) - img(l,b) - img(r,t) ) );

	// Get sum of 4th rectangle
	l = myRect.right  - halfScale;
	r = myRect.right  - pad;
	t = myRect.bottom - halfScale;
	b = myRect.bottom - pad;
	result += img(l,t) + img(r,b) - img(l,b) - img(r,t);

	// return the result
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the boundaries for the filter area around a
 *               point.
 *
 * Parameters:
 * [+] scale - the size of the filter 
 * [+] p     - the point around which the filter must be fit
 * Return: A rectangle that has the boundaries for a filter
 *---------------------------------------------------------------------------*/
Rect Surf::GetFilterBorders( int scale, const Point& p )
{
	Rect result;
	int halfScale = floor(scale/2);
	result.top    = p.y - halfScale;
	result.bottom = p.y + halfScale;
	result.left   = p.x - halfScale;
	result.right  = p.x + halfScale;

	return result;
}


/*---------------------------------------------------------------------------*
 * Description:  Use non-maximal suppression to find points of interest
 *
 * Parameters:
 * [+] interestImages  - pointer to the vector that will contain the results
 * [+] scales          - pointer to vector that contains the images convolved
 *                       at each scale in an octave
 * Return: nothing
 *---------------------------------------------------------------------------*/
void Surf::FindInterestPoints( vector <ImgInt> * interestImages, vector <ImgInt> * scales )
{
	ImgInt resultA, resultB;
	resultA.Reset( scales->at(0).Width(), scales->at(0).Height() );
	resultB.Reset( scales->at(0).Width(), scales->at(0).Height() );
	Set( &resultA, 0 );
	Set( &resultB, 0 );

	int pad = floor(9/2) + 1;

	for ( int i = pad ; i < scales->at(0).Width() - pad ; i++ )
	{
		for ( int j = pad ; j < scales->at(0).Height() - pad ; j++ )
		{
			if ( MaxAcrossScales( Point(i,j), scales->at(0), scales->at(1), scales->at(2) ) )
			{
				resultA(i,j) = scales->at(1)(i,j);
				if ( resultA(i,j) < 0 )
				{
					mInterestSigns.push_back( -1 );
				} else
				{
					mInterestSigns.push_back( 1 );
				}
				mInterestValues.push_back( abs(resultA(i,j)) );
				mInterestPoints.push_back( Point(i,j) );
			}
			if ( MaxAcrossScales( Point(i,j), scales->at(1), scales->at(2), scales->at(3) ) )
			{
				resultB(i,j) = scales->at(2)(i,j);
				if ( resultB(i,j) < 0 )
				{
					mInterestSigns.push_back( -1 );
				} else
				{
					mInterestSigns.push_back( 1 );
				}
				mInterestValues.push_back( abs(resultB(i,j)) );
				mInterestPoints.push_back( Point(i,j) );
			}
		}
	}
	interestImages->push_back(resultA);
	interestImages->push_back(resultB);

	Figure figA("Interest Image A");
	figA.Draw(resultA);
	Figure figB("Interest Image B");
	figB.Draw(resultB);
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the values of convolving an area using a 2nd 
 *               derivative filter about the horizontal and vertical.
 *
 * Parameters:
 * [+] p    - point within images to compare values
 * [+] imgA - image used for NMS
 * [+] imgB - image used for NMS
 * [+] imgC - image used for NMS
 * Return: the max value of a point in three images
 *---------------------------------------------------------------------------*/
int Surf::MaxOfThree( Point p, const ImgInt& imgA, const ImgInt& imgB, const ImgInt& imgC )
{
	int max = imgA( p.x, p.y );

	if ( imgB( p.x, p.y ) > max )
	{
		max = imgB( p.x, p.y );
	}

	if ( imgC( p.x, p.y ) > max )
	{
		max = imgC( p.x, p.y );
	}

	return max;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the values of convolving an area using a 2nd 
 *               derivative filter about the horizontal and vertical.
 *
 * Parameters:
 * [+] scale  - the size of the filter 
 * [+] myRect - the rectangle with the boundaries for a filter
 * [+] img    - the integral image
 * Return: a boolean to say whether or not the point of interest is max of 26
 *         neighbors
 *---------------------------------------------------------------------------*/
bool Surf::MaxAcrossScales( Point p, const ImgInt& imgA, const ImgInt& imgB, const ImgInt& imgC )
{
	bool result = true;
	int max     = imgB(p.x, p.y);

	for ( int i = p.x - 1 ; i <= p.x + 1 ; i++ )
	{
		for ( int j = p.y - 1 ; j <= p.y + 1 ; j++ )
		{
			int tempMax = MaxOfThree( Point(i,j), imgA, imgB, imgC );
			if ( tempMax > max )
			{
				max = tempMax;
			}
		}
	}

	if ( max != imgB(p.x, p.y) )
	{
		result = false;
	}

	return result;
}


/*---------------------------------------------------------------------------*
 * Description:  Method to draw a colored circle around a point of interest.
 *
 * Parameters:
 * [+] imgs - a vector of Hessian matrices
 * [+] img  - the original BGR image on which interest points will be marked
 * [+] out  - a copy of the original image with points of interest circled
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void Surf::CircleInterestPoints( vector <ImgInt> * imgs, const ImgBgr & img, ImgBgr * out )
{

	// min-max normalization
	vector <int>::iterator vit = mInterestValues.begin();
	int max = *max_element( mInterestValues.begin(), mInterestValues.end() );
	int min = max;
	int mean = 0;
	int sum = 0;
	while ( vit != mInterestValues.end() )
	{
		if ( *vit != 0 )
		{
			if ( *vit < min )
			{
				min = *vit;
			}
			sum += *vit;
			mean++;
		}
		vit++;
	}
	mean = sum / mean ;
	FILE *fout = fopen( "interestpoints.out", "w" );
	fprintf( fout, "Max: %d, Min: %d, Max-Min: %d, Mean: %d\n", max, min, max-min, mean );
	
	vector <double> interestValues;
	int counter = 0;
	double average = 0;
	int numNonZero = 0;
	vit = mInterestValues.begin();
	while ( vit != mInterestValues.end() )
	{
		int temp = *vit;
		double intVal = ((double)*vit - (double)min) / (double)( max - min );
		if ( *vit != 0 )
		{
			interestValues.push_back( intVal );
			fprintf( fout, "[%d] Value: %d, Value-Min: %d, Normalized value: %0.10lf\n", ++counter, temp, temp-min, intVal );
			average += intVal;
			numNonZero++;
		}
		else
		{
			interestValues.push_back( 0.0 );
		}
		vit++;
	}
	average = average / numNonZero;
	fprintf( fout, "Mean of normalized values: %lf", average );
	fclose(fout);

	*out = img;

	vector<double>::iterator dit = interestValues.begin();
	vector <int>::iterator   sit = mInterestSigns.begin();
	vector <Point>::iterator pit = mInterestPoints.begin();
	vit = mInterestValues.begin();

	while ( dit != interestValues.end() )
	{
		if ( *dit > average/2 )
		{
			if ( *sit > 0 )
			{
				DrawDot( (*pit), out, Bgr::GREEN, 1 );
				DrawCircle( (*pit), 8, out, Bgr::BLUE );					
			} else if ( *sit < 0 )
			{
				DrawDot( (*pit), out, Bgr::GREEN, 1 );
				DrawCircle( (*pit), 8, out, Bgr::RED );
			}
		}
		dit++;
		sit++;
		pit++;
	}

	Figure fig("SURF Detected Features");
	fig.Draw( *out );
}