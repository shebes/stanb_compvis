/*****************************************************************************
 * ImageMosaick.cpp
 *
 * Date: Thursday February 17, 2011
 *****************************************************************************/
#include "stdafx.h"
#include "ImageMosaick.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/*---------------------------------------------------------------------------*
 * Description:  Constructor for the ImageMosaick feature detector algorithm
 *
 * Parameters:
 * [+] fileName - the name of the file that will be the center of the panorama
 * Return: Nothing
 *---------------------------------------------------------------------------*/
ImageMosaick::ImageMosaick( CString fileName )
{
	debug = false;
	Initialize( fileName );
	TransferImageIntoLarger( mImg, &mResultImg );
	FindHomographies();
	
	Figure figBorders("Borders outlined");
	figBorders.Draw(mResultWithBorders);
}

/*---------------------------------------------------------------------------*
 * Description: Initialization loads and converts the original color image 
 *              and computes the integral image (using a blepo function)
 *
 * Parameters:
 * [+] fileName - the name and path of an image to load
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::Initialize( CString fileName )
{
	InitImages( fileName );
	Point imgSize( mImg.Width(), mImg.Height() );
	mResultImg.Reset( imgSize.x*2.8, imgSize.y*2.8 );
	mOrigin = Point2d( mResultImg.Width()/7*4 - (mImg.Width()/2), mResultImg.Height()/2 - (mImg.Height()/2) );
	ImgBgr::Pixel zeroPixel(0,0,0);
	Set( &mResultImg, zeroPixel );
	InitCorrespondingPoints();
}

/*---------------------------------------------------------------------------*
 * Description: An initialization method that loads all of the images that
 * will be included in the panorama.
 *
 * Parameters:
 * [+] directoryName - the directory in which all component images reside
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::InitImages( CString directoryName )
{
	Load( directoryName + "small_tillman06.jpg", &m06 );
	Load( directoryName + "small_tillman06.jpg", &mImg );
	Load( directoryName + "small_tillman07.jpg", &m07 );
	Load( directoryName + "small_tillman08.jpg", &m08 );
	Load( directoryName + "small_tillman09.jpg", &m09 );
	Load( directoryName + "small_tillman10.jpg", &m10 );
	Load( directoryName + "small_tillman11.jpg", &m11 );
	Load( directoryName + "small_tillman00.jpg", &m00 );
	Load( directoryName + "small_tillman01.jpg", &m01 );
	Load( directoryName + "small_tillman02.jpg", &m02 );
	Load( directoryName + "small_tillman03.jpg", &m03 );
	Load( directoryName + "small_tillman04.jpg", &m04 );
	Load( directoryName + "small_tillman05.jpg", &m05 );
}

/*---------------------------------------------------------------------------*
 * Description: Initialization method that turns the arrays of corresponding
 * points into Point2d vectors of points, and it translates the points of the
 * vectors that correspond to the center image to its new location in the 
 * larger canvas.
 *
 * Parameters:
 * [+] None
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::InitCorrespondingPoints()
{	
	int six_68[]   = {201,151,218,150,202,158,217,157,205,160,215,160,205,181,214,181,204,184,215,183,231,148,249,147,232,155,248,154,236,158,246,158,235,179,246,179,235,181,246,181,365,112,377,109,369,113,373,113,369,120,373,120,369,123,373,123,369,129,374,129,309,13,302,119};
	int eight_68[] = {50,239,65,238,50,246,65,246,53,249,63,249,52,269,62,269,51,272,61,272,81,237,98,236,82,244,97,243,85,246,95,246,83,267,94,267,82,270,93,270,213,202,225,201,217,205,221,205,217,212,221,212,217,215,221,215,217,221,221,221,163,106,153,210};
	
	int zero_06[]  = {118,7,128,7,118,28,128,28,118,52,128,52,150,4,160,3,150,25,160,25,150,50,160,49,117,93,127,92,117,114,127,114,149,90,159,90,149,111,159,110,13,6,22,6,13,28,22,27,13,52,22,52,73,41,282,50,242,94,273,14};
	int six_06[]   = {205,160,215,160,205,181,215,181,205,205,214,204,236,158,246,157,235,179,245,179,236,203,245,202,203,245,212,243,203,266,212,266,234,243,245,242,234,264,244,264,105,161,114,160,105,181,114,181,104,205,113,204,161,193,369,203,328,249,358,168};
	
	int one_16[]   = {125,129,114,108,125,106,114,84,160,132,158,82,147,82,140,133,157,105,189,118,107,165,126,164,112,193,140,162,158,162,156,190,109,74,23,134,19,106,10,84,20,82,16,169,15,192,8,106,68,55,42,60,45,48,135,51,219,30,212,43,241,23,210,35,7,13,145,106,226,50,280,72,247,50,262,54,258,62,275,33,288,32,276,56,289,57};
	int six_16[]   = {214,204,204,184,214,182,204,160,248,207,246,158,236,159,229,208,245,180,279,190,197,240,216,239,202,268,229,239,247,237,245,266,199,150,115,210,113,182,105,162,115,159,109,245,109,267,103,183,160,135,136,136,139,125,224,127,308,108,300,121,330,101,229,111,104,95,235,183,355,126,370,151,336,128,350,131,347,140,365,109,377,109,365,134,378,133};

	int zero_010[] = { 118, 7, 128, 6, 150, 4, 160, 3, 118, 27,128, 27, 150, 25, 160, 25, 118, 32, 128, 30,369, 44, 314, 183, 314, 189, 316, 52, 117, 93,127, 92, 149, 90, 159, 90, 117, 114, 127, 114,149, 112, 159, 112, 117, 117, 117, 138, 232, 145,231, 58, 255, 156, 111, 186, 129, 185, 149, 189 };
	int ten_010[]  = { 46, 94, 57, 94, 79, 92, 89, 92, 46, 114,56, 114, 79, 112, 89, 112, 46, 118, 56, 117,296, 136, 242, 274, 242, 281, 244, 142, 43, 180,53, 179, 76, 179, 86, 178, 42, 202, 53, 202,76, 199, 86, 201, 42, 205, 41, 226, 159, 235,160, 147, 183, 246, 33, 277, 52, 276, 73, 280 };
	
	int zero_0011[]   = {118, 52,128, 52,159, 49,149, 49,112, 89,131, 88,112, 142,131, 142,149, 91,159, 91,149, 136,159, 136,116, 213,126, 213,218, 261,232, 244,250, 222,244, 218,117, 115,127, 115,149, 113,159, 113,129, 186,113, 185,369, 44,315, 52,344, 64,364, 85,363, 72,380, 102};
	int eleven_0011[] = {72, 9,82, 9,115, 7,104, 7,66, 46,85, 46,66, 102,84, 102,104, 49,114, 49,104, 96,114, 96,69, 173,79, 173,172, 221,187, 205,205, 183,199, 178,70, 75,80, 75,103, 73,113, 73,82, 147,67, 145,326, 4,273, 12,301, 24,320, 45,319, 33,335, 62};

	int eight_89[] = {157,33,252,21,165,56,102,143,161,165,246,185,178,79,200,77,222,74,163,108,163,127,162,132,273,67,246,100,246,120,247,125,163,81,247,73,188,78,211,76,233,74,246,46,174,164,162,104,247,96,265,132,266,75,158,38,159,87,249,77};
	int nine_89[]  = {151,123,248,113,160,146,93,234,153,259,241,281,173,170,195,168,218,166,157,199,157,219,156,223,270,159,242,192,242,213,243,218,158,172,243,165,184,170,206,167,229,165,243,137,167,257,156,195,243,188,261,226,263,167,154,127,154,177,245,169};

	int five_54[] = {109,157,104,210,103,185,137,265,157,283,182,263,180,282,236,282,275,282,276,220,237,223,239,200,276,195,239,141,278,136,298,105,262,81,108,80,129,123,204,112,158,86,180,82,156,123,180,119,136,148,194,142,186,196,160,154,166,197,137,202};
	int four_54[] = {173,94,169,148,168,123,203,202,223,219,249,199,247,217,302,217,341,216,340,154,302,159,304,136,342,131,305,77,343,70,367,38,325,14,171,18,194,60,266,48,220,21,244,18,218,60,243,57,200,88,258,78,252,133,224,92,232,133,202,141};

	int four_42[] = {276,280, 276,272, 308,265, 317,264, 327,264, 309,213, 318,213, 338,212,328,213, 309,189, 309,168, 319,168, 328,166, 338,187,342,160, 254,194, 255,187, 247,236, 239,236, 220,237, 206,156, 199,85,199,91, 258,84, 233,115, 199,85, 199,92, 258,77, 258,84,225,93 };
	int two_42[]  = {114,202, 113,195, 144,186, 153,186, 163,185, 145,136, 154,136, 173,135,164,135, 145,112, 145,92, 155,91, 164,90, 173,110, 178,84,91,118, 92,111, 84,160, 75,160, 57,162, 42,81, 33,9, 33,14, 93,7, 68,39,33,7, 32,15, 93,1, 93,6, 60,15};

	int three_43[] = {160,23,177,20,195,17,160,65,177,63,194,60,264,11,283,7,264,56,283,53,200,128,200,165,263,140,263,94,291,92,291,139,183,166,182,131,154,69,155,19,158,168,165,131,262,192,281,191,290,191,183,150,177,42,264,34,195,40,283,32};
	int four_43[]  = {209,95,226,93,243,90,207,136,225,134,242,132,311,85,330,82,310,129,330,127,247,199,246,236,309,213,309,168,338,166,338,212,229,236,229,200,202,140,204,92,203,238,212,202,308,265,327,264,337,264,229,220,224,115,311,108,243,112,330,105};

	// these contain only 20 pairs of coordinates
	int six_67[]   = {160,134,309,103,336,127,330,102,309,38,353,68,355,126,369,151,201,151,250,147, 201,267,245,265,326,157,345,175,328,249,343,257,248,54,296,103,160,193,368,204};
	int seven_67[] = {18,127,170,98,195,122,189,97,170,34,213,66,215,123,228,147,60,145,110,142, 58,262,102,259,185,153,204,171,186,243,200,252,110,48,155,99,17,187,226,200};

	// these contain only 20 pairs of coordinates	
	int four_64[] = {253,55,360,56,338,125,249,196,294,69,340,161,304,164,338,190,254,55,254,153,319,8,378,56,227,57,232,92,308,188,357,38,304,45,315,44,326,17,305,132};
	int six_64[] = {31,131,134,139,112,204,20,274,70,147,112,240,77,242,109,267,31,131,18,230,98,89,151,137,3,131,7,167,80,265,132,120,82,123,92,123,104,97,79,209};

	CreatePointVectorFromArray( 32, six_68,      &mSixEight );
	TranslatePointsVector( mOrigin, &mSixEight );
	CreatePointVectorFromArray( 32, eight_68,    &mEightSix );
	CreatePointVectorFromArray( 20, six_67,      &mSixSeven );
	TranslatePointsVector( mOrigin, &mSixSeven );
	CreatePointVectorFromArray( 20, seven_67,    &mSevenSix );
	CreatePointVectorFromArray( 30, zero_06,     &mZeroSix  );
	CreatePointVectorFromArray( 30, six_06,      &mSixZero  );
	TranslatePointsVector( mOrigin, &mSixZero );
	CreatePointVectorFromArray( 30, six_16,      &mSixOne   );
	TranslatePointsVector( mOrigin, &mSixOne );
	CreatePointVectorFromArray( 30, one_16,      &mOneSix   );
	CreatePointVectorFromArray( 30, ten_010,     &mTenZero  );
	CreatePointVectorFromArray( 30, zero_010,    &mZeroTen  );
	CreatePointVectorFromArray( 30, eleven_0011, &mElevenZero );
	CreatePointVectorFromArray( 30, zero_0011,   &mZeroEleven );
	CreatePointVectorFromArray( 30, nine_89,     &mNineEight  );
	CreatePointVectorFromArray( 30, eight_89,    &mEightNine  );
	CreatePointVectorFromArray( 20, six_64,      &mSixFour  );
	TranslatePointsVector( mOrigin, &mSixFour );
	CreatePointVectorFromArray( 20, four_64,     &mFourSix  );
	CreatePointVectorFromArray( 30, five_54,     &mFiveFour );
	CreatePointVectorFromArray( 30, four_54,     &mFourFive );
	CreatePointVectorFromArray( 30, four_42,     &mFourTwo  );
	CreatePointVectorFromArray( 30, two_42,      &mTwoFour  );
	CreatePointVectorFromArray( 30, four_43,     &mFourThree);
	CreatePointVectorFromArray( 30, three_43,    &mThreeFour);
}

/*---------------------------------------------------------------------------*
 * Description: This method translates all of the points in a corresponding
 * points vector to the origin point passed in as the 1st parameter.
 *
 * Parameters:
 * [+] origin - the top leftmost corner of the central image when placed
 *              in the larger composite canvas
 * [+] pts    - the vector of corresponding points for an image
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::TranslatePointsVector( Point2d origin, vector <Point2d> * pts )
{
	vector <Point2d>::iterator it = pts->begin();
	while ( it != pts->end() )
	{
		it->x = origin.x + it->x;
		it->y = origin.y + it->y;
		it++;
	}
}


/*---------------------------------------------------------------------------*
 * Description: This method reads in an integer array and places the entries 
 * in a Point2d vector so it can be used with the homography and warp blepo
 * methods.
 *
 * Parameters:
 * [+] arr      - points of interest, in an array
 * [+] myValues - point vector to be created from points array
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::CreatePointVectorFromArray( int size, int arr[], vector <Point2d> * myValues )
{
	vector <Point2d> result(size);
	for ( int i = 0 ; i < size*2 ; i += 2 )
	{
		myValues->push_back( Point2d(arr[i], arr[i+1]) );
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a small image and places it in a larger
 * canvas so that other images can be warped around it.
 *
 * Parameters:
 * [+] transferee - image to be transferred
 * [+] img        - resultant image
 * [+] newCenter  - a point representing the pixel on which the transferee 
 *                  will be centered
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::TransferImageIntoLarger( const ImgBgr & transferee, ImgBgr * img )
{

	int i, j;
	for( i = 0 ; i < transferee.Width() ; i++ )
	{
		for( j = 0 ; j < transferee.Height() ; j++ )
		{
			int newx, newy;
			newx = i+(int)mOrigin.x;
			newy = j+(int)mOrigin.y;
			if ( newx > -1 && newy > -1 && newx < img->Width() && newy < img->Height() )
			{
				(*img)( newx, newy ) = transferee(i,j);
			}
		}
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method takes the point vectors, calculates the 
 * homographies, warps the images based on the homography matrices and 
 * stitches each image to the composite canvas, creating the final panorama.
 *
 * Parameters:
 * [+] None
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::FindHomographies()
{
	ImgFloat fx, fy;
	ImgBgr tempImg, tempImgB, tempImgC, tempImgD, tempImgE;
	ImgBgr tempImgF, tempImgG, tempImgH, tempImgI, tempImgJ, tempImgK;
	
	MatDbl homog(3,3), homogA(3,3), homogB(3,3);
	//PrintPoint2DVector( "SixEight.out", mSixEight );
	
	Figure figResult("Result Figure");
	figResult.Draw(mResultImg);
	mResultWithBorders = mResultImg;
	
	HomographyFit( mSixEight, mEightSix, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx08, &fy08 );
	Warp( m08, fx, fy, &tempImg);
	AddWarpedImage( m08, fx08, fy08, &mResultImg );
	figResult.Draw(mResultImg);
	if (debug) 
	{
		Figure figWarp("Small Tillman 8 Warped Image");
		figWarp.Draw(tempImg);
	}

	HomographyFit( mSixZero, mZeroSix, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx00, &fy00 );
	Warp( m00, fx00, fy00, &tempImgB);
	AddWarpedImage( m00, fx00, fy00, &mResultImg );
	figResult.Draw(mResultImg);
	if (debug) 
	{
		Figure figWarpB("Small Tillman 0 Warped Image");
		figWarpB.Draw(tempImgB);
	}

	HomographyFit( mSixOne, mOneSix, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx01, &fy01 );
	Warp( m01, fx01, fy01, &tempImgC);
	AddWarpedImage( m01, fx01, fy01, &mResultImg );
	figResult.Draw(mResultImg);
	if (debug) 
	{		
		Figure figWarpC("Small Tillman 1 Warped Image");
		figWarpC.Draw(tempImgC);
	}
	
	HomographyFit( mSixSeven, mSevenSix, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx07, &fy07 );
	Warp( m07, fx07, fy07, &tempImgD);
	AddWarpedImage( m07, fx07, fy07, &mResultImg );
	figResult.Draw(mResultImg);
	if (debug) 
	{
		Figure figWarpD("Small Tillman 7 Warped Image");
		figWarpD.Draw(tempImgD);
	}

	HomographyFit( mZeroTen, mTenZero, &homogB );
	HomographyFit( mSixZero, mZeroSix, &homogA );
	MatrixMultiply( homogB, homogA, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx10, &fy10 );
	Warp( m10, fx10, fy10, &tempImgE);
	AddWarpedImage( m10, fx10, fy10, &mResultImg );
	figResult.Draw(mResultImg);
	if ( debug )
	{
		Figure figWarpE("Small Tillman 10 Warped Image");
		figWarpE.Draw(tempImgE);
	}
	

	HomographyFit( mZeroEleven, mElevenZero, &homogB );
	HomographyFit( mSixZero, mZeroSix, &homogA );
	MatrixMultiply( homogB, homogA, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx11, &fy11 );
	Warp( m11, fx11, fy11, &tempImgF);
	AddWarpedImage( m11, fx11, fy11, &mResultImg );
	figResult.Draw(mResultImg);
	if ( debug )
	{
		Figure figWarpF("Small Tillman 11 Warped Image");
		figWarpF.Draw(tempImgF);
	}

	HomographyFit( mEightNine, mNineEight, &homogB );
	HomographyFit( mSixEight, mEightSix, &homogA );
	MatrixMultiply( homogB, homogA, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx09, &fy09 );
	Warp( m09, fx09, fy09, &tempImgG);
	AddWarpedImage( m09, fx09, fy09, &mResultImg );
	figResult.Draw(mResultImg);
	if (debug)
	{
		Figure figWarpG("Small Tillman 9 Warped Image");
		figWarpG.Draw(tempImgG);
	}

	HomographyFit( mSixFour, mFourSix, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx04, &fy04 );
	Warp( m04, fx04, fy04, &tempImgH);
	AddWarpedImage( m04, fx04, fy04, &mResultImg );
	figResult.Draw(mResultImg);
	if ( debug )
	{
		Figure figWarpH("Small Tillman 4 Warped Image");
		figWarpH.Draw(tempImgH);
	}

	HomographyFit( mFourFive, mFiveFour, &homogB );
	HomographyFit( mSixFour, mFourSix, &homogA );
	MatrixMultiply( homogB, homogA, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx05, &fy05 );
	Warp( m05, fx05, fy05, &tempImgI);
	AddWarpedImage( m05, fx05, fy05, &mResultImg );
	figResult.Draw(mResultImg);
	if ( debug )
	{
		Figure figWarpI("Small Tillman 5 Warped Image");
		figWarpI.Draw(tempImgI);
	}

	HomographyFit( mFourTwo, mTwoFour, &homogB );
	HomographyFit( mSixFour, mFourSix, &homogA );
	MatrixMultiply( homogB, homogA, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx02, &fy02 );
	Warp( m02, fx02, fy02, &tempImgJ);
	AddWarpedImage( m02, fx02, fy02, &mResultImg );
	figResult.Draw(mResultImg);
	if ( debug )
	{
		Figure figWarpJ("Small Tillman 2 Warped Image");
		figWarpJ.Draw(tempImgJ);
	}

	HomographyFit( mFourThree, mThreeFour, &homogB );
	HomographyFit( mSixFour, mFourSix, &homogA );
	MatrixMultiply( homogB, homogA, &homog );
	InitWarpHomography( homog, mResultImg.Width(), mResultImg.Height(), &fx03, &fy03 );
	Warp( m03, fx03, fy03, &tempImgK);
	AddWarpedImage( m03, fx03, fy03, &mResultImg );
	figResult.Draw(mResultImg);
	if ( debug )
	{
		Figure figWarpK("Small Tillman 3 Warped Image");
		figWarpK.Draw(tempImgK);
	}

	mResultWithBorders = mResultImg;

	AddWarpBorder( mResultImg, fx00, fy00, &mResultWithBorders, Bgr::MAGENTA );
	AddWarpBorder( m08, fx08, fy08, &mResultWithBorders, Bgr::GREEN  );
	AddWarpBorder( m00, fx00, fy00, &mResultWithBorders, Bgr::BLACK  );
	AddWarpBorder( m01, fx01, fy01, &mResultWithBorders, Bgr::WHITE  );
	AddWarpBorder( m07, fx07, fy07, &mResultWithBorders, Bgr::BLACK  );
	AddWarpBorder( m10, fx10, fy10, &mResultWithBorders, Bgr::RED    );
	AddWarpBorder( m11, fx11, fy11, &mResultWithBorders, Bgr::GREEN  );
	AddWarpBorder( m09, fx09, fy09, &mResultWithBorders, Bgr::BLUE   );
	AddWarpBorder( m04, fx04, fy04, &mResultWithBorders, Bgr::YELLOW );
	AddWarpBorder( m05, fx05, fy05, &mResultWithBorders, Bgr::WHITE  );
	AddWarpBorder( m02, fx02, fy02, &mResultWithBorders, Bgr::RED    );
	AddWarpBorder( m03, fx03, fy03, &mResultWithBorders, Bgr::YELLOW );
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a filename and a vector of points.  The 
 * method prints the points to the applications current directory in a file
 * name given by the file name parameter.
 *
 * Parameters:
 * [+] filename - name of the output file
 * [+] value    - vector of Point2d objects
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::PrintPoint2DVector( char * filename, const vector <Point2d> & value )
{
	FILE * fout = fopen( filename,"w");
	vector<Point2d>::const_iterator it = value.begin();
	while ( it != value.end() )
	{
		fprintf(fout, "Point (%f,%f)\n", it->x, it->y );
		it++;
	}
	fclose(fout);
}

/*---------------------------------------------------------------------------*
 * Description: Takes an image the size of the canvas and calculates the 
 * chamfer distance to pixels that are not valid.
 *
 * Parameters:
 * [+] img - composite image
 * [+] out - the chamfered version of the composite image
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::ChamferToImageBorders( const ImgBgr & img, ImgInt * out )
{
	// create the border gray image using the BGR image
	ImgGray border;
	border.Reset( img.Width(), img.Height() );
	Set( &border, 254 );
	
	for ( int i = 0 ; i < img.Width() ; i++ )
	{
		for ( int j = 0 ; j < img.Height() ; j++ )
		{
			if ( img(i,j) != ImgBgr::Pixel(0,0,0) )
			{
				border(i,j) = 0;
			}
		}
	}

	Chamfer( border, out );
}

/*---------------------------------------------------------------------------*
 * Description: Takes the pixel-wise functions created with InitWarpHomography
 * and calculates the chamfer distance to pixels that are not valid. 
 *
 * Parameters:
 * [+] fx  - an image that is created during warping that tells the new x 
 *           coordinates of the image
 * [+] fy  - an image that is created during warping that tells the new y 
 *           coordinates of the image
 * [+] out - the chamfered version of the component image
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::ChamferToImageBorders( const ImgFloat & fx, const ImgFloat & fy, ImgInt * out )
{
	// create the border image using the fx, fy images calculated from warping
	ImgGray border;
	border.Reset( fx.Width(), fx.Height() );
	Set( &border, 254 );
	
	for ( int i = 0 ; i < fx.Width() ; i++ )
	{
		for ( int j = 0 ; j < fx.Height() ; j++ )
		{
			if ( fx(i,j) > -1 && fx(i,j) < mImg.Width() && 
				 fy(i,j) > -1 && fy(i,j) < mImg.Height() )
			{
				border(i,j) = 0;
			}
		}
	}

	Chamfer( border, out );
}

/*---------------------------------------------------------------------------*
 * Description: This method blends in an image into the canvas or panorama.
 * This method uses the pixel-wise functions created with InitWarpHomography
 * to place the warped image into the canvas.
 *
 * Parameters:
 * [+] img - component image that will be warped onto the canvas
 * [+] fx  - an image that is created during warping that tells the new x 
 *           coordinates of the image
 * [+] fy  - an image that is created during warping that tells the new y 
 *           coordinates of the image
 * [+] out - canvas image with stitched in images
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::AddWarpedImage( const ImgBgr & img, const ImgFloat & fx, const ImgFloat & fy, ImgBgr * out )
{
	ImgInt  resultChamfered, warpChamfered;

	ChamferToImageBorders( mResultImg, &resultChamfered );
	ChamferToImageBorders( fx, fy, &warpChamfered);
	if ( debug )
	{
		Figure figResultChamfered("Chamfered Panorama");
		figResultChamfered.Draw( resultChamfered );
		Figure figWarpChamfered("Chamfered Warp Image");
		figWarpChamfered.Draw( warpChamfered );
	}

	double resultHalfWidth, imgHalfWidth, imgHalfHeight, halfLength;
	resultHalfWidth = out->Width() / 2;
	imgHalfWidth    = img.Width()  / 2;
	imgHalfHeight   = img.Height() / 2;
	halfLength      = sqrt( pow(imgHalfWidth,2) + pow(imgHalfHeight,2) );


	for ( int i = 0 ; i < out->Width() ; i++ )
	{
		for ( int j = 0 ; j < out->Height() ; j++ )
		{
			if ( fx(i,j) > -1 && fx(i,j) < img.Width() && 
				 fy(i,j) > -1 && fy(i,j) < img.Height() &&
				 (*out)(i,j) == ImgBgr::Pixel(0,0,0) )
			{
				(*out)(i,j) = img( fx(i,j), fy(i,j) );
			} 
			else if( fx(i,j) > -1 && fx(i,j) < img.Width() && 
			         fy(i,j) > -1 && fy(i,j) < img.Height() &&
			         (*out)(i,j) != ImgBgr::Pixel(0,0,0))
			{
				double r,g,b;
				double comp, warp;
				double weightedAvgR, weightedAvgW;
				comp = resultChamfered(i,j);
				warp = warpChamfered(i,j);
				weightedAvgR = comp / ( comp + warp ) ;
				weightedAvgW = warp / ( comp + warp ) ;

				r = ( weightedAvgR * (*out)(i,j).r ) + ( weightedAvgW * img( fx(i,j), fy(i,j) ).r );
				g = ( weightedAvgR * (*out)(i,j).g ) + ( weightedAvgW * img( fx(i,j), fy(i,j) ).g );
				b = ( weightedAvgR * (*out)(i,j).b ) + ( weightedAvgW * img( fx(i,j), fy(i,j) ).b );

				(*out)(i,j) = ImgBgr::Pixel((int)b,(int)g,(int)r);
			}
		}
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method outlines a component image in the panorama with a
 * a color.
 *
 * Parameters:
 * [+] img   - component image that will be warped onto the canvas
 * [+] fx    - an image that is created during warping that tells the new x 
 *             coordinates of the image
 * [+] fy    - an image that is created during warping that tells the new y 
 *             coordinates of the image
 * [+] out   - canvas image with stitched in images
 * [+] color - color with which to outline an image placed onto the canvas
 *
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageMosaick::AddWarpBorder( const ImgBgr & img, const ImgFloat & fx, const ImgFloat & fy, ImgBgr * out, const Bgr & color )
{
	Point tl, tr, bl, br;
	if ( img.Width() == mResultImg.Width() )
	{
		tl = Point( mOrigin.x, mOrigin.y );
		tr = Point( mOrigin.x + mImg.Width(), mOrigin.y );
		bl = Point( mOrigin.x, mOrigin.y + mImg.Height() );
		br = Point( mOrigin.x + mImg.Width(), mOrigin.y + mImg.Height() );
	}
	else
	{
		for ( int i = 0 ; i < fx.Width() ; i++ )
		{
			for ( int j = 0 ; j < fx.Height() ; j++ )
			{
				if ( (int)fx(i,j) == 0 && (int)fy(i,j) == 0 )
				{
					tl = Point(i,j);
				}
				if ( (int)fx(i,j) == img.Width()-1 && (int)fy(i,j) == 0 )
				{
					tr = Point(i,j);
				}
				if ( ((int)fx(i,j) > -2 && (int)fx(i,j) < 2)  && (int)fy(i,j) == img.Height()-1 )
				{
					bl = Point(i,j);
				}
				if ( (int)fx(i,j) == 0 && (int)fy(i,j) == img.Height()-1 )
				{
					bl = Point(i,j);
				}
				if ( (int)fx(i,j) == img.Width()-1 && (int)fy(i,j) == img.Height()-1 )
				{
					br = Point(i,j);
				}
			}
		}
	}

	DrawLine( tl, tr, out, color, 2 );
	DrawLine( tr, br, out, color, 2 );
	DrawLine( br, bl, out, color, 2 );
	DrawLine( bl, tl, out, color, 2 );
}
