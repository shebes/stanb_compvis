/*****************************************************************************
 * ImageMosaick.h
 *
 *****************************************************************************/

#pragma once

#ifndef _IMAGEMOSAICK_H_
#endif // _IMAGEMOSAICK_H_

#include "ImageProcessing.h"
#include <queue>

using namespace blepo;
using namespace std;

class ImageMosaick
{
public:
	// constructors
	ImageMosaick( CString fileName );

	void FindHomographies();
	void TranslatePointsVector( Point2d origin, vector <Point2d> * pts );
	void PrintPoint2DVector( char * filename, const vector <Point2d> & value );
	void CreatePointVectorFromArray( int size, int arr[], vector <Point2d> * myValues );

private:
	// variables
	bool debug;

	// New Origin
	Point2d mOrigin;

	// corresponding points
	vector <Point2d> mSixSeven,   mSevenSix;
	vector <Point2d> mSixEight,   mEightSix;
	vector <Point2d> mSixZero,    mZeroSix;
	vector <Point2d> mSixOne,     mOneSix;
	vector <Point2d> mZeroTen,    mTenZero;
	vector <Point2d> mZeroEleven, mElevenZero;
	vector <Point2d> mEightNine,  mNineEight;
	vector <Point2d> mSixFour,    mFourSix;
	vector <Point2d> mFiveFour,   mFourFive;
	vector <Point2d> mFourTwo,    mTwoFour;
	vector <Point2d> mFourThree,  mThreeFour;


	// Images
	ImgBgr  mImg; // Original input image
	ImgBgr  mResultImg; // The larger image into which the stitched result will be saved
	ImgBgr  mResultBorder; // The mosaicked image with image borders outlined
	ImgBgr  mResultWithBorders;
	ImgBgr  m00, m01, m02, m03, m04, m05, m06, m07, m08, m09, m10, m11;
	ImgFloat fx00, fx01, fx02, fx03, fx04, fx05, fx06, fx07, fx08, fx09, fx10, fx11;
	ImgFloat fy00, fy01, fy02, fy03, fy04, fy05, fy06, fy07, fy08, fy09, fy10, fy11;
	
	// class initialization
	void Initialize( CString fileName );
	void InitImages( CString myDirectory );
	void InitCorrespondingPoints();

	// other methods
	void AddWarpBorder( const ImgBgr & img, const ImgFloat & fx, const ImgFloat & fy, ImgBgr * out, const Bgr& color=Bgr::GREEN );
	void AddWarpedImage( const ImgBgr & img, const ImgFloat & fx, const ImgFloat & fy, ImgBgr * out );
	void TransferImageIntoLarger( const ImgBgr & transferee, ImgBgr * img );
	void ChamferToImageBorders( const ImgBgr & img, ImgInt * out );
	void ChamferToImageBorders( const ImgFloat & fx, const ImgFloat & fy, ImgInt * out );
};