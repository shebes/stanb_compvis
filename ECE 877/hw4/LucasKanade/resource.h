//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by LucasKanade.rc
//
#define IDD_LUCASKANADE_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_IMGPROC_GROUPBOX            1000
#define IDC_FLOODFILL4                  1001
#define IDC_FLOODFILL8                  1002
#define IDC_OUTLINE_FOREGROUND          1003
#define IDC_CANNY                       1004
#define IDC_WATERSHED_GROUP_BOX         1005
#define IDC_WATERSHED_SEGMENTATION      1006
#define IDC_EDIT_THRESHOLD              1007
#define IDC_LBL_THRESHOLD               1008
#define IDC_EDIT_BORDER_PIXEL_SIZE      1009
#define IDC_BORDER_MAX                  1010
#define IDC_EDIT_WATERSHED_SIGMA        1011
#define IDC_EDIT_CANNY_SIGMA            1012
#define IDC_CANNY_SIGMA                 1013
#define IDC_TEMPATE_MATCHING            1014
#define IDC_CANNY_MATCHING_GROUP        1015
#define IDC_STEREO_GROUP                1016
#define IDC_PENCIL_EPIPOLAR             1017
#define IDC_BTN_STEREO_MATCHING         1018
#define IDC_LBL_DISPARY_MIN             1019
#define IDC_BOX_HALFWIDTH               1020
#define IDC_LBL_DISPMAX                 1021
#define IDC_BOX_DISPMAX                 1022
#define IDC_BOX_DISPMIN                 1023
#define IDC_LK_GRP                      1024
#define IDC_LK_OPEN_DLG                 1025
#define IDC_LK_SIGMA                    1026
#define IDC_EDIT_LK_SIGMA               1027
#define IDC_EDIT_WINDOW_SIZE            1028
#define IDC_LBL_WINDOW_SIZE             1029
#define IDC_LETTER_DETECT               1030
#define IDC_ECE877                      1031
#define IDC_BOX_DETECT_THRESHOLD        1032
#define IDC_DETECT_THRESHOLD            1033
#define IDC_BTN_LEVELSET                1034
#define IDC_LBL_CVSIGMA                 1035
#define IDC_EDIT_CV_SIGMA               1036
#define IDC_GB_CHANVESE                 1037
#define IDC_LBL_MU                      1038
#define IDC_EDIT_CV_MU                  1039
#define IDC_LBL_CV_V                    1040
#define IDC_LBL_CV_LAMBDA_IN            1041
#define IDC_EDIT_CV_V                   1042
#define IDC_EDIT_CV_LAMBDA_IN           1043
#define IDC_EDIT_CV_LAMBDA_OUT          1044
#define IDC_LBL_CV_LAMBDA_OUT           1045
#define IDC_EDIT_CV_RUNS                1046
#define IDC_LBL_CV_Runs                 1047
#define IDC_BUTTON_SURF                 1049
#define IDC_BTN_MOSAICK                 1050
#define IDC_CHECK_VERBOSE               1051

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1052
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
