/*****************************************************************************
 * FeatureEigenPoints.h
 * File to contain all the image processing functions developed from the
 * ECE847 projects.
 *****************************************************************************/

#pragma once

#ifndef _FEATUREEIGENPOINTS_H_
#endif // _FEATUREEIGENPOINTS_H_

#include "ImageProcessing.h"

struct FeatureEigenPoint
{	
	double mineigen;
	blepo::Point fp;
	blepo::MatDbl eigenvalue;
};

class FeatureEigenPoints
{
public:

	// constructor
	FeatureEigenPoints();
	
	// variables
	std::vector<FeatureEigenPoint> m_feps;

	// methods
	void                                     Add(FeatureEigenPoint fep);
	bool                                     AddWithMinDist(FeatureEigenPoint fep, int d);
	void                                     Sort();
	void                                     PrintToFile(char * filename);
	static int                               DistanceToNeighbor(FeatureEigenPoint a, FeatureEigenPoint b);
	std::vector<FeatureEigenPoint> *         GetFeatures();
	std::vector<FeatureEigenPoint>::iterator GetFeaturesIteratorBegin();	
	std::vector<FeatureEigenPoint>::iterator GetFeaturesIteratorEnd();
	bool                                     FepComparator(FeatureEigenPoint a, FeatureEigenPoint b);
	
private:
	ImageProcessing   m_imgproc;
}; 