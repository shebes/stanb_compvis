/*****************************************************************************
 * StereoMatcher.cpp
 *
 * Date:Monday October 19, 2009
 *****************************************************************************/
#include "stdafx.h"
#include "StereoMatcher.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;


/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
StereoMatcher::StereoMatcher( ImgBgr firstImg, ImgBgr secondImg )
{
	Initialize( firstImg, secondImg, 0, 16, 5 );
}

/*---------------------------------------------------------------------------*
 * Description: Constructor
 *
 * Parameters:
 * [+] leftImg  - Left rectified stereo image
 * [+] rightImg - Right rectified stereo image
 * [+] minDisp  - smallest image disparity value
 * [+] maxDisp  - largest  image disparity value
 * [+] halfWidth- half size of the window used to get the average area pixel 
 *                value
 * Return: Nothing
 *---------------------------------------------------------------------------*/
StereoMatcher::StereoMatcher( ImgBgr firstImg, ImgBgr secondImg, int dispMin, int dispMax, int halfWidth )
{
	Initialize( firstImg, secondImg, dispMin, dispMax, halfWidth );
}

/*---------------------------------------------------------------------------*
 * Description: Initializer that sets initial member variables from passed in
 * variables and calls the methods to perform correllation based matching 
 * with a consistency check on rectified stereo images.
 *
 * Parameters:
 * [+] leftImg  - Left rectified stereo image
 * [+] rightImg - Right rectified stereo image
 * [+] minDisp  - smallest image disparity value
 * [+] maxDisp  - largest  image disparity value
 * [+] halfWidth- half size of the window used to get the average area pixel 
 *                value
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void StereoMatcher::Initialize( ImgBgr leftImg, ImgBgr rightImg, int dispMin, int dispMax, int halfWidth )
{
	m_imgLeft   = leftImg;
	m_imgRight  = rightImg;
	m_dispMin   = dispMin;
	m_dispMax   = dispMax;
	m_halfWidth = halfWidth;
	//BlockMatchOne(leftImg, rightImg, dispMin, dispMax, halfWidth);
	BlockMatchTwo(leftImg, rightImg, dispMin, dispMax, halfWidth);
	BlockMatchWithLeftToRightCheck();
}

/*---------------------------------------------------------------------------*
 * Description:  Method to perform correllation based block matching for
 * rectified stereo images.
 *
 * Parameters:
 * [+] leftImg  - Left rectified stereo image
 * [+] rightImg - Right rectified stereo image
 * [+] minDisp  - smallest image disparity value
 * [+] maxDisp  - largest  image disparity value
 * [+] halfWidth- half size of the window used to get the average area pixel 
 *                value
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void StereoMatcher::BlockMatchOne( ImgBgr leftImg, ImgBgr rightImg, int minDisp, int maxDisp, int w )
{
	m_disparityMap.Reset( leftImg.Width(), leftImg.Height() );
	ImgGray dispMap;
	dispMap.Reset( leftImg.Width(), leftImg.Height() );
	try {
		int ghat, dhat;
		for ( int y = 0 ; y < leftImg.Height() ; y++ )
		{
			for ( int x = 0 ; x < leftImg.Width() ; x++ )
			{
				ghat = INT_MAX;
				for ( int d = minDisp ; d < maxDisp; d++ )
				{
					int g = 0;
					for ( int j = -w ; j < w ; j++ )
					{
						for ( int i = -w ; i < w ; i++ )
						{
							int xi  = x + i ;
							int yj  = y + j ;
							int xid = x + i - d;
							
							if ( xi  < 0 ) { xi  = 0 ; }
							if ( yj  < 0 ) { yj  = 0 ; }
							if ( xid < 0 ) { xid = 0 ; }
							if ( xi  >= leftImg.Width()  ) { xi  = leftImg.Width()  - 1; }
							if ( yj  >= leftImg.Height() ) { yj  = leftImg.Height() - 1; }
							if ( xid >= leftImg.Width()  ) { xid = leftImg.Width()  - 1; }

							g += abs( leftImg(xi, yj).ToGray() - rightImg(xid, yj).ToGray() ) ;
						}
					}
					if ( g < ghat )
					{
						ghat = g;
						dhat = d;
					}
				}
				dispMap(x,y) = dhat;
			}
		}
		LinearlyScale( dispMap, 0, 255, &m_disparityMap );
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:  Method to perform correllation based block matching for
 * rectified stereo images.
 *
 * Parameters:
 * [+] leftImg  - Left rectified stereo image
 * [+] rightImg - Right rectified stereo image
 * [+] minDisp  - smallest image disparity value
 * [+] maxDisp  - largest  image disparity value
 * [+] halfWidth- half size of the window used to convolve the disparity map
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void StereoMatcher::BlockMatchTwo( ImgBgr leftImg, ImgBgr rightImg, int minDisp, int maxDisp, int halfWidth )
{

	int minDbar      = INT_MAX;
	int minDbarIndex = minDisp;
	try
	{		
		m_leftDispMap.Reset( leftImg.Width(), leftImg.Height() );
		ImgGray tempDispMap;
		tempDispMap.Reset( leftImg.Width(), leftImg.Height() );

		ComputeDbar();
		for ( int y = 0 ; y < leftImg.Height() ; y++ )
		{
			for ( int x = 0 ; x < leftImg.Width() ; x++ )
			{
				tempDispMap(x,y) = GetMinIn3D( &m_dbar, x, y );
			}
		}
		LinearlyScale( tempDispMap, 0, 255, &m_leftDispMap );
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:  Creates a vector of dissimilarity matrices the size of the
 * left and right rectified images.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void StereoMatcher::ComputeDbar()
{
	try
	{
		ImgGray dispMap;
		dispMap.Reset( m_imgRight.Width(), m_imgLeft.Height() );
		Set( &dispMap, 0.0 );
		vector<ImgGray> dbar( m_dispMax-m_dispMin, dispMap ); 
		vector<ImgGray> dbarP( m_dispMax-m_dispMin, dispMap ); 
		vector<int> kernel( m_halfWidth*2+1, 1 );

		for ( int d = m_dispMin ; d < m_dispMax ; d++ )
		{
			for ( int y = 0 ; y < m_imgLeft.Height() ; y++ ) 
			{
				for ( int x = 0 ; x < m_imgLeft.Width() ; x++ ) 
				{
					int xd = x - d;
					if ( xd < 0 )                 { xd = 0;                     }
					if ( xd > m_imgLeft.Width() ) { xd = m_imgLeft.Width() - 1; }
					dbar.at(d)(x,y) = abs( m_imgLeft(x, y).ToGray() - m_imgRight(xd, y).ToGray() ) ;
				}
			}			
			m_imageProcessor.ConvolveHorizontal( kernel, dbar.at(d), &dbar.at(d) );
			m_imageProcessor.ConvolveVertical(   kernel, dbar.at(d), &dbar.at(d) );
		}

		m_dbar = dbar;
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:  This method takes a vector of dissimilarity matrices and a
 * coordinate that represents the same pixel in each matrix and returns the 
 * index of the smallest dissimilarity in the vector for that point.
 *
 * Parameters:
 * [+] dbar - vector of images that are really dissimilarity matrices
 * [+]    x - horizontal coordinate location of image pixel
 * [+]    y - vertical coordinate location of image pixel
 * Return: An integer representing the index of a disparity level in the matrix
 *---------------------------------------------------------------------------*/
int StereoMatcher::GetMinIn3D( std::vector<blepo::ImgGray> * dbar, int x, int y )
{
	int result = 0;
	int min = INT_MAX;
	try
	{
		for ( int d = 0 ; d < dbar->size() ; d++ )
		{
			if ( dbar->at(d)(x,y) < min )
			{
				min    = dbar->at(d)(x,y);
				result = d;
			}
		}
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Takes the vector of dissimilarity values at each disparity
 * and makes sure the that resulting disparity map made from the smallest 
 * dissimilarity at each pixel between disparities contains image information
 * only from pixels visible in both left and right rectified images.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void StereoMatcher::BlockMatchWithLeftToRightCheck()
{
	try
	{
		int dLeft, dRight, xd;
		ImgGray tempMap;
		tempMap.Reset( m_imgRight.Width(), m_imgRight.Height() );
		m_checkedMap.Reset( m_imgRight.Width(), m_imgRight.Height() );
		for ( int y = 0 ; y < m_imgRight.Height() ; y++ )
		{
			for ( int x = 0 ; x < m_imgLeft.Width() ; x++ )
			{
				dLeft  = GetMinIn3D( &m_dbar, x, y );
				xd     = x - dLeft;
				if ( xd < 0 ) { x = 0; }
				dRight = GetMinIn3D( &m_dbar, xd, y );
				tempMap(x,y) = ( dLeft == dRight ) ? dLeft : 0 ;
			}
		}
		LinearlyScale( tempMap, 0, 255, &m_checkedMap );
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}


///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// PRINTERS ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// SETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// GETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgBgr StereoMatcher::getLeftImage()
{
	return m_imgLeft;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgBgr StereoMatcher::getRightImage()
{
	return m_imgRight;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgGray StereoMatcher::getDisparityMap()
{
	return m_disparityMap;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgGray StereoMatcher::getCheckedDisparityMap()
{
	return m_checkedMap;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgGray StereoMatcher::getLeftDisparityMap()
{
	return m_leftDispMap;
}