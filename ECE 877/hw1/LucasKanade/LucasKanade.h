// LucasKanade.h : main header file for the LUCASKANADE application
//

#if !defined(AFX_LUCASKANADE_H__313BA672_A632_4071_AE23_02412198D342__INCLUDED_)
#define AFX_LUCASKANADE_H__313BA672_A632_4071_AE23_02412198D342__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "ImageProcessing.h"
#include "FeatureEigenPoints.h"
#include "WatershedSegmenter.h"

#define DEFAULT_SIGMA 0.1
#define DEFAULT_WINDOW_SIZE 5

struct matdblcomparator {
	bool operator() (blepo::MatDbl i,blepo::MatDbl j) { return ( ImageProcessing::MatDblMin(&i) < ImageProcessing::MatDblMin(&j) ) ; }
};

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeApp:
// See LucasKanade.cpp for the implementation of this class
//

class CLucasKanadeApp : public CWinApp
{
public:
	CLucasKanadeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLucasKanadeApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLucasKanadeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

class LucasAndKanade
{
public:
	LucasAndKanade();
	LucasAndKanade( double sigma );
	LucasAndKanade( double sigma, int windowSize );

	int             getNumberOfFeatures();
	int             getProximityOfFeatures();
	int             getWindowSize();
	int				getHw();
	double          getWindowSizeWidth();
	double          getWindowSizeHeight();

	blepo::ImgFloat getXGradient();
	blepo::ImgFloat getYGradient();
	blepo::ImgBgr   getBgrImage();

	void            InitImage( CString fileName );
	void            NextImage( CString fileName );

private:

	int                            m_fNumber;
	int                            m_fProximity;
	int                            m_windowSize;
	int							   m_hw;
	float                          m_windowSizeWidth;
	float						   m_windowSizeHeight;
	double                         m_sigma;

	blepo::ImgBgr                  m_imgOne,   m_imgTwo;
	blepo::ImgGray                 m_gImgOne,  m_gImgTwo;
	blepo::ImgFloat                m_fImgOne,  m_fImgTwo;
	blepo::ImgFloat                m_gradxOne, m_gradxTwo;
	blepo::ImgFloat                m_gradyOne, m_gradyTwo;
	blepo::ImgFloat                m_gradxw,   m_gradyw;
	blepo::ImgFloat                m_img1w,    m_img2w;
	std::vector<CString>           m_imgList;
	std::vector<blepo::ImgBgr>     m_imgs;
	std::vector<CString>::iterator m_it;
	std::vector<blepo::Point>      m_featureList;
	std::vector<blepo::MatDbl>     m_eigenList;

	blepo::MatDbl                  m_covmat, m_errvec, m_deltaD;

	ImageProcessing                m_imgprocessor;
	FeatureEigenPoints             m_feps;
	

	void Initialize( double sigma, int windowSize );
	void LoadImage(     CString fileName );
	void LoadNextImage( CString fileName );
	void CalculateGradients( blepo::ImgFloat & img, blepo::ImgFloat *gradx, blepo::ImgFloat *grady );
	void setWindowSizeWidth(  double value );
	void setWindowSizeHeight( double value );

	// the f prefix stands for 'feature'
	static bool CompareMatDbl( blepo::MatDbl x, blepo::MatDbl y );

	void   LucasKanade(               FeatureEigenPoint * fep, blepo::MatDbl * d );
	void   DetectMostSalientFeatures( blepo::ImgBgr &img, int fNumber=100, int fProximity=8 );
	double Interpolate(         const blepo::ImgGray  & img, double x, double y );
	double Interpolate(         const blepo::ImgFloat & img, double x, double y );
	void   InterpolateW(        const blepo::ImgFloat & img, blepo::ImgFloat * result, double x, double y );
	void   Compute2x2GradientMatrix(  blepo::MatDbl * covmat, const blepo::ImgFloat & gradxw, const blepo::ImgFloat & gradyw);
	void   Compute2x2GradientMatrix(  blepo::MatDbl * covmat, const blepo::ImgFloat & gx, const blepo::ImgFloat & gy, blepo::Point cp);
	void   Compute2x1ErrorVector(     blepo::MatDbl * errvec, double x, double y);
	void   Compute2x1ErrorVector(     blepo::MatDbl * errvec, const blepo::ImgFloat & img1w, const blepo::ImgFloat & img2w, const blepo::ImgFloat & gradxw, const blepo::ImgFloat & gradyw );
	void   DrawFeaturePoints(         blepo::ImgBgr * img, FeatureEigenPoints * feps );
	void   Solve(                     blepo::MatDbl * deltaD, const blepo::MatDbl & covmat, const blepo::MatDbl & errvec );
	void   SwapImages();
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LUCASKANADE_H__313BA672_A632_4071_AE23_02412198D342__INCLUDED_)
