/*****************************************************************************
 * EpipolarLineDrawer.cpp
 *
 * Date:Thursday October 22, 2009
 *****************************************************************************/
#include "stdafx.h"
#include "EpipolarLineDrawer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;


/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
EpipolarLineDrawer::EpipolarLineDrawer( ImgBgr firstImg, ImgBgr secondImg )
{
	Initialize( firstImg, secondImg );
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void EpipolarLineDrawer::Initialize( ImgBgr firstImg, ImgBgr secondImg )
{
	m_imgOne = firstImg;
	m_imgTwo = secondImg;
	SetCorrespondingPoints();
	CreateMatrixA();
	//PrintMatDbl( m_nbyNine, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\MatrixA.out");
	ComputeFM();
	DrawEpipolarLines();
	//PrintMatDbl( m_fundamentalMatrix, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\FundamentalMatrix.out");
}

/*---------------------------------------------------------------------------*
 * Description: This method takes the corresponding points and creates an 
 * n by 9 vector that will be used to computer the fundamental matrix.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void EpipolarLineDrawer::CreateMatrixA()
{
	try
	{
		m_nbyNine.Reset( 9, m_XY.size() );

		int i = 0;
		while ( i < m_XY.size() )
		{
			m_nbyNine(0,i) = m_XY.at(i).x * m_XYPrime.at(i).x;
			m_nbyNine(1,i) = m_XY.at(i).x * m_XYPrime.at(i).y;
			m_nbyNine(2,i) = m_XY.at(i).x;
			m_nbyNine(3,i) = m_XY.at(i).y * m_XYPrime.at(i).x;
			m_nbyNine(4,i) = m_XY.at(i).y * m_XYPrime.at(i).y;
			m_nbyNine(5,i) = m_XY.at(i).y;
			m_nbyNine(6,i) = m_XYPrime.at(i).x;
			m_nbyNine(7,i) = m_XYPrime.at(i).y;
			m_nbyNine(8,i) = 1;
			i++;
		}
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method uses the blepo::Svd method and a n-by-9 matrix to
 * compute the fundamental matrix to draw lines on corresponding stereo 
 * images.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void EpipolarLineDrawer::ComputeFM()
{
	try
	{
		int x, y, k;
		MatDbl f, bigF, u, s, v;
		MatDbl uf, sf, vf;
		Svd( m_nbyNine, &u, &s, &v );
		//m_nbyNine = u * Diag(s) * Transpose(v) ; // simply a fact, not something needed for the algorithm at this point

		// create the 3x3 matrix F sub 1
		f.Reset( 3, 3 );

		for ( y = 0, k = 0 ; y < f.Height() ; y++ )
		{
			for ( x = 0 ; x < f.Width() ; x++, k++ )
			{
				f(x, y) = v(8, k);
			}
		}
		
		//PrintMatDbl( f, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\f.out" );

		Svd( f, &uf, &sf, &vf );
		//PrintMatDbl( Diag(sf), "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\sigma_of_f.out" );
		sf(0,2) = 0 ;
		bigF.Reset( 3, 3 );
		bigF = uf * Diag(sf) * Transpose(vf) ;
		
		//PrintMatDbl( f, "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\bigf.out" );

		// set bigF to the fundamental matrix member variable
		m_fundamentalMatrix.Reset(3,3);
		m_fundamentalMatrix = bigF;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Method draws epipolar lines on two stereo images using the 
 * corresponding points vector given by Dr. Birchfield.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void EpipolarLineDrawer::DrawEpipolarLines()
{
	try
	{
		for ( int i = 0 ; i < m_XY.size() ; i++ )
		{
			DrawEpipolarLine(&m_imgOne, false, m_XYPrime.at(i) );
			DrawEpipolarLine(&m_imgTwo, true,  m_XY.at(i)      );
		}
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Method uses a point to calculate an epipolar line in a stereo
 * image.
 *
 * Parameters:
 * [+]     img - The image on which an epipolar line will be drawn
 * [+] isPrime - A flag to let the method know how to calculate the epipolar 
 *               line
 * [+]   point - A point in a stereo image
 * Return: A set of points for a epipolar line
 *---------------------------------------------------------------------------*/
vector<Point> EpipolarLineDrawer::DrawEpipolarLine( ImgBgr * img, bool isPrime, Point point )
{
	vector<Point> result;
	try
	{
		double a, b, c;
		Point imgLmt( img->Width(), img->Height() );

		if ( isPrime )
		{
			a = m_fundamentalMatrix(0,0) * point.x +
				m_fundamentalMatrix(1,0) * point.y + 
				m_fundamentalMatrix(2,0);
			b = m_fundamentalMatrix(0,1) * point.x +
				m_fundamentalMatrix(1,1) * point.y + 
				m_fundamentalMatrix(2,1
				);
			c = m_fundamentalMatrix(0,2) * point.x +
				m_fundamentalMatrix(1,2) * point.y + 
				m_fundamentalMatrix(2,2);
		} else  //draw line in image one
		{
			a = m_fundamentalMatrix(0,0) * point.x +
				m_fundamentalMatrix(0,1) * point.y + 
				m_fundamentalMatrix(0,2);
			b = m_fundamentalMatrix(1,0) * point.x +
				m_fundamentalMatrix(1,1) * point.y + 
				m_fundamentalMatrix(1,2);
			c = m_fundamentalMatrix(2,0) * point.x +
				m_fundamentalMatrix(2,1) * point.y + 
				m_fundamentalMatrix(2,2);
		}

		vector<Point> plotPts = getPointsAtLimit( imgLmt, a, b, c );
		DrawLine( plotPts.at(0), plotPts.at(1), img, Bgr::RED );
		result = plotPts;

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method to get the points at the image limits using the
 * epipolar values for two stereo images.
 *
 * Parameters:
 * [+] imgLmt - a point with the width and height limits for the images on
 *              which epipolar lines are to be drawn
 * [+]      a - value to calculate epipolar lines
 * [+]      b - value to calculate epipolar lines
 * [+]      c - value to calculate epipolar lines
 * Return: The two points where a stereo line intersect the image border
 *---------------------------------------------------------------------------*/
vector<Point> EpipolarLineDrawer::getPointsAtLimit( Point imgLmt, double a, double b, double c )
{
	vector<Point> result;
	try
	{
		int y_xmin, y_xmax, x_ymin, x_ymax;

		y_xmin = CalculateY( 0, a, b, c );
		y_xmax = CalculateY( imgLmt.x-1, a, b, c );
		x_ymin = CalculateX( 0, a, b, c );
		x_ymax = CalculateX( imgLmt.y-1, a, b, c );

		if ( y_xmin > -1 && y_xmin < imgLmt.y )
		{
			Point temp(0,y_xmin);
			result.push_back(temp);
		}
		if ( y_xmax > -1 && y_xmax < imgLmt.y )
		{
			Point temp(imgLmt.x-1,y_xmax);
			result.push_back(temp);
		}
		if ( x_ymin > -1 && x_ymin < imgLmt.x )
		{
			Point temp(x_ymin, 0);
			result.push_back(temp);
		}
		if ( x_ymax > -1 && x_ymax < imgLmt.x )
		{
			Point temp(x_ymax, imgLmt.y-1);
			result.push_back(temp);
		}

	} catch (const Exception& exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Calculates the X coordinate of an epipolar line.
 *
 * Parameters:
 * [+] y - a vertical location in an image
 * [+] a - calculated from the fundamental matrix to draw epipolar lines
 * [+] b - calculated from the fundamental matrix to draw epipolar lines
 * [+] c - calculated from the fundamental matrix to draw epipolar lines
 * Return: a horizontal location in an image
 *---------------------------------------------------------------------------*/
int EpipolarLineDrawer::CalculateX( int y, double a, double b, double c )
{
	int result = -1 ;
	try
	{
		result = -( (c / a) + ( (b * y) / a) );
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Calculates the Y coordinate of an epipolar line.
 *
 * Parameters:
 * [+] y - a horizontal location in an image
 * [+] a - calculated from the fundamental matrix to draw epipolar lines
 * [+] b - calculated from the fundamental matrix to draw epipolar lines
 * [+] c - calculated from the fundamental matrix to draw epipolar lines
 * Return: a vertical location in an image
 *---------------------------------------------------------------------------*/
int EpipolarLineDrawer::CalculateY( int x, double a, double b, double c )
{
	int result = -1 ;
	try
	{
		result = -( (c / b) + ( (a * x) / b) );
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
	return result;
}

///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// PRINTERS ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void EpipolarLineDrawer::PrintMatDbl( MatDbl &value, char * fileName )
{
	try
	{
		FILE * fout = fopen(fileName, "w");
		int i, j;
		j = 0;
		while( j < value.Height() )
		{
			i = 0 ;
			while( i < value.Width() )
			{
				if ( i > 0 )
				{ 
					fprintf( fout, "," );
				}
				fprintf( fout, "[%d,%2d] %7.4f", i, j, value(i, j) );
				i++;
			}
			fprintf( fout, "\n" );
			j++;
		}
		fclose(fout);
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}


///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// SETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description: Use the arrays provided by Dr. Birchfield to create vectors
 * of points from which epipolar lines will be calculated.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void EpipolarLineDrawer::SetCorrespondingPoints()
{
	try
	{
		// Corresponding points between 'burgher1_small.jpg' and 'burgher2_small.jpg'
		double x1[] = {  158,    3,   56,   93,  125,  105,  213,  178,  244,  284,  
					     345,  347,  341,  355,  297,  322,  300,  122,  194,  171,  
					     149,  148,  135,  114,  225,  199,  200,  143,  138,  171,  
						   7,    9,    9,   31,   77,   33,   78,  131,  111,  273,  
					     256,  256,  321,  340,  355,  144 };
		double y1[] = {  107,   77,   88,   96,   77,   61,   42,   29,   75,   56,  
						  80,  111,  134,  138,  131,  127,  115,  168,  163,  188, 
					     217,  230,  237,  219,  226,  223,  212,  201,  151,  151,  
					     114,  100,   87,  157,  153,  125,  127,  130,  130,  132,  
					     134,  154,  158,  160,  160,  50 };
		double x2[] = {  189,   19,   74,  112,  142,  120,  229,  194,  258,  304,  
					     365,  365,  355,  374,  312,  336,  313,  155,  225,  203,  
					     180,  180,  169,  150,  258,  232,  228,  176,  171,  203,  
						  27,   27,   26,   49,   96,   53,   96,  147,  127,  286,  
					     272,  273,  338,  356,  374,  158 };
		double y2[] = {   97,   63,   76,   84,   63,   47,   29,   16,   65,   46,  
						  68,  100,  124,  126,  121,  118,  103,  157,  152,  177,  
					     204,  216,  222,  205,  216,  210,  200,  189,  139,  138,  
					     102,   89,   74,  145,  139,  111,  113,  118,  116,  119,  
					     122,  142,  147,  148,  150,  37 };

		// clear vectors of all data
		m_x1.clear();
		m_y1.clear();
		m_x2.clear();
		m_y2.clear();
		m_XY.clear();
		m_XYPrime.clear();

		// loop through the arrays and add the elements to the vector
		int i = 0 ;
		while ( i < 46 )
		{
			Point temp(x1[i],y1[i]);
			Point tempPrime(x2[i],y2[i]);
			m_XY.push_back(temp);
			m_XYPrime.push_back(tempPrime);

			m_x1.push_back(x1[i]);
			m_y1.push_back(y1[i]);
			m_x2.push_back(x2[i]);
			m_y2.push_back(y2[i]);

			// increment counter
			i++;
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
}




///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// GETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
vector<double> EpipolarLineDrawer::getX1()
{
	return m_x1;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
vector<double> EpipolarLineDrawer::getY1()
{
	return m_y1;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
vector<double> EpipolarLineDrawer::getX2()
{
	return m_x2;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
vector<double> EpipolarLineDrawer::getY2()
{
	return m_y2;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgBgr EpipolarLineDrawer::getImageOne()
{
	return m_imgOne;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgBgr EpipolarLineDrawer::getImageTwo()
{
	return m_imgTwo;
}