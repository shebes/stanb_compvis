/*****************************************************************************
 * LevelSet.h
 *
 *****************************************************************************/

#pragma once

#ifndef _LEVELSET_H_
#endif // _LEVELSET_H_

#include "ImageProcessing.h"
#include <queue>

using namespace blepo;

class ChanVeseLevelSet
{
public:
	ChanVeseLevelSet( float sigma, CString fileName, float mu, float v, float lambdaIn, float lambdaOut );
	
	void IterateOverLevel();
	ImgBgr GetContourOverImg();


	ImgBgr   getImg();
	ImgFloat getPhi();
	ImgFloat getContour();
	ImgFloat getPhiX();
	ImgFloat getPhiY();
	ImgFloat getNormPhiX();
	ImgFloat getNormPhiY();
	ImgFloat getDivNormPhi();
	ImgFloat getPhiGradient();
	ImgFloat getGradientDescent();
	ImgFloat getPhiDelta();

	float getMu();
	float getLambdaIn();
	float getLambdaOut();
	float getV();
	float getSigma();

	// The gaussian information is calculated and can be used for differentiating images
	// ImageProcessing::CalculateGaussianInfo( &gi, atof(sigmaValue) );
	// Loads normal Image
private:
	float m_sigma;
	float m_mu;        // scaling to detect objects of varied size
	float m_v;         // is a constraint on the area inside the curve, increasing the propagation speed
	float m_lambdaIn;
	float m_lambdaOut;

	GaussianInfo m_gi;

	ImgBgr m_img;

	ImgFloat m_phiX;
	ImgFloat m_phiY;
	ImgFloat m_phiGradient;

	ImgFloat m_floatImg;
	ImgFloat m_imgPhi;
	ImgFloat m_contourImg;
	ImgFloat m_normPhiX;
	ImgFloat m_normPhiY;
	ImgFloat m_divNormPhi;
	ImgFloat m_deltaPhi;
	ImgFloat m_gradDescentPhi;
	ImageProcessing imageProcessor;


	CString m_fileName;

	void Initialize( float sigma, CString fileName, float mu, float v, float lambdaIn, float lambdaOut );

	void CreateInitialPhi( ImgFloat * imgPhi, ImgFloat * imgContour );
	void ComputeDivergence( GaussianInfo & gi, ImgFloat & contourImg );
	void ComputePhiEnergyChange( ImgFloat & img, ImgFloat & grad, ImgFloat & divPhi, ImgFloat * delta );
	void ApplyGradientDescent( ImgFloat & phi, ImgFloat & deltaPhi, ImgFloat * newPhi );
	void ComputeContour();
	void CalculateChamferDistance( ImgFloat * newPhi );
	

	void setMu( float mu );
	void setLambdaIn( float value );
	void setLambdaOut( float value );
	void setV( float value );
	void setSigma( float sigma );
};