/*****************************************************************************
 * FeatureEigenPoints.cpp
 * Description: Holds the implementation of FeatureEigenPoints processing 
 *              methods developed for ECE847 homework assignments.
 *
 * Date: Saturday November 7, 2009
 *****************************************************************************/
#include "stdafx.h"
#include "FeatureEigenPoints.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

bool FepComp(FeatureEigenPoint a, FeatureEigenPoint b)
{
	return a.mineigen < b.mineigen;
}

/*---------------------------------------------------------------------------*
 * Description: Default empty constructor
 *
 * Parameters:
 * [+] None
 * Return: nothing
 *---------------------------------------------------------------------------*/
FeatureEigenPoints::FeatureEigenPoints()
{}


/*---------------------------------------------------------------------------*
 * Description: Method to add FeatureEigenPoint to this object vector of
 * FeatureEigenPoints
 *
 * Parameters:
 * [+] fep - a FeatureEigenPoint to add to this object
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FeatureEigenPoints::Add(FeatureEigenPoint fep)
{
	m_feps.push_back(fep);
}


/*---------------------------------------------------------------------------*
 * Description: stl sort vector of FeatureEigenPoints, used to be a
 * bubblesort
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FeatureEigenPoints::Sort()
{
	sort( m_feps.begin(), m_feps.end(), FepComp );
}

/*---------------------------------------------------------------------------*
 * Description: Compare two FeatureEigenPoint structures
 *
 * Parameters:
 * [+] a - a struct containing an eigen vector and a feature point
 * [+] b - a struct containing an eigen vector and a feature point
 * Return: True if a is less than b, false otherwise
 *---------------------------------------------------------------------------*/
bool FeatureEigenPoints::FepComparator(FeatureEigenPoint a, FeatureEigenPoint b)
{
	return a.mineigen < b.mineigen;
}

/*---------------------------------------------------------------------------*
 * Description: Method to print this object to a file.
 *
 * Parameters:
 * [+] filename - output file name to which we should print
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FeatureEigenPoints::PrintToFile(char * filename)
{
	try
	{
		FILE * fout = fopen( filename, "w" );
		int count = 1;

		vector<FeatureEigenPoint>::iterator it;
		for ( it = m_feps.begin(); it != m_feps.end() ; it++ )
		{
			fprintf( fout, "[%d] min eigen value: %.2f, Point(%d,%d), Eigen Values(%.2f,%.2f)\n", count++, it->mineigen, it->fp.x, it->fp.y, it->eigenvalue(0,0), it->eigenvalue(0,1) );
		}

		fclose(fout);
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: A method that returns the integer distance between two
 * FeatureEigenPoints.
 *
 * Parameters:
 * [+] a - a FeatureEigenPoint whose mahattan distance to neighbor we must 
 *         determine
 * [+] b - a FeatureEigenPoint whose mahattan distance to neighbor we must 
 *         determine
 * Return: An integer that gives the floor of the distance between points
 *---------------------------------------------------------------------------*/
int FeatureEigenPoints::DistanceToNeighbor(FeatureEigenPoint a, FeatureEigenPoint b)
{
	int result = INT_MAX;
	try
	{
		int dx, dy;
		dx = a.fp.x - b.fp.x;
		dy = a.fp.y - b.fp.y;
		double dist = sqrt( pow(dx,2) + pow(dy,2) );
		result = (int)floor(dist);
	} 
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:  Method that adds a feature point to the feature point list 
 * if it is a minimum distance from the other feature points present in the 
 * list.
 *
 * Parameters:
 * [+] fep - a FeatureEigenPoint object that groups the point and eigen values
 * [+] d   - the minimum distance value for two feature points
 * Return: True if the feature point is added, false if it is too close to 
 *         another feature point.
 *---------------------------------------------------------------------------*/
bool FeatureEigenPoints::AddWithMinDist(FeatureEigenPoint fep, int d)
{
	bool result = true;
	try
	{
		vector<FeatureEigenPoint>::iterator it = m_feps.begin();
		while ( it != m_feps.end() )
		{
			if ( DistanceToNeighbor( *it, fep ) < d )
			{
				result = false;
				break;
			}
			it++ ;
		}
		if ( result ) 
		{
			Add(fep);
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Return a reference to the vector of FeatureEigenPoint 
 * structures
 *
 * Parameters:
 * [+] none
 * Return: A reference to the vector of FeatureEigenPoint structures
 *---------------------------------------------------------------------------*/
vector<FeatureEigenPoint> * FeatureEigenPoints::GetFeatures()
{
	return &m_feps;
}

/*---------------------------------------------------------------------------*
 * Description: Return the beginning iterator for the vector of 
 * FeatureEigenPoints
 *
 * Parameters:
 * [+] none
 * Return: The beginning iterator for the vector of FeatureEigenPoints
 *---------------------------------------------------------------------------*/
vector<FeatureEigenPoint>::iterator FeatureEigenPoints::GetFeaturesIteratorBegin()
{
	return m_feps.begin();
}

/*---------------------------------------------------------------------------*
 * Description: Return the end iterator for the vector of FeatureEigenPoints
 *
 * Parameters:
 * [+] none
 * Return: The end iterator for the vector of FeatureEigenPoints
 *---------------------------------------------------------------------------*/
vector<FeatureEigenPoint>::iterator FeatureEigenPoints::GetFeaturesIteratorEnd()
{
	return m_feps.end();
}
