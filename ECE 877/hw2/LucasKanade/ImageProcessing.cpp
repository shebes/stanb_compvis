/*****************************************************************************
 * ImageProcessing.cpp
 * Description: Holds the implementation of image processing methods developed 
 *              for ECE847 homework assignments.
 *
 * Date: Wednesday September 9, 2009 Matthew's 22nd birthday
 *****************************************************************************/
#include "stdafx.h"
#include "ImageProcessing.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;

/*---------------------------------------------------------------------------*
 * Default constructor
 *---------------------------------------------------------------------------*/

ImageProcessing::ImageProcessing()
{
	m_outputDirectory = "C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\";
}

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of four to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) originImg - the image wherein an area of pixels will be colored
 * Returns: A blepo color image
 *---------------------------------------------------------------------------*/
ImgBgr ImageProcessing::Floodfill(blepo::Point seedPoint, ImgBgr originImg)
{
	ImgBgr result ;
	
	try 
	{
		Bgr oldColor = Bgr( *originImg.Begin(seedPoint.x, seedPoint.y) );
		Bgr fllColor = Bgr(100, 0, 0);	
		Point imgLmt(originImg.Width(), originImg.Height());
		
		result = originImg;
		result(seedPoint.x, seedPoint.y) = fllColor ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			Point neighbor;

			// get north neighbor
			neighbor = Point( location.x, location.y-1 );
			if ( neighbor.y > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get east neighbor
			neighbor = Point( location.x + 1, location.y );
			if (  neighbor.x < imgLmt.x ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get south neighbor
			neighbor = Point( location.x, location.y + 1 );
			if ( neighbor.y < imgLmt.y ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}

			// get west neighbor
			neighbor = Point( location.x - 1, location.y );
			if ( neighbor.x > -1 ) 
			{
				if ( oldColor == Bgr( *result.Begin(neighbor.x, neighbor.y) ) )
				{
					*result.Begin(neighbor.x, neighbor.y) = fllColor;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of four to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) seedImage - the image wherein an area of pixels will be colored
 *---------------------------------------------------------------------------*/
ImgBgr ImageProcessing::Floodfill4Bgr(Point seedPoint, ImgBgr &seedImage)
{	
	ImgBgr result;
	ImageProcessing imageProcessor;
	try 
	{
		Bgr oldColor = Bgr( seedImage(seedPoint.x, seedPoint.y) );
		Bgr fllColor = Bgr(100, 0, 0);	
		Point imgLmt( seedImage.Width(), seedImage.Height() );
		
		result = seedImage;
		result(seedPoint.x, seedPoint.y) = fllColor ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			queue<Point> neighbors;
			imageProcessor.GetNeighbors4(location, imgLmt, &neighbors);
			while ( !neighbors.empty() )
			{
				Point neighbor = imageProcessor.PopQueue(&neighbors);
				if ( result(neighbor.x, neighbor.y) == oldColor )
				{
					result( neighbor.x, neighbor.y ) = fllColor ;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of eight to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) seedImage - the image wherein an area of pixels will be colored
 *---------------------------------------------------------------------------*/
ImgBgr ImageProcessing::Floodfill8Bgr(Point seedPoint, ImgBgr &seedImage)
{	
	ImgBgr result;
	ImageProcessing imageProcessor;
	try 
	{
		Bgr oldColor = Bgr( seedImage(seedPoint.x, seedPoint.y) );
		Bgr fllColor = Bgr(100, 0, 0);	
		Point imgLmt( seedImage.Width(), seedImage.Height() );
		
		result = seedImage;
		result(seedPoint.x, seedPoint.y) = fllColor ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			queue<Point> neighbors;
			imageProcessor.GetNeighbors8(location, imgLmt, &neighbors);
			while ( !neighbors.empty() )
			{
				Point neighbor = imageProcessor.PopQueue(&neighbors);
				if ( result(neighbor.x, neighbor.y) == oldColor )
				{
					result( neighbor.x, neighbor.y ) = fllColor ;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of four to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) seedImage - the image wherein an area of pixels will be colored
 * (+) labels    - the image wherein an area of pixels will be colored
 *---------------------------------------------------------------------------*/
void ImageProcessing::Floodfill4(blepo::Point seedPoint, ImgGray &seedImage, ImgInt * out, int fllClr)
{	
	try 
	{
		ImageProcessing imageProcessor;
		int oldColor = seedImage(seedPoint.x, seedPoint.y) ;

		Point imgLmt( seedImage.Width(), seedImage.Height() );

		(*out)( seedPoint.x, seedPoint.y) = fllClr ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			queue<Point> neighbors;
			imageProcessor.GetNeighbors4(location, imgLmt, &neighbors);
			while ( !neighbors.empty() )
			{
				Point neighbor = imageProcessor.PopQueue(&neighbors);
				if ( (*out)(neighbor.x, neighbor.y) == -1 && seedImage(neighbor.x, neighbor.y) == oldColor )
				{
					(*out)(neighbor.x, neighbor.y) = fllClr ;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Method fills an area in an image with the same color pixels with a 
 * separate color.  This algorithm uses a neighborhood of eight to determine
 * adjacent pixels to color.
 * 
 * Parameters:
 * (+) seedPoint - a point in the image selected by the user
 * (+) seedImage - the image wherein an area of pixels will be colored
 * (+) labels    - the image wherein an area of pixels will be colored
 *---------------------------------------------------------------------------*/
void ImageProcessing::Floodfill8(blepo::Point seedPoint, ImgGray &seedImage, ImgInt * out, int fllClr)
{	
	try 
	{
		out->Reset( seedImage.Width(), seedImage.Height() );
		ImageProcessing imageProcessor;
		int oldColor = seedImage(seedPoint.x, seedPoint.y) ;

		Point imgLmt( seedImage.Width(), seedImage.Height() );

		(*out)( seedPoint.x, seedPoint.y) = fllClr ;

		stack <Point> frontier;
		frontier.push(seedPoint);
		
		while ( !frontier.empty() )
		{
			Point location = frontier.top();
			frontier.pop();

			queue<Point> neighbors;
			imageProcessor.GetNeighbors8(location, imgLmt, &neighbors);
			while ( !neighbors.empty() )
			{
				Point neighbor = imageProcessor.PopQueue(&neighbors);
				if ( (*out)(neighbor.x, neighbor.y) == -1 && seedImage(neighbor.x, neighbor.y) == oldColor )
				{
					(*out)(neighbor.x, neighbor.y) = fllClr ;
					frontier.push(neighbor);
				}
			}
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameter(s):
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
Point ImageProcessing::PopQueue( queue<Point> * value )
{
	Point result = NULL ;
	try
	{
		if ( !value->empty() )
		{
			result = value->front();
			value->pop();
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * This method belongs in a different file, a utilities file.
 *---------------------------------------------------------------------------*/
void ImageProcessing::GetNeighbors4( Point location, Point maxXY, queue<Point> * neighbors)
{
	queue<Point> result;
	try
	{
		if ( location.x-1 > -1 ) 
		{
			Point a( location.x - 1, location.y );
			result.push(a);
		}
		if ( location.x+1 < maxXY.x ) 
		{
			Point b( location.x + 1, location.y );
			result.push(b);
		}
		if ( location.y - 1 > -1 )
		{
			Point c( location.x, location.y - 1 );
			result.push(c);
		}
		if ( location.y + 1 < maxXY.y )
		{
			Point d( location.x, location.y + 1 );
			result.push(d);
		}
		*neighbors = result;
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * This method belongs in a different file, a utilities file.
 *---------------------------------------------------------------------------*/
void ImageProcessing::GetNeighbors8( Point location, Point maxXY, queue<Point> * neighbors)
{
	queue<Point> result;
	try
	{
		if ( location.x-1 > -1 ) 
		{
			Point a( location.x - 1, location.y );
			result.push(a);
		}
		if ( location.x+1 < maxXY.x ) 
		{
			Point b( location.x + 1, location.y );
			result.push(b);
		}
		if ( location.y - 1 > -1 )
		{
			Point c( location.x, location.y - 1 );
			result.push(c);
		}
		if ( location.y + 1 < maxXY.y )
		{
			Point d( location.x, location.y + 1 );
			result.push(d);
		}

		if ( location.x-1 > -1 && location.y-1 > -1 ) 
		{
			Point e( location.x - 1, location.y - 1 );
			result.push(e);
		}
		if ( location.x+1 < maxXY.x && location.y-1 > -1 ) 
		{
			Point f( location.x + 1, location.y - 1 );
			result.push(f);
		}
		if ( location.x - 1 > -1 && location.y + 1 < maxXY.y )
		{
			Point g( location.x - 1, location.y + 1 );
			result.push(g);
		}
		if ( location.y + 1 < maxXY.y && location.x + 1 < maxXY.x )
		{
			Point h( location.x + 1, location.y + 1 );
			result.push(h);
		}
		*neighbors = result;
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * This method belongs in a different file, a utilities file.
 *---------------------------------------------------------------------------*/
char *        ImageProcessing::getLocalTime()
{
	time_t theTime;
	time( &theTime );
	tm *t = localtime( &theTime );
	return asctime(t);
}


/*---------------------------------------------------------------------------*
 * Description: It tests whether or not an element is present in a vector.
 *              A function that works in linear time.  
 * 
 * Parameters:
 * [+] element - an integer that you want to test to see whether or not it is
 *               in the passed in vector
 * [+] data - the vector against which you will test for the presence of an 
 *            element
 * Return: A boolean that says whether or not the element is in the passed in
 *         vector.
 *---------------------------------------------------------------------------*/
bool ImageProcessing::IsInVector( int element, vector<int> data )
{
	bool result = false;
	try 
	{
		vector<int>::iterator it;
		for ( it = data.begin() ; it != data.end() ; it++ )
		{
			if ( element == *it )
			{
				result = true;
				break;
			}
		}
	} 
	catch (Exception exp)
	{
		exp.Display();
	}
	return result ;
}


/*---------------------------------------------------------------------------*
 * Method creates a normalized histogram from a gray scale image.
 * 
 * Parameters:
 * (+) img - the image for which we will calculate a normalized histogram.
 * Returns: A vector of type float containing the normalized histogram.
 *---------------------------------------------------------------------------*/
vector<float> ImageProcessing::CreateNormHist(ImgBgr img)
{
	// integer variables
	int i;
	int nPixels = 0 ;
	int hist[HIST_MAX];

	// containers
	vector<float> resultHist(HIST_MAX, 0.0);

	// input/output
	FILE *fout = fopen("ImgHistogram.out", "w");

	try 
	{
		for ( i = 0 ; i < HIST_MAX ; i++ )
		{
			hist[i] = 0;
		}

		// create initial histogram and count the number of pixels
		ImgBgr::Iterator it ;
		for ( it = img.Begin() ; it != img.End() ; it++ )
		{
			hist[it->ToGray()]++;
			nPixels++;
		}
		
		fprintf( fout, "%s\n", getLocalTime() );
		fprintf( fout, "There are %d pixels in the loaded image.\n", nPixels );
		
		float sum = 0 ;
		// create normalized histogram
		for ( i = 0 ; i < HIST_MAX ; i++ )
		{
			resultHist.at(i) = (float)hist[i] / (float)nPixels;
			sum += resultHist.at(i);
			fprintf( fout, "[%3d] %f\n", i, resultHist.at(i) );
		}
		fprintf( fout, "Average normalized value %f", (sum/HIST_MAX) );
		fclose(fout);

	} catch (Exception exp)
	{
		exp.Display();
	}

	return resultHist;
}



/*---------------------------------------------------------------------------*
 * Method creates a cumulative histogram from a gray scale image.
 * 
 * Parameters:
 * (+) img - the image for which a cumulative histogram will be made
 * Returns: A vector of type float containing the cumulative histogram.
 *---------------------------------------------------------------------------*/
vector<float> ImageProcessing::CreateCumuHist(ImgBgr img)
{
	return CreateCumuHist( CreateNormHist(img) ) ;
}


/*---------------------------------------------------------------------------*
 * Method creates a cumulative histogram from a gray scale image.
 * 
 * Parameters:
 * (+) imgHist - the normalized histogram for an image
 * Returns: A vector of type float containing the cumulative histogram.
 *---------------------------------------------------------------------------*/
vector<float> ImageProcessing::CreateCumuHist(vector<float> imgHist)
{
	// container
	vector<float> resultHist(HIST_MAX, 0.0);

	try 
	{
		// input/output
		FILE *fout = fopen("ImgCumulativeHistogram.txt", "w");

		fprintf( fout, "%s\n", getLocalTime() );
		for ( int idx = 0 ; idx < HIST_MAX ; idx++ )
		{
			float cumVal = 0.0;
			if ( idx == 0 )
			{
				cumVal = 0.0;
			}
			else 
			{
				cumVal = resultHist.at(idx-1);
			}
			resultHist.at(idx) = imgHist.at(idx) + cumVal;
			fprintf( fout, "[%3d] %f\n", idx, resultHist.at(idx) );
		}
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}

	return resultHist;
}



/*----------------------------------------------------------------------------*
 * Take a cumulative histogram of an image and transform the image.
 *
 * Parameters:
 * (*) img - A color image
 * Return: A transformed color image.
 *----------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::TransformImageToBgr(ImgBgr img)
{
	ImgBgr result;

	vector<float> cumuHist = CreateCumuHist(img);
	
	try 
	{
		ImgBgr::Iterator it ;
		for ( it = img.Begin() ; it != img.End() ; it++ )
		{
			it->FromInt( (int)((HIST_MAX-1) * cumuHist.at( it->ToGray() )), Bgr::BLEPO_BGR_GRAY);
		}

		result = img;
	} catch(Exception exp)
	{
		exp.Display();
	}

	return  result;
}


/*----------------------------------------------------------------------------*
 * Take a cumulative histogram of an image and transform the image.
 *
 * Parameters:
 * (*) img - A color image
 * Return: A transformed gray image.
 *----------------------------------------------------------------------------*/
ImgGray       ImageProcessing::TransformImageToGray(ImgBgr img)
{
	ImgGray result( img.Width(), img.Height() );
	ImgBgr transformed = TransformImageToBgr(img);

	try 
	{
		ImgBgr::Iterator it = transformed.Begin();
		ImgGray::Iterator grayIt = result.Begin();
		while ( it != transformed.End() )
		{
			*grayIt = it->ToGray() ;
			it++;
			grayIt++;
		}
	} catch( Exception exp )
	{
		exp.Display();
	}

	return  result ;
}




/*---------------------------------------------------------------------------*
 * Description: Takes an image and uses its cumulative gray scale histogram
 *              to turn it into a binary image using the passed in threshold.
 *
 * Parameters:
 * [+] img - A Bgr image we want to see as a binary image
 * [+] threshold - A value that differentiates the foreground from the 
 *                 background in a gray scale image
 * Return: A binary image based on the passed in threshold value
 *---------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::CreateBinaryImgUsingThreshold(ImgBgr img, double threshold)
{
	ImgBgr result = img;

	try 
	{
		vector<float> cumuHist = CreateCumuHist(img);

		ImgBgr::Iterator it = result.Begin();
		while( it != result.End() )
		{
			if ( cumuHist[it->ToGray()] > threshold ) 
			{
				it->FromInt( HIST_MAX-1, Bgr::BLEPO_BGR_GRAY); // HIST_MAX-1 is white
			}
			else
			{
				it->FromInt( 0, Bgr::BLEPO_BGR_GRAY); // 0 is black
			}

			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}

	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Takes an image and uses its cumulative gray scale histogram
 *              to turn it into a binary image using the passed in threshold.
 *
 * Parameters:
 * [+] img - A Bgr image we want to see as a binary image
 * [+] threshold - A value that differentiates the foreground from the 
 *                 background in a gray scale image
 * Return: A binary image based on the passed in threshold value
 *---------------------------------------------------------------------------*/
void ImageProcessing::CreateImgBinaryWithThreshold(ImgBgr &img, ImgBinary * out, double threshold)
{
	ImgBinary result;
	try 
	{
		result.Reset( img.Width(), img.Height() );
		vector<float> cumuHist = CreateCumuHist(img);

		ImgBgr::Iterator it = img.Begin();
		ImgBinary::Iterator bit = result.Begin();
		while( it != img.End() )
		{
			if ( cumuHist[it->ToGray()] > threshold ) 
			{
				*bit = false; // black
			}
			else
			{
				*bit = true;  // white
			}

			it++;
			bit++;
		}
		*out = result;
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image and dilates it based on a
 *              3x3 matrix with a color value that is passed in as a 
 *              parameter.
 *
 * Parameters: 
 * (+) img - A binary image
 * (+) matchingValue - An integer of 0 or 255, the color value of the pixels 
 *                     in the 3x3 matrix.
 * Return value: A dilated binary image
 *---------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::DilateImage(ImgBgr img, int matchingValue, int numTimes)
{
	ImgBgr result = img;
	try
	{
		int nonMatchingValue = ( matchingValue == 0 )? HIST_MAX-1 : 0 ;
		while ( numTimes-- > 0 ) {
			for ( int y = 0 ; y < img.Height() ; y++ )
			{
				for ( int x = 0 ; x < img.Width() ; x++ ) 
				{
					if ( x-1 > 0 && x+1 < img.Width() &&
						 y-1 > 0 && y+1 < img.Height() )
					{
						ImgBgr::Iterator it5 = result.Begin(  x,  y);
						ImgBgr::Iterator it6 = result.Begin(x+1,  y);
						ImgBgr::Iterator it7 = result.Begin(x-1,y+1);
						ImgBgr::Iterator it8 = result.Begin(  x,y+1);
						ImgBgr::Iterator it9 = result.Begin(x+1,y+1);
						if ( it5->ToGray() == matchingValue || 
							 it6->ToGray() == matchingValue || 
							 it7->ToGray() == matchingValue || 
							 it8->ToGray() == matchingValue || 
							 it9->ToGray() == matchingValue )
						{
							it5->FromInt( matchingValue, Bgr::BLEPO_BGR_GRAY );
						} else
						{
							it5->FromInt( nonMatchingValue, Bgr::BLEPO_BGR_GRAY );
						}
					}
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image and erodes it based on a
 *              3x3 matrix with a color value that is passed in as a 
 *              parameter.
 *
 * Parameters: 
 * (+) img - A binary image
 * (+) matchingValue - An integer of 0 or 255, the color value of the pixels 
 *                     in the 3x3 matrix.
 * Return value: An eroded binary image
 *---------------------------------------------------------------------------*/
ImgBgr        ImageProcessing::ErodeImage(ImgBgr img, int matchingValue, int numTimes)
{
	ImgBgr result = img;
	try
	{
		int nonMatchingValue = ( matchingValue == 0 )? HIST_MAX-1 : 0 ;
		while ( numTimes-- > 0 )
		{
			for ( int y = 0 ; y < img.Height() ; y++ )
			{
				for ( int x = 0 ; x < img.Width() ; x++ ) 
				{
					if ( x-1 > 0 && x+1 < img.Width() &&
						 y-1 > 0 && y+1 < img.Height() )
					{

						ImgBgr::Iterator it5 = result.Begin(  x,  y);
						ImgBgr::Iterator it6 = result.Begin(x+1,  y);
						ImgBgr::Iterator it7 = result.Begin(x-1,y+1);
						ImgBgr::Iterator it8 = result.Begin(  x,y+1);
						ImgBgr::Iterator it9 = result.Begin(x+1,y+1);
						if ( it5->ToGray() == matchingValue && 
							 it6->ToGray() == matchingValue && 
							 it7->ToGray() == matchingValue && 
							 it8->ToGray() == matchingValue && 
							 it9->ToGray() == matchingValue )
						{
							it5->FromInt( matchingValue, Bgr::BLEPO_BGR_GRAY );
						} else
						{
							it5->FromInt( nonMatchingValue, Bgr::BLEPO_BGR_GRAY );
						}
					}
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
	return result;
}


/*---------------------------------------------------------------------------*
 * Description: This method creates a composite image from two thresholded 
 *              images in order to create a binary image that clearly 
 *              delineates between the image background and foreground.
 * 
 * Parameters:
 * [+] htImg - an integer image that was given a high threshold value to 
 *             discriminate between the images background and foreground
 * [+] ltImg - an integer image that was given a low threshold value to 
 *             discriminate between the images background and foreground
 * [+] out - a binary image that represents the merging of the 2 thresholded 
 *           images
 * Returns: A vector containing the labels that are found in both the high
 *          and low thresholded images.
 *---------------------------------------------------------------------------*/
vector<int>   ImageProcessing::MergeImages(const ImgInt& htImg, const ImgInt& ltImg, ImgBinary *out)
{
	vector<int> validLabels;

	try
	{
		FILE *fout = fopen("SimilarLabels.out", "w");
		out->Reset( htImg.Width(), htImg.Height() );
		ImgInt::ConstIterator p, q ;
		int i = 0;
		for ( p = ltImg.Begin(), q = htImg.Begin() ; p != ltImg.End() ; p++, q++ )
		{
			if ( *p > 0 && *q > 0 )
			{
				if ( validLabels.empty() ) 
				{
					fprintf( fout, "[%5d] %3d\n", ++i, *p);
					validLabels.push_back(*p);
				} 
				else if ( !IsInVector(*p, validLabels) )
				{
					fprintf( fout, "[%5d] %3d\n", ++i, *p);
					validLabels.push_back(*p);
				}
			}
		}
		ImgBinary::Iterator r;
		for ( p = ltImg.Begin(), r = out->Begin() ; p != ltImg.End() ; p++, r++ )
		{
			if ( IsInVector(*p, validLabels) )
			{
				*r = ImgBinary::Pixel(true);
			}
			else
			{
				*r = ImgBinary::Pixel(false);
			}
		}
		fclose(fout);
	} 
	catch (Exception exp) 
	{
		exp.Display();
	}

	return validLabels;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a map...
 * 
 * Parameters:
 * [+] validLabels  - ...
 * [+] labelsB      - ...
 * Returns: A map of ...
 *---------------------------------------------------------------------------*/
std::map< int,vector<double> >    ImageProcessing::CalculateMoments( vector<int> validLabels, const ImgInt& labelsB )
{
	std::map< int,vector<double> > momentMap;
	try 
	{
		// create the empty moment map
		vector<double> moments(6, 0.0);
		vector<int>::iterator it;
		for ( it = validLabels.begin() ; it != validLabels.end() ; it++ )
		{
			momentMap[*it] = moments;
		}

		//FILE *fout = fopen("momentslist.out", "w");
		for ( int y = 0 ; y < labelsB.Height() ; y++ )
		{
			for ( int x = 0 ; x < labelsB.Width() ; x++ )
			{
				int label = *labelsB.Begin(x, y);
				if ( IsInVector( label, validLabels) )
				{
					momentMap[label].at(0) = momentMap[label].at(0) + 1;
					momentMap[label].at(1) = momentMap[label].at(1) + (double)x;
					momentMap[label].at(2) = momentMap[label].at(2) + (double)y;
					momentMap[label].at(3) = momentMap[label].at(3) + ((double)x * (double)y);
					momentMap[label].at(4) = momentMap[label].at(4) + ((double)x * (double)x);
					momentMap[label].at(5) = momentMap[label].at(5) + ((double)y * (double)y);
				}
			}
		}

		map< int,vector<double> >::iterator mapit;
		for( mapit = momentMap.begin() ; mapit != momentMap.end() ; mapit++ )
		{
			vector<double> moments = mapit->second ;
			//vector<double>::iterator miter;
			//fprintf( fout, "[%2d] ", mapit->first ) ;
			//int i = 0 ;
			//for( miter = moments.begin() ; miter != moments.end() ; miter++ )
			//{
			//	fprintf( fout, "(Moment%d %10.0f) ", i++, *miter );
			//}
			//fprintf( fout, "[(Xc,Yc) (%5.5f,%5.5f)]", moments.at(1)/moments.at(0), moments.at(2)/moments.at(0) );
			//fprintf( fout, "\n" );
		}
		//fclose(fout);
	} 
	catch (Exception exp)
	{
		exp.Display();
	}

	return momentMap;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a map...
 * 
 * Parameters:
 * [+] htImg - A reference to the high threshold image
 * [+] ltImg - A reference to the low threshold image
 * [+] out   - A reference to the binary image to be created from the
 *             thresholded images
 * [+] allPointsltImg - ...
 * [+] momentsMap - ..
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void  ImageProcessing::MergeThresholdedImages(ImgBgr * htImg, ImgBgr * ltImg, ImgBinary * out, map< int, vector< Point > > * allPoints, map< int, vector< double > > * momentsMap)
{
	ImageProcessing imageProcessor;
	ImgBgr result;
	ImgInt labels, labelsB;
	vector< ConnectedComponentProperties < ImgBgr::Pixel > > ccProps, ccPropsB;
	try 
	{
		ImgBgr openClosedImg  = imageProcessor.DilateImage(imageProcessor.ErodeImage(*htImg, HIST_MAX-1, 3), HIST_MAX-1, 3);
		ImgBgr openClosedImgB = imageProcessor.DilateImage(imageProcessor.ErodeImage(*ltImg, HIST_MAX-1, 3), HIST_MAX-1, 3);
		int nLabels  = ConnectedComponents4(openClosedImg, &labels, &ccProps);
		int nLabelsB = ConnectedComponents4(openClosedImgB, &labelsB, &ccPropsB);

		vector<int> validLabels = MergeImages( labels, labelsB, out ) ;
		map< int, vector<double> > momentMap = CalculateMoments( validLabels, labelsB );		
		*momentsMap = momentMap;

		vector<int>::iterator labelit;
		map< int, vector<Point> > chainedPoints;
		for ( labelit = validLabels.begin() ; labelit != validLabels.end() ; labelit++ )
		{
			vector<Point> chain;
			WallFollow(labelsB, *labelit, &chain) ;
			chainedPoints[*labelit] = chain;
		}
		*allPoints = chainedPoints;
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method draws borders around the foreground objects of a processed 
 *      image based on different eigenvalues.
 * 
 * Parameters:
 * [+] img       - A reference to the unmodified original image
 * [+] allPoints - ...
 * [+] ei        - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void     ImageProcessing::DrawBorders( ImgBgr * img, const map< int, vector<Point> >& allPoints, map<int, eigeninfo> ei )
{
	map< int, vector<Point> >::const_iterator allit;
	for( allit = allPoints.begin() ; allit != allPoints.end() ; allit++ )
	{
		Bgr borderColor = Bgr::RED;
		eigeninfo ein = ei[allit->first];
		// if eccentrictiy is greater than 2.5 you have a banana
		if ( ein.eccentricity > 2.5 )
		{
			borderColor = Bgr::YELLOW;
		} else if ( ein.chain.size() < 300 )
		{
			borderColor = Bgr::RED;
		} else if ( ein.chain.size() > 300 && ein.chain.size() < 500 ) 
		{
			borderColor = Bgr::GREEN;
		}
		Point centroid(ein.xc, ein.yc);

		Point crossAOne( ein.xc + 5, ein.yc) ;
		Point crossATwo( ein.xc - 5, ein.yc) ;
		Point crossBOne( ein.xc, ein.yc + 5) ;
		Point crossBTwo( ein.xc, ein.yc - 5) ;
		DrawLine( crossAOne, crossATwo, img, Bgr::BLUE );
		DrawLine( crossBOne, crossBTwo, img, Bgr::BLUE );

		Point offCentroidA(ein.xc+ein.majorx, ein.yc+ein.majory);		
		Point offCentroidB(ein.xc-ein.majorx, ein.yc-ein.majory);
		Point offCentroidTwoA(ein.xc+ein.minorx, ein.yc+ein.minory);
		Point offCentroidTwoB(ein.xc-ein.minorx, ein.yc-ein.minory);

		DrawLine( offCentroidA, offCentroidB, img, Bgr::CYAN );
		DrawLine( offCentroidTwoA, offCentroidTwoB, img, Bgr::CYAN );
		vector<Point> tempVector = allit->second;
		vector<Point>::iterator pointit ;
		for ( pointit = tempVector.begin() ; pointit != tempVector.end() ; pointit++ )
		{
			Point tempPoint = *pointit;
			*(img->Begin(tempPoint.x, tempPoint.y)) =  borderColor;
		}
	}
	Figure fig("Outlined image") ;
	fig.Draw(*img);
}


/*---------------------------------------------------------------------------*
 * Description: 
 *		This method calculates eigen values for the foreground objects in an
 * image using the information passed into the function.
 * 
 * Parameters:
 * [+] ei          - ...
 * [+] momentMap   - ...
 * [+] chainPoints - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::CalculateEigenValues(map<int, eigeninfo> * ei, const map< int, vector<double> >& momentMap, map< int, vector<Point> > chainPoints)
{
	try 
	{
		FILE *fout = fopen("eigen.out", "w");
		map<int, eigeninfo> eiginfo;

		map< int, vector<double> >::const_iterator mit;
		map<int, eigeninfo>::iterator eit;

		for ( mit = momentMap.begin() ; mit != momentMap.end() ; mit++ )
		{
			eigeninfo ein;
			
			ein.chain = chainPoints[mit->first] ;

			ein.m00 = mit->second.at(0);
			ein.m10 = mit->second.at(1);
			ein.m01 = mit->second.at(2);
			ein.m11 = mit->second.at(3);
			ein.m20 = mit->second.at(4);
			ein.m02 = mit->second.at(5);

			double xc = mit->second.at(1) / mit->second.at(0);
			double yc = mit->second.at(2) / mit->second.at(0);
			double mu00 = ein.mu00 = mit->second.at(0);
			double mu10 = ein.mu10 = 0.0;
			double mu01 = ein.mu01 = 0.0;
			double mu11 = ein.mu11 = mit->second.at(3) - ( yc * mit->second.at(1) );
			double mu20 = ein.mu20 = mit->second.at(4) - ( xc * mit->second.at(1) );
			double mu02 = ein.mu02 = mit->second.at(5) - ( yc * mit->second.at(2) );

			ein.xc = xc;
			ein.yc = yc;

			// calculate direction using moment info
			// y = 2mu11
			// x = mu20 - mu02
			double y = 2 * mu11;
			double x = mu20 - mu02;
			ein.direction = 0.5 * atan2(y, x) ;

			// a = mu20+mu02
			// b = 4m11^2
			// x = mu20-mu02
			// c = x^2
			// d = b + c
			// e = sqrt(d)
			// f = a + e
			// g = a - e
			double a = mu20 + mu02 ;
			double b = 4 * pow(mu11, 2);
			double c = pow( x, 2 );
			double d = b + c ;
			double e = sqrt(d);
			double f = a + e ;
			double g = a - e ;

			ein.lambdamax = sqrt(f);
			ein.lambdamin = sqrt(g);
			ein.eccentricity = ein.lambdamax / ein.lambdamin ;			
			ein.compactness = (4 * 3.14 * ein.m00) / ein.chain.size();

			ein.eigenmax = (1 / (2*mit->second.at(0))) * f;
			ein.eigenmin = (1 / (2*mit->second.at(0))) * g;
			
			// calculate major axis directions for line drawing
			ein.majorx = sqrt(ein.eigenmax) * cos(ein.direction);
			ein.majory = sqrt(ein.eigenmax) * sin(ein.direction);

			// calculate minor axis directions for line drawing
			ein.minorx = - ( sqrt(ein.eigenmin) * sin(ein.direction) );
			ein.minory = sqrt(ein.eigenmin) * cos(ein.direction);

			eiginfo[mit->first] = ein;
			fprintf( fout, "[Label %2d] [eccentricity %5.3f] [direction %5.3f]\n", mit->first, ein.eccentricity, ein.direction);
		}
		*ei = eiginfo;
		fclose(fout);
	} catch(Exception exp) { exp.Display() ; }
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvolveVertical  (GaussianInfo &gi, ImgFloat &img, ImgFloat *out, bool useDerivative)
{
	try 
	{
		int x, y, z;
		int mu, width;
		double convolutionValue;
		
		vector<double> kernel;
		if ( useDerivative )
		{
			kernel = gi.dg;
		} else 
		{
			kernel = gi.g;
		}
		width = gi.w;
		mu = gi.mu;

		*out = img;
		
		for( x = 0 ; x < img.Width() ; x++ )
		{
			for( y = 0 ; y < img.Height() ; y++ )
			{
				convolutionValue = 0.0;
				for ( z = 0 ; z < width ; z++ ) 
				{
					int yp = y - z;
					if ( yp < 0 || yp > img.Height() ) 
					{
						yp = y;
					}
					double cv = kernel.at(z) * img(x,yp);
					convolutionValue += cv;
				}
				(*out)(x,y) = convolutionValue;
			}
		}
	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvolveVertical  (vector<int> kernel, ImgGray &img, ImgGray *out)
{
	try 
	{
		int     x, y, z, convolutionValue, kernelSum = 0;
		ImgGray result;
		
		result.Reset( img.Width(), img.Height() );
		Set( &result, 0 );

		for ( int n = 0 ; n < kernel.size() ; n++ )
		{
			kernelSum += kernel.at(n);
		}
		
		for( x = 0 ; x < img.Width() ; x++ )
		{
			for( y = 0 ; y < img.Height() ; y++ )
			{
				convolutionValue = 0.0;
				for ( z = 0 ; z < kernel.size() ; z++ ) 
				{
					int yp = y - z;
					if ( yp < 0 )
					{
						yp = 0;
					}
					if ( yp >= img.Height() ) 
					{
						yp = img.Height() - 1;
					}
					int cv = kernel.at(z) * img(x,yp);
					convolutionValue += cv;
				}
				result(x,y) = convolutionValue / kernelSum;
			}
		}
		
		*out = result;

	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvolveHorizontal(vector<int> kernel, ImgGray &img, ImgGray *out)
{
	try 
	{
		int x, y, z, convolutionValue, kernelSum = 0;
		ImgGray result;

		result.Reset( img.Width(), img.Height() );
		Set( &result, 0 );

		for ( int n = 0 ; n < kernel.size() ; n++ )
		{
			kernelSum += kernel.at(n);
		}

		for( y = 0 ; y < img.Height() ; y++ )
		{
			for( x = 0 ; x < img.Width() ; x++ )
			{
				convolutionValue = 0.0;
				for ( z = 0 ; z < kernel.size() ; z++ ) 
				{
					int xp = x - z;
					if ( xp < 0 ) 
					{
						xp = 0;
					}
					if ( xp >= img.Width() ) 
					{
						xp = img.Width() - 1;
					}
					int cv = kernel.at(z) * img(xp,y);
					convolutionValue += cv;
				}
				result(x,y) = convolutionValue / kernelSum;
			}
		}

		*out = result;
	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvolveHorizontal(GaussianInfo &gi, ImgFloat &img, ImgFloat *out, bool useDerivative)
{
	try 
	{
		int x, y, z;
		int mu, width;
		double convolutionValue;
		
		vector<double> kernel;
		if ( useDerivative )
		{
			kernel = gi.dg;
		} else 
		{
			kernel = gi.g;
		}
		width = gi.w;
		mu = gi.mu;

		out->Reset( img.Width(), img.Height() );

		for( y = 0 ; y < img.Height() ; y++ )
		{
			//for( x = mu ; x < img.Width()-mu ; x++ )
			for( x = 0 ; x < img.Width() ; x++ )
			{
				convolutionValue = 0.0;
				for ( z = 0 ; z < width ; z++ ) 
				{
					int xp = x - z;
					if ( xp < 0 || xp > img.Width() ) 
					{
						xp = x;
					}
					double cv = kernel.at(z) * img(xp,y);
					convolutionValue += kernel.at(z) * img(xp,y);
				}
				(*out)(x,y) = convolutionValue;
			}
		}
	} catch ( Exception exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgFloat(ImgFloat * img, float initializationValue)
{
	try
	{
		ImgFloat::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = initializationValue;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgGray(ImgGray * img, int initializationValue)
{
	try
	{
		ImgGray::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = initializationValue;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}


/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgInt(ImgInt * img, int initializationValue)
{
	try
	{
		ImgInt::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = initializationValue;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgBinary(  ImgBinary* img, int initializationValue)
{
	try
	{
		ImgBinary::Iterator it = img->Begin();
		while( it != img->End() )
		{
			bool val = true;
			if ( initializationValue > 0 ) {
				val = false;
			}

			*it = val;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * Parameters:
 * [+] param_01 - ...
 * [+] param_02 - ...
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::InitializeImgBgr(     ImgBgr   * img, Bgr color)
{
	try
	{
		ImgBgr::Iterator it = img->Begin();
		while( it != img->End() )
		{
			*it = color;
			it++;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This is a Shrinivas inspired method to convert a blepo float image 
 * into a blepo gray image.
 * 
 * Parameters:
 * [+] iFloat - a float image you want transformed into a gray image
 * [+] iGray  - a reference to the blepo gray image you want to create from a 
 *             float image
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ConvertFG( ImgFloat & iFloat, ImgGray * iGray )
{
	try 
	{
		iGray->Reset( iFloat.Width(), iFloat.Height() );
		InitializeImgGray(iGray, 0);
		
		ImgFloat::Pixel min = Min(iFloat);
		ImgFloat::Pixel max = Max(iFloat);

		int i = 0;
		ImgFloat::Iterator itFlt;
		ImgGray::Iterator itGry;
		for ( itGry = iGray->Begin(), itFlt = iFloat.Begin() ; itFlt != iFloat.End() ; itFlt++, itGry++ )
		{
			double value = ( (*itFlt - min) / (max - min) ) * 255;
			*itGry = (int)value;
		}
	}
	catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This is a Shrinivas inspired method to convert a blepo float image 
 * into a blepo gray image.
 * 
 * Parameters:
 * [+] iFloat - a float image you want transformed into a gray image
 * [+] iGray  - a reference to the blepo gray image you want to create from a 
 *             float image
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ScaleGrayLevelImage( ImgGray& iGray, ImgGray * out )
{
	try 
	{
		ImgGray result;
		result.Reset( iGray.Width(), iGray.Height() );
		
		ImgGray::Pixel min = Min(iGray);
		ImgGray::Pixel max = Max(iGray);

		int i = 0;
		ImgGray::Iterator itGry;
		ImgGray::Iterator itOut;
		for ( itGry = iGray.Begin(), itOut = result.Begin() ; itGry != iGray.End() ; itOut++, itGry++ )
		{
			int value = ( (*itGry - min) / (max - min) ) * 255;
			*itOut = (int)value;
		}
		*out = result;
	}
	catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method uses a Shrinivas inspired image convert function to
 * turn a blepo float image into a blepo gray image and displays it.
 * 
 * Parameters:
 * [+] img   - a float image you want displayed as a gray image
 * [+] title - the name you want for a title for the image to be displayed
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ViewFloatImageAsGray( ImgFloat * img, char * title)
{
	try
	{
		ImgGray result ;
		ConvertFG( *img, &result ) ;
		Figure fig(title);
		fig.Draw(result);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 * 
 * Parameters:
 * [+] value - a real number
 *---------------------------------------------------------------------------*/
int ImageProcessing::ValueIsInRegion(double value)
{
	int result = 0 ;
	try
	{
		double pi = 22.0/7.0;
		if ( value < (pi/8) && value >= (-pi/8) )
		{
			result = 1;
		} else if ( value < (3*pi/8) && value >= (pi/8)  )
		{
			result = 2;
		} else if ( value < (5*pi/8) && value >= (3*pi/8)  )
		{
			result = 3;
		} else if ( value < (7*pi/8) && value >= (5*pi/8)  )
		{
			result = 4;
		}
	}
	catch (Exception exp)
	{
		exp.Display();
	}
	return result ;
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		
 * 
 * Parameters:
 * [+] bImg - a binary image from which chamfer distances will be calculated
 * [+] iImg - integer image with chamfer manhattan distances
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::Chamfer(ImgBinary & bImg, ImgInt * iImg)
{
	int x, y, maxInt, upperLimit ;

	try 
	{
		FILE *fout = fopen("chamfer.out", "w") ;
		maxInt     = INT_MAX;
		upperLimit = maxInt - 1;
		fprintf( fout, "Maximum integer value: %d\n", maxInt );

		iImg->Reset( bImg.Width(), bImg.Height() );

		InitializeImgInt( iImg, 0 );

		// 1st pass
		for( y = 0 ; y < bImg.Height() ; y++ )
		{
			for ( x = 0 ; x < bImg.Width() ; x++ )
			{
				if ( x-1 > -1 && y-1 > -1 )
				{
					int leftValue = (*iImg)(x-1, y);
					int topValue  = (*iImg)(x, y-1);
					if ( upperLimit > leftValue )
					{
						leftValue++;
					}
					if ( upperLimit > topValue )
					{
						topValue++;
					}

					(*iImg)(x,y) = bImg(x,y) ? 0 : min( maxInt, min( topValue, leftValue ) );
				}
			}
		}

		//Figure figChamferPassOne("Chamfer pass 1");
		//figChamferPassOne.Draw( *iImg );

		// 2nd pass
		for( y = bImg.Height() - 1 ; y > -1 ; y-- )
		{
			for ( x = bImg.Width() - 1 ; x > -1 ; x-- )
			{
				if ( x+1 < bImg.Width() && y+1 < bImg.Height() )
				{
					int rightValue   = (*iImg)(x+1, y);
					int bottomValue  = (*iImg)(x, y+1);
					if ( upperLimit > rightValue )
					{
						rightValue++;
					}
					if ( upperLimit > bottomValue )
					{
						bottomValue++;
					}

					(*iImg)(x,y) = bImg(x,y) ? 0 : min( (*iImg)(x,y), min( bottomValue, rightValue ) );
				}
			}
		}

		//Figure figChamferPassTwo("Chamfer pass 2");
		//figChamferPassTwo.Draw( *iImg );
		

		// 3rd pass
		//for( y = 0 ; y < bImg.Height() ; y++ )
		//{
		//	for ( x = 0 ; x < 10 ; x++ )//for ( x = 0 ; x < bImg.Width() ; x++ )
		//	{
		//		fprintf( fout, "[(%3d,%3d)=%10d] ", x, y, (*iImg)(x,y) );
		//	}
		//	fprintf( fout, "\n" );
		//}

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}



/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates the gaussian kernel from the input sigma value.
 * This method also creates the derivative gaussian kernel and other useful
 * values for gaussian calculations.
 * 
 * Parameters:
 * [+] gi    - the gaussian information including its kernels and derivatives
 * [+] sigma - the standard deviation for a gaussian kernel
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::CalculateGaussianInfo(GaussianInfo * gi, double sigma)
{	
	try
	{
		FILE *fout = fopen( "gaussianvalues.txt", "w" );
		CString str;
		int i; // counter
		gi->sigma = sigma;
		gi->variance = pow(gi->sigma, 2);

		if ( ceil(gi->variance + 0.5) < ceil(gi->variance) )
		{
			gi->k = (int)floor(2 * gi->variance);
		} else
		{
			gi->k = (int)ceil(2 * gi->variance);
		}
		gi->mu = (int)ceil(2 * gi->sigma);
		gi->w  = 2 * gi->mu + 1;
		gi->widthOverSigma = (double)gi->w / sigma;

		gi->sum  = 0.0;
		gi->dsum = 0.0;
		gi->g.resize(gi->w, 0.0);
		gi->dg.resize(gi->w, 0.0);

		for ( i = 0 ; i < gi->w ; i++ )
		{
			double x  = i - gi->mu;
			double varianceTimesTwo = 2 * gi->variance;
			double xSquared = pow( x, 2 );

			gi->g.at(i)  = exp( - xSquared / varianceTimesTwo );
			gi->dg.at(i) = (-x * gi->g.at(i)) ;
			gi->sum  += gi->g.at(i);
			gi->dsum += gi->dg.at(i) * i ;

			fprintf(fout,"Gaussian at %d: %f\tDerivative: %f\n",i,gi->g.at(i), gi->dg.at(i));
		}

		fprintf(fout, "Normalized Gaussian\n");
		for ( i = 0 ; i < gi->w ; i++ )
		{
			gi->g.at(i)  /= gi->sum ;
			gi->dg.at(i) /= -gi->dsum ;
			fprintf(fout,"Gaussian at %d: %f\tDerivative: %f\n",i,gi->g.at(i), gi->dg.at(i) );
		}

		str.Format("\n\n"
			"Sigma value: %f\r\n"
			"Variance: %f\r\n"
			"Ceiling of variance: %f\r\n"
			"k: %d\r\n"
			"Mu: %d\r\n"
			"Width: %d\r\n"
			"Width over Sigma: %f\r\n"
			"Sum of values in gaussian: %f\r\n"
			"Sum of values in derivative gaussian: %f\r\n", 
			gi->sigma, gi->variance, ceil(gi->variance), gi->k, gi->mu, gi->w, gi->widthOverSigma, gi->sum, gi->dsum);
		fprintf(fout, "%s", str);
		//AfxMessageBox(str, MB_ICONINFORMATION);

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}


/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient image by convolving an image by a 
 * gaussian and its derivative.
 * 
 * Parameters:
 * [+] gi  - the gaussian information including its kernels and derivatives
 * [+] img - the original image
 * [+] out - a float image that represents the original image convolved with 
 *           the vertical gaussian kernel, and then in turn with the 
 *           horizontal kernel derivative
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::MakeGradientImageYXPrime(GaussianInfo * gi, ImgBgr &img, ImgFloat *out)
{
	try
	{
		ImgFloat floatImg, floatImgAgain, finalImg ;
		floatImgAgain.Reset( img.Width(), img.Height() );
		finalImg.Reset( img.Width(), img.Height() );

		Convert(img, &floatImg);

		ImageProcessing::InitializeImgFloat(&floatImgAgain, 0.0);
		ImageProcessing::InitializeImgFloat(&finalImg, 0.0);

		ImageProcessing::ConvolveVertical( *gi, floatImg, &floatImgAgain, false);
		ImageProcessing::ConvolveHorizontal( *gi, floatImgAgain, &finalImg, true);

		*out = finalImg;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient image by convolving an image by a 
 * gaussian and its derivative.
 * 
 * Parameters:
 * [+] gi  - the gaussian information including its kernels and derivatives
 * [+] img - the original image
 * [+] out - a float image that represents the original image convolved with 
 *           the horizontal gaussian kernel, and then in turn with the 
 *           vertical kernel derivative
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::MakeGradientImageXYPrime(GaussianInfo * gi, ImgBgr &img, ImgFloat *out)
{
	try
	{
		ImgFloat floatImg, floatImgAgain, finalImg ;
		floatImgAgain.Reset( img.Width(), img.Height() );
		finalImg.Reset( img.Width(), img.Height() );

		Convert(img, &floatImg);

		ImageProcessing::InitializeImgFloat(&floatImgAgain, 0.0);
		ImageProcessing::InitializeImgFloat(&finalImg, 0.0);

		ImageProcessing::ConvolveHorizontal( *gi, floatImg, &floatImgAgain, false);
		ImageProcessing::ConvolveVertical( *gi, floatImgAgain, &finalImg, true);

		*out = finalImg;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient magnitude image from two gradient 
 * convolved images in the horizontal and vertical directions.
 * 
 * Parameters:
 * [+] imgGx - the gradient image in the x direction (horizontal)
 * [+] imgGy - the gradient image in the y direction (vertical)
 * [+] out   - a float image that represents the merging gradient magnitude
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::CalculateGradientMagnitude(ImgFloat &imgGx, ImgFloat &imgGy, ImgFloat *out)
{
	try
	{
		ImgFloat result;

		result.Reset( imgGx.Width(), imgGx.Height() );
		ImageProcessing::InitializeImgFloat(&result, 0.0);

		int x, y;
		for ( y = 0 ; y < imgGx.Height() ; y++ )
		{
			for ( x = 0 ; x < imgGx.Width() ; x++ )
			{
				double gxSquared = pow( imgGx(x,y), 2 ) ;
				double gySquared = pow( imgGy(x,y), 2 ) ;
				result(x,y) = sqrt( gxSquared + gySquared );
			}
		}

		*out = result;
	} catch(Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient direction image from two gradient 
 * convolved images in the horizontal and vertical directions.
 * 
 * Parameters:
 * [+] imgGx - the gradient image in the x direction (horizontal)
 * [+] imgGy - the gradient image in the y direction (vertical)
 * [+] out   - a float image that represents the merged gradient images used
 *             for creating the gradient magnitude image
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::CalculateGradientDirection(ImgFloat &imgGx, ImgFloat &imgGy, ImgFloat * out)
{
	try
	{
		ImgFloat result;

		result.Reset( imgGx.Width(), imgGx.Height() );
		ImageProcessing::InitializeImgFloat(&result, 0.0);

		int x, y;
		for ( y = 0 ; y < imgGx.Height() ; y++ )
		{
			for ( x = 0 ; x < imgGx.Width() ; x++ )
			{
				double directionValue = atan2( imgGy(x,y), imgGx(x,y));
				result(x,y) = directionValue;
			}
		}

		*out = result;
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a gradient direction image from two gradient 
 * convolved images in the horizontal and vertical directions.
 * 
 * Parameters:
 * [+] imgMag - the magnitude image created from the image convolved by the 
 *              X and Y gaussian gradients
 * [+] imgDir - the angle or direction images created from the original image
 *              convolved by the X and Y gaussian gradients
 * [+] out    - a float image that represents the magnitude image with only 
 *              local maxima present
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::NonMaximalSuppression(ImgFloat &imgMag, ImgFloat &imgDir, ImgFloat *out)
{
	try 
	{
		out->Reset( imgMag.Width(), imgMag.Height() );
		ImageProcessing::InitializeImgFloat( out, 0.0 );
		int x, y;
		for ( y = 0 ; y < imgMag.Height() ; y++ )
		{
			for ( x = 0 ; x < imgMag.Width() ; x++ )
			{
				ImgFloat::Pixel neighborA;
				ImgFloat::Pixel neighborB;
				int region = ValueIsInRegion( abs( imgDir(x,y) ) );
				if ( region == 1 )
				{
					if ( x-1 > -1 && x+1 < imgMag.Width() )
					{
						neighborA = imgMag(x-1,y) ;
						neighborB = imgMag(x+1,y) ;
						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else if ( region == 2 ) {
					if ( x-1 > -1 && x+1 < imgMag.Width() && y-1 > -1 && y+1 < imgMag.Height() )
					{
						neighborA = imgMag(x-1,y-1) ;
						neighborB = imgMag(x+1,y+1) ;

						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else if ( region == 3 ) {
					if ( x-1 > -1 && x+1 < imgMag.Width() && y-1 > -1 && y+1 < imgMag.Height() )
					{
						neighborA = imgMag(x,y-1) ;
						neighborB = imgMag(x,y+1) ;

						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else if ( region == 4 ) {
					if ( x-1 > -1 && x+1 < imgMag.Width() && y-1 > -1 && y+1 < imgMag.Height() )
					{
						neighborA = imgMag(x+1,y-1) ;
						neighborB = imgMag(x-1,y+1) ;

						if ( imgMag(x,y) < neighborA || imgMag(x,y) < neighborB )
						{
							(*out)(x,y) = 0 ;
						} else
						{
							(*out)(x,y) = imgMag(x,y) ;
						}
					}
				} else
				{
					(*out)(x,y) = imgMag(x,y) ;
				}
			}
		}
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method uses the data in two disparately thresholded images and 
 * combines them to get an image with the best defined edges.
 * 
 * Parameters:
 * [+] imgSup - the image with local maxima suppressed to reduce noise and 
 *              allow for the identification of better edges
 * [+] out    - a float image that represents the merging of disparately
 *              thresholded images
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::HysteresisEdgeLinking(ImgFloat &imgSup, ImgBinary * out)
{
	try
	{
		FILE *fout = fopen( "sortedmaximalimage.out", "w" );

		int thresholdHi, thresholdLo;
		Point hiThreshold, loThreshold;

		ImgBinary binaryImg, binaryImgLo;
		binaryImgLo.Reset( imgSup.Width(), imgSup.Height() );
		binaryImg.Reset( imgSup.Width(), imgSup.Height() );
		ImgFloat suppressedImg = imgSup ;
		sort( imgSup.Begin(), imgSup.End() );

		// worked with 5 kinda
		thresholdHi = ( imgSup.Width() * imgSup.Height() ) / 6 ;
		thresholdLo = thresholdHi / 3 ;
		if ( thresholdLo == 0 )
		{
			thresholdLo = 1 ;
		}

		fprintf( fout, "Image Width: %d\nImage Height: %d\n", imgSup.Width(), imgSup.Height() );
		fprintf( fout, "High Threshold: %d\nLow Threshold: %d\n\n", thresholdHi, thresholdLo );

		ImgFloat::Pixel lowThreshold = imgSup( imgSup.Width() * imgSup.Height() - thresholdLo );
		ImgFloat::Pixel highThreshold = imgSup( imgSup.Width() * imgSup.Height() - thresholdHi );

		fprintf( fout, "Low threshold pixel value: %f\nHigh threshold pixel value: %f\n\n", lowThreshold, highThreshold );

		// Loop creates the high and low threshold images from the non-maximally suppressed image
		int x, y ;
		for ( y = 0 ; y < imgSup.Height() ; y++ )
		{			
			for ( x = 0 ; x < imgSup.Width() ; x++ )
			{
				if ( suppressedImg(x,y) >= highThreshold )
				{
					binaryImg(x,y) = 1;
				} else
				{
					binaryImg(x,y) = 0;
				}

				if ( suppressedImg(x,y) >= lowThreshold )
				{
					binaryImgLo(x,y) = 1;
				} else 
				{
					binaryImgLo(x,y) = 0;
				}

				if ( x < imgSup.Width() && y < imgSup.Height())
				{
					fprintf( fout, "\n[(%3d,%3d) %3.3f, %3.3f]", x, y, imgSup(x,y), suppressedImg(x,y) );
				}
				if ( x == imgSup.Width() ) fprintf(fout, "\r\n");
			}
		}

		Figure fig("Low Threshold Binary Image");
		fig.Draw(binaryImg);

		Figure figLo("High Threshold Binary Image");
		figLo.Draw(binaryImgLo); 
		
		ImgInt labels, labelsB;
		out->Reset(binaryImg.Width(), binaryImg.Height());
		vector< ConnectedComponentProperties < ImgBinary::Pixel > > ccProps, ccPropsB;

		int nLabels  = ConnectedComponents8(binaryImgLo, &labels, &ccProps);
		int nLabelsB = ConnectedComponents8(binaryImg, &labelsB, &ccPropsB);

		fprintf( fout, "\nNumber of high threshold labels %d\nNumber of low threshold labels %d\n\n", nLabels, nLabelsB );

		vector<int> validLabels = ImageProcessing::MergeImages( labels, labelsB, out ) ;
		
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a probability map which denotes, by its minimum
 * value, the best most likely spot in the image where the template should be
 * centered.
 * 
 * Parameters:
 * [+] img     - an image in which we want to match an object to a template
 * [+] imgT    - the template for an object in another image
 * [+] pMatrix - an integer image containing probability values to say which
 *               point in the image is would be the best spot to place the 
 *               template
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::MatchTemplate(ImgInt & img, ImgBinary & imgT, ImgInt * pMatrix)
{
	try
	{
		int x, y, tHalfWidth, tHalfHeight;
		pMatrix->Reset( img.Width(), img.Height() );
		ImageProcessing::InitializeImgInt(pMatrix, INT_MAX - 1);

		tHalfWidth  = imgT.Width()  / 2;
		tHalfHeight = imgT.Height() / 2;

		for ( y = tHalfHeight ; y < img.Height() - tHalfHeight ; y++ )
		{
			for ( x = tHalfWidth ; x < img.Width() - tHalfWidth ; x++ )
			{
				int xp, yp, i, j ;
				int pValue = 0;
				for( xp = x - tHalfWidth, i = 0 ; xp < x + tHalfWidth ; xp++, i++ )
				{
					for( yp = y - tHalfHeight, j = 0 ; yp < y + tHalfHeight ; yp++, j++ )
					{
						pValue += imgT(i,j) * img(xp,yp);
					}
				}
				(*pMatrix)(x,y) = pValue;
			}
		}
		ImgInt::Pixel minValue = Min(*pMatrix);
		ImgInt::Iterator it = pMatrix->Begin();
		while ( it != pMatrix->End() )
		{
			if ( *it == INT_MAX-1 )
			{
				*it = minValue * 10;
			}
			it++;
		}
		ImageProcessing::ToFileImgInt( pMatrix, "probabilityMatrix.out");
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}


/*---------------------------------------------------------------------------*
 * Description: 
 *		This method creates a probability map which denotes, by its minimum
 * value, the best most likely spot in the image where the rectangle to cover
 * the closest model match should be centered.
 * 
 * Parameters:
 * [+] img     - an image in which we want to match an object to a template
 * [+] imgM    - the model for an object in another image
 * [+] pMatrix - an integer image containing probability values to say which
 *               point in the image is would be the best spot to place the 
 *               template
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::MatchModel( ImgInt & img, ImgInt & imgM, ImgInt * pMatrix )
{
	try
	{
		int x, y, tHalfWidth, tHalfHeight;
		pMatrix->Reset( img.Width(), img.Height() );
		ImageProcessing::InitializeImgInt(pMatrix, INT_MAX - 1);

		tHalfWidth  = imgM.Width()  / 2;
		tHalfHeight = imgM.Height() / 2;

		for ( y = tHalfHeight ; y < img.Height() - tHalfHeight ; y++ )
		{
			for ( x = tHalfWidth ; x < img.Width() - tHalfWidth ; x++ )
			{
				int xp, yp, i, j ;
				int pValue = 0;
				for( xp = x - tHalfWidth, i = 0 ; xp < x + tHalfWidth ; xp++, i++ )
				{
					for( yp = y - tHalfHeight, j = 0 ; yp < y + tHalfHeight ; yp++, j++ )
					{
						pValue += pow( img(xp,yp) - imgM(i,j), 2 );
					}
				}
				(*pMatrix)(x,y) = sqrt(pValue);
			}
		}
		ImgInt::Pixel minValue = Min(*pMatrix);
		ImgInt::Iterator it = pMatrix->Begin();
		while ( it != pMatrix->End() )
		{
			if ( *it == INT_MAX-1 )
			{
				*it = minValue * 10;
			}
			it++;
		}
		ImageProcessing::ToFileImgInt( pMatrix, "probabilityMatrix.out");
	}
	catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method draws a rectangle around an object in the original image
 * based on the lowest value in the probability map.
 * 
 * Parameters:
 * [+] img     - an image in which we want to match an object to a template
 *               on which we will draw a rectangle around the matched object
 * [+] imgT    - the template for an object in another image
 * [+] pMatrix - an integer image containing probability values to say which
 *               point in the image is would be the best spot to place the 
 *               template
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::DrawRectangle( ImgBgr * img, ImgBgr &imgT, ImgInt & probabilityMatrix )
{
	try
	{
		FILE * fout = fopen("drawrectangle.out", "w");
		ImgInt::Pixel minValue = 300;//Min(probabilityMatrix);
		fprintf( fout, "Min value %d\n", minValue );

		int x, y, xc, yc;
		int tHalfWidth, tHalfHeight;
		
		tHalfWidth  = imgT.Width() / 2;
		tHalfHeight = imgT.Height() / 2;

		for ( y = tHalfHeight ; y < img->Height() - tHalfHeight ; y++ )
		{
			for ( x = tHalfWidth ; x < img->Width() - tHalfWidth ; x++ )
			{
				if ( probabilityMatrix(x,y) <= minValue ) 
				{
					xc = x;
					yc = y;
					
					fprintf( fout, "Min found at (x,y)=(%3d,%3d)\n", xc, yc );

					Point loLeft( xc - tHalfWidth, yc + tHalfHeight );
					Point loRight( xc + tHalfWidth, yc + tHalfHeight );
					Point hiLeft( xc - tHalfWidth, yc - tHalfHeight );
					Point hiRight( xc + tHalfWidth, yc - tHalfHeight );

					DrawLine( loLeft,   hiLeft, img, Bgr::BLUE );
					DrawLine( hiLeft,  hiRight, img, Bgr::BLUE );
					DrawLine( hiRight, loRight, img, Bgr::BLUE );
					DrawLine( loRight,  loLeft, img, Bgr::BLUE );
				}
			}
		}

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: 
 *		This method draws rectangles around objects in the original image
 * based on a thresholded value in the probability map.
 * 
 * Parameters:
 * [+] img     - an image in which we want to match an object to a template
 *               on which we will draw a rectangle around the matched object
 * [+] imgT    - the template for an object in another image
 * [+] pMatrix - an integer image containing probability values to say which
 *               point in the image is would be the best spot to place the 
 *               template
 * Returns: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::DrawRectangles( ImgBgr * img, ImgBgr &imgT, ImgInt & probabilityMatrix, int threshold )
{
	try
	{
		FILE * fout = fopen("drawrectangle.out", "w");
		ImgInt::Pixel minValue = Min(probabilityMatrix);
		if ( threshold > -1 )
		{
			minValue = threshold;
		}
		fprintf( fout, "Threshold value %d\n", minValue );

		int x, y, xc, yc;
		int numRectangles, misrecognitions;
		int tHalfWidth, tHalfHeight;
		Point lastRecognition;
		lastRecognition.x = 0;
		lastRecognition.y = 0;
		
		numRectangles   = 0 ;
		misrecognitions = 0;
		tHalfWidth      = imgT.Width() / 2;
		tHalfHeight     = imgT.Height() / 2;

		for ( y = tHalfHeight ; y < img->Height() - tHalfHeight ; y++ )
		{
			for ( x = tHalfWidth ; x < img->Width() - tHalfWidth ; x++ )
			{
				if ( probabilityMatrix(x,y) <= minValue ) 
				{
					numRectangles++;
					xc = x;
					yc = y;
					
					if ( abs(lastRecognition.x - xc) < tHalfWidth || abs(lastRecognition.y - xc) < tHalfHeight )
					{
						misrecognitions++;
					}
					
					fprintf( fout, "Min found at (x,y)=(%3d,%3d)\n", xc, yc );

					Point loLeft( xc - tHalfWidth, yc + tHalfHeight );
					Point loRight( xc + tHalfWidth, yc + tHalfHeight );
					Point hiLeft( xc - tHalfWidth, yc - tHalfHeight );
					Point hiRight( xc + tHalfWidth, yc - tHalfHeight );

					DrawLine( loLeft,   hiLeft, img, Bgr::RED );
					DrawLine( hiLeft,  hiRight, img, Bgr::RED );
					DrawLine( hiRight, loRight, img, Bgr::RED );
					DrawLine( loRight,  loLeft, img, Bgr::RED );
				}
			}
		}

		fprintf( fout, "\nNumber of rectangles drawn %d", numRectangles );
		fprintf( fout, "\nNumber misrecognitions %d\n", misrecognitions );
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*----------------------------------------------------------------------------*
 * Description:  Prints the data contained in a vector of CPoint vectors
 * to file.
 * 
 * Parameters:
 * [+] pointVV  - a vector of a vector of CPoint objects
 * [+] filename - a location of a file on disk
 *----------------------------------------------------------------------------*/
void ImageProcessing::ToFilePointVectorVector( vector< vector<Point> > * pointVV, char * fileName )
{
	try
	{
		FILE * fout = fopen( fileName, "w" );
		int i =0 ;
		vector< vector<Point> >::iterator it = pointVV->begin();
		for ( i = 0 ; i < HIST_MAX ; i++)
		{
			vector<Point> cptVector = pointVV->at(i) ;
			fprintf( fout, "Number of pixels at graylevel %3d: %5d\n", i, cptVector.size() );
			if ( cptVector.size() > 0 )
			{
				vector<Point>::iterator it = cptVector.begin();
				while ( it != cptVector.end() )
				{
					fprintf( fout, "P(%3d,%3d)\t", it->x, it->y);
					it++;
				}
			}
			fprintf(fout,"\n");
		}

		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *      Prints an Integer Image values to file in x,y columns.
 * Parameter(s):
 * [+] img
 * [+] fileName - location to which the output file should be 
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ImageProcessing::ToFileImgInt(blepo::ImgInt * img, char * fileName)
{
	try
	{
		FILE * fout = fopen( fileName, "w" );
		int x, y;
		for (y = 0 ; y < img->Height() ; y++ )
		{				
			for (x = 0 ; x < img->Width() ; x++ )
			{
				//if ( x < 5 )
				//{
					fprintf(fout, "[(%3d,%3d) : %3d]\t", x, y, (*img)(x,y) );
				//}
			}
			fprintf(fout, "\n");
		}
		fclose(fout);
		fout = fopen( "out.csv", "w" );
		for (y = 0 ; y < img->Height() ; y++ )
		{				
			for (x = 0 ; x < img->Width() ; x++ )
			{
				fprintf(fout, "%3d", (*img)(x,y) );
				if ( x < img->Width() - 1 )
				{
					fprintf(fout, ",");
				}
			}
			fprintf(fout, "\n");
		}
		fclose(fout);
	} catch (Exception exp)
	{
		exp.Display() ;
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 * 
 * Parameter(s):
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ImageProcessing::ToFileIntVector( vector<int> * value, char * filename )
{
	try
	{
		FILE * fout = fopen( filename, "w" ) ;
		int i = 0 ;
		while ( i < value->size() )
		{
			if ( i > 0 )
			{
				fprintf( fout, "\n" );
			}
			fprintf( fout, "[%2d] %d", i, value->at(i) );
			i++;
		}
		fclose(fout);
	} catch( const Exception & exp )
	{
		exp.Display() ;
	}
}



/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ImageProcessing::ToFileMatDbl( MatDbl &value, char * fileName )
{
	try
	{
		FILE * fout = fopen(fileName, "w");
		int i, j;
		j = 0;
		while( j < value.Height() )
		{
			i = 0 ;
			while( i < value.Width() )
			{
				if ( i > 0 )
				{ 
					fprintf( fout, "," );
				}
				fprintf( fout, "[%d,%2d] %7.4f", i, j, value(i, j) );
				i++;
			}
			fprintf( fout, "\n" );
			j++;
		}
		fclose(fout);
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
int ImageProcessing::GetFilesInDir (CString dir, vector<CString> * files)
{

	int result = 0;

	try
	{	
		WIN32_FIND_DATA ffd;
		LARGE_INTEGER filesize;
		HANDLE hFind = INVALID_HANDLE_VALUE;
		TCHAR szDir[MAX_PATH];

		for ( int i = 0 ; i < dir.GetLength() ; i++ )
		{
			szDir[i] = dir.GetAt(i);
		}

		// Find the first file in the directory.
		hFind = FindFirstFile(szDir, &ffd);

		// List all the files in the directory with some info about them.
		do
		{
			if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
			}
			else
			{
				CString fileName = "";
				
				for ( int i = 0 ; i < MAX_PATH ; i++ )
				{
					fileName += ffd.cFileName[i] ;
				}
				files->push_back(fileName);
				filesize.LowPart = ffd.nFileSizeLow;
				filesize.HighPart = ffd.nFileSizeHigh;
				_tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
			}
		}
		while (FindNextFile(hFind, &ffd) != 0);
	} catch (const Exception& exp)
	{
		exp.Display();
	}
	
	return result;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ImageProcessing::ToFileCStringVector( vector<CString> value, char * filename )
{
	try
	{
		FILE *fout = fopen( filename, "w" );
		vector<CString>::iterator it = value.begin();

		while ( it != value.end() )
		{
			fprintf( fout, "%s\n", *it );
			it++;
		}
		fclose(fout);
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ImageProcessing::CreateFileList( vector<CString> * fList, CString fName, CString fExt, CString fTitle, int iFirst, int iLast )
{
	try
	{
		vector<CString> result;

		int i = iFirst;
		do
		{			

			CString fNewName = fName;
			CString fRealNum("");
			fRealNum.Format("%d", i);
			fNewName.Replace( fTitle, "img0" + fRealNum );
			result.push_back(fNewName);

		} while ( ++i < iLast + 1 );
		*fList = result;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ImageProcessing::PrintVectorCovMat( vector<MatDbl> * covmatList, char * filename )
{
	try
	{
		FILE * fout   = fopen ( getOutputFile(filename), "w" );

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
char * ImageProcessing::getOutputDirectory()
{
	return m_outputDirectory;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
const char * ImageProcessing::getOutputFile( char * filename )
{
	string result = getOutputDirectory() ;
	result.append(filename);
	
	return result.c_str();
}

/*---------------------------------------------------------------------------*
 * Description: Find the minimum value in a MatDbl object.
 *
 * Parameters:
 * [+] mat - the double valued matrix in which you need find the minimum value
 * Return: The minimum value from the matrix
 *---------------------------------------------------------------------------*/
double ImageProcessing::MatDblMin( MatDbl * mat )
{
	double result = FLT_MAX;
	try
	{
		MatDbl::Iterator it;
		for ( it = mat->Begin() ; it != mat->End() ; it++ )
		{
			if ( *it < result )
			{
				result = *it;
			}
		}
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Divide one image by another
 *
 * Parameters:
 * [+] imgA - 1st image will be the numerator
 * [+] imgB - 2nd image will be the denominator
 * Return: The minimum value from the matrix
 *---------------------------------------------------------------------------*/
void ImageProcessing::Divide( const ImgFloat & imgA, const ImgFloat & imgB, ImgFloat * out )
{
	out->Reset(imgA.Width(), imgA.Height());
	ImgFloat::ConstIterator p = imgA.Begin();
	ImgFloat::ConstIterator q = imgB.Begin();
	ImgFloat::Iterator r      = out->Begin();
	while (p != imgA.End())  *r++ = (*p++) / (*q++);
}

/*---------------------------------------------------------------------------*
 * Description: Average together all values in an image
 *
 * Parameters:
 * [+] img - image for which you want the mean value
 * Return: The mean value from the image
 *---------------------------------------------------------------------------*/
float ImageProcessing::GetImageMean( const blepo::ImgFloat& img )
{
	return blepo::Sum(img) / (img.Width() * img.Height() ) ;
}

/*---------------------------------------------------------------------------*
 * Description: Average together all values inside the contour of an image
 *
 * Parameters:
 * [+] img - image for which you want the mean value inside the contour
 * Return: The mean value inside the contour
 *---------------------------------------------------------------------------*/
float ImageProcessing::CalculateCi( const blepo::ImgFloat& img, const blepo::ImgFloat& contour )
{
	FILE *fout = fopen("civalues.out","w");
	ImgFloat::ConstIterator p = img.Begin();
	ImgFloat::ConstIterator q = contour.Begin();
	float sumN = 0, sumD = 0, hval = 0;
	int index = 0;
	while ( p != img.End() )
	{
		hval = 1 / ( 1 + exp(-(*q)) );
		if ( hval > 0.5 )
		{
			sumN += *p * hval;
			sumD += 1;
			fprintf( fout, "[%d] H(phi(x)) = %lf\tImg value: %f\tContour value: %f\n", ++index, hval, *p, *q );
		}
		p++;
		q++;
	}
	if (sumD == 0 )
	{
		sumD = 1;
	}
	fclose(fout);
	return ( sumN / sumD );
}


/*---------------------------------------------------------------------------*
 * Description: Average together all values outside the contour of an image
 *
 * Parameters:
 * [+] img - image for which you want the mean value outside the contour
 * Return: The mean value outside the contour
 *---------------------------------------------------------------------------*/
float ImageProcessing::CalculateCo( const blepo::ImgFloat& img, const blepo::ImgFloat& phi )
{
	FILE *fout = fopen("covalues.out", "w");
	ImgFloat::ConstIterator p = img.Begin();
	ImgFloat::ConstIterator q = phi.Begin();
	float sumN = 0, sumD = 0, hval = 0, oneMinusHval;
	int index = 0;
	while ( p != img.End() )
	{
		hval = 1 / ( 1 + exp(-(*q)) );
		oneMinusHval = 1.0 - hval;
		if ( oneMinusHval > 0.5 )
		{
			sumN += (*p) * oneMinusHval;
			sumD += 1;
			fprintf( fout, "[%d] 1-H(phi(x)) = %lf\n", ++index, oneMinusHval );
		}
		p++;
		q++;
	}
	if (sumD == 0 )
	{
		sumD = 1;
	}
	fclose(fout);
	return ( sumN / sumD );
}




