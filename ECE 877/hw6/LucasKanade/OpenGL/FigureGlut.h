#ifndef __FIGURE_GLUT_H__
#define __FIGURE_GLUT_H__

//Include OpenGL header files, so that we can use OpenGL
#include "OpenGL/glut.h"
#include <afxwin.h>  // TCHAR
// be sure to #include <blepo.h> first
//#include "../../../alpha1/src/Matrix/Matrix.h"  
//#include "../../../alpha1/src/Image/Image.h"  
#include <vector>
#include <float.h>  // FLT_MAX


/**
This class makes it easy to create a window that uses OpenGL.  
Only one window can be created at a time with this implementation, due to the fact that GLUT
does not allow arbitrary information to be passed to callbacks.  Could be fixed with some work.
*/

class FigureGlut
{
public:
  FigureGlut(const char* title, int width, int height);
  FigureGlut(const char* title, int width, int height, int* argc, TCHAR* argv[]);
  
  ~FigureGlut() {}
  
  void EnterMainLoop()
  {
    glutMainLoop(); //Start the main loop.  glutMainLoop doesn't return.
  }
  
protected:
  virtual void OnRedraw() {}
  virtual void OnKeypress(unsigned char key, int x, int y) {}
  virtual void OnResize(int w, int h) {}
  virtual void OnMouseMove(int x, int y) {}
  virtual void OnMouseMoveWhileButtonDown(int x, int y) {}
  virtual void OnMouseClickUpOrDown(int button, int state, int x, int y) {}
  
private:
  void iInitialize(const char* title, int width, int height, int* argc, TCHAR* argv[]);
  
  static void iRedraw()
  {
    g_which_one->OnRedraw();
  }
  static void iHandleKeypress(unsigned char key, int x, int y)  //The key that was pressed	 & The current mouse coordinates
  {
    g_which_one->OnKeypress(key, x, y);
  }
  static void iHandleResize(int w, int h)
  {
    g_which_one->OnResize(w, h);
  }
  static void iHandleMouseMove(int x, int y)
  {
    g_which_one->OnMouseMove(x, y);
  }
  static void iHandleMouseMoveWhileButtonDown(int x, int y)
  {
    g_which_one->OnMouseMoveWhileButtonDown(x, y);
  }
  static void iHandleMouseClickUpOrDown(int button, int state, int x, int y)
  {
    g_which_one->OnMouseClickUpOrDown(button, state, x, y);
  }
private:
  static FigureGlut* g_which_one;  // need to change this architecture to allow for multiple windows
};


/**
This class creates a window that uses OpenGL and implements basic routines like mouse callbacks for 
navigating (translating, zooming, and rotation) the space, and for drawing points.
*/

class MyFigureGlut : public FigureGlut
{
public:
  struct ColoredPoint
  {
    ColoredPoint() : x(0), y(0), z(0), color(0,0,0) {}
    ColoredPoint(float xx, float yy, float zz, const blepo::Bgr& c) : x(xx), y(yy), z(zz), color(c) {}
    float x, y, z;
    blepo::Bgr color;
  };

  struct Cuboid
  {
    float min_x, max_x, min_y, max_y, min_z, max_z;
  };

public:
  MyFigureGlut(const char* title, int width, int height, int* argc, TCHAR* argv[]);
  MyFigureGlut(const char* title, int width, int height);
  ~MyFigureGlut() {}
  
  static void GetBoundingBoxOfPoints(const std::vector<ColoredPoint>& pts, Cuboid* out);

  void SetTranslation(float x, float y, float z);
  void GetTranslation(float* x, float* y, float* z) const;
  void SetRotation(float x, float y);
  void GetRotation(float* x, float* y) const;
  void InitParameters();
  
  void SetPoints(const std::vector<ColoredPoint>& pts);

protected:
  virtual void OnDrawPoints();

  virtual void OnDrawScene()
  {
    OnDrawPoints();
  }

  virtual void OnRedraw();
  virtual void OnKeypress(unsigned char key, int x, int y);  
  virtual void OnResize(int w, int h);
  virtual void OnMouseMoveWhileButtonDown(int x, int y);  
  virtual void OnMouseClickUpOrDown(int button, int state, int x, int y);
  
private:
public:
  float m_zoom;                       // Zooming (or Scaling) factor
  float m_pos_x, m_pos_y, m_pos_z;    // X, Y and Z translation
  float m_rot_x, m_rot_y;             // X, Y rotation
  CPoint m_last_mouse_location;       // Last location of the mouse
  bool m_control, m_shift;            // Whether keyboard buttons down

  // data to display
  std::vector<ColoredPoint> m_pts;
};

#endif // __FIGURE_GLUT_H__
