#include <assert.h>
#include "stdafx.h"
#include "blepo.h"  // you must include blepo.h before including FigureGlut.h
#include "FigureGlut.h"

//////////////////////////////////////////////////////////////////////////////////
// FigureGlut

FigureGlut*  FigureGlut::g_which_one = NULL;

FigureGlut::FigureGlut(const char* title, int width, int height)
{ 
  int argc = 1;
  TCHAR* argv[1];
  argv[0] = "dummy-exe-name";
  iInitialize(title, width, height, &argc, argv);
}

FigureGlut::FigureGlut(const char* title, int width, int height, int* argc, TCHAR* argv[])
{
  iInitialize(title, width, height, argc, argv);
}

void FigureGlut::iInitialize(const char* title, int width, int height, int* argc, TCHAR* argv[])
{
  if (g_which_one != NULL)
  {
    assert(0);  // error!  Can only have one GLUT window due to our implementation
    throw 0;
  }

  g_which_one = this;

  if (width < 0 || height < 0)
  {
    width = height = 600;  // default window size
  }

	//Initialize GLUT
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height); //Set the window size

	//Create the window
	glutCreateWindow(title);

  //Initialize rendering
  //Makes 3D drawing work when something is in front of something else
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_COLOR_MATERIAL);

	//Set handler functions for drawing, keypresses, and window resizes
	glutDisplayFunc(iRedraw);
	glutKeyboardFunc(iHandleKeypress);
	glutReshapeFunc(iHandleResize);
	glutMotionFunc(iHandleMouseMoveWhileButtonDown);
	glutPassiveMotionFunc(iHandleMouseMove);
	glutMouseFunc(iHandleMouseClickUpOrDown);
}


//////////////////////////////////////////////////////////////////////////////////
// MyFigureGlut

MyFigureGlut::MyFigureGlut(const char* title, int width, int height, int* argc, TCHAR* argv[]) 
  : FigureGlut(title, width, height, argc, argv)
{
  InitParameters();
}

MyFigureGlut::MyFigureGlut(const char* title, int width, int height) 
  : FigureGlut(title, width, height)
{
  InitParameters();
}

void MyFigureGlut::SetTranslation(float x, float y, float z)
{
  m_pos_x = x;
  m_pos_y = y;
  m_pos_z = z;
}

void MyFigureGlut::GetTranslation(float* x, float* y, float* z) const
{
  *x = m_pos_x;
  *y = m_pos_y;
  *z = m_pos_z;
}

void MyFigureGlut::SetRotation(float x, float y)
{
  m_rot_x = x;
  m_rot_y = y;
}

void MyFigureGlut::GetRotation(float* x, float* y) const
{
  *x = m_rot_x;
  *y = m_rot_y;
}

void MyFigureGlut::InitParameters()
{
  m_pos_x = m_pos_y = m_pos_z = 0.0f;
  m_rot_x = m_rot_y = 0.0f;
  m_zoom = 0.6f;
  m_last_mouse_location = CPoint(-1, -1);
  m_control = false;
  m_shift = false;
}

void MyFigureGlut::SetPoints(const std::vector<ColoredPoint>& pts)
{
  m_pts = pts;

  // back up viewpoint so we can see points
  {
    Cuboid box;
    GetBoundingBoxOfPoints(pts, &box);
    float zmax = box.max_z;
    // if zmax is too small, the object cannot be displayed.(?????)
    if ( zmax > 0 && zmax < 1) zmax += 1;  //zmax could be zero.
    if ( zmax == 0) zmax += 1.5;  
    SetTranslation(0, 0, -zmax);
  }
}

void MyFigureGlut::GetBoundingBoxOfPoints(const std::vector<ColoredPoint>& pts, Cuboid* out)
{
  assert(out);
  out->min_x = out->min_y = out->min_z =  FLT_MAX;
  out->max_x = out->max_y = out->max_z = -FLT_MAX;

  for ( int i = 0 ; i < pts.size() ; i++ )
  {
    if ( pts[i].x < out->min_x )  out->min_x = pts[i].x;
    if ( pts[i].y < out->min_y )  out->min_y = pts[i].y;
    if ( pts[i].z < out->min_z )  out->min_z = pts[i].z;
    if ( pts[i].x > out->max_x )  out->max_x = pts[i].x;
    if ( pts[i].y > out->max_y )  out->max_y = pts[i].y;
    if ( pts[i].z > out->max_z )  out->max_z = pts[i].z;      
  }
}

void MyFigureGlut::OnDrawPoints()
{
  glPointSize(2.0);
  glBegin(GL_POINTS);
  for ( int i = 0 ; i < m_pts.size() ; i++ )
  {
    const ColoredPoint& p = m_pts[i];
    glColor3f(p.color.r / 255.0, p.color.g / 255.0, p.color.b /255.0);
    glVertex3f(p.x, p.y, p.z);	
  }	
  glEnd();
}

void MyFigureGlut::OnRedraw()
{
  //Clear information from last draw
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glMatrixMode(GL_MODELVIEW); //Switch to the drawing perspective
  glLoadIdentity(); //Reset the drawing perspective
  glPushMatrix(); //Save the current state of transformations
  glTranslatef(m_pos_x , m_pos_y , m_pos_z); //- _zmax);
  glScalef(m_zoom, m_zoom, m_zoom);
  glRotatef( m_rot_y, 1.0f, 0.0f, 0.0f);
  glRotatef( m_rot_x, 0.0f, 0.0f, 1.0f);

  OnDrawScene();

  glPopMatrix();  //Undo
  glutSwapBuffers(); //Send the 3D scene to the screen
}

void MyFigureGlut::OnKeypress(unsigned char key, int x, int y)
{
  switch (key) 
  {
  case 27: //Escape key
    exit(0);
    break;
  }
}

void MyFigureGlut::OnResize(int w, int h)
{
  // tell OpenGL how to convert from coordinates to pixel values
  glViewport(0, 0, w, h);
  
  glMatrixMode(GL_PROJECTION); //Switch to setting the camera perspective
  
  // set the camera perspective
  glLoadIdentity(); //Reset the camera
  gluPerspective(45.0,                  //The camera angle
                 (double)w / (double)h, //The width-to-height ratio
                 1.0,                   //The near z clipping coordinate
                 200.0);                //The far z clipping coordinate
}

void MyFigureGlut::OnMouseMoveWhileButtonDown(int x, int y)
{
  if ( m_last_mouse_location != CPoint(-1, -1) )
  {
    int dx = x - m_last_mouse_location.x;
    int dy = y - m_last_mouse_location.y;
    
    if (m_shift)
    { // zoom in / out
      float new_zoom = m_zoom - (dy / 100.0f);
      if (new_zoom > 0) // if this is not done, the image is reversed
      {
        m_zoom = new_zoom;
      }
    }
    else if (m_control)
    { // translate
      m_pos_x += dx * 0.1f;
      m_pos_y -= dy * 0.1f;
    }
    else if (!m_control && !m_shift)
    { // rotate
      m_rot_x += dx;
      m_rot_y += dy;
    }
  }
  glutPostRedisplay();
  m_last_mouse_location = CPoint( x, y );
}

void MyFigureGlut::OnMouseClickUpOrDown(int button, int state, int x, int y)
{
  int mod = glutGetModifiers();
  
  if (state == GLUT_DOWN)
  {
    m_control = (mod == GLUT_ACTIVE_CTRL);
    m_shift = (mod == GLUT_ACTIVE_SHIFT);
  }
  else
  {  
    assert( state == GLUT_UP );
    m_last_mouse_location = CPoint(-1, -1);
    m_control = false;
    m_shift = false;
  }
}
