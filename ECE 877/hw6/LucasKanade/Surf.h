/*****************************************************************************
 * Surf.h
 *
 *****************************************************************************/

#pragma once

#ifndef _SURF_H_
#endif // _SURF_H_

#include "ImageProcessing.h"
#include <queue>

using namespace blepo;
using namespace std;

class Surf
{
public:
	// constructors
	Surf( CString fileName );
	void CreateScaledImages( const ImgInt& img, vector <ImgInt> * scaledImages );
	void PrintScaledImages( int scaleStart );
private:
	// variables
	bool debug;

	// Images
	ImgBgr  mImg;
	ImgBgr  mResult;
	ImgGray mGrayImg;
	ImgInt  mIntImg;
	ImgInt  mIntegralImg;
	ImgBgr  mDetectedFeatures;
	ImgInt  mInterestImageA;
	ImgInt  mInterestImageB;

	// Feature vectors
	vector <ImgInt> mScaledImages;
	vector <ImgInt> mInterestImages;

	// Filters
	ImgInt mFilterYY;
	ImgInt mFilterXX;
	ImgInt mFilterXY;

	// Interest Point vector
	vector <Point> mInterestPoints;
	vector <int>   mInterestValues;
	vector <int>   mInterestSigns;

	// Filter vectors
	vector <ImgInt> mFiltersXX;
	vector <ImgInt> mFiltersYY;
	vector <ImgInt> mFiltersXY;
	
	// class initialization
	void Initialize( CString fileName );

	// other methods
	void TransposeImgInt(const ImgInt& img, ImgInt* out );
	void FindInterestPoints( vector <ImgInt> * interestImages, vector <ImgInt> * scales );
	void CircleInterestPoints( vector <ImgInt> * imgs, const ImgBgr & img, ImgBgr * out );

	int GetDxx( int scale, const Rect& myRect, const ImgInt& img );
	int GetDyy( int scale, const Rect& myRect, const ImgInt& img );
	int GetDxy( int scale, const Rect& myRect, const ImgInt& img );

	int MaxOfThree(      Point p, const ImgInt& imgA, const ImgInt& imgB, const ImgInt& imgC );
	bool MaxAcrossScales( Point p, const ImgInt& imgA, const ImgInt& imgB, const ImgInt& imgC );	

	Rect GetFilterBorders( int scale, const Point& p );
};