/*****************************************************************************
 * StereoMatcher.h
 *
 *****************************************************************************/

#pragma once

#ifndef _STEREOMATCHER_H_
#endif // _STEREOMATCHER_H_

#include "ImageProcessing.h"
#include <queue>

class StereoMatcher
{
public:
	StereoMatcher( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg );
	StereoMatcher( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg, int dispMin, int dispMax, int halfWidth );

	blepo::ImgBgr   getLeftImage();
	blepo::ImgBgr   getRightImage();
	blepo::ImgGray  getDisparityMap();
	blepo::ImgGray  getCheckedDisparityMap();
	blepo::ImgGray  getLeftDisparityMap();

	void BlockMatchOne( blepo::ImgBgr leftImg, blepo::ImgBgr rightImg, int minDisp, int maxDisp, int halfWidth );
	void BlockMatchTwo( blepo::ImgBgr leftImg, blepo::ImgBgr rightImg, int minDisp, int maxDisp, int halfWidth );

private:

	int                         m_dispMin;
	int                         m_dispMax;
	int                         m_halfWidth;
	blepo::ImgBgr               m_imgLeft;
	blepo::ImgBgr               m_imgRight;
	blepo::ImgGray              m_disparityMap;
	blepo::ImgGray              m_checkedMap;
	blepo::ImgGray              m_rightDispMap;
	blepo::ImgGray              m_leftDispMap;
	ImageProcessing             m_imageProcessor;
	std::vector<blepo::ImgGray> m_dbar;

	int  GetMinIn3D( std::vector<blepo::ImgGray> * dbar, int x, int y );
	void Initialize( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg, int dispMin, int dispMax, int halfWidth );
	void ComputeDbar();
	void BlockMatchWithLeftToRightCheck();
};