/*****************************************************************************
 * FlatReconstruction.cpp
 *
 * Date: Saturday March 26, 2011
 *****************************************************************************/
#include "stdafx.h"
#include "FlatReconstruction.h"
#include "TrafficReconstruction.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


/*---------------------------------------------------------------------------*
 * Description:  Constructor for the FlatReconstruction projective to flat
 * plane reconstructive algorithm.
 *
 * Parameters:
 * [+] fileName - the name of the file that will be the center of the panorama
 * Return: Nothing
 *---------------------------------------------------------------------------*/
FlatReconstruction::FlatReconstruction( CString fileName )
{
	mDebug = true;
	Initialize( fileName );
}

/*---------------------------------------------------------------------------*
 * Description: Initialization loads the initial image and sets up the 
 * corresponding point vectors for the projective and flat images.
 *
 * Parameters:
 * [+] fileName - the name and path of an image to load
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FlatReconstruction::Initialize( CString fileName )
{
	Load( fileName, &mCheckeredImg );
	mCheckeredResult.Reset( mCheckeredImg.Width(), 620 );
	//mCheckeredResult.Reset( mCheckeredImg.Width(), mCheckeredImg.Height() );
	InitCorrespondingPoints();
}

/*---------------------------------------------------------------------------*
 * Description:  Establishes the set of points needed to modify the projective
 * image to one that is flat. A rectangle is found in the original image and
 * its corners are mapped to corners in a flat plane.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FlatReconstruction::InitCorrespondingPoints()
{
	Point2d topLeft;
	// Order of points from checkered image TL,TR,M,BL,BR
	mCheckeredFlat.push_back( Point2d(180,37) );
	mCheckeredFlat.push_back( Point2d(311,34) );
	//mCheckeredFlat.push_back( Point2d(226,70) );
	mCheckeredFlat.push_back( Point2d(11,137) );
	mCheckeredFlat.push_back( Point2d(294,197) );

	mFlatCheckered.push_back( Point2d(20,90) );
	mFlatCheckered.push_back( Point2d(mCheckeredImg.Width()-20,90) );
	//mFlatCheckered.push_back( Point2d(160,120) );
	mFlatCheckered.push_back( Point2d(20,mCheckeredResult.Height()-40) );
	mFlatCheckered.push_back( Point2d( mCheckeredImg.Width()-20, mCheckeredResult.Height()-40 ) );
}

/*---------------------------------------------------------------------------*
 * Description: This function takes the corresponding point vectors, computes 
 * the homography, and warps the projective image into something more flat.
 *
 * Parameters:
 * [+] None 
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FlatReconstruction::FindHomographies()
{
	ImgFloat fx, fy;
	HomographyFit( mFlatCheckered, mCheckeredFlat, &mHCheckered );
	InitWarpHomography( mHCheckered, mCheckeredResult.Width(), mCheckeredResult.Height(), &fx, &fy );
	Warp( mCheckeredImg, fx, fy, &mCheckeredResult );
	Inverse( mHCheckered, &mInverseHomog );
}

/*---------------------------------------------------------------------------*
 * Description: This function takes a blepo Array object of a blepo MouseClick
 * structure that contains points. It draws points and a line on the 
 * projective and flat images.  The points on the flat image is mapped using
 * the inverse homography matrix.  This function also creates a dialogue box
 * that displays the distance of the line in world coordinates based on the
 * values from the flat image.
 *
 * Parameters:
 * [+] points - an array of MouseClick structures with contain points 
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void FlatReconstruction::DrawPoints(Array<Figure::MouseClick> points)
{
	Array<Figure::MouseClick>::Iterator it = points.Begin();
	while ( it != points.End() )
	{
		DrawDot(it->pt, &mCheckeredImg, Bgr::RED);
		++it;
	}
	DrawLine( points[0].pt, points[1].pt, &mCheckeredImg, Bgr::RED );

	MatDbl myPts(3);
	myPts(0) = points[0].pt.x;
	myPts(1) = points[0].pt.y;
	myPts(2) = 1;
	
	MatDbl myPtsB(3);
	myPtsB(0) = points[1].pt.x;
	myPtsB(1) = points[1].pt.y;
	myPtsB(2) = 1;

	MatDbl result, resultB;

	MatrixMultiply( mInverseHomog, myPts,  &result  );
	MatrixMultiply( mInverseHomog, myPtsB, &resultB );

	result(0) = result(0) / result(2);
	result(1) = result(1) / result(2);
	result(2) = result(2) / result(2);

	resultB(0) = resultB(0) / resultB(2);
	resultB(1) = resultB(1) / resultB(2);
	resultB(2) = resultB(2) / resultB(2);
	
	double dist = sqrt( pow((double)(159-195),2) + pow((double)(454-420), 2) );
	double distConversion = (double)16 / dist;


	CString message;
	if ( result(0) > -1 && result(0) < mCheckeredResult.Width() &&
		result(1) > -1 && result(1) < mCheckeredResult.Height() &&
		resultB(0) > -1 && resultB(0) < mCheckeredResult.Width() &&
		resultB(1) > -1 && resultB(1) < mCheckeredResult.Height() )
	{
		Point ptA( result(0), result(1) );
		Point ptB( resultB(0), resultB(1) );
		dist = sqrt( pow((ptB.x-ptA.x),2) + pow((ptB.y-ptA.y), 2) );
		double distInches = dist * distConversion;
		DrawDot( ptA, &mCheckeredResult, Bgr::RED);
		DrawDot( ptB, &mCheckeredResult, Bgr::RED);
		DrawLine( ptA, ptB, &mCheckeredResult, Bgr::RED );
		//message.Format("Image Coordinates\r\n"
		//	"(x,y) = (%f,%f,%f)\r\n"
		//	"World Coordinates\r\n"
		//	"(x',y')->(x2',y2') = (%d,%d)--(%d,%d)\r\n"
		//	"Distance %f inches\r\n", 
		//	myPts(0), myPts(1),myPts(2), 
		//	ptA.x, ptA.y, ptB.x, ptB.y, distInches );
		message.Format( "Distance %f inches", distInches );
	}
	else
	{
		message.Format("Points aren't within bounds of world image.");
	}

	mFigProjective.Draw( mCheckeredImg );
	mFigFlat.Draw( mCheckeredResult );

	AfxMessageBox(message, MB_ICONINFORMATION);

}

Figure * FlatReconstruction::GetProjectiveFigure(char * title)
{
	mFigProjective.SetTitle(title);
	return &mFigProjective;
}

Figure * FlatReconstruction::GetFlatFigure(char * title)
{
	mFigFlat.SetTitle(title);
	return &mFigFlat;
}

ImgBgr * FlatReconstruction::GetProjectiveImage()
{
	return &mCheckeredImg;
}


ImgBgr * FlatReconstruction::GetFlatImage()
{
	return &mCheckeredResult;
}



/******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 *************************New Class TrafficReconstruction**********************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/


/*---------------------------------------------------------------------------*
 * Description:  Constructor for the TrafficReconstruction
 *
 * Parameters:
 * [+] fileName - the name of the file that will be the center of the panorama
 * Return: Nothing
 *---------------------------------------------------------------------------*/
TrafficReconstruction::TrafficReconstruction( CString fileName )
{
	mDebug = true;
	Initialize( fileName );
}

/*---------------------------------------------------------------------------*
 * Description: Initialization loads a traffic image and initializes the 
 * corresponding points between the projective and flat planes.
 *
 * Parameters:
 * [+] fileName - the name and path of an image to load
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::Initialize( CString fileName )
{
	Load( fileName, &mImg );
	mBirdsEyeResult.Reset( 150, 350 );
	InitCorrespondingPoints();
}

/*---------------------------------------------------------------------------*
 * Description:  Establishes the set of points needed to modify the projective
 * image to one that is flat. A rectangle is found in the original image and
 * its corners are mapped to corners in a flat representation.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::InitCorrespondingPoints()
{
	// Order of points from checkered image TL,TR,BL,BR
	mProjectiveFlat.push_back( Point2d(73.346954,47.732311) );
	mProjectiveFlat.push_back( Point2d(135.875961,43.49295) );
	mProjectiveFlat.push_back( Point2d(212,160) );
	mProjectiveFlat.push_back( Point2d(343.753448,131.073776) );

	mFlatProjective.push_back( Point2d(0,120) );
	mFlatProjective.push_back( Point2d(mBirdsEyeResult.Width()-15,120) );
	mFlatProjective.push_back( Point2d(0,mBirdsEyeResult.Height()-10) );
	mFlatProjective.push_back( Point2d(mBirdsEyeResult.Width()-15,mBirdsEyeResult.Height()-10) );
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a color image in the projective plane, and 
 * uses a homography calculated for a flat reconstruction and warps the image.
 *
 * Parameters:
 * [+] img - an image in the projective plane
 * [+] out - the input same image reconstructed onto a flat plane
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::GetWarpTrafficImage( const ImgBgr &img, ImgBgr * out)
{
	ImgFloat fx, fy;
	InitWarpHomography( mHomog, mBirdsEyeResult.Width(), mBirdsEyeResult.Height(), &fx, &fy );
	Warp( img, fx, fy, out );
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a binary image in the projective plane, and 
 * uses a homography calculated for a flat reconstruction and warps the image.
 *
 * Parameters:
 * [+] img - an image in the projective plane
 * [+] out - the input same image reconstructed onto a flat plane
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::GetWarpTrafficImage( const ImgBinary &img, ImgBinary * out)
{
	ImgFloat fx, fy;
	InitWarpHomography( mHomog, mBirdsEyeResult.Width(), mBirdsEyeResult.Height(), &fx, &fy );
	mFx = fx;
	mFy = fy;
	Warp( img, fx, fy, out );
}

/*---------------------------------------------------------------------------*
 * Description: This function takes the corresponding point vectors, computes 
 * the homography, and warps the projective image into something more flat.
 *
 * Parameters:
 * [+] None 
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::FindHomographies()
{
	ImgFloat fx, fy;
	HomographyFit( mFlatProjective, mProjectiveFlat, &mHomog );
	InitWarpHomography( mHomog, mBirdsEyeResult.Width(), mBirdsEyeResult.Height(), &fx, &fy );
	mFx = fx;
	mFy = fy;
	Warp( mBackground, fx, fy, &mBirdsEyeResult );
}

/*---------------------------------------------------------------------------*
 * Description: Creates the next image name using the original input image.
 * This method is specific to the file extension and naming convention of the
 * traffic images.
 *
 * Parameters:
 * [+] baseFileName - 1st part of the file name for an image in the traffic
 *                    sequence
 * [+] counter - the sequence number of the image for which we want a name
 * Return: Nothing
 *---------------------------------------------------------------------------*/
CString TrafficReconstruction::GetNextImageName( CString baseFileName, int counter )
{
	CString result;

	int numBase = 10;
	char * imgNumPre;
	char * imgNum;

	if ( counter < 10 )
	{
		imgNum    = (char *)malloc(2);
		imgNumPre = (char *)malloc(3);
		imgNumPre[0] = '0'; 
		imgNumPre[1] = '0';
		imgNumPre[2] = '\0';
	} else
	{
		imgNum    = (char *)malloc(2);
		imgNumPre = (char *)malloc(2);
		imgNumPre[0] = '0';
		imgNumPre[1] = '\0';
	}
	result = baseFileName + imgNumPre + itoa(counter,imgNum,numBase) + ".jpg";

	return result;
}

/*---------------------------------------------------------------------------*
 * Description: This method cuts the last 7 characters from an input image,
 * removing the extension and sequence number.
 *
 * Parameters:
 * [+] fileName - The absolute path for an input image
 * Return: Nothing
 *---------------------------------------------------------------------------*/
CString TrafficReconstruction::GetBaseFileName( CString fileName )
{
	int fnLength = fileName.GetLength()-6;

	char * baseFN = (char *)malloc(fnLength);
	char * fileN = fileName.GetBuffer(fileName.GetLength());

	strncpy( baseFN, fileN, fnLength );
	baseFN[fnLength-1] = '\0';
	CString baseFileName(baseFN);

	return baseFileName;
}

/*---------------------------------------------------------------------------*
 * Description: This method runs through all of the images in a sequence and
 * averages them to get a background image that is representative of the 
 * entire sequence.
 *
 * Parameters:
 * [+] fileName  - The absolute path for an input image
 * [+] meanImg   - The resulting background image
 * [+] numImages - The number of images in the sequence
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::CalculateMeanBackgroundImage( CString fileName, int numImages )
{
	int counter = 0 ;
	ImgBgr * meanImg = &mBackground;
	ImgBgr tempImg;
	ImgInt bImg, gImg, rImg;
	Load ( fileName, meanImg );
	
	// these images will be used to keep a sum of the BGR values from the multiple
	// traffic images
	bImg.Reset( meanImg->Width(), meanImg->Height() );
	gImg.Reset( meanImg->Width(), meanImg->Height() );
	rImg.Reset( meanImg->Width(), meanImg->Height() );
	Set( &bImg, 0 );
	Set( &gImg, 0 );
	Set( &rImg, 0 );


	CString baseFileName = GetBaseFileName(fileName);

	for ( counter = 0 ; counter < numImages ; counter++ )
	{
		CString myFileName = GetNextImageName(baseFileName, counter);

		Load( myFileName, &tempImg );
		for ( int i = 0 ; i < tempImg.Width(); i++ )
		{
			for ( int j = 0 ; j < tempImg.Height() ; j++ )
			{
				bImg(i,j) += tempImg(i,j).b;
				gImg(i,j) += tempImg(i,j).g;
				rImg(i,j) += tempImg(i,j).r;
			}
		}
	}
	
	// get the mean of the traffic images
	for ( int i = 0 ; i < meanImg->Width(); i++ )
	{
		for ( int j = 0 ; j < meanImg->Height() ; j++ )
		{
			(*meanImg)(i,j).b = (int)bImg(i,j)/counter;
			(*meanImg)(i,j).g = (int)gImg(i,j)/counter;
			(*meanImg)(i,j).r = (int)rImg(i,j)/counter;
		}
	}
}

void TrafficReconstruction::MassageBlobs( ImgBinary * img )
{
	ImgBinary tempImg;
	tempImg.Reset( img->Width(), img->Height() );
	
	Dilate3x3( *img, &tempImg );
	Dilate3x3( tempImg, img );
	Dilate3x3( *img, &tempImg );
	//Erode3x3(  binaryImg, &erodedImg );
	Erode3x3(  tempImg, img );
	//Dilate3x3( tempImg, &binaryImg );
	Dilate3x3( *img, &tempImg );
	Dilate3x3( tempImg, img );
	//Dilate3x3( *img, &tempImg );
	//Erode3x3(  tempImg, img );
	Erode3x3(  *img, &tempImg );
	Dilate3x3( tempImg, img );
	//Dilate3x3( *img, &tempImg );
}

/*---------------------------------------------------------------------------*
 * Description: This method subtracts the sequecne mean background from an 
 * image and creates a binary image from the result to differentiate the 
 * foreground from the background.  It then uses morphological functions to 
 * consolidate the foreground blobs.
 *
 * Parameters:
 * [+] fileName - The absolute path for an input image
 * [+] background - the mean background calculated from the sequence of images
 * [+] numImages - the number of images in the sequence
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::SegmentTraffic( CString fileName, int numImages )
{
	int base = 10;
	char * buffer;
	ImgBgr * background = &mBackground;
	ImgBgr tempImg, trafficImg, warpTrafficImg;
	ImgBinary binaryImg, erodedImg, warpBinTrafficImg;
	CString baseFileName = GetBaseFileName(fileName);

	Figure figBackground("Background Image");
	figBackground.Draw(mBackground);

	mFigSegmented.SetTitle("Binary Traffic");
	mFigSubtracted.SetTitle("BGR Traffic");
	for ( int counter = 0 ; counter < numImages ; counter++ )
	{
		CString myFileName = GetNextImageName(baseFileName, counter);
		Load( myFileName, &trafficImg );
		Subtract( *background, trafficImg, &tempImg );

		CreateBinaryImage( tempImg, &binaryImg );
		// black out the on the screen timer
		Set( &binaryImg, Rect(0,225,184,240), 0 );		
		MassageBlobs( &binaryImg );

		mFigSubtracted.Draw(trafficImg);
		buffer = (char *)malloc(4);
		itoa(counter+1, buffer, base);
		CString segmentedTitle( "Binary Traffic #" );
		strcat(segmentedTitle.GetBuffer(100),buffer);
		mFigSegmented.SetTitle( segmentedTitle );
		mFigSegmented.PlaceToTheRightOf(mFigSubtracted);
		mFigSegmented.Draw(binaryImg);

		GetWarpTrafficImage( binaryImg, &warpBinTrafficImg );
		//GetWarpTrafficImage( erodedImg, &warpBinTrafficImg );
		GetWarpTrafficImage( trafficImg, &warpTrafficImg );

		mFigWarpBgr.PlaceToTheRightOf(mFigSegmented);
		mFigWarpBgr.SetTitle("Warped Traffic");
		mFigWarpBgr.Draw(warpTrafficImg);

		mFigWarpBin.PlaceToTheRightOf(mFigWarpBgr);
		mFigWarpBin.SetTitle("Warped Segmented Traffic");
		mFigWarpBin.Draw(warpBinTrafficImg);
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method subtracts the sequecne mean background from an 
 * image and creates a binary image from the result to differentiate the 
 * foreground from the background.  It then uses morphological functions to 
 * consolidate the foreground blobs.  After create the blobs it uses a
 * technique to derive 3D coordinates using the blobs and the color from the
 * original image.
 *
 * Parameters:
 * [+] fileName - The absolute path for an input image
 * [+] background - the mean background calculated from the sequence of images
 * [+] numImages - the number of images in the sequence
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::CreateColoredPoints( CString fileName, int numImages )
{
	ImgBgr * background = &mBackground;
	ImgBgr tempImg, trafficImg, warpTrafficImg;
	ImgBinary binaryImg, erodedImg, warpBinTrafficImg;
	CString baseFileName = GetBaseFileName(fileName);

	mFigSegmented.SetTitle("Binary Traffic");

	MyFigureGlut mfg("3D reconstructed traffic", 500,500 );
	int startCounterAt = 25;
	numImages          = 26;
	for ( int counter = startCounterAt ; counter < numImages ; counter++ )
	{
		CString myFileName = GetNextImageName(baseFileName, counter);
		Load( myFileName, &trafficImg );
		Subtract( *background, trafficImg, &tempImg );

		CreateBinaryImage( tempImg, &binaryImg );
		// black out the on the screen timer
		Set( &binaryImg, Rect(0,225,184,240), 0 );		
		MassageBlobs( &binaryImg );

		mFigSegmented.Draw(binaryImg);
		GetWarpTrafficImage( binaryImg, &warpBinTrafficImg );
		GetWarpTrafficImage( trafficImg, &warpTrafficImg );
		
		vector<MyFigureGlut::ColoredPoint> cp;
		for ( int i = 0 ; i < binaryImg.Width() ; i++ )
		{
			for ( int j = 0 ; j < binaryImg.Height() ; j++ )
			{
				vector<double> myCoords = GetTransformed3DCoordinates( mFx, mFy, binaryImg, i, j );
				// for image 33 x-190, y-350
				cp.push_back( MyFigureGlut::ColoredPoint(95-myCoords.at(0), 180-myCoords.at(1), -myCoords.at(2), trafficImg( i, j ) ) );
			}
		}
		mfg.SetPoints( cp );
		mfg.EnterMainLoop();
	}
}

/*---------------------------------------------------------------------------*
 * Description: This method takes a color traffic image from which the 
 * background has been subtracted, thresholds it to create a binary image.
 *
 * Parameters:
 * [+] img - A color image from which the background has been subtracted
 * [+] binaryImg - A reference to where the binary image should be connected
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void TrafficReconstruction::CreateBinaryImage( ImgBgr & img, ImgBinary * binaryImg )
{
	binaryImg->Reset( img.Width(), img.Height() );
	ImgBgr::Iterator it = img.Begin();
	ImgBinary::Iterator bit = binaryImg->Begin();
	while ( it != img.End() )
	{
		*bit = ( abs(it->b) < 100 && abs(it->g) < 100 && abs(it->r) < 100 ) ? 0 : 1;
		++it;
		++bit;
	}
}

/*---------------------------------------------------------------------------*
 * Description:  This method takes a point from a binary image and calculates
 * the lower bound of a foreground blob.
 *
 * Parameters:
 * [+] img - A binary image with blobs set as 1 and background as 0.
 * [+] u - horizontal location of point within image
 * [+] v - vertical location of point within image
 * Return: Pixel that represents how far down the foreground blob is from
 * the point passed in
 *---------------------------------------------------------------------------*/
int TrafficReconstruction::GetBottom( const ImgBinary & img, int u, int v )
{
	int result, j = v;
	while( j < img.Height() && img(u,j) == 1 ) { j++; }
	result = j - 1;
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Perform coordinate transformations to construct 3D points.
 *
 * Parameters:
 * [+] fx - function that maps horizontal projective and real world coordinates
 * [+] fy - function that maps vertical projective and real world coordinates
 * [+] img - A binary image with blobs set as 1 and background as 0.
 * [+] u - horizontal location of point within image
 * [+] v - vertical location of point within image
 * Return: 3 coordinates that represent x, y, and z
 *---------------------------------------------------------------------------*/
vector <double> TrafficReconstruction::GetTransformed3DCoordinates( const ImgFloat & fx, const ImgFloat & fy, const ImgBinary & img, int u, int v )
{
	vector<double> result(3);
	int v0, u1;
	double x0, y0, x1, y1, z;

	v0 = GetBottom( img, u, v );
	u1 = ( u < img.Width()/2 ) ? u + abs(v-v0) : u - abs(v-v0);
	x0 = fx(u, v0);
	y0 = fy(u, v0);
	x1 = fx(u1, v0);
	y1 = fy(u1, v0);

	z = ( img(u,v) == 1 ) ? sqrt( pow( x1 - x0, 2 ) + pow( y1 - y0, 2 ) ) : 0;

	result.at(0) = x0;
	result.at(1) = y0;
	result.at(2) = z;

	return result;
}

/*---------------------------------------------------------------------------*
 * Description: This method returns a copy of the mean background image.
 *
 * Parameters:
 * [+] None
 * Return: A copy of the mean background image.
 *---------------------------------------------------------------------------*/
ImgBgr * TrafficReconstruction::GetBackground()
{
	return &mBackground;
}