/*****************************************************************************
 * EpipolarLineDrawer.h
 *
 *****************************************************************************/

#pragma once

#ifndef _EPIPOLARLINEDRAWER_H_
#endif // _EPIPOLARLINEDRAWER_H_

#include "ImageProcessing.h"
#include <queue>

class EpipolarLineDrawer
{
public:
	//EpipolarLineDrawer( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg );
	EpipolarLineDrawer( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg, bool normalized=false );

	void PrintMatDbl( blepo::MatDbl &mat, char * fileName );

	std::vector<double>       getX1();
	std::vector<double>       getY1();
	std::vector<double>       getX2();
	std::vector<double>       getY2();

	blepo::ImgBgr             getImageOne();
	blepo::ImgBgr             getImageTwo();

	std::vector<blepo::Point> getPointsAtLimit(blepo::Point imgLmt, double a, double b, double c);
	std::vector<blepo::Point> DrawEpipolarLine( blepo::ImgBgr * img, bool isPrime, blepo::Point point );
	std::vector<blepo::Point> DrawNormalizedEpipolarLine( blepo::ImgBgr * img, bool isPrime, blepo::Point point );

	int                       CalculateX( int y, double a, double b, double c );
	int                       CalculateY( int x, double a, double b, double c );

private:

	// Vectors to 
	std::vector<double> m_x1;
	std::vector<double> m_y1;
	std::vector<double> m_x2;
	std::vector<double> m_y2;

	std::vector<blepo::Point> m_XY, m_XYPrime;
	std::vector<blepo::Point2d> m_XYNorm, m_XYPrimeNorm;
	blepo::Point2d mXYMean, mXYMeanPrime;

	blepo::ImgBgr m_imgOne;
	blepo::ImgBgr m_imgTwo;

	blepo::MatDbl m_fundamentalMatrix;
	blepo::MatDbl mFMHat;
	blepo::MatDbl m_nbyNine, mNByNineNorm;

	void Initialize( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg );
	void NormalizedInit( blepo::ImgBgr firstImg, blepo::ImgBgr secondImg );
	void SetCorrespondingPoints();
	void NormalizeCoordinates( const std::vector<blepo::Point> & myPoints, blepo::Point2d * normalizedPoint );
	void CalculateDAvgAndS( const blepo::Point2d meanPoint, 
		const std::vector<blepo::Point> & myPoints, std::vector<double> * result );
	void CreateMatrixA();
	void CreateNormalizedMatrixA();
	void ComputeFM();
	void ComputeNormalizedFM();
	void DrawEpipolarLines();
	void DrawNormalizedEpipolarLines();

	void CreateNormalizedPoints( const std::vector<blepo::Point> & myPoints,
		std::vector<blepo::Point2d> * newPoints, blepo::Point2d * meanPoint );
};