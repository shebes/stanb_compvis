/*****************************************************************************
 * TrafficReconstruction.h
 *
 *****************************************************************************/

#pragma once

#ifndef _TRAFFICRECONSTRUCTION_H_
#endif // _TRAFFICRECONSTRUCTION_H_

//#include "string.h"
#include "ImageProcessing.h"
#include "FigureGlut.h"
#include <queue>

using namespace blepo;
using namespace std;

class TrafficReconstruction
{
public:
	// constructors
	TrafficReconstruction( CString fileName );

	void FindHomographies();
	void CalculateMeanBackgroundImage( CString fileName, int numImages );
	void SegmentTraffic( CString fileName, int numImages );
	void CreateColoredPoints( CString fileName, int numImages );

	vector <double> GetTransformed3DCoordinates( const ImgFloat & fx, const ImgFloat & fy, const ImgBinary & img, int u, int v );

	CString GetNextImageName( CString baseFileName, int counter );
	CString GetBaseFileName( CString fileName );

	ImgBgr * GetBackground();

private:
	// variables
	bool mDebug;

	ImgBgr mImg;
	ImgBgr mBackground;
	ImgBgr mBirdsEyeResult;
	ImgFloat mFx, mFy;

	vector<Point2d> mProjectiveFlat;
	vector<Point2d> mFlatProjective;

	MatDbl mHomog;

	// Figures
	Figure mFigSubtracted, mFigSegmented;
	Figure mFigWarpBgr, mFigWarpBin;

	// class initialization
	void Initialize( CString fileName );
	void InitCorrespondingPoints();
	void GetWarpTrafficImage( const ImgBgr &img, ImgBgr * out);
	void GetWarpTrafficImage( const ImgBinary &img, ImgBinary * out);
	void MassageBlobs( ImgBinary * img );
	void CreateBinaryImage( ImgBgr & img, ImgBinary * binaryImg );
	

	int GetBottom( const ImgBinary & img, int u, int v );
	int GetSide(   const ImgBinary & img, int u, int v );
};