/*****************************************************************************
 * FlatReconstruction.h
 *
 *****************************************************************************/

#pragma once

#ifndef _FLATRECONSTRUCTION_H_
#endif // _FLATRECONSTRUCTION_H_

#include "ImageProcessing.h"
#include <queue>

using namespace blepo;
using namespace std;

class FlatReconstruction
{
public:
	// constructors
	FlatReconstruction( CString fileName );

	void FindHomographies();
	void DrawPoints(Array<Figure::MouseClick> points);

	ImgBgr * GetProjectiveImage();
	ImgBgr * GetFlatImage();

	Figure * GetProjectiveFigure(char * title);
	Figure * GetFlatFigure(char * title);

private:
	// variables
	bool mDebug;

	ImgBgr mCheckeredImg;
	ImgBgr mCheckeredResult;

	vector<Point2d> mCheckeredFlat;
	vector<Point2d> mFlatCheckered;

	MatDbl mHCheckered, mInverseHomog;

	// Figures
	Figure mFigProjective;
	Figure mFigFlat;

	// class initialization
	void Initialize( CString fileName );
	void InitCorrespondingPoints();
};