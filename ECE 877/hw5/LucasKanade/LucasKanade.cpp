// LucasKanade.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "LucasKanade.h"
#include "LucasKanadeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;
using namespace blepo;


/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeApp

BEGIN_MESSAGE_MAP(CLucasKanadeApp, CWinApp)
	//{{AFX_MSG_MAP(CLucasKanadeApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeApp construction

CLucasKanadeApp::CLucasKanadeApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CLucasKanadeApp object

CLucasKanadeApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeApp initialization

BOOL CLucasKanadeApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CLucasKanadeDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}



/*---------------------------------------------------------------------------*
 * Description: Default empty constructor.
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
LucasAndKanade::LucasAndKanade()
{ 
	Initialize( DEFAULT_SIGMA, DEFAULT_WINDOW_SIZE );
}

/*---------------------------------------------------------------------------*
 * Description: 
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
LucasAndKanade::LucasAndKanade( double sigma )
{
	Initialize( sigma, DEFAULT_WINDOW_SIZE );
}

/*---------------------------------------------------------------------------*
 * Description: 
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
LucasAndKanade::LucasAndKanade( double sigma, int windowSize )
{
	Initialize( sigma, windowSize );
}


/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void LucasAndKanade::Initialize( double sigma, int windowSize )
{
	try
	{
		m_sigma      = sigma;
		m_windowSize = windowSize;
		m_hw         = windowSize / 2;
		
		m_img1w.Reset(  getWindowSize(), getWindowSize() );
		m_img2w.Reset(  getWindowSize(), getWindowSize() );
		m_gradxw.Reset( getWindowSize(), getWindowSize() );
		m_gradyw.Reset( getWindowSize(), getWindowSize() );
		Set( &m_img1w,  0.0 );
		Set( &m_img2w,  0.0 );
		Set( &m_gradxw, 0.0 );
		Set( &m_gradyw, 0.0 );
		
		m_covmat.Reset(2,2);
		m_errvec.Reset(1,2);
		m_deltaD.Reset(1,2);
		Set( &m_covmat, 0.0 );
		Set( &m_errvec, 0.0 );
		Set( &m_deltaD, 0.0 );
	} catch ( const Exception & exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Create the x and y gradients for an image.  The gradients are
 * stored in member variables of this object.
 *
 * Parameters:
 * [+] img - image for which we create x and y gradients
 * [+] gx  - reference to img to contain x gradient
 * [+] gy  - reference to img to contain y gradient
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::CalculateGradients( ImgFloat & img, ImgFloat *gx, ImgFloat *gy )
{
	try
	{
		blepo::Gradient( img, m_sigma, gx, gy );
	} catch ( const Exception & exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: The method that takes the 1st image in a sequence and gets the 
 * interesting points that will be tracked throughout the rest of the 
 * sequence.
 *
 * Parameters:
 * [+] fileName - the filename of the image to load
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::InitImage( CString fileName )
{
	try
	{
		LoadImage( fileName ) ;
		DetectMostSalientFeatures( m_imgOne );
		DrawFeaturePoints( &m_imgOne, &m_feps);
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void LucasAndKanade::LoadImage( CString fileName )
{
	try
	{
		Load( fileName, &m_imgOne ) ;
		Convert( m_imgOne, &m_fImgOne );
	} catch ( const Exception & exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void LucasAndKanade::LoadNextImage( CString fileName )
{
	try
	{
		Load( fileName, &m_imgTwo ) ;
		Convert( m_imgTwo, &m_fImgTwo );
	} catch ( const Exception & exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void LucasAndKanade::NextImage( CString fileName )
{
	try
	{
		LoadNextImage(fileName);

		// initialize the d matrix
		MatDbl d(1,2);
		int i = 0 ;
		vector<FeatureEigenPoint>::iterator it = m_feps.GetFeaturesIteratorBegin();
		while ( it != m_feps.GetFeaturesIteratorEnd() )
		{
			Set(&d, 0.0);
			FeatureEigenPoint oldFep = *it;
			LucasKanade( it, &d );
			
			it++ ;
			i++;
		}
		SwapImages();
		DrawFeaturePoints( &m_imgOne, &m_feps );
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void LucasAndKanade::LucasKanade( FeatureEigenPoint * fep, MatDbl * d )
{
	try
	{
		double newx    = 0.0, 
			   newy    = 0.0,
			   dx      = 0.0, 
			   dy      = 0.0;
		CalculateGradients( m_fImgOne, &m_gradxOne, &m_gradyOne );
		
		InterpolateW( m_fImgOne,  &m_img1w,  fep->fp.x, fep->fp.y );
		InterpolateW( m_gradxOne, &m_gradxw, fep->fp.x, fep->fp.y );
		InterpolateW( m_gradyOne, &m_gradyw, fep->fp.x, fep->fp.y );
		Compute2x2GradientMatrix( &m_covmat, m_gradxw,  m_gradyw  );

		for ( int i = 0 ; i < getWindowSize()*2 ; i++ )
		{
			dx = fep->fp.x + (*d)(0,0);
			dy = fep->fp.y + (*d)(0,1);
			Set( &m_img2w, 0.0 );
			InterpolateW( m_fImgTwo, &m_img2w, dx, dy );
			Set( &m_errvec, 0.0 );
			Compute2x1ErrorVector( &m_errvec, m_img1w, m_img2w, m_gradxw, m_gradyw );
			Set( &m_deltaD, 0.0 );
			Solve( &m_deltaD, m_covmat, m_errvec );
			(*d)(0,0) += m_deltaD(0,0);
			(*d)(0,1) += m_deltaD(0,1);
		}

		newx = fep->fp.x + (*d)(0,0);
		newy = fep->fp.y + (*d)(0,1);
		if ( newx > getHw() && newx < m_fImgOne.Width() - getHw() )
		{
			fep->fp.x = floor(newx+0.5);
		}
		if ( newy > getHw() && newy < m_fImgOne.Height() - getHw() )
		{
			fep->fp.y = floor(newy+0.5);
		}
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Method that creates a list of N interesting points to track
 * in the sequence of images
 *
 * Parameters:
 * [+] img        - image in which features are to be detected
 * [+] fNumber    - number of features to be found and used
 * [+] fProximity - distance features must be from one another
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::DetectMostSalientFeatures( ImgBgr &img, int fNumber, int fProximity )
{
	try
	{	
		// calculate the gradient images for the initial image
		CalculateGradients( m_fImgOne, &m_gradxOne, &m_gradyOne );
		// initialize a vector to contain a list of points for which 
		// covariance matrices have been calculated
		FeatureEigenPoints feps;

		for ( int y = getHw() ; y < img.Height() - getHw() ; y++ )
		{
			for ( int x = getHw() ; x < img.Width() - getHw() ; x++ )
			{
				Point cp(x,y);
				MatDbl covmat(2,2);
				Set( &covmat, 0.0 );
				Compute2x2GradientMatrix( &covmat, m_gradxOne, m_gradyOne, cp );
				MatDbl eigenVals;
				EigenSymm( covmat, &eigenVals );

				double minEigen = ImageProcessing::MatDblMin( &eigenVals );

				FeatureEigenPoint fep;
				fep.mineigen = minEigen;
				fep.fp = cp;
				fep.eigenvalue = eigenVals;

				if ( minEigen > 1000.0 )
				{
					feps.Add(fep);
				}
			}
		}
		feps.Sort();
		//feps.PrintToFile("C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\feps.out");

		vector<FeatureEigenPoint>::reverse_iterator rit;
		int counter = 0 ;
		for ( rit = feps.m_feps.rbegin(); rit != feps.m_feps.rend() ; rit++ )
		{
			if ( m_feps.AddWithMinDist( *rit, fProximity ) )
			{
				
				if ( ++counter == fNumber ) 
				{
					break;
				}
			}
		}

		//m_feps.PrintToFile("C:\\Users\\shebes\\hw\\ece847\\blepo\\output\\feps100.out");
	} catch (const Exception& exp)
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: We use a 5x5, by default, window to calculate the 2x2 
 * covariance matrix.  The window size can be changed.
 *
 * Parameters:
 * [+] covmat     - reference to a 2x2 covariance matrix
 * [+] p          - point for which we are calculating the covariance matrix
 * [+] windowSize - area of image used to calculate the 2x2 covariance matrix
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::Compute2x2GradientMatrix( MatDbl * covmat, const ImgFloat & gradxw, const ImgFloat & gradyw)
{
	try
	{
		double xx = 0.0,
			   xy = 0.0,
			   yy = 0.0;

		int x, y;
		for ( y = 0 ; y < getWindowSize() ; y++ )
		{
			for ( x = 0 ; x < getWindowSize() ; x++ )
			{
				xx += pow( gradxw(x,y), 2 );
				xy += gradxw(x,y) * gradyw(x,y);
				yy += pow( gradyw(x,y), 2 );
			}
		}

		(*covmat)(0,0) = xx;
		(*covmat)(0,1) = xy;
		(*covmat)(1,0) = xy;
		(*covmat)(1,1) = yy;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: We use a 5x5, by default, window to calculate the 2x2 
 * covariance matrix.  The window size can be changed.
 *
 * Parameters:
 * [+] covmat - reference to a 2x2 covariance matrix
 * [+] gx     - x gradient of 1st image
 * [+] gy     - y gradient of 1st image
 * [+] cp     - point for which we are calculating the covariance matrix
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::Compute2x2GradientMatrix( MatDbl * covmat, const ImgFloat & gx, const ImgFloat & gy, Point cp )
{
	try
	{
		covmat->Reset(2,2);
		Set( covmat, 0.0 );
		double xx = 0.0,
			   xy = 0.0,
			   yy = 0.0;

		int x, y;
		for ( y = cp.y - getHw() ; y <= cp.y + getHw() ; y++ )
		{
			for ( x = cp.x - getHw() ; x <= cp.x + getHw() ; x++ )
			{
				xx += pow( gx(x,y), 2 );
				xy += gx(x,y) * gy(x,y);
				yy += pow( gy(x,y), 2 );
			}
		}

		(*covmat)(0,0) = xx;
		(*covmat)(0,1) = xy;
		(*covmat)(1,0) = xy;
		(*covmat)(1,1) = yy;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Calculate the error vector using the temporal distance 
 * between two images and values of the image gradients
 *
 * Parameters:
 * [+] errvec - error vector between two images
 * [+] x      - point in horizontal
 * [+] y      - point in vertical
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::Compute2x1ErrorVector( blepo::MatDbl * errvec, double x, double y)
{
	try
	{
		double ex = 0.0, 
			   ey = 0.0;

		int hw = getWindowSize() / 2 ;

		if ( x - getHw() >= getHw() && y - getHw() >= getHw() && x + getHw() < m_gradxOne.Width()-getHw() && y + getHw() <= m_gradxOne.Height()-getHw() )
		{
			int i, j;
			for ( i = x - getHw(); i <= x + getHw() ; i++ )
			{
				for ( j = y - getHw(); j <= y + getHw() ; j++ )
				{
					double temporalDistance = m_fImgOne(i,j) - m_fImgTwo(i,j) ;
					ex += m_gradxOne( i, j ) * temporalDistance;
					ey += m_gradyOne( i, j ) * temporalDistance;
				}
			}
		}
		(*errvec)(0,0) = ex;
		(*errvec)(0,1) = ey;

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Calculate the error vector using the temporal distance 
 * between two images and values of the image gradients
 *
 * Parameters:
 * [+] errvec - error vector between two images
 * [+] img1w  - window containing interpolated values for the 1st image
 * [+] img2w  - window containing interpolated values for the 2nd image
 * [+] gradxw - window containing interpolated values for the x gradient
 * [+] gradyw - window containing interpolated values for the y gradient
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::Compute2x1ErrorVector( MatDbl * errvec, 
										    const ImgFloat & img1w, 
											const ImgFloat & img2w, 
											const ImgFloat & gradxw, 
											const ImgFloat & gradyw )
{
	try
	{
		double ex = 0.0,
			   ey = 0.0;
		for ( int j = 0 ; j < getWindowSize(); j++ )
		{	
			for ( int i = 0 ; i < getWindowSize(); i++ )
			{
				double temporalDistance = img1w(i,j) - img2w(i,j);
				ex += gradxw(i,j) * temporalDistance;
				ey += gradyw(i,j) * temporalDistance;
			}
		}
		(*errvec)(0,0) = ex;
		(*errvec)(0,1) = ey;
	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Ascertain a value for a non integer coordinate in an image
 *
 * Parameters:
 * [+] img - image of gray level values
 * [+] cp  - a coordinate in the image
 * Return: a value for a non integer coordinate in the image
 *---------------------------------------------------------------------------*/
double LucasAndKanade::Interpolate( const ImgFloat & img, double x, double y )
{
	double result = 0.0;
	try
	{
		double x0, y0, ax, ay;

		x0 = floor(x);
		y0 = floor(y);
		ax = x - x0;
		ay = y - y0;
		result = (1-ax) * (1-ay) * img(x0  , y0  )
			   +   ax   * (1-ay) * img(x0+1, y0  )
			   + (1-ax) *   ay   * img(x0  , y0+1)
			   +   ax   *   ay   * img(x0+1, y0+1) ;

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Interpolate for a group points in a piece or window of an
 * image
 *
 * Parameters:
 * [+] img - image of gray level values
 * [+] cp  - a coordinate in the image
 * Return: a small set of points whose values have been interpolated using
 *         its surrounding pixels
 *---------------------------------------------------------------------------*/
void LucasAndKanade::InterpolateW(const ImgFloat& img, ImgFloat * result, double x, double y )
{
	try
	{
		ImgFloat answer( getWindowSize(), getWindowSize() );
		Set( &answer, 0.0 );
		double dx = 0.0,
			   dy = 0.0;

		for ( int j = 0 ; j < getWindowSize() ; j++ )
		{
			for ( int i = 0 ; i < getWindowSize() ; i++ )
			{
				dx = x + (i-getHw());
				dy = y + (j-getHw());

				if ( dx < getHw() ) 
				{
					dx = getHw() ;
				} else if ( dx > img.Width() - getHw() )
				{
					dx = img.Width() - getHw() - 1;
				} else if ( dy < getHw() ) 
				{
					dy = getHw() ;
				} else if ( dy > img.Height() - getHw() )
				{
					dy = img.Height() - 1 - getHw();
				}
				answer(i,j) = Interpolate( img, dx, dy );
				i = i;
			}
		}
		*result = answer;

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Method that returns true if the 1st parameter has a lower
 * minimum value than the 2nd paramter.
 *
 * Parameters:
 * [+] i - a blepo::MatDbl
 * [+] j - a blepo::MatDbl
 * Return: TRUE if matrix i has a min value less than matrix j's min value
 *---------------------------------------------------------------------------*/
bool LucasAndKanade::CompareMatDbl(MatDbl i,MatDbl j) 
{ 
	return ( ImageProcessing::MatDblMin(&i) < ImageProcessing::MatDblMin(&j) ) ;
}

/*---------------------------------------------------------------------------*
 * Description: Use the FeatureEigenPoints object to draw the interesting
 * points on the original image.
 *
 * Parameters:
 * [+] img  - a pointer to the image on which feature points will be drawn
 * [+] feps - a pointer to the FeatureEigenPoints object
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::DrawFeaturePoints( blepo::ImgBgr * img, FeatureEigenPoints * feps )
{
	try
	{
		int hw = getWindowSize() / 2;
		vector<FeatureEigenPoint>::iterator it = feps->GetFeaturesIteratorBegin();
		while ( it != feps->GetFeaturesIteratorEnd() )
		{
			blepo::DrawCircle( it->fp, 1, img, Bgr::RED, getHw());
			it++;
		}
	} catch ( const Exception & exp )
	{
		exp.Display();
	}

}

/*---------------------------------------------------------------------------*
 * Description: Swap image 2 and image 1, getting the object ready for the 
 * next image in the movie sequence.
 *
 * Parameters:
 * [+] none
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::SwapImages()
{
	try
	{
		int width  = m_imgOne.Width(),
			height = m_imgOne.Height();

		ImgBgr tempBgr( width, height );
		tempBgr    = m_imgOne;
		m_imgOne.Reset();
		m_imgOne   = m_imgTwo;
		m_imgTwo.Reset();
		m_imgTwo   = tempBgr;

		ImgFloat tempFloat( width, height );		
		tempFloat  = m_fImgOne;
		m_fImgOne.Reset();
		Convert( m_imgOne, &m_fImgOne );
		m_fImgTwo.Reset();
		m_fImgTwo  = tempFloat;

		m_gradxOne.Reset();
		m_gradyOne.Reset();
		CalculateGradients(m_fImgOne, &m_gradxOne, &m_gradyOne);

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}

/*---------------------------------------------------------------------------*
 * Description: Solves a linear equation to find the motion vector for an
 * interesting point.
 *
 * Parameters:
 * [+] deltaD - the motion vector for the interesting point
 * [+] covmat - the covariance matrix
 * [+] errvec - the error vector
 * Return: nothing
 *---------------------------------------------------------------------------*/
void LucasAndKanade::Solve( blepo::MatDbl * deltaD, const blepo::MatDbl & covmat, const blepo::MatDbl & errvec )
{
	try
	{

		double xx  = covmat(0,0),
			   xy  = covmat(0,1),
			   yy  = covmat(1,1),
			   ex  = errvec(0,0),
			   ey  = errvec(0,1);
		double det = (xx * yy) - pow(xy,2);

		(*deltaD)(0,0) = ( (yy * ex) - (xy * ey) ) / det;
		(*deltaD)(0,1) = ( (xx * ey) - (xy * ex) ) / det;

	} catch ( const Exception& exp )
	{
		exp.Display();
	}
}
 

///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// GETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


/*---------------------------------------------------------------------------*
 * Description: Return the original image
 *
 * Parameters:
 * [+] none
 * Return: the original image as an ImgBgr
 *---------------------------------------------------------------------------*/
blepo::ImgBgr   LucasAndKanade::getBgrImage()
{
	return m_imgOne;
}

/*---------------------------------------------------------------------------*
 * Description: Return the window size for calculating motion
 *
 * Parameters:
 * [+] none
 * Return: nothing
 *---------------------------------------------------------------------------*/
int LucasAndKanade::getWindowSize()
{
	return m_windowSize;
}

/*---------------------------------------------------------------------------*
 * Description: Return the window size for calculating motion
 *
 * Parameters:
 * [+] none
 * Return: nothing
 *---------------------------------------------------------------------------*/
int LucasAndKanade::getHw()
{
	return m_hw;
}
