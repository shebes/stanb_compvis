/*****************************************************************************
 * TrafficReconstruction.h
 *
 *****************************************************************************/

#pragma once

#ifndef _TRAFFICRECONSTRUCTION_H_
#endif // _TRAFFICRECONSTRUCTION_H_

//#include "string.h"
#include "ImageProcessing.h"
#include <queue>

using namespace blepo;
using namespace std;

class TrafficReconstruction
{
public:
	// constructors
	TrafficReconstruction( CString fileName );

	void FindHomographies();
	void CalculateMeanBackgroundImage( CString fileName, int numImages );
	void SegmentTraffic( CString fileName, int numImages );
	CString GetNextImageName( CString baseFileName, int counter );
	CString GetBaseFileName( CString fileName );

	ImgBgr * GetBackground();


private:
	// variables
	bool mDebug;

	ImgBgr mImg;
	ImgBgr mBackground;
	ImgBgr mBirdsEyeResult;

	vector<Point2d> mProjectiveFlat;
	vector<Point2d> mFlatProjective;

	MatDbl mHomog;

	// Figures
	Figure mFigSubtracted, mFigSegmented;
	Figure mFigWarpBgr, mFigWarpBin;

	// class initialization
	void Initialize( CString fileName );
	void InitCorrespondingPoints();
	void GetWarpTrafficImage( const ImgBgr &img, ImgBgr * out);
	void GetWarpTrafficImage( const ImgBinary &img, ImgBinary * out);
};