// LucasKanadeDlg.h : header file
//

#if !defined(AFX_LUCASKANADEDLG_H__D5452C78_B2CB_4C1D_975E_7A3FD396C84D__INCLUDED_)
#define AFX_LUCASKANADEDLG_H__D5452C78_B2CB_4C1D_975E_7A3FD396C84D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CLucasKanadeDlg dialog

class CLucasKanadeDlg : public CDialog
{
// Construction
public:
	CLucasKanadeDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CLucasKanadeDlg)
	enum { IDD = IDD_LUCASKANADE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLucasKanadeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;
	char * m_cFileDialogFilter;

	// Generated message map functions
	//{{AFX_MSG(CLucasKanadeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnFloodfill4();
	afx_msg void OnFloodfill8();
	afx_msg void OnOutlineForeground();
	afx_msg void OnCanny();
	afx_msg void OnTempateMatching();
	afx_msg void OnWatershedSegmentation();
	afx_msg void OnLkOpenDlg();
	afx_msg void OnPencilEpipolar();
	afx_msg void OnBtnStereoMatching();
	afx_msg void OnLetterDetect();
	afx_msg void OnBtnLevelset();
	afx_msg void OnButtonSurf();
	afx_msg void OnBtnMosaick();
	afx_msg void OnBtnFlatrecon();
	afx_msg void OnBtnTrafficrecon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LUCASKANADEDLG_H__D5452C78_B2CB_4C1D_975E_7A3FD396C84D__INCLUDED_)
