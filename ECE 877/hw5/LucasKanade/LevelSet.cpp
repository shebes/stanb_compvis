/*****************************************************************************
 * LevelSet.cpp
 *
 * Date: Tuesday February 1, 2011
 *****************************************************************************/
#include "stdafx.h"
#include "LevelSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace blepo;
using namespace std;


/*---------------------------------------------------------------------------*
 * Description:  Constructor for the Chan-Vese level set algorithm
 *
 * Parameters:
 * [+] sigma     - Value used to determine the kernel used for getting the 
 *                 derivative for the implicit function
 * [+] fileName  - name of the file for which the implicit function is derived
 * [+] mu        - a variable used to tweak the behavior of the algorithm
 * [+] v         - a variable used to tweak the behavior of the algorithm
 * [+] lambdaIn  - a variable used to tweak the behavior of the algorithm
 * [+] lambdaOut - a variable used to tweak the behavior of the algorithm
 * Return: Nothing
 *---------------------------------------------------------------------------*/
ChanVeseLevelSet::ChanVeseLevelSet( float sigma, CString fileName, float mu, float v, float lambdaIn, float lambdaOut )
{
	Initialize( sigma, fileName, mu, v, lambdaIn, lambdaOut );
}

/*---------------------------------------------------------------------------*
 * Description:  Method to iteratively improve the contour
 *
 * Parameters:
 * [+] None
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::IterateOverLevel( bool recalculatePhi )
{
	ComputeDivergence( m_gi, m_imgPhi );
	ComputePhiEnergyChange( m_floatImg, m_phiGradient, m_divNormPhi, &m_deltaPhi );

	if ( recalculatePhi )
	{
		ApplyGradientDescent( m_imgPhi, m_deltaPhi, &m_imgPhi );
		CalculateChamferDistance( &m_imgPhi );
	}
	UpdateContour( m_imgPhi );
}

/*---------------------------------------------------------------------------*
 * Description: Initializer that sets initial member variables from passed in
 * variables and calls the methods to perform correllation based matching 
 * with a consistency check on rectified stereo images.
 *
 * Parameters:
 * [+] sigma     - Value used to determine the kernel used for getting the 
 *                 derivative for the implicit function
 * [+] fileName  - name of the file for which the implicit function is derived
 * [+] mu        - a variable used to tweak the behavior of the algorithm
 * [+] v         - a variable used to tweak the behavior of the algorithm
 * [+] lambdaIn  - a variable used to tweak the behavior of the algorithm
 * [+] lambdaOut - a variable used to tweak the behavior of the algorithm
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::Initialize( float sigma, CString fileName, float mu, float v, float lambdaIn, float lambdaOut )
{
	setLambdaIn(   lambdaIn );
	setLambdaOut( lambdaOut );
	setMu(               mu );
	setV(                 v );
	setSigma(         sigma );
	
	Load(    fileName, &m_img   );
	Convert( m_img,   &m_floatImg );
	CreateInitialPhi( &m_imgPhi, &m_contourImg );
	ImageProcessing::CalculateGaussianInfo( &m_gi, sigma );
}


/*---------------------------------------------------------------------------*
 * Description: This method initializes the values for the implicit function 
 * and the contour.
 *
 * Parameters:
 * [+] imgPhi - a reference to the implicit function
 * [+] imgContour - a reference to the contour function
 * Return: none
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::CreateInitialPhi( ImgFloat * imgPhi, ImgFloat * imgContour )
{
	ImgFloat contour, phi;
	phi.Reset( m_floatImg.Width(), m_floatImg.Height() );
	contour.Reset( m_floatImg.Width(), m_floatImg.Height() );
	Set( &contour, 1.0 );
	Set( &phi, -1.0 );

	int outline = 50;
	for ( int i = outline ; i < m_floatImg.Width() - outline ; i++ )
	{
		for ( int j = outline ; j < m_floatImg.Height() - outline; j++ )
		{
			phi(i,j) = 1;
			if ( i == outline || j == outline || i == m_floatImg.Height() - outline || m_floatImg.Width() - outline )
			{
				contour(i,j) = 0;
			}
		}
	}
	*imgContour = contour;
	*imgPhi = phi;

}

/*---------------------------------------------------------------------------*
 * Description: This method uses the contour image to draw the contour in the
 * original image.
 *
 * Parameters:
 * [+] none
 * Return: none
 *---------------------------------------------------------------------------*/
ImgBgr ChanVeseLevelSet::GetContourOverImg()
{
	ImgBgr result;
	result.Reset( m_contourImg.Width(), m_contourImg.Height() ) ;
	result = m_img;

	ImgBgr::Iterator p = result.Begin();
	ImgFloat::ConstIterator q = m_contourImg.Begin();

	while ( p != result.End() )
	{
		if ( *q == 0.0 )
		{
			*p = Bgr::RED;
		}
		p++;
		q++;
	}
	return result;
}

/*---------------------------------------------------------------------------*
 * Description: Method to compute the divergence from the contour 
 * using derivatives of the contour.
 *
 * Parameters:
 * [+] gi - Object contains the gaussian kernel used to get the derivative of
 *          the contour image or phi.
 * [+] phi - implicit function
 * Return: none
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::ComputeDivergence( GaussianInfo & gi, ImgFloat & phi )
{
	// Convolving the image about the vertical and horizontal axes
	// using a gaussian kernel creates a differentiated image in the
	// vertical and horizontal directions


	// Convolving the image about the vertical and horizontal axes
	// using a gaussian kernel creates a differentiated image in the
	// vertical and horizontal directions

	imageProcessor.ConvolveVertical( gi, phi, &m_phiY, true );
	imageProcessor.ConvolveHorizontal( gi, phi, &m_phiX, true );
	imageProcessor.CalculateGradientMagnitude( m_phiX, m_phiY, &m_phiGradient );
	float mean = imageProcessor.GetImageMean( m_phiGradient );
	Divide( m_phiX, mean, &m_normPhiX );
	Divide( m_phiY, mean, &m_normPhiY );
	Add( m_normPhiX, m_normPhiY, &m_divNormPhi );
}

/*---------------------------------------------------------------------------*
 * Description: Method to compute the change in the contour using the original
 * image, the gradient image, the divergent image and the algorithm constants.
 *
 * Parameters:
 * [+] img    - the original image
 * [+] grad   - the gradient of the implicit function
 * [+] divPhi - the divergence of the implicit function
 * [+] delta  - the change in phi based on the values in and outside of the 
 *              contour boundary
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::ComputePhiEnergyChange( ImgFloat & img, ImgFloat & grad, ImgFloat & divPhi, ImgFloat * delta )
{
	// Create the intermediate image floats
	ImgFloat eIn, eOut, eInSquared, eOutSquared;
	ImgFloat eInLambda, eOutLambda, muDivPhi;
	ImgFloat tempA, tempB;
	delta->Reset( img.Width(), img.Height() );
	Set( delta, 0.0 );

	Subtract( img, imageProcessor.CalculateCi(img, m_imgPhi), &eIn );
	Subtract( img, imageProcessor.CalculateCo(img, m_imgPhi), &eOut );
	Multiply( eIn, eIn, &eInSquared );
	Multiply( eOut, eOut, &eOutSquared );
	Multiply( eInSquared, getLambdaIn(), &eInLambda );
	Multiply( eOutSquared, getLambdaOut(), &eOutLambda );
	Multiply( divPhi, getMu(), &muDivPhi );
	Add( eInLambda, getV(), &tempA );
	Subtract( tempA, eOutLambda, &tempB );
	Subtract( tempB, muDivPhi, &tempA );
	Multiply( grad, -1.0, &tempB );
	Multiply( tempA, tempB, delta);
}

/*---------------------------------------------------------------------------*
 * Description: At this point the change in energy has been calculated.  In 
 * order for the Chan-Vese level set function to improve phi has to be updated
 * using the current phi and the change in energy of phi.
 *
 * Parameters:
 * [+] phi      - main functional image from which a contour is calculated
 * [+] deltaPhi - the change in energy in the main functional
 * [+] newPhi   - the current phi plus the change in energy
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::ApplyGradientDescent( ImgFloat & phi, ImgFloat & deltaPhi, ImgFloat * newPhi )
{
	ImgFloat  tempFloat;
	ImgBinary tempBinary;

	newPhi->Reset( phi.Width(), phi.Height() );
	Add( phi, deltaPhi, newPhi);
}

/*---------------------------------------------------------------------------*
 * Description: After calculating the change in energy and obtaining an 
 * updated phi, a new contour needs to be calculated, and phi is reinitialized
 * to the signed distance of the contour.  CalculateChamferDistance creates
 * a chamfered version of phi and recalculates the contour.
 *
 * Parameters:
 * [+] newPhi      - implicit function image from which a contour is calculated
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::CalculateChamferDistance( ImgFloat * newPhi )
{
	float threshold = 0.0;
	double thresh = 0.0;
	ImgBinary binImg;
	ImgGray   gImg;
	ImgInt    iImg;
	ImgFloat  tempFloat;
	
	tempFloat.Reset( newPhi->Width(), newPhi->Height() );
	ImgFloat::Iterator      p = tempFloat.Begin();
	ImgFloat::ConstIterator q = newPhi->Begin();
	float max = Max(*newPhi);
	while( p != tempFloat.End() ) 
	{ 
		*p = abs(*q);
		p++;
		q++;
	}
	
	Convert( tempFloat, &gImg );
	threshold = imageProcessor.GetImageMean(tempFloat);
	Threshold( tempFloat, threshold,  &binImg );
	imageProcessor.Chamfer( binImg, &iImg );
	Convert( iImg, &tempFloat );

	p = tempFloat.Begin();
	q = newPhi->Begin();
	while ( p != tempFloat.End() )
	{
		if ( *q <= 0 )
		{
			*p = *p * -1;
		}
		p++;
		q++;
	}
	*newPhi = tempFloat;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+] phi      - implicit function image from which a contour is calculated
 * Return: Nothing
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::UpdateContour( const ImgFloat & phi )
{
	Set( &m_contourImg, 1);

	// create the contour image from phi
	for ( int i = 0 ; i < m_contourImg.Width() - 1 ; i++ )
	{
		for ( int j = 0 ; j < m_contourImg.Height() - 1 ; j++ )
		{
			if ( (phi(i,j) > 0 && phi(i+1,j) < 0) ||
				(phi(i,j) > 0 && phi(i,j+1) < 0) ||
				(phi(i,j) < 0 && phi(i+1,j) > 0) ||
				(phi(i,j) < 0 && phi(i,j+1) > 0))
			{
				m_contourImg(i,j) = 0;
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// PRINTERS ///////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// SETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::setLambdaIn( float lambdaIn )
{
	m_lambdaIn = lambdaIn;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::setLambdaOut( float lambdaOut )
{
	m_lambdaOut = lambdaOut;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::setMu( float mu )
{
	m_mu = mu;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::setV( float v )
{
	m_v = v;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
void ChanVeseLevelSet::setSigma( float sigma )
{
	m_sigma = sigma;
}



///////////////////////////////////////////////////////////////////////////////
////////////////////////////////// GETTERS ////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat ChanVeseLevelSet::getPhi()
{
	return m_imgPhi;
}


/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgBgr   ChanVeseLevelSet::getImg()
{
	return m_img;
}


/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getContour()
{
	return m_contourImg;
}



/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getPhiX()
{
	return m_phiX;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getPhiY()
{
	return m_phiY;
}


/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getNormPhiX()
{
	return m_normPhiX;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getNormPhiY()
{
	return m_normPhiY;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat ChanVeseLevelSet::getDivNormPhi()
{
	return m_divNormPhi;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getPhiDelta()
{
	return m_deltaPhi;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat  ChanVeseLevelSet::getPhiGradient()
{
	return m_phiGradient;
}
/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
ImgFloat ChanVeseLevelSet::getGradientDescent()
{
	return m_gradDescentPhi;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
float ChanVeseLevelSet::getMu()
{
	return m_mu;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
float ChanVeseLevelSet::getLambdaIn()
{
	return m_lambdaIn;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
float ChanVeseLevelSet::getLambdaOut()
{
	return m_lambdaOut;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
float ChanVeseLevelSet::getV()
{
	return m_v;
}

/*---------------------------------------------------------------------------*
 * Description:
 *
 * Parameters:
 * [+]
 * Return:
 *---------------------------------------------------------------------------*/
float ChanVeseLevelSet::getSigma()
{
	return m_sigma;
}
