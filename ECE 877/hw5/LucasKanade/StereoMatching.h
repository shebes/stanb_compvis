// StereoMatching.h : main header file for the STEREOMATCHING application
//

#if !defined(AFX_STEREOMATCHING_H__1AD1748E_F3EE_40D7_9C32_42DE73B39B73__INCLUDED_)
#define AFX_STEREOMATCHING_H__1AD1748E_F3EE_40D7_9C32_42DE73B39B73__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
//#include "..\src\blepo.h"   // blepo

/////////////////////////////////////////////////////////////////////////////
// CStereoMatchingApp:
// See StereoMatching.cpp for the implementation of this class
//

class CStereoMatchingApp : public CWinApp
{
public:
	CStereoMatchingApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStereoMatchingApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CStereoMatchingApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STEREOMATCHING_H__1AD1748E_F3EE_40D7_9C32_42DE73B39B73__INCLUDED_)
